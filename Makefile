all:
	$(MAKE) -C src all
	if [ ! -d bin ]; then mkdir bin; fi
	cp src/*.class bin/
	cp src/*.jar bin/
	$(MAKE) -C src clean
clean:
	$(MAKE) -C src clean
	-rm -rf bin
