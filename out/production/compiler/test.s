
#func getHash $r2
#$r3 = mul $r2 237
#$r4 = rem $r3 $r0
#ret $r4
#retnull
#func put $r5 $r6
#$r8 = move 0
#$r9 = call getHash $r5
#$r7 = move $r9
#$r10 = mul 4 $r7
#$r11 = add $r1 $r10
#$r12 = load 4 $r11 0
#$r13 = seq $r12 0
#br $r13 %true0 %out1
#%true0
#$r14 = mul 4 $r7
#$r15 = add $r1 $r14
#$r16 = load 4 $r15 0
#$r16 = alloc 3
#store 4 $r15 $r16 0
#$r17 = mul 4 $r7
#$r18 = add $r1 $r17
#$r19 = load 4 $r18 0
#$r20 = mul 4 0
#$r21 = add $r19 $r20
#$r22 = load 4 $r21 0
#store 4 $r21 $r5 0
#$r23 = mul 4 $r7
#$r24 = add $r1 $r23
#$r25 = load 4 $r24 0
#$r26 = mul 4 1
#$r27 = add $r25 $r26
#$r28 = load 4 $r27 0
#store 4 $r27 $r6 0
#$r29 = mul 4 $r7
#$r30 = add $r1 $r29
#$r31 = load 4 $r30 0
#$r32 = mul 4 2
#$r33 = add $r31 $r32
#$r34 = load 4 $r33 0
#store 4 $r33 0 0
#retnull
#jump %out1
#%out1
#$r35 = mul 4 $r7
#$r36 = add $r1 $r35
#$r37 = load 4 $r36 0
#$r8 = move $r37
#jump %cond2
#%cond2
#$r38 = mul 4 0
#$r39 = add $r8 $r38
#$r40 = load 4 $r39 0
#$r41 = sne $r40 $r5
#br $r41 %loop3 %out4
#%loop3
#$r42 = mul 4 2
#$r43 = add $r8 $r42
#$r44 = load 4 $r43 0
#$r45 = seq $r44 0
#br $r45 %true5 %out6
#%true5
#$r46 = mul 4 2
#$r47 = add $r8 $r46
#$r48 = load 4 $r47 0
#$r48 = alloc 3
#store 4 $r47 $r48 0
#$r49 = mul 4 2
#$r50 = add $r8 $r49
#$r51 = load 4 $r50 0
#$r52 = mul 4 0
#$r53 = add $r51 $r52
#$r54 = load 4 $r53 0
#store 4 $r53 $r5 0
#$r55 = mul 4 2
#$r56 = add $r8 $r55
#$r57 = load 4 $r56 0
#$r58 = mul 4 2
#$r59 = add $r57 $r58
#$r60 = load 4 $r59 0
#store 4 $r59 0 0
#jump %out6
#%out6
#$r61 = mul 4 2
#$r62 = add $r8 $r61
#$r63 = load 4 $r62 0
#$r8 = move $r63
#jump %cond2
#%out4
#$r64 = mul 4 1
#$r65 = add $r8 $r64
#$r66 = load 4 $r65 0
#store 4 $r65 $r6 0
#retnull
#func get $r67
#$r68 = move 0
#$r69 = call getHash $r67
#$r70 = mul 4 $r69
#$r71 = add $r1 $r70
#$r72 = load 4 $r71 0
#$r68 = move $r72
#jump %cond7
#%cond7
#$r73 = mul 4 0
#$r74 = add $r68 $r73
#$r75 = load 4 $r74 0
#$r76 = sne $r75 $r67
#br $r76 %loop8 %out9
#%loop8
#$r77 = mul 4 2
#$r78 = add $r68 $r77
#$r79 = load 4 $r78 0
#$r68 = move $r79
#jump %cond7
#%out9
#$r80 = mul 4 1
#$r81 = add $r68 $r80
#$r82 = load 4 $r81 0
#ret $r82
#retnull
#func main
#$r1 = alloc 100
#$r83 = move 0
#jump %cond10
#%cond10
#$r84 = slt $r83 $r0
#br $r84 %loop11 %out13
#%loop11
#$r85 = mul 4 $r83
#$r86 = add $r1 $r85
#$r87 = load 4 $r86 0
#store 4 $r86 0 0
#jump %step12
#%step12
#$r88 = move $r83
#$r83 = add $r83 1
#jump %cond10
#%out13
#$r83 = move 0
#jump %cond14
#%cond14
#$r89 = slt $r83 1000
#br $r89 %loop15 %out17
#%loop15
#$r90 = call put $r83 $r83
#jump %step16
#%step16
#$r91 = move $r83
#$r83 = add $r83 1
#jump %cond14
#%out17
#$r83 = move 0
#jump %cond18
#%cond18
#$r92 = slt $r83 1000
#br $r92 %loop19 %out21
#%loop19
#$r93 = call func__toString $r83
#$r95 = call func__stringConcatenate $r93 $r94
#$r96 = call get $r83
#$r97 = call func__toString $r96
#$r98 = call func__stringConcatenate $r95 $r97
#$r99 = call func__println $r98
#jump %step20
#%step20
#$r100 = move $r83
#$r83 = add $r83 1
#jump %cond18
#%out21
#ret 0
#retnull
# Built by Ficos 16/5/2
# All rights reserved.
#
#
# All test passed.
#
# Attention:
# 1. to use the built-in functions, you need to call "_buffer_init" function without any args before entering the source main function
# 	(jal _buffer_init)
# 2. just paste all of this in front of your MIPS code
#
# All supported functions:
# 		FunctionName			args
# 1.	func__print 			$a0: the string
# 2.	func__println			$a0: the string
# 3.	func__getString			---
# 4.	func__getInt			---
# 5.	func__toString			$a0: the integer
# 6.	func__string.length 	$a0: the string
# 7.	func__string.substring  $a0: the string,  $a1: left pos(int), $a2: right pos(int)
# 8.	func__string.parseInt 	$a0: the string
# 9.	func__string.ord 		$a0: the string,  $a1: pos(int)
# 10.	func__array.size 		$a0: the array
# 11.	func__stringConcatenate $a0: left string, $a1: right string
# 12.	func__stringIsEqual 	$a0: left string, $a1: right string
# 13.	func__stringLess 		$a0: left string, $a1: right string
# 14.	func__stringLeq	 		$a0: left string, $a1: right string
# 15.	func__stringGeq	 		$a0: left string, $a1: right string
# 16.	func__stringNeq	 		$a0: left string, $a1: right string
# 17.	func__stringLarge 		$a0: left string, $a1: right string
#
# Calling Conventions:
# 1. args placed in $a0, $a1, $a2
# 2. return in $v0
# 3. follow the MIPS calling convention, be careful on regs when calling these functions
# 4. all used regs are presented in the front of the function
#
# Conventions in using string:
# 1. string object is simply a register contains the initial address of the string
# 2. front of every initial address of a string are a word containing the length of the string
#    e.g.
#    .data
#  		  .word 6
# 	 str: .asciiz "hello\n"
# 		  .align 2
# 3. every string ends with '\0', which is not counted in the length
#
# Conventions in using array:
# 1. front of every initial address of a array are a word containing the size of the array

.data
_end: .asciiz "\n"
	.align 2
_buffer: .space 256
	.align 2
# 	.word 19
# str: .asciiz "-123456abcdefgh\n"
# 	.align 2

# 	.word 6
# str2: .asciiz "hello\n"
# 	.align 2

.text
# main:
# 	subu $sp, $sp, 4
# 	sw $ra, 0($sp)
	# jal _buffer_init

	# Test print/println
	# la $a0, str
	# jal func_println
	# la $a0, str2
	# jal func_print

	# Test getString, string_copy
	# jal func__getString
	# move $s0, $v0
	# move $a0, $s0
	# jal func__print
	# move $a0, $s0
	# jal func__string.length


	# Test string.length
	# la $a0, str2
	# jal func_string.length
	# move $a0, $v0
	# li $v0, 1
	# syscall

	# Test getInt
	# jal func_getInt
	# move $a0, $v0
	# li $v0, 1
	# syscall

	# Test toString
	# li $a0, -232312312
	# jal func__toString
	# move $a0, $v0
	# jal func__println

	# Test subString
	# la $a0 str
	# li $a1 1
	# li $a2 9
	# jal func__string.substring
	# move $a0, $v0
	# li $v0, 4
	# syscall
	# la $a0 str
	# syscall

	# Test parseInt
	# la $a0 str
	# jal func__string.parseInt
	# move $a0, $v0
	# li $v0, 1
	# syscall

	# Test string.ord
	# la $a0 str
	# li $a1, 5
	# jal func_string.ord
	# move $a0, $v0
	# li $v0, 1
	# syscall

	# Test stringconcatinate
	# la $a0 str
	# la $a1 str2
	# jal func__stringConcatenate
	# move $a0, $v0
	# jal func__print


	# Test StringIsEqual
	# la $a0 str
	# la $a1 str2
	# jal func__stringIsEqual
	# move $a0, $v0
	# li $v0, 1
	# syscall

	# Test StringLess
	# la $a0 str
	# la $a1 str2
	# jal func_stringLess
	# move $a0, $v0
	# li $v0, 1
	# syscall

	# lw $ra, 0($sp)
	# addu $sp, $sp, 4
	# jr $ra

# _buffer_init:
# 	li $a0, 256
# 	li $v0, 9
# 	syscall
# 	sw $v0, _buffer
# 	jr $ra

# copy the string in $a0 to buffer in $a1, with putting '\0' in the end of the buffer
###### Checked ######
# used $v0, $a0, $a1
_string_copy:
	_begin_string_copy:
	lb $v0, 0($a0)
	beqz $v0, _exit_string_copy
	sb $v0, 0($a1)
	add $a0, $a0, 1
	add $a1, $a1, 1
	j _begin_string_copy
	_exit_string_copy:
	sb $zero, 0($a1)
	jr $ra

# string arg in $a0
###### Checked ######
# Change(5/4): you don't need to preserve reg before calling it
func__print:
	li $v0, 4
	syscall
	jr $ra

# string arg in $a0
###### Checked ######
# Change(5/4): you don't need to preserve reg before calling it
func__println:
	li $v0, 4
	syscall
	la $a0, _end
	syscall
	jr $ra

# count the length of given string in $a0
###### Checked ######
# used $v0, $v1, $a0
_count_string_length:
	move $v0, $a0

	_begin_count_string_length:
	lb $v1, 0($a0)
	beqz $v1, _exit_count_string_length
	add $a0, $a0, 1
	j _begin_count_string_length

	_exit_count_string_length:
	sub $v0, $a0, $v0
	jr $ra

# non arg, string in $v0
###### Checked ######
# used $a0, $a1, $t0, $v0, (used in _count_string_length) $v1
func__getString:
	subu $sp, $sp, 4
	sw $ra, 0($sp)

	la $a0, _buffer
	li $a1, 255
	li $v0, 8
	syscall

	jal _count_string_length

	move $a1, $v0			# now $a1 contains the length of the string
	add $a0, $v0, 5			# total required space = length + 1('\0') + 1 word(record the length of the string)
	li $v0, 9
	syscall
	sw $a1, 0($v0)
	add $v0, $v0, 4
	la $a0, _buffer
	move $a1, $v0
	move $t0, $v0
	jal _string_copy
	move $v0, $t0

	lw $ra, 0($sp)
	addu $sp, $sp, 4
	jr $ra

# non arg, int in $v0
###### Checked ######
# Change(5/4): you don't need to preserve reg before calling it
func__getInt:
	li $v0, 5
	syscall
	jr $ra

# int arg in $a0
###### Checked ######
# Bug fixed(5/2): when the arg is a neg number
# Change(5/4): use less regs, you don't need to preserve reg before calling it
# used $v0, $v1
func__toString:
	subu $sp, $sp, 24
	sw $a0, 0($sp)
	sw $t0, 4($sp)
	sw $t1, 8($sp)
	sw $t2, 12($sp)
	sw $t3, 16($sp)
	sw $t5, 20($sp)

	# first count the #digits
	li $t0, 0			# $t0 = 0 if the number is a negnum
	bgez $a0, _skip_set_less_than_zero
	li $t0, 1			# now $t0 must be 1
	neg $a0, $a0
	_skip_set_less_than_zero:
	beqz $a0, _set_zero

	li $t1, 0			# the #digits is in $t1
	move $t2, $a0
	move $t3, $a0
	li $t5, 10

	_begin_count_digit:
	div $t2, $t5
	mflo $v0			# get the quotient
	mfhi $v1			# get the remainder
	bgtz $v0 _not_yet
	bgtz $v1 _not_yet
	j _yet
	_not_yet:
	add $t1, $t1, 1
	move $t2, $v0
	j _begin_count_digit

	_yet:
	beqz $t0, _skip_reserve_neg
	add $t1, $t1, 1
	_skip_reserve_neg:
	add $a0, $t1, 5
	li $v0, 9
	syscall
	sw $t1, 0($v0)
	add $v0, $v0, 4
	add $t1, $t1, $v0
	sb $zero, 0($t1)
	sub $t1, $t1, 1

	_continue_toString:
	div $t3, $t5
	mfhi $v1
	add $v1, $v1, 48	# in ascii 48 = '0'
	sb $v1, 0($t1)
	sub $t1, $t1, 1
	mflo $t3
	# bge $t1, $v0, _continue_toString
	bnez $t3, _continue_toString

	beqz $t0, _skip_place_neg
	li $v1, 45
	sb $v1, 0($t1)
	_skip_place_neg:
	# lw $ra, 0($sp)
	# addu $sp, $sp, 4

	lw $a0, 0($sp)
	lw $t0, 4($sp)
	lw $t1, 8($sp)
	lw $t2, 12($sp)
	lw $t3, 16($sp)
	lw $t5, 20($sp)

	addu $sp, $sp, 24
	jr $ra

	_set_zero:
	li $a0, 6
	li $v0, 9
	syscall
	li $a0, 1
	sw $a0, 0($v0)
	add $v0, $v0, 4
	li $a0, 48
	sb $a0, 0($v0)

	lw $a0, 0($sp)
	lw $t0, 4($sp)
	lw $t1, 8($sp)
	lw $t2, 12($sp)
	lw $t3, 16($sp)
	lw $t5, 20($sp)

	addu $sp, $sp, 24
	jr $ra


# string arg in $a0
# the zero in the end of the string will not be counted
###### Checked ######
# you don't need to preserve reg before calling it
func__string.length:
	lw $v0, -4($a0)
	jr $ra

# string arg in $a0, left in $a1, right in $a2
###### Checked ######
# used $a0, $a1, $t0, $t1, $t2, $v1, $v0
func__string.substring:
	subu $sp, $sp, 4
	sw $ra, 0($sp)

	move $t0, $a0

	sub $t1, $a2, $a1
	add $t1, $t1, 1		# $t1 is the length of the substring
	add $a0, $t1, 5
	li $v0, 9
	syscall
	sw $t1, 0($v0)
	add $v1, $v0, 4

	add $a0, $t0, $a1
	add $t2, $t0, $a2
	lb $t1, 1($t2)		# store the ori_begin + right + 1 char in $t1
	sb $zero, 1($t2)	# change it to 0 for the convenience of copying
	move $a1, $v1
	jal _string_copy
	move $v0, $v1
	sb $t1, 1($t2)

	lw $ra, 0($sp)
	addu $sp, $sp, 4
	jr $ra

# string arg in
###### Checked ######
# 16/5/4 Fixed a serious bug: can not parse negtive number
# used $v0, $v1
func__string.parseInt:
	subu $sp, $sp, 16
	sw $a0, 0($sp)
	sw $t0, 4($sp)
	sw $t1, 8($sp)
	sw $t2, 12($sp)

	li $v0, 0

	lb $t1, 0($a0)
	li $t2, 45
	bne $t1, $t2, _skip_parse_neg
	li $t1, 1			#if there is a '-' sign, $t1 = 1
	add $a0, $a0, 1
	j _skip_set_t1_zero

	_skip_parse_neg:
	li $t1, 0
	_skip_set_t1_zero:
	move $t0, $a0
	li $t2, 1

	_count_number_pos:
	lb $v1, 0($t0)
	bgt $v1, 57, _begin_parse_int
	blt $v1, 48, _begin_parse_int
	add $t0, $t0, 1
	j _count_number_pos

	_begin_parse_int:
	sub $t0, $t0, 1

	_parsing_int:
	blt $t0, $a0, _finish_parse_int
	lb $v1, 0($t0)
	sub $v1, $v1, 48
	mul $v1, $v1, $t2
	add $v0, $v0, $v1
	mul $t2, $t2, 10
	sub $t0, $t0, 1
	j _parsing_int

	_finish_parse_int:
	beqz $t1, _skip_neg
	neg $v0, $v0
	_skip_neg:

	lw $a0, 0($sp)
	lw $t0, 4($sp)
	lw $t1, 8($sp)
	lw $t2, 12($sp)
	addu $sp, $sp, 16
	jr $ra

# string arg in $a0, pos in $a1
###### Checked ######
# used $v0, $v1
func__string.ord:
	add $v1, $a0, $a1
	lb $v0, 0($v1)
	jr $ra

# array arg in $a0
# used $v0
func__array.size:
	lw $v0, -4($a0)
	jr $ra

# string1 in $a0, string2 in $a1
###### Checked ######
# change(16/5/4): use less regs, you don't need to preserve reg before calling it
# used $v0, $v1
func__stringConcatenate:

	subu $sp, $sp, 24
	sw $ra, 0($sp)
	sw $a0, 4($sp)
	sw $a1, 8($sp)
	sw $t0, 12($sp)
	sw $t1, 16($sp)
	sw $t2, 20($sp)

	lw $t0, -4($a0)		# $t0 is the length of lhs
	lw $t1, -4($a1)		# $t1 is the length of rhs
	add $t2, $t0, $t1

	move $t1, $a0

	add $a0, $t2, 5
	li $v0, 9
	syscall

	sw $t2, 0($v0)
	move $t2, $a1

	add $v0, $v0, 4
	move $v1, $v0

	move $a0, $t1
	move $a1, $v1
	jal _string_copy

	move $a0, $t2
	add $a1, $v1, $t0
	# add $a1, $a1, 1
	jal _string_copy

	move $v0, $v1
	lw $ra, 0($sp)
	lw $a0, 4($sp)
	lw $a1, 8($sp)
	lw $t0, 12($sp)
	lw $t1, 16($sp)
	lw $t2, 20($sp)
	addu $sp, $sp, 24
	jr $ra

# string1 in $a0, string2 in $a1
###### Checked ######
# change(16/5/4): use less regs, you don't need to preserve reg before calling it
# used $a0, $a1, $v0, $v1
func__stringIsEqual:
	# subu $sp, $sp, 8
	# sw $a0, 0($sp)
	# sw $a1, 4($sp)

	lw $v0, -4($a0)
	lw $v1, -4($a1)
	bne $v0, $v1, _not_equal

	_continue_compare_equal:
	lb $v0, 0($a0)
	lb $v1, 0($a1)
	beqz $v0, _equal
	bne $v0, $v1, _not_equal
	add $a0, $a0, 1
	add $a1, $a1, 1
	j _continue_compare_equal

	_not_equal:
	li $v0, 0
	j _compare_final

	_equal:
	li $v0, 1

	_compare_final:
	# lw $a0, 0($sp)
	# lw $a1, 4($sp)
	# addu $sp, $sp, 8
	jr $ra


# string1 in $a0, string2 in $a1
###### Checked ######
# change(16/5/4): use less regs, you don't need to preserve reg before calling it
# used $a0, $a1, $v0, $v1
func__stringLess:
	# subu $sp, $sp, 8
	# sw $a0, 0($sp)
	# sw $a1, 4($sp)

	_begin_compare_less:
	lb $v0, 0($a0)
	lb $v1, 0($a1)
	blt $v0, $v1, _less_correct
	bgt $v0, $v1, _less_false
	beqz $v0, _less_false
	add $a0, $a0, 1
	add $a1, $a1, 1
	j _begin_compare_less

	_less_correct:
	li $v0, 1
	j _less_compare_final

	_less_false:
	li $v0, 0

	_less_compare_final:

	# lw $a0, 0($sp)
	# lw $a1, 4($sp)
	# addu $sp, $sp, 8
	jr $ra

# string1 in $a0, string2 in $a1
# used $a0, $a1, $v0, $v1
func__stringLarge:
	subu $sp, $sp, 4
	sw $ra, 0($sp)

	jal func__stringLess

	xor $v0, $v0, 1

	lw $ra, 0($sp)
	addu $sp, $sp, 4
	jr $ra

# string1 in $a0, string2 in $a1
# used $a0, $a1, $v0, $v1
func__stringLeq:
	subu $sp, $sp, 12
	sw $ra, 0($sp)
	sw $a0, 4($sp)
	sw $a1, 8($sp)

	jal func__stringLess

	bnez $v0, _skip_compare_equal_in_Leq

	lw $a0, 4($sp)
	lw $a1, 8($sp)
	jal func__stringIsEqual

	_skip_compare_equal_in_Leq:
	lw $ra, 0($sp)
	addu $sp, $sp, 12
	jr $ra

# string1 in $a0, string2 in $a1
# used $a0, $a1, $v0, $v1
func__stringGeq:
	subu $sp, $sp, 12
	sw $ra, 0($sp)
	sw $a0, 4($sp)
	sw $a1, 8($sp)

	jal func__stringLess

	beqz $v0, _skip_compare_equal_in_Geq

	lw $a0, 4($sp)
	lw $a1, 8($sp)
	jal func__stringIsEqual
	xor $v0, $v0, 1

	_skip_compare_equal_in_Geq:
	xor $v0, $v0, 1
	lw $ra, 0($sp)
	addu $sp, $sp, 12
	jr $ra

# string1 in $a0, string2 in $a1
# used $a0, $a1, $v0, $v1
func__stringNeq:
	subu $sp, $sp, 4
	sw $ra, 0($sp)

	jal func__stringIsEqual

	xor $v0, $v0, 1

	lw $ra, 0($sp)
	addu $sp, $sp, 4
	jr $ra
.data
#1
.word 1
r94: .asciiz " "
.align 2
r0: .word 0
r1: .word 0
.text
getHash:
subu $sp,$sp,24
sw $fp,16($sp)
sw $ra,20($sp)
sw $t0,8($sp)
sw $t1,12($sp)
addiu $fp,$sp,20

#mul
lw $t0,24($sp)
li $t1,237
mul $t0,$t0,$t1
sw $t0,0($sp)

#rem
lw $t0,0($sp)
lw $t1,r0
rem $t0,$t0,$t1
sw $t0,4($sp)

#ret
lw $v0,4($sp)
lw $fp,16($sp)
lw $ra,20($sp)
lw $t0,8($sp)
lw $t1,12($sp)
addiu $sp,$sp,24
jr $ra

#retnull
lw $fp,16($sp)
lw $ra,20($sp)
lw $t0,8($sp)
lw $t1,12($sp)
addiu $sp,$sp,24
jr $ra
put:
subu $sp,$sp,260
sw $fp,252($sp)
sw $ra,256($sp)
sw $t0,244($sp)
sw $t1,248($sp)
addiu $fp,$sp,256

#move
li $t1,0
move $t0,$t1
sw $t0,8($sp)

#dcall
lw $t0,260($sp)
sw $t0,0($sp)
jal getHash
move $t0,$v0
sw $t0,12($sp)

#move
lw $t1,12($sp)
move $t0,$t1
sw $t0,4($sp)

#mul
li $t0,4
lw $t1,4($sp)
mul $t0,$t0,$t1
sw $t0,16($sp)

#add
lw $t0,r1
lw $t1,16($sp)
add $t0,$t0,$t1
sw $t0,20($sp)

#load
lw $t1,20($sp)
lw $t0,0($t1)
sw $t0,24($sp)

#seq
lw $t0,24($sp)
li $t1,0
seq $t0,$t0,$t1
sw $t0,28($sp)

#br
lw $t0,28($sp)
beq $t0,1,true0
jal out1

#label
true0:

#mul
li $t0,4
lw $t1,4($sp)
mul $t0,$t0,$t1
sw $t0,32($sp)

#add
lw $t0,r1
lw $t1,32($sp)
add $t0,$t0,$t1
sw $t0,36($sp)

#load
lw $t1,36($sp)
lw $t0,0($t1)
sw $t0,40($sp)

#alloc
li $v0,9
li $t0,12
addiu $a0,$t0,4
syscall
divu  $t0,$t0,4
sw $t0,0($v0)
addiu $v0,$v0,4
sw $v0,40($sp)

#store
lw $t0,40($sp)
lw $t1,36($sp)
sw $t0,0($t1)

#mul
li $t0,4
lw $t1,4($sp)
mul $t0,$t0,$t1
sw $t0,44($sp)

#add
lw $t0,r1
lw $t1,44($sp)
add $t0,$t0,$t1
sw $t0,48($sp)

#load
lw $t1,48($sp)
lw $t0,0($t1)
sw $t0,52($sp)

#mul
li $t0,4
li $t1,0
mul $t0,$t0,$t1
sw $t0,56($sp)

#add
lw $t0,52($sp)
lw $t1,56($sp)
add $t0,$t0,$t1
sw $t0,60($sp)

#load
lw $t1,60($sp)
lw $t0,0($t1)
sw $t0,64($sp)

#store
lw $t0,260($sp)
lw $t1,60($sp)
sw $t0,0($t1)

#mul
li $t0,4
lw $t1,4($sp)
mul $t0,$t0,$t1
sw $t0,68($sp)

#add
lw $t0,r1
lw $t1,68($sp)
add $t0,$t0,$t1
sw $t0,72($sp)

#load
lw $t1,72($sp)
lw $t0,0($t1)
sw $t0,76($sp)

#mul
li $t0,4
li $t1,1
mul $t0,$t0,$t1
sw $t0,80($sp)

#add
lw $t0,76($sp)
lw $t1,80($sp)
add $t0,$t0,$t1
sw $t0,84($sp)

#load
lw $t1,84($sp)
lw $t0,0($t1)
sw $t0,88($sp)

#store
lw $t0,264($sp)
lw $t1,84($sp)
sw $t0,0($t1)

#mul
li $t0,4
lw $t1,4($sp)
mul $t0,$t0,$t1
sw $t0,92($sp)

#add
lw $t0,r1
lw $t1,92($sp)
add $t0,$t0,$t1
sw $t0,96($sp)

#load
lw $t1,96($sp)
lw $t0,0($t1)
sw $t0,100($sp)

#mul
li $t0,4
li $t1,2
mul $t0,$t0,$t1
sw $t0,104($sp)

#add
lw $t0,100($sp)
lw $t1,104($sp)
add $t0,$t0,$t1
sw $t0,108($sp)

#load
lw $t1,108($sp)
lw $t0,0($t1)
sw $t0,112($sp)

#store
li $t0,0
lw $t1,108($sp)
sw $t0,0($t1)

#retnull
lw $fp,252($sp)
lw $ra,256($sp)
lw $t0,244($sp)
lw $t1,248($sp)
addiu $sp,$sp,260
jr $ra

#jump
jal out1

#label
out1:

#mul
li $t0,4
lw $t1,4($sp)
mul $t0,$t0,$t1
sw $t0,116($sp)

#add
lw $t0,r1
lw $t1,116($sp)
add $t0,$t0,$t1
sw $t0,120($sp)

#load
lw $t1,120($sp)
lw $t0,0($t1)
sw $t0,124($sp)

#move
lw $t1,124($sp)
move $t0,$t1
sw $t0,8($sp)

#jump
jal cond2

#label
cond2:

#mul
li $t0,4
li $t1,0
mul $t0,$t0,$t1
sw $t0,128($sp)

#add
lw $t0,8($sp)
lw $t1,128($sp)
add $t0,$t0,$t1
sw $t0,132($sp)

#load
lw $t1,132($sp)
lw $t0,0($t1)
sw $t0,136($sp)

#sne
lw $t0,136($sp)
lw $t1,260($sp)
sne $t0,$t0,$t1
sw $t0,140($sp)

#br
lw $t0,140($sp)
beq $t0,1,loop3
jal out4

#label
loop3:

#mul
li $t0,4
li $t1,2
mul $t0,$t0,$t1
sw $t0,144($sp)

#add
lw $t0,8($sp)
lw $t1,144($sp)
add $t0,$t0,$t1
sw $t0,148($sp)

#load
lw $t1,148($sp)
lw $t0,0($t1)
sw $t0,152($sp)

#seq
lw $t0,152($sp)
li $t1,0
seq $t0,$t0,$t1
sw $t0,156($sp)

#br
lw $t0,156($sp)
beq $t0,1,true5
jal out6

#label
true5:

#mul
li $t0,4
li $t1,2
mul $t0,$t0,$t1
sw $t0,160($sp)

#add
lw $t0,8($sp)
lw $t1,160($sp)
add $t0,$t0,$t1
sw $t0,164($sp)

#load
lw $t1,164($sp)
lw $t0,0($t1)
sw $t0,168($sp)

#alloc
li $v0,9
li $t0,12
addiu $a0,$t0,4
syscall
divu  $t0,$t0,4
sw $t0,0($v0)
addiu $v0,$v0,4
sw $v0,168($sp)

#store
lw $t0,168($sp)
lw $t1,164($sp)
sw $t0,0($t1)

#mul
li $t0,4
li $t1,2
mul $t0,$t0,$t1
sw $t0,172($sp)

#add
lw $t0,8($sp)
lw $t1,172($sp)
add $t0,$t0,$t1
sw $t0,176($sp)

#load
lw $t1,176($sp)
lw $t0,0($t1)
sw $t0,180($sp)

#mul
li $t0,4
li $t1,0
mul $t0,$t0,$t1
sw $t0,184($sp)

#add
lw $t0,180($sp)
lw $t1,184($sp)
add $t0,$t0,$t1
sw $t0,188($sp)

#load
lw $t1,188($sp)
lw $t0,0($t1)
sw $t0,192($sp)

#store
lw $t0,260($sp)
lw $t1,188($sp)
sw $t0,0($t1)

#mul
li $t0,4
li $t1,2
mul $t0,$t0,$t1
sw $t0,196($sp)

#add
lw $t0,8($sp)
lw $t1,196($sp)
add $t0,$t0,$t1
sw $t0,200($sp)

#load
lw $t1,200($sp)
lw $t0,0($t1)
sw $t0,204($sp)

#mul
li $t0,4
li $t1,2
mul $t0,$t0,$t1
sw $t0,208($sp)

#add
lw $t0,204($sp)
lw $t1,208($sp)
add $t0,$t0,$t1
sw $t0,212($sp)

#load
lw $t1,212($sp)
lw $t0,0($t1)
sw $t0,216($sp)

#store
li $t0,0
lw $t1,212($sp)
sw $t0,0($t1)

#jump
jal out6

#label
out6:

#mul
li $t0,4
li $t1,2
mul $t0,$t0,$t1
sw $t0,220($sp)

#add
lw $t0,8($sp)
lw $t1,220($sp)
add $t0,$t0,$t1
sw $t0,224($sp)

#load
lw $t1,224($sp)
lw $t0,0($t1)
sw $t0,228($sp)

#move
lw $t1,228($sp)
move $t0,$t1
sw $t0,8($sp)

#jump
jal cond2

#label
out4:

#mul
li $t0,4
li $t1,1
mul $t0,$t0,$t1
sw $t0,232($sp)

#add
lw $t0,8($sp)
lw $t1,232($sp)
add $t0,$t0,$t1
sw $t0,236($sp)

#load
lw $t1,236($sp)
lw $t0,0($t1)
sw $t0,240($sp)

#store
lw $t0,264($sp)
lw $t1,236($sp)
sw $t0,0($t1)

#retnull
lw $fp,252($sp)
lw $ra,256($sp)
lw $t0,244($sp)
lw $t1,248($sp)
addiu $sp,$sp,260
jr $ra
get:
subu $sp,$sp,80
sw $fp,72($sp)
sw $ra,76($sp)
sw $t0,64($sp)
sw $t1,68($sp)
addiu $fp,$sp,76

#move
li $t1,0
move $t0,$t1
sw $t0,4($sp)

#dcall
lw $t0,80($sp)
sw $t0,0($sp)
jal getHash
move $t0,$v0
sw $t0,8($sp)

#mul
li $t0,4
lw $t1,8($sp)
mul $t0,$t0,$t1
sw $t0,12($sp)

#add
lw $t0,r1
lw $t1,12($sp)
add $t0,$t0,$t1
sw $t0,16($sp)

#load
lw $t1,16($sp)
lw $t0,0($t1)
sw $t0,20($sp)

#move
lw $t1,20($sp)
move $t0,$t1
sw $t0,4($sp)

#jump
jal cond7

#label
cond7:

#mul
li $t0,4
li $t1,0
mul $t0,$t0,$t1
sw $t0,24($sp)

#add
lw $t0,4($sp)
lw $t1,24($sp)
add $t0,$t0,$t1
sw $t0,28($sp)

#load
lw $t1,28($sp)
lw $t0,0($t1)
sw $t0,32($sp)

#sne
lw $t0,32($sp)
lw $t1,80($sp)
sne $t0,$t0,$t1
sw $t0,36($sp)

#br
lw $t0,36($sp)
beq $t0,1,loop8
jal out9

#label
loop8:

#mul
li $t0,4
li $t1,2
mul $t0,$t0,$t1
sw $t0,40($sp)

#add
lw $t0,4($sp)
lw $t1,40($sp)
add $t0,$t0,$t1
sw $t0,44($sp)

#load
lw $t1,44($sp)
lw $t0,0($t1)
sw $t0,48($sp)

#move
lw $t1,48($sp)
move $t0,$t1
sw $t0,4($sp)

#jump
jal cond7

#label
out9:

#mul
li $t0,4
li $t1,1
mul $t0,$t0,$t1
sw $t0,52($sp)

#add
lw $t0,4($sp)
lw $t1,52($sp)
add $t0,$t0,$t1
sw $t0,56($sp)

#load
lw $t1,56($sp)
lw $t0,0($t1)
sw $t0,60($sp)

#ret
lw $v0,60($sp)
lw $fp,72($sp)
lw $ra,76($sp)
lw $t0,64($sp)
lw $t1,68($sp)
addiu $sp,$sp,80
jr $ra

#retnull
lw $fp,72($sp)
lw $ra,76($sp)
lw $t0,64($sp)
lw $t1,68($sp)
addiu $sp,$sp,80
jr $ra
main:
subu $sp,$sp,96
sw $fp,88($sp)
sw $ra,92($sp)
sw $t0,80($sp)
sw $t1,84($sp)
addiu $fp,$sp,92
li $t1,100
move $t0,$t1
sw $t0,r0

#alloc
li $v0,9
li $t0,400
addiu $a0,$t0,4
syscall
divu  $t0,$t0,4
sw $t0,0($v0)
addiu $v0,$v0,4
sw $v0,r1

#move
li $t1,0
move $t0,$t1
sw $t0,8($sp)

#jump
jal cond10

#label
cond10:

#slt
lw $t0,8($sp)
lw $t1,r0
slt $t0,$t0,$t1
sw $t0,12($sp)

#br
lw $t0,12($sp)
beq $t0,1,loop11
jal out13

#label
loop11:

#mul
li $t0,4
lw $t1,8($sp)
mul $t0,$t0,$t1
sw $t0,16($sp)

#add
lw $t0,r1
lw $t1,16($sp)
add $t0,$t0,$t1
sw $t0,20($sp)

#load
lw $t1,20($sp)
lw $t0,0($t1)
sw $t0,24($sp)

#store
li $t0,0
lw $t1,20($sp)
sw $t0,0($t1)

#jump
jal step12

#label
step12:

#move
lw $t1,8($sp)
move $t0,$t1
sw $t0,28($sp)

#add
lw $t0,8($sp)
addi $t0,$t0,1
sw $t0,8($sp)

#jump
jal cond10

#label
out13:

#move
li $t1,0
move $t0,$t1
sw $t0,8($sp)

#jump
jal cond14

#label
cond14:

#slt
lw $t0,8($sp)
li $t1,1000
slt $t0,$t0,$t1
sw $t0,32($sp)

#br
lw $t0,32($sp)
beq $t0,1,loop15
jal out17

#label
loop15:

#dcall
lw $t0,8($sp)
sw $t0,0($sp)
lw $t0,8($sp)
sw $t0,4($sp)
jal put
move $t0,$v0
sw $t0,36($sp)

#jump
jal step16

#label
step16:

#move
lw $t1,8($sp)
move $t0,$t1
sw $t0,40($sp)

#add
lw $t0,8($sp)
addi $t0,$t0,1
sw $t0,8($sp)

#jump
jal cond14

#label
out17:

#move
li $t1,0
move $t0,$t1
sw $t0,8($sp)

#jump
jal cond18

#label
cond18:

#slt
lw $t0,8($sp)
li $t1,1000
slt $t0,$t0,$t1
sw $t0,44($sp)

#br
lw $t0,44($sp)
beq $t0,1,loop19
jal out21

#label
loop19:

#dcall
lw $t0,8($sp)
sw $t0,0($sp)
move $a0,$t0
jal func__toString
move $t0,$v0
sw $t0,48($sp)

#dcall
lw $t0,48($sp)
sw $t0,0($sp)
move $a0,$t0
la $t0,r94
sw $t0,4($sp)
move $a1,$t0
jal func__stringConcatenate
move $t0,$v0
sw $t0,56($sp)

#dcall
lw $t0,8($sp)
sw $t0,0($sp)
jal get
move $t0,$v0
sw $t0,60($sp)

#dcall
lw $t0,60($sp)
sw $t0,0($sp)
move $a0,$t0
jal func__toString
move $t0,$v0
sw $t0,64($sp)

#dcall
lw $t0,56($sp)
sw $t0,0($sp)
move $a0,$t0
lw $t0,64($sp)
sw $t0,4($sp)
move $a1,$t0
jal func__stringConcatenate
move $t0,$v0
sw $t0,68($sp)

#dcall
lw $t0,68($sp)
sw $t0,0($sp)
move $a0,$t0
jal func__println
move $t0,$v0
sw $t0,72($sp)

#jump
jal step20

#label
step20:

#move
lw $t1,8($sp)
move $t0,$t1
sw $t0,76($sp)

#add
lw $t0,8($sp)
addi $t0,$t0,1
sw $t0,8($sp)

#jump
jal cond18

#label
out21:

#ret
li $v0,0
lw $fp,88($sp)
lw $ra,92($sp)
lw $t0,80($sp)
lw $t1,84($sp)
addiu $sp,$sp,96
jr $ra

#retnull
lw $fp,88($sp)
lw $ra,92($sp)
lw $t0,80($sp)
lw $t1,84($sp)
addiu $sp,$sp,96
jr $ra

