/*
ret		ret $src
retnull retnull;
jump	jump %target
br		br $cond %ifTrue %ifFalse


store	store size $addr $src offset    // M[$addr+offset : $addr+offset+size-1] <- $src
load	$dest = load size $addr offset  // $dest <- M[$addr+offset : $addr+offset+size-1]
alloc	$dest = alloc $size

call	call funcname $op1 $op2 $op3 ...
dcall	$dest = call funcname $op1 $op2 $op3 ...

move	$dest = move $src

phi		$dest = phi %block1 $reg1 %block2 $reg2 ...

neg		$dest = neg $src
add		$dest = add $src1 $src2
sub		$dest = sub $src1 $src2
mul		$dest = mul $src1 $src2
div		$dest = div $src1 $src2
rem		$dest = rem $src1 $src2

shl		$dest = shl $src1 $src2
shr		$dest = shr $src1 $src2
and		$dest = and $src1 $src2
xor		$dest = xor $src1 $src2
or		$dest = or $src1 $src2
not		$dest = not $src

slt		$dest = slt $src1 $src2
sgt		$dest = sgt $src1 $src2
sle		$dest = sle $src1 $src2
sge		$dest = sge $src1 $src2
seq		$dest = seq $src1 $src2
sne		$dest = sne $src1 $src2
func 	func funcname $op1 $op2 ...
lable   %lable
*/
import java.util.ArrayList;

/**
 * Created by xuezhendong on 16/4/27.
 */

public class IR
{
    static ArrayList<instr> instrlist = new ArrayList<instr>();
    static int mainmaxarg = 0;
    static int mainregnum = 0;
    void print()
    {
        for (int i = 0; i < instrlist.size(); ++i)
        {
            instr inst = instrlist.get(i);
            System.out.print("#");
            if (inst.op .equals( "ret"))
            {
                System.out.printf("ret ");
                print(inst.src1);
                System.out.println();
            }
            if (inst.op .equals( "retnull"))
            {
                System.out.printf("retnull");
                System.out.println();
            }
            if (inst.op .equals( "jump"))
            {
                System.out.printf("jump ");
                print(inst.l1);
                System.out.println();
            }
            if (inst.op .equals( "br"))
            {
                System.out.printf("br ");
                print(inst.src1);
                System.out.printf(" ");
                print(inst.l1);
                System.out.printf(" ");
                print(inst.l2);
                System.out.println();
            }
            if (inst.op .equals( "store"))
            {
                System.out.printf("store ");
                System.out.print(inst.size);
                System.out.printf(" ");
                print(inst.addr);
                System.out.printf(" ");
                print(inst.src1);
                System.out.printf(" ");
                System.out.print(inst.offset);
                System.out.println();
            }
            if (inst.op .equals( "load"))
            {
                print(inst.dest);
                System.out.printf(" = load ");
                System.out.print(inst.size);
                System.out.printf(" ");
                print(inst.addr);
                System.out.printf(" ");
                System.out.print(inst.offset);
                System.out.println();
            }
            if (inst.op .equals( "alloc"))
            {
                print(inst.dest);
                System.out.printf(" = alloc ");
                print(inst.asize);
                System.out.println();
            }
            if (inst.op .equals("dcall"))
            {
                print(inst.dest);
                System.out.printf(" = call ");
                System.out.printf(inst.funcname);
                for (int j = 0; j < inst.oplist.size(); ++j)
                {
                    System.out.printf(" ");
                    print(inst.oplist.get(j));
                }
                System.out.println();
            }
            if (inst.op .equals( "move"))
            {
                print(inst.dest);
                System.out.printf(" = move ");
                print(inst.src1);
                System.out.println();
            }
            if (inst.op .equals( "neg"))
            {
                print(inst.dest);
                System.out.printf(" = neg ");
                print(inst.src1);
                System.out.println();
            }
            if (inst.op .equals( "add"))
            {
                print(inst.dest);
                System.out.printf(" = add ");
                print(inst.src1);
                System.out.printf(" ");
                print(inst.src2);
                System.out.println();
            }
            if (inst.op .equals( "sub"))
            {
                print(inst.dest);
                System.out.printf(" = sub ");
                print(inst.src1);
                System.out.printf(" ");
                print(inst.src2);
                System.out.println();
            }
            if (inst.op .equals( "mul"))
            {
                print(inst.dest);
                System.out.printf(" = mul ");
                print(inst.src1);
                System.out.printf(" ");
                print(inst.src2);
                System.out.println();
            }
            if (inst.op .equals( "div"))
            {
                print(inst.dest);
                System.out.printf(" = div ");
                print(inst.src1);
                System.out.printf(" ");
                print(inst.src2);
                System.out.println();
            }
            if (inst.op .equals( "rem"))
            {
                print(inst.dest);
                System.out.printf(" = rem ");
                print(inst.src1);
                System.out.printf(" ");
                print(inst.src2);
                System.out.println();
            }
            if (inst.op .equals( "shl"))
            {
                print(inst.dest);
                System.out.printf(" = shl ");
                print(inst.src1);
                System.out.printf(" ");
                print(inst.src2);
                System.out.println();
            }
            if (inst.op .equals( "shr"))
            {
                print(inst.dest);
                System.out.printf(" = shr ");
                print(inst.src1);
                System.out.printf(" ");
                print(inst.src2);
                System.out.println();
            }
            if (inst.op .equals( "and"))
            {
                print(inst.dest);
                System.out.printf(" = and ");
                print(inst.src1);
                System.out.printf(" ");
                print(inst.src2);
                System.out.println();
            }
            if (inst.op .equals( "or"))
            {
                print(inst.dest);
                System.out.printf(" = or ");
                print(inst.src1);
                System.out.printf(" ");
                print(inst.src2);
                System.out.println();
            }
            if (inst.op .equals( "xor"))
            {
                print(inst.dest);
                System.out.printf(" = xor ");
                print(inst.src1);
                System.out.printf(" ");
                print(inst.src2);
                System.out.println();
            }
            if (inst.op .equals( "not"))
            {
                print(inst.dest);
                System.out.printf(" = not ");
                print(inst.src1);
                System.out.println();
            }
            if (inst.op .equals( "slt"))
            {
                print(inst.dest);
                System.out.printf(" = slt ");
                print(inst.src1);
                System.out.printf(" ");
                print(inst.src2);
                System.out.println();
            }
            if (inst.op .equals( "sgt"))
            {
                print(inst.dest);
                System.out.printf(" = sgt ");
                print(inst.src1);
                System.out.printf(" ");
                print(inst.src2);
                System.out.println();
            }
            if (inst.op .equals( "sle"))
            {
                print(inst.dest);
                System.out.printf(" = sle ");
                print(inst.src1);
                System.out.printf(" ");
                print(inst.src2);
                System.out.println();
            }
            if (inst.op .equals( "sge"))
            {
                print(inst.dest);
                System.out.printf(" = sge ");
                print(inst.src1);
                System.out.printf(" ");
                print(inst.src2);
                System.out.println();
            }
            if (inst.op .equals( "seq"))
            {
                print(inst.dest);
                System.out.printf(" = seq ");
                print(inst.src1);
                System.out.printf(" ");
                print(inst.src2);
                System.out.println();
            }
            if (inst.op .equals( "sne"))
            {
                print(inst.dest);
                System.out.printf(" = sne ");
                print(inst.src1);
                System.out.printf(" ");
                print(inst.src2);
                System.out.println();
            }
            if (inst.op .equals( "func"))
            {
                System.out.printf("func ");
                System.out.printf(inst.funcname);
                for (int j = 0; j < inst.oplist.size(); ++j)
                {
                    System.out.printf(" ");
                    print(inst.oplist.get(j));
                }
                System.out.println();
            }
            if (inst.op .equals( "label"))
            {
                print(inst.l1);
                System.out.println();
            }
        }
    }
    void print(src s)
    {
        if (s.sort == 1)
        {
            System.out.print("$r" + s.r.No);
        }
        else
        {
            System.out.print(s.c);
        }
    }
    void print(register r)
    {
        System.out.print("$r" + r.No);
    }
    void print(label l)
    {
        System.out.print("%" + l.name);
    }

}
class register
{
    //static int total;
    //static int globaltotal;
    static ArrayList<register> reglist = new ArrayList<register>();
    static ArrayList<register> globallist = new ArrayList<register>();
    int No = 0;
    int global = -1;
    //int addr;
    src addr = new src();
    int inmemory = 0;
    int offset = 0;
    int isstring = 0;
    int size = 0;
    int arg = -1;
    ArrayList<instr> glbinstrlist = new ArrayList<instr>();

}
class rstring
{
    static ArrayList<rstring> strlist = new ArrayList<rstring>();
    int No = 0;
    String ctx = "";
    register r = new register();
    int len = 0;
}

class label
{
    //static int total;
    static ArrayList<label> lablist = new ArrayList<label>();
    static int mainNo;
    String name;
    int No;
}

class src
{
    int c;
    register r;
    int sort;
}
class instr
{
    String op;
    src src1 = new src();
    src src2 = new src();
    register dest = new register();

    src addr = new src();
    int size = 0;
    src asize = new src();
    int offset;
    label l1 = new label();
    label l2 = new label();
    String funcname;
    ArrayList<src> oplist = new ArrayList<src>();
    int maxarg = 0;
    int regnum = 0;
}
