import java.io.*;
import org.antlr.v4.runtime.*;
import org.antlr.v4.runtime.tree.*;

public class Main {
    public static void main(String[] args) throws IOException {
        String inputFile = null;
        if ( args.length>0 ) inputFile = args[0];
        InputStream is = System.in;
        if ( inputFile!=null ) is = new FileInputStream(inputFile);
        ANTLRInputStream input = new ANTLRInputStream(is);
        aLexer lexer = new aLexer(input);
        CommonTokenStream tokens = new CommonTokenStream(lexer);
        aParser parser = new aParser(tokens);
        ParseTree tree = parser.program(); // a is the starting rule
        if (parser.getNumberOfSyntaxErrors() != 0)
        {
            throw new RuntimeException("Syntax Error");
        }
        try
        {

            symboltable sbtb = new symboltable();

            sbtb.beginscope();
            //System.out.println(tree.toStringTree(parser));
            System.out.println();

            property p = new property();
            p.f.name = Name.getSymbolName("print");

            p.sort = 2;
            p.f.typereturn = Name.getSymbolName("void");
            p.f.dim = 0;
            property t = new property();
            t.sort = 1;
            t.v.name = Name.getSymbolName("str");
            t.v.classname = Name.getSymbolName("string");
            param tmp = new param();
            tmp.name = Name.getSymbolName("str");
            tmp.type = t;
            p.f.list.add(tmp);
            sbtb.put(p.f.name, p);


            p = new property();
            p.f.name = Name.getSymbolName("println");
            p.sort = 2;
            p.f.typereturn = Name.getSymbolName("void");
            p.f.dim = 0;
            t = new property();
            t.sort = 1;
            t.v.name = Name.getSymbolName("str");
            t.v.classname = Name.getSymbolName("string");
            tmp = new param();
            tmp.name = t.v.name;
            tmp.type = t;
            p.f.list.add(tmp);
            sbtb.put(p.f.name, p);

            p = new property();
            p.f.name = Name.getSymbolName("getString");
            p.sort = 2;
            p.f.typereturn = Name.getSymbolName("string");
            p.f.dim = 0;
            sbtb.put(p.f.name, p);

            p = new property();
            p.f.name = Name.getSymbolName("getInt");
            p.sort = 2;
            p.f.typereturn = Name.getSymbolName("int");
            p.f.dim = 0;
            sbtb.put(p.f.name, p);


            p = new property();
            p.f.name = Name.getSymbolName("toString");
            p.sort = 2;
            p.f.typereturn = Name.getSymbolName("string");
            p.f.dim = 0;
            t = new property();
            t.sort = 1;
            t.v.name = Name.getSymbolName("i");
            t.v.classname = Name.getSymbolName("int");
            tmp = new param();
            tmp.name = t.v.name;
            tmp.type = t;
            p.f.list.add(tmp);
            sbtb.put(p.f.name, p);


            p = new property();
            p.c.name = Name.getSymbolName("string");
            p.sort = 0;
            sbtb.put(p.c.name, p);
            property f;
            f = new property();
            f.sort = 2;
            f.f.name = Name.getSymbolName("length");
            f.f.typereturn = Name.getSymbolName("int");
            f.f.dim = 0;
            p.c.list.put(f.f.name, f);


            f = new property();
            f.sort = 2;
            f.f.name = Name.getSymbolName("substring");
            f.f.typereturn = Name.getSymbolName("string");
            f.f.dim = 0;
            tmp = new param();
            tmp.type = new property();
            tmp.type.v.classname = Name.getSymbolName("int");
            tmp.type.v.name = Name.getSymbolName("left");
            tmp.name = tmp.type.v.name;
            f.f.list.add(tmp);

            tmp = new param();
            tmp.type = new property();
            tmp.type.v.classname = Name.getSymbolName("int");
            tmp.type.v.name = Name.getSymbolName("right");
            tmp.name = tmp.type.v.name;

            f.f.list.add(tmp);

            p.c.list.put(f.f.name, f);

            f = new property();
            f.sort = 2;
            f.f.name = Name.getSymbolName("parseInt");
            f.f.typereturn = Name.getSymbolName("int");
            f.f.dim = 0;
            p.c.list.put(f.f.name, f);


            f = new property();
            f.sort = 2;
            f.f.name = Name.getSymbolName("ord");
            f.f.typereturn = Name.getSymbolName("int");
            f.f.dim = 0;
            tmp = new param();
            tmp.type = new property();
            tmp.type.v.classname = Name.getSymbolName("int");
            tmp.type.v.name = Name.getSymbolName("pos");
            tmp.name = tmp.type.v.name;
            f.f.list.add(tmp);
            p.c.list.put(f.f.name, f);


            p = new property();
            p.c.name = Name.getSymbolName("int");
            p.sort = 0;
            sbtb.put(p.c.name, p);


            p = new property();
            p.c.name = Name.getSymbolName("bool");
            p.sort = 0;
            sbtb.put(p.c.name, p);

            Visitor1 Visitor11 = new Visitor1();
            Visitor11.sbtb = sbtb;
            Visitor11.visit(tree);
            sbtb = Visitor11.sbtb;

            Visitor2 Visitor22 = new Visitor2();
            Visitor22.sbtb = sbtb;
            Visitor22.visit(tree);
            sbtb = Visitor22.sbtb;

            Visitor3 Visitor33 = new Visitor3();
            Visitor33.sbtb = sbtb;
            Visitor33.visit(tree);
            sbtb = Visitor33.sbtb;

            Visitor4 Visitor44 = new Visitor4();
            Visitor44.sbtb = sbtb;
            Visitor44.visit(tree);
            sbtb = Visitor44.sbtb;

            IR ir = new IR();
            ir.print();

            builtin.print();
            mips m = new mips();
            m.getmips();
            sbtb.endscope();

        }
        catch (Exception e)
        {
            System.exit(1);
        }

    }
}


