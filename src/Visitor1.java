
public class Visitor1  extends aBaseVisitor < Void>  {

    symboltable sbtb;

    @Override public  Void visitClass_decl(aParser.Class_declContext ctx)
    {
        property p = new property();
        p.sort = 0;
        p.c.name = Name.getSymbolName(ctx.ID().getText());
        if (sbtb.get(p.c.name) == null)
        {
            sbtb.put(p.c.name, p);
        }
        else
        {
            throw new RuntimeException();
        }
        return visitChildren(ctx);
    }


}