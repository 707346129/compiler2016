import java.util.Hashtable;

public class Visitor2  extends aBaseVisitor < Void>  {

    symboltable sbtb;
    property fp;
    property cp;

    @Override public Void visitClass_decl(aParser.Class_declContext ctx)
    {
        Name n = Name.getSymbolName(ctx.ID().getText());

        cp = sbtb.get(n);
        visitChildren(ctx);

        return null;
    }

    @Override public Void visitMemberlist1(aParser.Memberlist1Context ctx)
    {
        property p = new property();
        p.sort = 1;
        p.v.name = Name.getSymbolName(ctx.ID().getText());
        String t = ctx.type().getText();
        p.v.dim = 0;
        for (int i = t.length() - 1; i >= 0; i = i - 2)
        {
            if (t.charAt(i) == ']') p.v.dim ++;
            else
            {
                p.v.classname = Name.getSymbolName(t.substring(0,i + 1));
                break;
            }
        }
        if (sbtb.get(p.v.classname) == null || sbtb.get(p.v.classname).sort != 0)
        {
            throw new RuntimeException();
        }
        p.v.memberNo = cp.c.num;
        if (cp.c.list.get(p.v.name) == null)
        {
            cp.c.list.put(p.v.name, p);
        }
        else
        {
            throw new RuntimeException();
        }
        cp.c.num++;
        return visitChildren(ctx);
    }

    @Override public Void visitMemberlist2(aParser.Memberlist2Context ctx)
    {
        property p = new property();
        p.sort = 1;
        p.v.name = Name.getSymbolName(ctx.ID().getText());
        String t = ctx.type().getText();
        p.v.dim = 0;
        for (int i = t.length() - 1; i >= 0; i = i - 2)
        {
            if (t.charAt(i) == ']') p.v.dim ++;
            else
            {
                p.v.classname = Name.getSymbolName(t.substring(0,i + 1));
                break;
            }
        }
        if (sbtb.get(p.v.classname) == null || sbtb.get(p.v.classname).sort != 0)
        {
            throw new RuntimeException();
        }
        p.v.memberNo = cp.c.num;
        if (cp.c.list.get(p.v.name) == null)
        {
            cp.c.list.put(p.v.name, p);
        }
        else
        {
            throw new RuntimeException();
        }
        cp.c.num++;
        return visitChildren(ctx);
    }

    @Override public Void visitFunc1(aParser.Func1Context ctx)
    {
        fp = new property();
        fp.sort = 2;
        fp.f.name = Name.getSymbolName(ctx.ID().getText());
        String t = ctx.type().getText();
        fp.f.dim = 0;
        for (int i = t.length() - 1; i >= 0; i = i - 2)
        {
            if (t.charAt(i) == ']') fp.f.dim ++;
            else
            {
                fp.f.typereturn = Name.getSymbolName(t.substring(0,i + 1));
                break;
            }
        }
        if (sbtb.get(fp.f.typereturn) == null || sbtb.get(fp.f.typereturn).sort != 0)
        {
            throw new RuntimeException();
        }
        if (fp.f.name.toString().equals("main") && !(fp.f.typereturn.toString().equals("int") && fp.f.dim == 0))
        {
            throw new RuntimeException();
        }

        visitChildren(ctx);
        if (sbtb.get(fp.f.name) == null)
        {
            sbtb.put(fp.f.name, fp);
        }
        else
        {
            throw new RuntimeException();
        }

        return null;
    }

    @Override public Void visitFunc2(aParser.Func2Context ctx)
    {
        fp = new property();
        fp.sort = 2;
        fp.f.name = Name.getSymbolName(ctx.ID().getText());
        fp.f.typereturn = Name.getSymbolName("void");
        fp.f.dim = 0;
        if (fp.f.name.toString().equals("main"))
        {
            throw new RuntimeException();
        }
        visitChildren(ctx);

        if (sbtb.get(fp.f.name) == null)
        {
            sbtb.put(fp.f.name, fp);
        }
        else
        {
            throw new RuntimeException();
        }
        return null;
    }


    @Override public Void visitPdl1(aParser.Pdl1Context ctx)
    {
        property p = new property();
        p.sort = 1;
        p.v.name = Name.getSymbolName(ctx.ID().getText());
        String t = ctx.type().getText();
        p.v.dim = 0;
        for (int i = t.length() - 1; i >= 0; i = i - 2)
        {
            if (t.charAt(i) == ']') p.v.dim ++;
            else
            {
                p.v.classname = Name.getSymbolName(t.substring(0,i + 1));
                break;
            }
        }
        if (sbtb.get(p.v.classname) == null || sbtb.get(p.v.classname).sort != 0)
        {
            throw new RuntimeException();
        }
        param tmp = new param();
        tmp.type = p;
        tmp.name = p.v.name;
        fp.f.list.add(tmp);
        return visitChildren(ctx);
    }

    @Override public Void visitPdl2(aParser.Pdl2Context ctx)
    {
        property p = new property();
        p.sort = 1;
        p.v.name = Name.getSymbolName(ctx.ID().getText());
        String t = ctx.type().getText();
        p.v.dim = 0;
        for (int i = t.length() - 1; i >= 0; i = i - 2)
        {
            if (t.charAt(i) == ']') p.v.dim ++;
            else
            {
                p.v.classname = Name.getSymbolName(t.substring(0,i + 1));
                break;
            }
        }
        if (sbtb.get(p.v.classname) == null || sbtb.get(p.v.classname).sort != 0)
        {
            throw new RuntimeException();
        }

        param tmp = new param();
        tmp.type = p;
        tmp.name = p.v.name;
        fp.f.list.add(tmp);
        return visitChildren(ctx);
    }

}