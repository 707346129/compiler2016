
import org.antlr.v4.runtime.tree.AbstractParseTreeVisitor;

import java.util.ArrayList;

class inform
{
	String exprtype;
	int dim = 0;
	int lvalue = 0;
	String funcname;
	int stringmethod = 0;
	int func = 0;
	int arraysize = 0;
	ArrayList <property> param = new ArrayList <property>();
}

public class Visitor3  extends aBaseVisitor < inform>  {
	symboltable sbtb;
	property fp;
	int infunc = 0;
	int firstfunc = 0;
	String returntype;
	int returndim = 0;
	int infor = 0;

    @Override public inform visitProgram1(aParser.Program1Context ctx) { return visitChildren(ctx); }

	@Override public inform visitProgram2(aParser.Program2Context ctx) { return visitChildren(ctx); }

	@Override public inform visitProgram3(aParser.Program3Context ctx) { return visitChildren(ctx); }

	@Override public inform visitProgram4(aParser.Program4Context ctx) { return visitChildren(ctx); }

	@Override public inform visitProgram5(aParser.Program5Context ctx) { return visitChildren(ctx); }

	@Override public inform visitProgram6(aParser.Program6Context ctx) { return visitChildren(ctx); }

	@Override public inform visitBlock_stmt(aParser.Block_stmtContext ctx)
	{
		sbtb.beginscope();
		if (firstfunc == 1)
		{

			for (int i = 0; i <= fp.f.list.size() - 1; ++i)
			{
				if (sbtb.get(fp.f.list.get(i).name) == null ||
						(sbtb.get(fp.f.list.get(i).name) != null
								&& sbtb.get(fp.f.list.get(i).name).sort == 1
								&& sbtb.getscope(fp.f.list.get(i).name) == null))
				{
					sbtb.put(fp.f.list.get(i).name, fp.f.list.get(i).type);
				}
				else
				{
					throw new RuntimeException();
				}
			}
			firstfunc = 0;
		}
		visitChildren(ctx);

		sbtb.endscope();
		return null;
	}

	@Override public inform visitStmt_list(aParser.Stmt_listContext ctx)
	{
		visitChildren(ctx);
		return null;
	}


	@Override public inform visitVar1(aParser.Var1Context ctx)
	{
		property p = new property();
		p.sort = 1;
		p.v.name = Name.getSymbolName(ctx.ID().getText());
		String t = ctx.type().getText();
		p.v.dim = 0;
		for (int i = t.length() - 1; i >= 0; i = i - 2)
		{
			if (t.charAt(i) == ']') p.v.dim ++;
			else
			{
				p.v.classname = Name.getSymbolName(t.substring(0,i + 1));
				break;
			}
		}
		if (sbtb.get(p.v.classname) == null || sbtb.get(p.v.classname).sort != 0)
		{
			throw new RuntimeException();
		}

		if (sbtb.get(p.v.name) == null ||
				(sbtb.get(p.v.name) != null && sbtb.get(p.v.name).sort == 1 && sbtb.getscope(p.v.name) == null))
		{
			sbtb.put(p.v.name, p);
		}
		else
		{
			throw new RuntimeException();
		}
		visitChildren(ctx);
		return null;
	}

	@Override public inform visitVar2(aParser.Var2Context ctx)
	{
		
		property p = new property();
		p.sort = 1;
		p.v.name = Name.getSymbolName(ctx.ID().getText());
		String t = ctx.type().getText();
		p.v.dim = 0;
		for (int i = t.length() - 1; i >= 0; i = i - 2)
		{
			if (t.charAt(i) == ']') p.v.dim ++;
			else
			{
				p.v.classname = Name.getSymbolName(t.substring(0,i + 1));
				break;
			}
		}
		if (sbtb.get(p.v.classname) == null || sbtb.get(p.v.classname).sort != 0)
		{
			throw new RuntimeException();
		}
		if (sbtb.get(p.v.name) == null ||
				(sbtb.get(p.v.name) != null && sbtb.get(p.v.name).sort == 1 && sbtb.getscope(p.v.name) == null))
		{
			sbtb.put(p.v.name, p);
		}
		else
		{
			throw new RuntimeException();
		}
		inform inf = visit(ctx.expr());
		if (inf.exprtype.equals(p.v.classname.toString()) && inf.dim == p.v.dim)
		{

		}
		else
		{
			if (inf.exprtype.equals("null"))
			{
				if (p.v.dim == 0)
				{
					if (p.v.classname.toString().equals("int")
							||p.v.classname.toString().equals("string")
							||p.v.classname.toString().equals("bool"))
					{
						throw new RuntimeException();
					}
				}
			}
			else
			{
				throw new RuntimeException();
			}
		}
		
		visitChildren(ctx);
		return null;
	}


	@Override public inform visitFunc1(aParser.Func1Context ctx)
	{
		firstfunc = 1;
		fp = sbtb.get(Name.getSymbolName(ctx.ID().getText()));
		returntype = fp.f.typereturn.toString();
		returndim = fp.f.dim;
		visit(ctx.block_stmt());
		return null;
	}

	@Override public inform visitFunc2(aParser.Func2Context ctx)
	{
		firstfunc = 1;
		fp = sbtb.get(Name.getSymbolName(ctx.ID().getText()));
		returntype = "void";
		returndim = 0;
		visit(ctx.block_stmt());
		return null;
	}

	@Override public inform visitPdl1(aParser.Pdl1Context ctx) { return visitChildren(ctx); }

	@Override public inform visitPdl2(aParser.Pdl2Context ctx) { return visitChildren(ctx); }

	@Override public inform visitType5(aParser.Type5Context ctx)
	{
		return visitChildren(ctx);
	}

	@Override public inform visitType4(aParser.Type4Context ctx) { return visitChildren(ctx); }

	@Override public inform visitType3(aParser.Type3Context ctx) { return visitChildren(ctx); }

	@Override public inform visitType2(aParser.Type2Context ctx) { return visitChildren(ctx); }

	@Override public inform visitType1(aParser.Type1Context ctx) { return visitChildren(ctx); }

	@Override public inform visitStmt1(aParser.Stmt1Context ctx) { return visitChildren(ctx); }

	@Override public inform visitStmt2(aParser.Stmt2Context ctx) { return visitChildren(ctx); }

	@Override public inform visitStmt3(aParser.Stmt3Context ctx) { return visitChildren(ctx); }

	//if
	@Override public inform visitStmt4(aParser.Stmt4Context ctx)
	{
		inform inf = visit(ctx.expr());
		if (inf.exprtype.equals("bool") && inf.dim == 0)
		{

		}
		else
		{
			throw new RuntimeException();
		}
		sbtb.beginscope();
		visitChildren(ctx);
		sbtb.endscope();
		return null;
	}

	@Override public inform visitStmt5(aParser.Stmt5Context ctx)
	{
		inform inf = visit(ctx.expr());
		if (inf.exprtype.equals("bool") && inf.dim == 0)
		{

		}
		else
		{
			throw new RuntimeException();
		}
		sbtb.beginscope();
		visit(ctx.stmt(0));
		sbtb.endscope();
		sbtb.beginscope();
		visit(ctx.stmt(1));
		sbtb.endscope();
		return null;
	}
	//return
	@Override public inform visitStmt6(aParser.Stmt6Context ctx)
	{
		inform inf = new inform();
		if (returntype.equals("void"))
		{
			if (ctx.expr() == null)
			{

			}
			else
			{
				throw new RuntimeException();
			}

		}
		else
		{
			if (ctx.expr() == null)
			{
				throw new RuntimeException();
			}
			else
			{
				inf = visit(ctx.expr());
			}
			if (inf.exprtype.equals(returntype) && inf.dim == returndim)
			{

			}
			else
			{
				if (inf.exprtype.equals("null"))
				{
					if (returndim == 0)
					{
						if (returntype.equals("int")
								||returntype.equals("string")
								||returntype.equals("bool"))
						{
							throw new RuntimeException();
						}
					}
				}
				else
				{
					throw new RuntimeException();
				}
			}
		}
		visitChildren(ctx);
		return null;

	}

	//break;
	@Override public inform visitStmt7(aParser.Stmt7Context ctx)
	{
		if (infor == 0)
		{
			throw new RuntimeException();
		}
		visitChildren(ctx);
		return null;
	}

	//continue;
	@Override public inform visitStmt8(aParser.Stmt8Context ctx)
	{
		if (infor == 0)
		{
			throw new RuntimeException();
		}
		visitChildren(ctx);
		return null;
	}

	//while
	@Override public inform visitStmt9(aParser.Stmt9Context ctx)
	{
		infor++;
		inform inf = visit(ctx.expr());
		if (inf.exprtype.equals("bool") && inf.dim == 0)
		{

		}
		else
		{
			throw new RuntimeException();
		}
		sbtb.beginscope();
		visit(ctx.stmt());
		sbtb.endscope();
		infor--;
		return null;
	}

	//for
	@Override public inform visitStmt10(aParser.Stmt10Context ctx)
	{
		infor++;
		inform inf = new inform();
		if (ctx.bb != null)
		{
			inf = visit(ctx.bb);
		}
		if (ctx.aa != null)
		{
			visit(ctx.aa);
		}
		if (ctx.cc != null)
		{
			visit(ctx.cc);
		}
		if (ctx.bb == null ||(ctx.bb != null && inf.exprtype.equals("bool") && inf.dim == 0))
		{

		}
		else
		{
			throw new RuntimeException();
		}

		sbtb.beginscope();
		visit(ctx.stmt());
		sbtb.endscope();
		infor--;
		return null;
	}
	//ID
	@Override public inform visitPrimary1(aParser.Primary1Context ctx)
	{
		//System.out.println(ctx.ID().getText());
		inform inf = new inform();
		inf.lvalue = 1;
		property p = new property();
		if (sbtb.get(Name.getSymbolName(ctx.ID().getText())) == null)
		{
			throw new RuntimeException();
		}
		else
		{
			p = sbtb.get(Name.getSymbolName(ctx.ID().getText()));
		}
		if (p.sort == 0)
		{
			throw new RuntimeException();
		}
		if (p.sort == 1)
		{
			inf.exprtype = p.v.classname.toString();
			inf.dim = p.v.dim;
			inf.func = 0;
		}
		if (p.sort == 2)
		{
			inf.lvalue = 0;
			inf.funcname = p.f.name.toString();
			inf.exprtype = p.f.typereturn.toString();
			inf.dim = p.f.dim;
			inf.func = 1;
		}
		return inf;
	}
	//constant
	@Override public inform visitPrimary2(aParser.Primary2Context ctx)
	{
		inform inf = new inform();

		inf.lvalue = 0;
		inf.exprtype = visit(ctx.constant()).exprtype;
		inf.dim = 0;
		inf.func = 0;
		return inf;
	}
	//(expr)
	@Override public inform visitPrimary3(aParser.Primary3Context ctx)
	{
		inform inf = new inform();
		inf.lvalue = 0;
		inf.exprtype = visit(ctx.expr()).exprtype;
		inf.dim = visit(ctx.expr()).dim;
		inf.func = 0;
		return inf;
	}

	//postfix '++'
	@Override public inform visitPost4(aParser.Post4Context ctx)
	{
		inform inf = new inform();
		inform v = visit(ctx.postfix());
		if (v.func == 1 || v.lvalue == 0)
		{
			throw new RuntimeException();
		}
		if (v.exprtype.equals("int") && v.dim == 0)
		{

		}
		else
		{
			throw new RuntimeException();
		}
		inf.lvalue = 0;
		inf.exprtype = "int";
		inf.dim = 0;
		inf.func = 0;

		return inf;
	}
	//postfix '--'
	@Override public inform visitPost5(aParser.Post5Context ctx)
	{
		inform inf = new inform();
		inform v = visit(ctx.postfix());
		if (v.func == 1 || v.lvalue == 0)
		{
			throw new RuntimeException();
		}
		if (v.exprtype.equals("int") && v.dim == 0)
		{

		}
		else
		{
			throw new RuntimeException();
		}
		inf.lvalue = 0;
		inf.exprtype = "int";
		inf.dim = 0;
		inf.func = 0;
		return inf;
	}

	//postfix '[' expr ']'
	@Override public inform visitPost2(aParser.Post2Context ctx)
	{
		inform inf = new inform();
		inform v = visit(ctx.postfix());
		if (v.func == 1 || v.dim == 0)
		{
			throw new RuntimeException();
		}
		inf.lvalue = v.lvalue;
		inf.exprtype = v.exprtype;

		inf.dim = v.dim - 1;
		inf.func = 0;
		inform e = visit(ctx.expr());
		if (e.exprtype.equals("int") && e.dim == 0)
		{

		}
		else
		{
			throw new RuntimeException();
		}
		return inf;
	}

	//postfix '.' ID
	@Override public inform visitPost3(aParser.Post3Context ctx)
	{
		inform inf = new inform();
		inform v = visit(ctx.postfix());

		Name n = Name.getSymbolName(ctx.ID().getText());
		if (v.func == 1)
		{
			throw new RuntimeException();
		}
		if (v.exprtype.equals("string") && v.dim == 0)
		{
			Name str = Name.getSymbolName("string");

			if (sbtb.get(str).c.list.get(n) == null)
			{
				throw new RuntimeException();
			}
			else
			{
				inf.funcname = n.toString();
				inf.func = 1;
				inf.exprtype = sbtb.get(str).c.list.get(n).f.typereturn.toString();
				inf.stringmethod = 1;
				inf.dim = sbtb.get(str).c.list.get(n).f.dim;
				inf.lvalue = 0;
			}
		}
		else
		{
			if (v.dim > 0 && n.toString().equals("size"))
			{
				inf.funcname = "size";
				inf.func = 1;
				inf.exprtype = "int";
				inf.arraysize = 1;
				inf.dim = 0;
				inf.lvalue = 0;
			}
			else
			{
				Name vn = Name.getSymbolName(v.exprtype);
				if (sbtb.get(vn).c.list.get(n) == null || v.dim > 0)
				{
					throw new RuntimeException();
				}
				else
				{
					inf.exprtype = sbtb.get(vn).c.list.get(n).v.classname.toString();
					inf.func = 0;
					inf.dim = sbtb.get(vn).c.list.get(n).v.dim;
					inf.lvalue = v.lvalue;
				}
			}
		}

		return inf;
	}

	//postfix '(' param_list ')'
	@Override public inform visitPost6(aParser.Post6Context ctx)
	{
		inform inf = new inform();
		inform v = visit(ctx.postfix());
		if (v.func == 0)
		{
			throw new RuntimeException();
		}
		property f = new property();
		if (v.stringmethod == 1)
		{
			property p = sbtb.get(Name.getSymbolName("string"));
			f = p.c.list.get(Name.getSymbolName(v.funcname));

		}
		else
		{
			if (v.arraysize == 1)
			{
				f.sort = 2;
				f.f.name = Name.getSymbolName("size");
				f.f.typereturn = Name.getSymbolName("int");
			}
			else
			{
				f = sbtb.get(Name.getSymbolName(v.funcname));
			}
		}
		inf.func = 0;
		inf.stringmethod = 0;
		inf.arraysize = 0;
		inf.dim = v.dim;
		inf.exprtype = v.exprtype;
		inf.lvalue = 0;
		if (ctx.param_list() != null)
		{
			inf.param = visit(ctx.param_list()).param;
		}
		else
		{
			inf.param.clear();
		}
		if (inf.param.size() != f.f.list.size())
		{
			throw new RuntimeException();
		}
		int len = inf.param.size();
		for (int i = 0; i <= len - 1; ++i)
		{
			if (!inf.param.get(i).v.classname.toString().equals(f.f.list.get(len - i - 1).type.v.classname.toString())
					|| inf.param.get(i).v.dim != f.f.list.get(len - i - 1).type.v.dim)
			{
				throw new RuntimeException();
			}
		}
		return inf;
	}

	//primary
	@Override public inform visitPost1(aParser.Post1Context ctx)
	{

		return visit(ctx.primary());
	}

	@Override public inform visitPl1(aParser.Pl1Context ctx)
	{
		inform inf = new inform();
		inform v = visit(ctx.expr());
		property p = new property();
		p.v.classname = Name.getSymbolName(v.exprtype);
		p.sort = 1;
		p.v.name = Name.getSymbolName(ctx.expr().getText());
		p.v.dim = v.dim;
		inf.param.add(p);

		return inf;
	}

	@Override public inform visitPl2(aParser.Pl2Context ctx)
	{
		inform inf = new inform();
		inform v = visit(ctx.param_list());
		inf.param = v.param;
		v = visit(ctx.expr());
		property p = new property();
		p.v.classname = Name.getSymbolName(v.exprtype);
		p.sort = 1;
		p.v.name = Name.getSymbolName(ctx.expr().getText());
		p.v.dim = v.dim;
		inf.param.add(p);
		return inf;
	}

	@Override public inform visitUnary1(aParser.Unary1Context ctx)
	{
		return visit(ctx.postfix());
	}
	//++
	@Override public inform visitUnary2(aParser.Unary2Context ctx)
	{
		inform inf = visit(ctx.unary());
		if (inf.lvalue == 0)
		{
			throw new RuntimeException();
		}
		if (inf.exprtype.equals("int") && inf.dim == 0)
		{

		}
		else
		{
			throw new RuntimeException();
		}
		inf.lvalue = 0;
		return inf;
	}

	//--
	@Override public inform visitUnary3(aParser.Unary3Context ctx)
	{
		inform inf = visit(ctx.unary());
		if (inf.lvalue == 0)
		{
			throw new RuntimeException();
		}
		if (inf.exprtype.equals("int") && inf.dim == 0)
		{

		}
		else
		{
			throw new RuntimeException();
		}
		inf.lvalue = 0;
		return inf;
	}
	//~
	@Override public inform visitUnary4(aParser.Unary4Context ctx)
	{
		inform inf = visit(ctx.unary());
		if (inf.exprtype.equals("int") && inf.dim == 0)
		{

		}
		else
		{
			throw new RuntimeException();
		}
		inf.lvalue = 0;
		return inf;
	}
	//unaryOperator unary
	@Override public inform visitUnary5(aParser.Unary5Context ctx)
	{
		inform inf = visit(ctx.unary());
		inf.lvalue = 0;
		String o = ctx.unaryOperator().getText();
		if (o.equals("!"))
		{
			if (inf.exprtype.equals("bool") && inf.dim == 0)
			{

			}
			else
			{
				throw new RuntimeException();
			}
		}
		else
		{
			if (inf.exprtype.equals("int") && inf.dim == 0)
			{

			}
			else
			{
				throw new RuntimeException();
			}
		}
		return inf;
	}

	@Override public inform visitUnaryOperator(aParser.UnaryOperatorContext ctx)
	{
		return visitChildren(ctx);
	}

	@Override public inform visitMe1(aParser.Me1Context ctx)
	{
		return visit(ctx.unary());
	}

	@Override public inform visitMe3(aParser.Me3Context ctx)
	{
		inform l = visit(ctx.multiplicativeExpression());
		inform r = visit(ctx.unary());
		inform inf = new inform();
		if (l.exprtype.equals("int") && l.dim == 0 && r.exprtype.equals("int") && r.dim == 0)
		{

		}
		else
		{
			throw new RuntimeException();
		}
		inf = l;
		inf.lvalue = 0;
		return inf;
	}

	@Override public inform visitMe2(aParser.Me2Context ctx)
	{
		inform l = visit(ctx.multiplicativeExpression());
		inform r = visit(ctx.unary());
		inform inf = new inform();
		if (l.exprtype.equals("int") && l.dim == 0 && r.exprtype.equals("int") && r.dim == 0)
		{

		}
		else
		{
			throw new RuntimeException();
		}
		inf = l;
		inf.lvalue = 0;
		return inf;
	}
	@Override public inform visitMe4(aParser.Me4Context ctx)
	{
		inform l = visit(ctx.multiplicativeExpression());
		inform r = visit(ctx.unary());
		inform inf = new inform();
		if (l.exprtype.equals("int") && l.dim == 0 && r.exprtype.equals("int") && r.dim == 0)
		{

		}
		else
		{
			throw new RuntimeException();
		}
		inf = l;
		inf.lvalue = 0;
		return inf;
	}

	@Override public inform visitAdd2(aParser.Add2Context ctx)
	{
		inform inf = new inform();
		inform l = visit(ctx.additiveExpression());
		inform r = visit(ctx.multiplicativeExpression());
		if ((l.exprtype.equals("int") || l.exprtype.equals("string")) && l.dim == 0
				&& l.exprtype.equals(r.exprtype) && r.dim == 0)
		{
		}
		else
		{
			throw new RuntimeException();
		}
		inf = l;
		inf.lvalue = 0;
		return inf;
	}

	@Override public inform visitAdd1(aParser.Add1Context ctx)
	{
		return visit(ctx.multiplicativeExpression());
	}

	@Override public inform visitAdd3(aParser.Add3Context ctx)
	{
		inform inf = new inform();
		inform l = visit(ctx.additiveExpression());
		inform r = visit(ctx.multiplicativeExpression());
		if (l.exprtype.equals("int")  && l.dim == 0 && l.exprtype.equals(r.exprtype) && r.dim == 0)
		{
		}
		else
		{
			throw new RuntimeException();
		}
		inf = l;
		inf.lvalue = 0;
		return inf;
	}

	@Override public inform visitShift1(aParser.Shift1Context ctx)
	{
		return visit(ctx.additiveExpression());
	}

	@Override public inform visitShift2(aParser.Shift2Context ctx)
	{
		inform inf = new inform();
		inform l = visit(ctx.shiftExpression());
		inform r = visit(ctx.additiveExpression());
		if (l.exprtype.equals("int")  && l.dim == 0 && l.exprtype.equals(r.exprtype) && r.dim == 0)
		{
		}
		else
		{
			throw new RuntimeException();
		}
		inf = l;
		inf.lvalue = 0;
		return inf;
	}

	@Override public inform visitShift3(aParser.Shift3Context ctx)
	{
		inform inf = new inform();
		inform l = visit(ctx.shiftExpression());
		inform r = visit(ctx.additiveExpression());
		if (l.exprtype.equals("int")  && l.dim == 0 && l.exprtype.equals(r.exprtype) && r.dim == 0)
		{
		}
		else
		{
			throw new RuntimeException();
		}
		inf = l;
		inf.lvalue = 0;
		return inf;
	}
	@Override public inform visitRelation5(aParser.Relation5Context ctx)
	{
		inform inf = new inform();
		inform l = visit(ctx.relationalExpression());
		inform r = visit(ctx.shiftExpression());
		if ((l.exprtype.equals("int") || l.exprtype.equals("string")) && l.dim == 0
				&& l.exprtype.equals(r.exprtype) && r.dim == 0)
		{
		}
		else
		{
			throw new RuntimeException();
		}
		inf = l;
		inf.exprtype = "bool";
		inf.dim = 0;
		inf.lvalue = 0;
		return inf;
	}

	@Override public inform visitRelation4(aParser.Relation4Context ctx)
	{
		inform inf = new inform();
		inform l = visit(ctx.relationalExpression());
		inform r = visit(ctx.shiftExpression());
		if ((l.exprtype.equals("int") || l.exprtype.equals("string")) && l.dim == 0
				&& l.exprtype.equals(r.exprtype) && r.dim == 0) {
		} else {
			throw new RuntimeException();
		}
		inf = l;
		inf.exprtype = "bool";
		inf.dim = 0;
		inf.lvalue = 0;
		return inf;
	}

	@Override public inform visitRelation3(aParser.Relation3Context ctx)
	{
		inform inf = new inform();
		inform l = visit(ctx.relationalExpression());
		inform r = visit(ctx.shiftExpression());
		if ((l.exprtype.equals("int") || l.exprtype.equals("string")) && l.dim == 0
				&& l.exprtype.equals(r.exprtype) && r.dim == 0) {
		} else {
			throw new RuntimeException();
		}
		inf = l;
		inf.exprtype = "bool";
		inf.dim = 0;
		inf.lvalue = 0;
		return inf;
	}

	@Override public inform visitRelation2(aParser.Relation2Context ctx)
	{
		inform inf = new inform();
		inform l = visit(ctx.relationalExpression());
		inform r = visit(ctx.shiftExpression());
		if ((l.exprtype.equals("int") || l.exprtype.equals("string")) && l.dim == 0
				&& l.exprtype.equals(r.exprtype) && r.dim == 0) {
		} else {
			throw new RuntimeException();
		}
		inf = l;
		inf.exprtype = "bool";
		inf.dim = 0;
		inf.lvalue = 0;
		return inf;
	}

	@Override public inform visitRelation1(aParser.Relation1Context ctx)
	{
		return visit(ctx.shiftExpression());
	}

	@Override public inform visitEqual1(aParser.Equal1Context ctx)
	{
		return visit(ctx.relationalExpression());
	}

	@Override public inform visitEqual2(aParser.Equal2Context ctx)
	{
		inform inf = new inform();
		inform l = visit(ctx.equalityExpression());
		inform r = visit(ctx.relationalExpression());

		if ((l.exprtype.equals("int") || l.exprtype.equals("string") || l.exprtype.equals("bool")) && l.dim == 0
				&& l.exprtype.equals(r.exprtype) && r.dim == 0)
		{

		}
		else
		{
			if (l.exprtype.equals("null"))
			{
				if (r.dim == 0)
				{
					if (r.exprtype.equals("int")
							||r.exprtype.equals("string")
							||r.exprtype.equals("bool"))
					{
						throw new RuntimeException();
					}
				}
			}
			else
			{	if (r.exprtype.equals("null"))
				{
					if (l.dim == 0)
					{
						if (l.exprtype.equals("int")
							||l.exprtype.equals("string")
							||l.exprtype.equals("bool"))
						{
							throw new RuntimeException();
						}
					}
				}
				else
				{
					throw new RuntimeException();
				}
			}
		}
		inf = l;
		inf.exprtype = "bool";
		inf.dim = 0;
		inf.lvalue = 0;
		return inf;
	}

	@Override public inform visitEqual3(aParser.Equal3Context ctx)
	{
		inform inf = new inform();
		inform l = visit(ctx.equalityExpression());
		inform r = visit(ctx.relationalExpression());
		if ((l.exprtype.equals("int") || l.exprtype.equals("string") || l.exprtype.equals("bool")) && l.dim == 0
				&& l.exprtype.equals(r.exprtype) && r.dim == 0)
		{

		}
		else
		{
			if (l.exprtype.equals("null"))
			{
				if (r.dim == 0)
				{
					if (r.exprtype.equals("int")
							||r.exprtype.equals("string")
							||r.exprtype.equals("bool"))
					{
						throw new RuntimeException();
					}
				}
			}
			else
			{
				if (r.exprtype.equals("null"))
				{
					if (l.dim == 0)
					{
						if (l.exprtype.equals("int")
							||l.exprtype.equals("string")
							||l.exprtype.equals("bool"))
						{
							throw new RuntimeException();
						}
					}
				}
				else
				{
					throw new RuntimeException();
				}
			}
		}
		inf = l;
		inf.exprtype = "bool";
		inf.dim = 0;
		inf.lvalue = 0;
		return inf;
	}

	@Override public inform visitAnd2(aParser.And2Context ctx)
	{
		inform inf = new inform();
		inform l = visit(ctx.andExpression());
		inform r = visit(ctx.equalityExpression());
		if (l.exprtype.equals("int") && l.dim == 0 && l.exprtype.equals(r.exprtype) && r.dim == 0) {
		} else {
			throw new RuntimeException();
		}
		inf = l;
		inf.lvalue = 0;
		return inf;
	}

	@Override public inform visitAnd1(aParser.And1Context ctx)
	{
		return visit(ctx.equalityExpression());
	}

	@Override public inform visitEor2(aParser.Eor2Context ctx)
	{
		inform inf = new inform();
		inform l = visit(ctx.exclusiveOrExpression());
		inform r = visit(ctx.andExpression());
		if (l.exprtype.equals("int") && l.dim == 0 && l.exprtype.equals(r.exprtype) && r.dim == 0) {
		} else {
			throw new RuntimeException();
		}
		inf = l;
		inf.lvalue = 0;
		return inf;
	}

	@Override public inform visitEor1(aParser.Eor1Context ctx)
	{
		return visit(ctx.andExpression());
	}

	@Override public inform visitIor1(aParser.Ior1Context ctx)
	{
		return visit(ctx.exclusiveOrExpression());
	}

	@Override public inform visitIor2(aParser.Ior2Context ctx)
	{
		inform inf = new inform();
		inform l = visit(ctx.inclusiveOrExpression());
		inform r = visit(ctx.exclusiveOrExpression());
		if (l.exprtype.equals("int") && l.dim == 0 && l.exprtype.equals(r.exprtype) && r.dim == 0) {
		} else {
			throw new RuntimeException();
		}
		inf = l;
		inf.lvalue = 0;
		return inf;
	}

	@Override public inform visitLand2(aParser.Land2Context ctx)
	{
		inform inf = new inform();
		inform l = visit(ctx.logicalAndExpression());
		inform r = visit(ctx.inclusiveOrExpression());

		if (l.exprtype.equals("bool") && l.dim == 0 && l.exprtype.equals(r.exprtype) && r.dim == 0) {
		} else {
			throw new RuntimeException();
		}
		inf = l;
		inf.lvalue = 0;
		return inf;
	}

	@Override public inform visitLand1(aParser.Land1Context ctx)
	{
		return visit(ctx.inclusiveOrExpression());
	}

	@Override public inform visitLor1(aParser.Lor1Context ctx)
	{
		return visit(ctx.logicalAndExpression());
	}

	@Override public inform visitLor2(aParser.Lor2Context ctx)
	{
		inform inf = new inform();
		inform l = visit(ctx.logicalOrExpression());
		inform r = visit(ctx.logicalAndExpression());

		if (l.exprtype.equals("bool") && l.dim == 0 && l.exprtype.equals(r.exprtype) && r.dim == 0) {
		} else {
			throw new RuntimeException();
		}
		inf = l;
		inf.lvalue = 0;
		return inf;
	}


	@Override public inform visitAssign1(aParser.Assign1Context ctx)
	{

		return visit(ctx.logicalOrExpression());
	}

	@Override public inform visitAssign2(aParser.Assign2Context ctx)
	{
		inform inf = new inform();
		inform r = visit(ctx.expr());
		inform l = visit(ctx.unary());

		if (l.lvalue == 0)
		{
			throw new RuntimeException();
		}
		if ( (!l.exprtype.equals("null")) && l.dim == r.dim && l.exprtype.equals(r.exprtype)) {
		}
		else
		{
			if (r.exprtype.equals("null"))
			{
				if (l.dim == 0)
				{
					if (l.exprtype.equals("int")
							||l.exprtype.equals("string")
							||l.exprtype.equals("bool"))
					{
						throw new RuntimeException();
					}
				}
			}
			else
			{
				throw new RuntimeException();
			}
		}
		inf = l;
		inf.lvalue = 0;

		return inf;
	}

	@Override public inform visitAssignmentOperator(aParser.AssignmentOperatorContext ctx)
	{
		return visitChildren(ctx);
	}


	@Override public inform visitDim1(aParser.Dim1Context ctx)
	{
		
		inform inf = new inform();
		inform e = visit(ctx.expr());
		
		inform d = new inform();
		d.dim = 0;
		if (ctx.dimexpr() != null)
		{
			d = visit(ctx.dimexpr());
		}

		
		if (e.exprtype.equals("int") && e.dim == 0)
		{

		}
		else
		{
			throw new RuntimeException();
		}
		inf.dim = d.dim + 1;
		
		return inf;
	}

	@Override public inform visitDim2(aParser.Dim2Context ctx)
	{
		inform inf = new inform();
		inform e = visit(ctx.expr());
		inform d = visit(ctx.dim_expr());
		if (e.exprtype.equals("int") && e.dim == 0)
		{

		}
		else
		{
			throw new RuntimeException();
		}
		inf.dim = d.dim + 1;
		
		return inf;
	}

	@Override public inform visitDim3(aParser.Dim3Context ctx)
	{
		
		inform inf = new inform();
		inf.dim = 1;
		return inf;
	}

	@Override public inform visitDim4(aParser.Dim4Context ctx)
	{
		
		inform inf = new inform();
		inform d = visit(ctx.dimexpr());
		inf.dim = d.dim + 1;
		return inf;
	}

	@Override public inform visitExpr2(aParser.Expr2Context ctx)
	{
		property p = new property();
		p.sort = 1;
		String t = ctx.type().getText();
		p.v.dim = 0;
		if (t.charAt(t.length() - 1) == ']')
		{
			throw new RuntimeException();
		}
		Name tp = Name.getSymbolName(t);
		if (sbtb.get(tp) == null || sbtb.get(tp).sort != 0)
		{
			throw new RuntimeException();
		}
		inform inf = new inform();
		inf.exprtype = t;
		inf.dim = 0;
		inf.lvalue = 0;
		if (ctx.dim_expr() != null)
		{
			inf.dim = visit(ctx.dim_expr()).dim;
		}
		if (inf.dim == 0 && (inf.exprtype.equals("int")||inf.exprtype.equals("bool")||inf.exprtype.equals("string")))
		{
			throw new RuntimeException();
		}
		return inf;
	}

	@Override public inform visitExpr1(aParser.Expr1Context ctx)
	{

		return visit(ctx.assignmentExpression());
	}

	@Override public inform visitConst1(aParser.Const1Context ctx)
	{
		inform inf = new inform();
		inf.exprtype = "int";
		inf.dim = 0;
		inf.lvalue = 0;
		inf.func = 0;
		return inf;
	}

	@Override public inform visitConst2(aParser.Const2Context ctx)
	{
		inform inf = new inform();
		inf.exprtype = "bool";
		inf.dim = 0;
		inf.lvalue = 0;
		inf.func = 0;
		return inf;
	}

	@Override public inform visitConst3(aParser.Const3Context ctx)
	{
		inform inf = new inform();
		inf.exprtype = "string";
		inf.dim = 0;
		inf.lvalue = 0;
		inf.func = 0;
		return inf;
	}

	@Override public inform visitConst4(aParser.Const4Context ctx)
	{
		inform inf = new inform();
		inf.exprtype = "null";
		inf.dim = 0;
		inf.lvalue = 0;
		inf.func = 0;
		return inf;
	}
}