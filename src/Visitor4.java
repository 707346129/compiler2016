/*
Jump Instruction:
ret		ret $src
jump	jump %target
br		br $cond %ifTrue %ifFalse

		Memory Access Instruction:
store	store size $addr $src offset    // M[$addr+offset : $addr+offset+size-1] <- $src
load	$dest = load size $addr offset  // $dest <- M[$addr+offset : $addr+offset+size-1]
alloc	$dest = alloc $size

		Function Call Instruction:
call	call funcname $op1 $op2 $op3 ...
dcall	$dest = call funcname $op1 $op2 $op3 ...

		Register Transfer Instruction:
move	$dest = move $src

		Phi Instruction:
phi		$dest = phi %block1 $reg1 %block2 $reg2 ...

		Arithmetic Instruction:
neg		$dest = neg $src
add		$dest = add $src1 $src2
sub		$dest = sub $src1 $src2
mul		$dest = mul $src1 $src2
div		$dest = div $src1 $src2
rem		$dest = rem $src1 $src2

		Bitwise Instruction:
shl		$dest = shl $src1 $src2
shr		$dest = shr $src1 $src2
and		$dest = and $src1 $src2
xor		$dest = xor $src1 $src2
or		$dest = or $src1 $src2
not		$dest = not $src

		Condition Set Instruction:
slt		$dest = slt $src1 $src2
sgt		$dest = sgt $src1 $src2
sle		$dest = sle $src1 $src2
sge		$dest = sge $src1 $src2
seq		$dest = seq $src1 $src2
sne		$dest = sne $src1 $src2
func 	func funcname $op1 $op2 ...
*/
import org.omg.CORBA.Object;

import java.util.ArrayList;
import java.util.Hashtable;

class info
{
	String exprtype;
	int dim = 0;
	int lvalue = 0;
	String funcname;
	int stringmethod = 0;
	int func = 0;
	int arraysize = 0;
	ArrayList <property> param = new ArrayList <property>();
	ArrayList <src> oplist = new ArrayList <src>();
	src s = new src();
	register lr = new register();
	int newexpr = 0;

}

class forlabels
{
	label l1 = new label();
	label l2 = new label();
	label l3 = new label();
	label l4 = new label();
}
public class Visitor4  extends aBaseVisitor < info>  {
	symboltable sbtb;
	property fp;
	instr ins = new instr();
	int infunc = 0;
	int firstfunc = 0;
	String returntype;
	ArrayList<forlabels> forlist = new ArrayList<forlabels>();

    @Override public info visitProgram1(aParser.Program1Context ctx) { return visitChildren(ctx); }

	@Override public info visitProgram2(aParser.Program2Context ctx) { return visitChildren(ctx); }

	@Override public info visitProgram3(aParser.Program3Context ctx) { return visitChildren(ctx); }

	@Override public info visitProgram4(aParser.Program4Context ctx) { return visitChildren(ctx); }

	@Override public info visitProgram5(aParser.Program5Context ctx) { return visitChildren(ctx); }

	@Override public info visitProgram6(aParser.Program6Context ctx) { return visitChildren(ctx); }


	@Override public info visitFunc1(aParser.Func1Context ctx)
	{
		fp = new property();
		firstfunc = 1;
		infunc = 1;
		ins = new instr();
		ins.op = "func";


		fp.sort = 2;
		fp.f.name = Name.getSymbolName(ctx.ID().getText());
		//returntype = fp.f.typereturn.toString();
		//returndim = fp.f.dim;
		ins.funcname = fp.f.name.toString();

		ins.regnum = 0;
		ins.maxarg = 0;
		if (ins.funcname.equals("move"))
		{
			ins.funcname = "func__move";
		}

		if (ctx.param_decl_list() != null) visit(ctx.param_decl_list());
		IR.instrlist.add(ins);

		int left = register.reglist.size();


		visit(ctx.block_stmt());
		int right = register.reglist.size();
		ins.regnum = right - left;
		for (int i = left; i < right; ++i)
		{
			register.reglist.get(i).offset = 4 * (i - left);
			if (ins.funcname.equals("main"))
			{
				register.reglist.get(i).offset += IR.mainregnum * 4;
			}
		}
		if (ins.funcname.equals("main"))
		{
			IR.mainregnum += ins.regnum;
		}

		instr inst = new instr();
		inst.op = "retnull";
		IR.instrlist.add(inst);

		infunc = 0;
		return null;
	}



	@Override public info visitFunc2(aParser.Func2Context ctx)
	{
		fp = new property();
		firstfunc = 1;
		infunc = 1;
		ins = new instr();
		ins.op = "func";

		fp.sort = 2;
		fp.f.name = Name.getSymbolName(ctx.ID().getText());
		ins.funcname = fp.f.name.toString();
		ins.regnum = 0;
		ins.maxarg = 0;
		if (ins.funcname.equals("move"))
		{
			ins.funcname = "func__move";
		}


		if (ctx.param_decl_list() != null) visit(ctx.param_decl_list());
		IR.instrlist.add(ins);

		int left = register.reglist.size();
		/*ins = new instr();
		ins.op = "label";

		label l = new label();
		l.No = label.lablist.size();
		l.name = ins.funcname;
		if (l.name.equals("main"))
			label.mainNo = l.No;
		label.lablist.add(l);
		ins.l1 = l;

		IR.instrlist.add(ins);*/

		visit(ctx.block_stmt());

		int right = register.reglist.size();
		ins.regnum = right - left;
		for (int i = left; i < right; ++i)
		{
			register.reglist.get(i).offset = 4 * (i - left);
			if (ins.funcname.equals("main"))
			{
				register.reglist.get(i).offset += IR.mainregnum * 4;
			}
		}
		if (ins.funcname.equals("main"))
		{
			IR.mainregnum += ins.regnum;
		}
		instr inst = new instr();
		inst.op = "retnull";
		IR.instrlist.add(inst);

		infunc = 0;
		return null;
	}


	@Override public info visitPdl1(aParser.Pdl1Context ctx)
	{
		property p = new property();
		p.sort = 1;
		p.v.name = Name.getSymbolName(ctx.ID().getText());
		String t = ctx.type().getText();
		p.v.dim = 0;
		for (int i = t.length() - 1; i >= 0; i = i - 2)
		{
			if (t.charAt(i) == ']') p.v.dim ++;
			else
			{
				p.v.classname = Name.getSymbolName(t.substring(0,i + 1));
				break;
			}
		}
		register r = new register();
		r.No = register.reglist.size();
		register.reglist.add(r);
		p.v.reg = r;
		src s = new src();
		s.sort = 1;
		s.r = r;
		s.r.arg = ins.oplist.size();

		ins.oplist.add(s);
		param tmp = new param();
		tmp.type = p;
		tmp.name = p.v.name;
		fp.f.list.add(tmp);

		return visitChildren(ctx);
	}

	@Override public info visitPdl2(aParser.Pdl2Context ctx)
	{
		property p = new property();
		p.sort = 1;
		p.v.name = Name.getSymbolName(ctx.ID().getText());
		String t = ctx.type().getText();
		p.v.dim = 0;
		for (int i = t.length() - 1; i >= 0; i = i - 2)
		{
			if (t.charAt(i) == ']') p.v.dim ++;
			else
			{
				p.v.classname = Name.getSymbolName(t.substring(0,i + 1));
				break;
			}
		}

		register r = new register();
		r.No = register.reglist.size();
		register.reglist.add(r);
		p.v.reg = r;
		src s = new src();
		s.sort = 1;
		s.r = r;
		s.r.arg = ins.oplist.size();
		ins.oplist.add(s);

		param tmp = new param();
		tmp.type = p;
		tmp.name = p.v.name;
		fp.f.list.add(tmp);

		return visitChildren(ctx);
	}

	@Override public info visitBlock_stmt(aParser.Block_stmtContext ctx)
	{
		sbtb.beginscope();
		if (firstfunc == 1)
		{

			for (int i = 0; i <= fp.f.list.size() - 1; ++i)
			{

				sbtb.put(fp.f.list.get(i).name, fp.f.list.get(i).type);

			}
			firstfunc = 0;
		}
		visitChildren(ctx);

		sbtb.endscope();
		return null;
	}

	@Override public info visitStmt_list(aParser.Stmt_listContext ctx)
	{
		visitChildren(ctx);
		return null;
	}

	@Override public info visitVar1(aParser.Var1Context ctx)
	{
		property p = new property();
		p.sort = 1;
		p.v.name = Name.getSymbolName(ctx.ID().getText());
		String t = ctx.type().getText();
		p.v.dim = 0;
		for (int i = t.length() - 1; i >= 0; i = i - 2)
		{
			if (t.charAt(i) == ']') p.v.dim ++;
			else
			{
				p.v.classname = Name.getSymbolName(t.substring(0,i + 1));
				break;
			}
		}

		register r = new register();
		r.No = register.reglist.size();
		register.reglist.add(r);

		if (infunc == 1)
		{

		}
		else
		{
			r.global = register.globallist.size();
			register.globallist.add(r);

		}
		p.v.reg = r;
		sbtb.put(p.v.name, p);
		visitChildren(ctx);
		return null;
	}

	@Override public info visitVar2(aParser.Var2Context ctx)
	{

		property p = new property();
		p.sort = 1;
		p.v.name = Name.getSymbolName(ctx.ID().getText());
		String t = ctx.type().getText();
		p.v.dim = 0;
		for (int i = t.length() - 1; i >= 0; i = i - 2)
		{
			if (t.charAt(i) == ']') p.v.dim ++;
			else
			{
				p.v.classname = Name.getSymbolName(t.substring(0,i + 1));
				break;
			}
		}

		int tp = IR.instrlist.size();

		int left = register.reglist.size();


		info inf = visit(ctx.expr());
		int right = register.reglist.size();
		if (infunc == 0) {
			for (int i = left; i < right; ++i) {
				register.reglist.get(i).offset = 4 * (i - left) + IR.mainregnum * 4;
			}
			IR.mainregnum += (right - left);
		}

		register r = new register();
		r.No = register.reglist.size();
		register.reglist.add(r);
		if (infunc == 0)
		{
			r.global = register.globallist.size();
			register.globallist.add(r);
		}
		if (inf.newexpr == 1)
		{

			if (p.v.dim == 0)
			{
				Name tt = p.v.classname;
				property pp = sbtb.get(tt);

					instr inst = new instr();
					inst.op = "alloc";
					inst.asize = new src();
					inst.asize.sort = 0;
					inst.asize.c = pp.c.num;
					inst.dest = r;
					IR.instrlist.add(inst);

				p.v.reg = r;
			}
			else
			{
					instr inst = new instr();
					inst.op = "alloc";
					inst.asize = new src();
					inst.asize = inf.s;

					inst.dest = r;
					IR.instrlist.add(inst);
			}
			p.v.reg = r;
		}
		else
		{
			if (inf.exprtype.equals("string"))
			{


				instr inst = new instr();
				inst.op = "move";
				inst.src1 = new src();
				inst.src1.r = inf.s.r;
				inst.src1.sort = 1;
				inst.dest = r;
				IR.instrlist.add(inst);

				p.v.reg = inf.s.r;

			}
			else
			{

				instr inst = new instr();
				inst.op = "move";
				inst.dest = r;
				inst.src1 = inf.s;
				IR.instrlist.add(inst);

				p.v.reg = r;
			}
		}
		if (infunc == 0)
		{
			while (IR.instrlist.size() > tp)
			{
				r.glbinstrlist.add(IR.instrlist.get(tp));
				IR.instrlist.remove(tp);
			}
		}
		sbtb.put(p.v.name, p);
		return null;
	}



	@Override public info visitType5(aParser.Type5Context ctx)
	{
		return visitChildren(ctx);
	}

	@Override public info visitType4(aParser.Type4Context ctx) { return visitChildren(ctx); }

	@Override public info visitType3(aParser.Type3Context ctx) { return visitChildren(ctx); }

	@Override public info visitType2(aParser.Type2Context ctx) { return visitChildren(ctx); }

	@Override public info visitType1(aParser.Type1Context ctx) { return visitChildren(ctx); }

	@Override public info visitStmt1(aParser.Stmt1Context ctx) { return visitChildren(ctx); }

	@Override public info visitStmt2(aParser.Stmt2Context ctx) { return visitChildren(ctx); }

	@Override public info visitStmt3(aParser.Stmt3Context ctx) { return visitChildren(ctx); }

	//if
	@Override public info visitStmt4(aParser.Stmt4Context ctx)
	{
		info inf = visit(ctx.expr());

		label l1 = new label();
		l1.No = label.lablist.size();
		l1.name = "true" + String.valueOf(l1.No);
		label.lablist.add(l1);
		label l2 = new label();
		l2.No = label.lablist.size();
		l2.name = "out" + String.valueOf(l2.No);
		label.lablist.add(l2);

		instr inst = new instr();
		inst.op = "br";
		inst.src1 = inf.s;
		inst.l1 = l1;
		inst.l2 = l2;
		IR.instrlist.add(inst);

		sbtb.beginscope();

		inst = new instr();
		inst.op = "label";
		inst.l1 = l1;
		IR.instrlist.add(inst);

		visit(ctx.stmt());

		inst = new instr();
		inst.op = "jump";
		inst.l1 = l2;
		IR.instrlist.add(inst);

		sbtb.endscope();
		inst = new instr();
		inst.op = "label";
		inst.l1 = l2;
		IR.instrlist.add(inst);
		return null;
	}

	//if else
	@Override public info visitStmt5(aParser.Stmt5Context ctx)
	{
		info inf = visit(ctx.expr());
		label l1 = new label();
		l1.No = label.lablist.size();
		l1.name = "true" + String.valueOf(l1.No);
		label.lablist.add(l1);
		label l2 = new label();
		l2.No = label.lablist.size();
		l2.name = "false" + String.valueOf(l2.No);
		label.lablist.add(l2);
		label l3 = new label();
		l3.No = label.lablist.size();
		l3.name = "out" + String.valueOf(l3.No);
		label.lablist.add(l3);

		instr inst = new instr();
		inst.op = "br";
		inst.src1 = inf.s;
		inst.l1 = l1;
		inst.l2 = l2;
		IR.instrlist.add(inst);

		sbtb.beginscope();

		inst = new instr();
		inst.op = "label";
		inst.l1 = l1;
		IR.instrlist.add(inst);

		visit(ctx.stmt(0));

		inst = new instr();
		inst.op = "jump";
		inst.l1 = l3;
		IR.instrlist.add(inst);

		sbtb.endscope();

		sbtb.beginscope();

		inst = new instr();
		inst.op = "label";
		inst.l1 = l2;
		IR.instrlist.add(inst);

		visit(ctx.stmt(1));

		inst = new instr();
		inst.op = "jump";
		inst.l1 = l3;
		IR.instrlist.add(inst);

		sbtb.endscope();

		inst = new instr();
		inst.op = "label";
		inst.l1 = l3;
		IR.instrlist.add(inst);
		return null;
	}
	//return
	@Override public info visitStmt6(aParser.Stmt6Context ctx)
	{
		info inf = new info();
		if (ctx.expr()!= null)
		{
			inf = visit(ctx.expr());

			instr inst = new instr();
			inst.src1 = inf.s;
			inst.op = "ret";
			IR.instrlist.add(inst);
		}
		else
		{
			instr inst = new instr();
			inst.op = "retnull";
			IR.instrlist.add(inst);
		}
		return null;

	}

	//break;
	@Override public info visitStmt7(aParser.Stmt7Context ctx)
	{
		visitChildren(ctx);
		instr inst = new instr();
		inst.op = "jump";
		inst.l1 = forlist.get(forlist.size() - 1).l4;
		IR.instrlist.add(inst);
		return null;
	}

	//continue;
	@Override public info visitStmt8(aParser.Stmt8Context ctx)
	{

		visitChildren(ctx);
		instr inst = new instr();
		inst.op = "jump";
		inst.l1 = forlist.get(forlist.size() - 1).l3;
		IR.instrlist.add(inst);
		return null;
	}

	//while
	@Override public info visitStmt9(aParser.Stmt9Context ctx)
	{

		label l1 = new label();
		l1.No = label.lablist.size();
		l1.name = "cond" + String.valueOf(l1.No);
		label.lablist.add(l1);
		label l2 = new label();
		l2.No = label.lablist.size();
		l2.name = "loop" + String.valueOf(l2.No);
		label.lablist.add(l2);
		label l3 = new label();
		l3.No = label.lablist.size();
		l3.name = "out" + String.valueOf(l3.No);
		label.lablist.add(l3);

		forlabels fl = new forlabels();
		fl.l1 = l1;
		fl.l3 = l1;
		fl.l2 = l2;
		fl.l4 = l3;
		forlist.add(fl);

		instr inst = new instr();
		inst.op = "jump";
		inst.l1 = l1;
		IR.instrlist.add(inst);

		inst = new instr();
		inst.op = "label";
		inst.l1 = l1;
		IR.instrlist.add(inst);


			info inf = visit(ctx.expr());
			inst = new instr();
			inst.op = "br";
			inst.src1 = inf.s;
			inst.l1 = l2;
			inst.l2 = l3;
			IR.instrlist.add(inst);


		sbtb.beginscope();

		inst = new instr();
		inst.op = "label";
		inst.l1 = l2;
		IR.instrlist.add(inst);

		visit(ctx.stmt());

		inst = new instr();
		inst.op = "jump";
		inst.l1 = l1;
		IR.instrlist.add(inst);

		sbtb.endscope();
		inst = new instr();
		inst.op = "label";
		inst.l1 = l3;
		IR.instrlist.add(inst);

		forlist.remove(forlist.size() - 1);
		return null;
	}

	//for
	@Override public info visitStmt10(aParser.Stmt10Context ctx)
	{
		info inf = new info();

		label l1 = new label();
		l1.No = label.lablist.size();
		l1.name = "cond" + String.valueOf(l1.No);
		label.lablist.add(l1);
		label l2 = new label();
		l2.No = label.lablist.size();
		l2.name = "loop" + String.valueOf(l2.No);
		label.lablist.add(l2);
		label l3 = new label();
		l3.No = label.lablist.size();
		l3.name = "step" + String.valueOf(l3.No);
		label.lablist.add(l3);
		label l4 = new label();
		l4.No = label.lablist.size();
		l4.name = "out" + String.valueOf(l4.No);
		label.lablist.add(l4);

		forlabels fl = new forlabels();
		fl.l1 = l1;
		fl.l2 = l2;
		fl.l3 = l3;
		fl.l4 = l4;
		forlist.add(fl);

		if (ctx.aa != null)
		{
			visit(ctx.aa);
		}
		instr inst = new instr();
		inst.op = "jump";
		inst.l1 = l1;
		IR.instrlist.add(inst);

		inst = new instr();
		inst.op = "label";
		inst.l1 = l1;
		IR.instrlist.add(inst);
		if (ctx.bb != null)
		{
			inf = visit(ctx.bb);
			inst = new instr();
			inst.op = "br";
			inst.src1 = inf.s;
			inst.l1 = l2;
			inst.l2 = l4;
			IR.instrlist.add(inst);
		}
		else
		{
			inst = new instr();
			inst.op = "jump";
			inst.l1 = l2;
			IR.instrlist.add(inst);
		}

		inst = new instr();
		inst.op = "label";
		inst.l1 = l2;
		IR.instrlist.add(inst);
		sbtb.beginscope();

		visit(ctx.stmt());

		sbtb.endscope();

		inst = new instr();
		inst.op = "jump";
		inst.l1 = l3;
		IR.instrlist.add(inst);

		inst = new instr();
		inst.op = "label";
		inst.l1 = l3;
		IR.instrlist.add(inst);

		if (ctx.cc != null)
		{
			visit(ctx.cc);
		}

		inst = new instr();
		inst.op = "jump";
		inst.l1 = l1;
		IR.instrlist.add(inst);

		inst = new instr();
		inst.op = "label";
		inst.l1 = l4;
		IR.instrlist.add(inst);

		forlist.remove(forlist.size() - 1);

		return null;
	}
	//ID
	@Override public info visitPrimary1(aParser.Primary1Context ctx)
	{
		info inf = new info();
		inf.lvalue = 1;
		property p = new property();
		p = sbtb.get(Name.getSymbolName(ctx.ID().getText()));

		if (p.sort == 1)
		{
			inf.exprtype = p.v.classname.toString();
			inf.dim = p.v.dim;
			inf.func = 0;
			inf.s = new src();
			inf.s.sort = 1;
			inf.s.r = p.v.reg;
		}
		if (p.sort == 2)
		{
			inf.lvalue = 0;
			inf.funcname = p.f.name.toString();
			inf.exprtype = p.f.typereturn.toString();
			inf.dim = p.f.dim;
			inf.func = 1;
		}
		return inf;
	}
	//constant
	@Override public info visitPrimary2(aParser.Primary2Context ctx)
	{
		info inf = visit(ctx.constant());

		inf.lvalue = 0;
		inf.dim = 0;
		inf.func = 0;
		return inf;
	}
	//(expr)
	@Override public info visitPrimary3(aParser.Primary3Context ctx)
	{
		info inf = visit(ctx.expr());
		inf.lvalue = 0;
		inf.func = 0;
		return inf;
	}

	//postfix '++'
	@Override public info visitPost4(aParser.Post4Context ctx)
	{
		info inf = new info();
		info v = visit(ctx.postfix());

		instr inst = new instr();
		inst.op = "move";
		inst.src1.sort = 1;
		inst.src1.r = v.s.r;
		inst.dest = new register();
		inst.dest.No = register.reglist.size();
		register.reglist.add(inst.dest);
		IR.instrlist.add(inst);

		inf.s.sort = 1;
		inf.s.r = inst.dest;

		inst = new instr();
		inst.op = "add";
		inst.src1.sort = 1;
		inst.src1.r = v.s.r;
		inst.src2.sort = 0;
		inst.src2.c = 1;
		inst.dest = inst.src1.r;
		IR.instrlist.add(inst);

		if (v.s.r.inmemory == 1)
		{
			instr in = new instr();
			in.op = "store";
			in.src1 = new src();
			in.src1.sort = 1;
			in.src1.r = inst.dest;
			in.size = 4;
			in.offset = 0;
			in.addr = v.s.r.addr;
			IR.instrlist.add(in);
		}

		inf.lvalue = 0;
		inf.exprtype = "int";
		inf.dim = 0;
		inf.func = 0;
		return inf;
	}
	//postfix '--'
	@Override public info visitPost5(aParser.Post5Context ctx)
	{
		info inf = new info();
		info v = visit(ctx.postfix());

		instr inst = new instr();

		inst.op = "move";
		inst.src1.sort = 1;
		inst.src1.r = v.s.r;
		inst.dest = new register();
		inst.dest.No = register.reglist.size();
		register.reglist.add(inst.dest);
		IR.instrlist.add(inst);

		inf.s.sort = 1;
		inf.s.r = inst.dest;

		inst = new instr();
		inst.op = "sub";
		inst.src1.sort = 1;
		inst.src1.r = v.s.r;
		inst.src2.sort = 0;
		inst.src2.c = 1;
		inst.dest = inst.src1.r;
		IR.instrlist.add(inst);

		if (v.s.r.inmemory == 1)
		{
			instr in = new instr();
			in.op = "store";
			in.src1 = new src();
			in.src1.sort = 1;
			in.src1.r = inst.dest;
			in.size = 4;
			in.offset = 0;
			in.addr = v.s.r.addr;
			IR.instrlist.add(in);
		}

		inf.lvalue = 0;
		inf.exprtype = "int";
		inf.dim = 0;
		inf.func = 0;
		return inf;
	}

	//postfix '[' expr ']'
	@Override public info visitPost2(aParser.Post2Context ctx)
	{
		info inf = new info();
		info v = visit(ctx.postfix());

		inf.lvalue = v.lvalue;
		inf.exprtype = v.exprtype;

		inf.dim = v.dim - 1;
		inf.func = 0;
		info e = visit(ctx.expr());
		inf.s = new src();
		inf.s.sort = 1;

		register r1 = new register();
		r1.No = register.reglist.size();
		register.reglist.add(r1);
		register r2 = new register();
		r2.No = register.reglist.size();
		register.reglist.add(r2);
		register r3 = new register();
		r3.No = register.reglist.size();
		register.reglist.add(r3);


		instr inst = new instr();
		inst.op = "mul";
		inst.src1 = new src();
		inst.src1.sort = 0;
		inst.src1.c = 4;
		inst.dest = r1;
		inst.src2 = e.s;
		IR.instrlist.add(inst);

		inst = new instr();
		inst.op = "add";
		inst.src1 = v.s;
		inst.dest = r2;
		inst.src2 = new src();
		inst.src2.sort = 1;
		inst.src2.r = r1;
		IR.instrlist.add(inst);

		inst = new instr();
		inst.dest = r3;
		inst.dest.inmemory = 1;
		inst.op = "load";
		inst.size = 4;
		inst.addr = new src();
		inst.addr.sort = 1;
		inst.addr.r = r2;
		inst.dest.addr = inst.addr;
		inst.offset = 0;
		IR.instrlist.add(inst);

		inf.s.r = inst.dest;
		return inf;
	}

	//postfix '.' ID
	@Override public info visitPost3(aParser.Post3Context ctx)
	{
		info inf = new info();
		info v = visit(ctx.postfix());

		Name n = Name.getSymbolName(ctx.ID().getText());

		if (v.exprtype.equals("string") && v.dim == 0)
		{
			Name str = Name.getSymbolName("string");

                inf.funcname = n.toString();
                inf.func = 1;
                inf.exprtype = sbtb.get(str).c.list.get(n).f.typereturn.toString();
                inf.stringmethod = 1;
                inf.dim = sbtb.get(str).c.list.get(n).f.dim;
                inf.lvalue = 0;
                if (v.s.r.isstring == 1)
                {
                    instr inst = new instr();
                    inst.dest = new register();
                    inst.dest.No = register.reglist.size();
                    register.reglist.add(inst.dest);
                    inst.op = "move";
                    inst.src1 = v.s;
                    IR.instrlist.add(inst);

                    inf.s = new src();
                    inf.s.sort = 1;
                    inf.s.r = inst.dest;
                }
				else
                {
                    inf.s = v.s;
                }

		}
		else
		{
			if (v.dim > 0 && n.toString().equals("size"))
			{
				inf.funcname = "size";
				inf.func = 1;
				inf.exprtype = "int";
				inf.arraysize = 1;
				inf.dim = 0;
				inf.lvalue = 0;

				inf.s = v.s;
			}
			else
			{
				Name vn = Name.getSymbolName(v.exprtype);

					property p = sbtb.get(vn).c.list.get(n);
					property pc = sbtb.get(vn);
					inf.exprtype = p.v.classname.toString();

					inf.func = 0;
					inf.dim = sbtb.get(vn).c.list.get(n).v.dim;
					inf.lvalue = v.lvalue;

					instr inst = new instr();
					register r1 = new register();
					r1.No = register.reglist.size();
					register.reglist.add(r1);
					register r2 = new register();
					r2.No = register.reglist.size();
					register.reglist.add(r2);
					register r3 = new register();
					r3.No = register.reglist.size();
					register.reglist.add(r3);

					inst = new instr();
					inst.op = "mul";
					inst.src1 = new src();
					inst.src1.sort = 0;
					inst.src1.c = 4;
					inst.dest = r1;
					inst.src2 = new src();
					inst.src2.sort = 0;
					inst.src2.c = p.v.memberNo;
					IR.instrlist.add(inst);

					inst = new instr();
					inst.op = "add";
					inst.src1 = v.s;
					inst.dest = r2;
					inst.src2 = new src();
					inst.src2.sort = 1;
					inst.src2.r = r1;
					IR.instrlist.add(inst);

					inst = new instr();
					inst.op = "load";
					inst.dest = r3;
					inst.dest.inmemory = 1;
					inst.size = 4;
					inst.addr = new src();
					inst.addr.sort = 1;
					inst.addr.r = r2;
					inst.dest.addr = inst.addr;
					inst.offset = 0;
					IR.instrlist.add(inst);

					inf.s.sort = 1;
					inf.s.r = inst.dest;

			}
		}

		return inf;
	}

	//postfix '(' param_list ')'
	@Override public info visitPost6(aParser.Post6Context ctx)
	{
		info inf = new info();

		info v = visit(ctx.postfix());

		property f = new property();
		if (v.stringmethod == 1)
		{
			property p = sbtb.get(Name.getSymbolName("string"));
			f = p.c.list.get(Name.getSymbolName(v.funcname));
		}
		else
		{
			if (v.arraysize == 1)
			{
				f.sort = 2;
				f.f.name = Name.getSymbolName("size");
				f.f.typereturn = Name.getSymbolName("int");
			}
			else
			{
				f = sbtb.get(Name.getSymbolName(v.funcname));
			}
		}

		inf.func = 0;
		inf.stringmethod = 0;
		inf.arraysize = 0;
		inf.dim = v.dim;
		inf.exprtype = v.exprtype;
		inf.lvalue = 0;
		if (ctx.param_list() != null)
		{
			inf.oplist = visit(ctx.param_list()).oplist;
		}
		else
		{
			inf.oplist.clear();
		}

		int len = inf.oplist.size();
		instr inst = new instr();
		inst.op = "dcall";
		inst.funcname = v.funcname;
		if (inst.funcname.equals("print")
				|| inst.funcname.equals("println")
				|| inst.funcname.equals("getString")
				|| inst.funcname.equals("getInt")
				|| inst.funcname.equals("toString")
				|| inst.funcname.equals("move"))
		{
			inst.funcname = "func__" + inst.funcname;
		}
		if (v.stringmethod == 1)
		{
			inst.funcname = "func__string." + inst.funcname;
		}
		if (v.arraysize == 1)
		{
			inst.funcname = "func__array." + inst.funcname;
		}

		inst.dest = new register();
		inst.dest.No = register.reglist.size();
		register.reglist.add(inst.dest);
		if (v.stringmethod == 1)
		{
			inst.oplist.add(v.s);
		}

		if (v.arraysize == 1)
		{
			inst.oplist.add(v.s);
		}

		for (int i = 0; i <= len - 1; ++i)
		{
			inst.oplist.add(inf.oplist.get(len - i - 1));
		}
		if (infunc == 1)
		{
			if (inst.oplist.size() > ins.maxarg) ins.maxarg = inst.oplist.size();
		}
		else
		{
			if (inst.oplist.size() > IR.mainmaxarg) IR.mainmaxarg = inst.oplist.size();
		}
		IR.instrlist.add(inst);
		inf.s.sort = 1;
		inf.s.r = inst.dest;
		return inf;
	}

	@Override public info visitPl1(aParser.Pl1Context ctx)
	{
		info inf = new info();
		info v = visit(ctx.expr());

		inf.oplist.add(v.s);

		return inf;
	}

	@Override public info visitPl2(aParser.Pl2Context ctx)
	{
		info inf = new info();
		info v = visit(ctx.param_list());
		inf.oplist = v.oplist;

		v = visit(ctx.expr());

		inf.oplist.add(v.s);
		return inf;
	}

	//primary
	@Override public info visitPost1(aParser.Post1Context ctx)
	{

		return visit(ctx.primary());
	}

	//postfix
	@Override public info visitUnary1(aParser.Unary1Context ctx)
	{
		return visit(ctx.postfix());
	}

	//++
	@Override public info visitUnary2(aParser.Unary2Context ctx)
	{
		info f = visit(ctx.unary());
		info inf= new info();

		instr inst = new instr();
		inst.op = "add";
		inst.src1 = f.s;
		inst.src2 = new src();
		inst.src2.sort = 0;
		inst.src2.c = 1;
		inst.dest = inst.src1.r;
		IR.instrlist.add(inst);

		if (f.s.r.inmemory == 1)
		{
			instr in = new instr();
			in.op = "store";
			in.src1 = new src();
			in.src1.sort = 1;
			in.src1.r = inst.dest;
			in.size = 4;
			in.offset = 0;
			in.addr = f.s.r.addr;
			IR.instrlist.add(in);

		}
		inf.s = new src();
		inf.s.sort = 1;
		inf.s.r = inst.dest;

		inf.lvalue = 0;
        inf.exprtype = "int";
        inf.dim = 0;
		return inf;
	}

	//--
	@Override public info visitUnary3(aParser.Unary3Context ctx)
	{
		info f = visit(ctx.unary());
		info inf= new info();

		instr inst = new instr();
		inst.op = "sub";
		inst.src1 = f.s;
		inst.src2 = new src();
		inst.src2.sort = 0;
		inst.src2.c = 1;
		inst.dest = inst.src1.r;
		IR.instrlist.add(inst);

		if (f.s.r.inmemory == 1)
		{
			instr in = new instr();
			in.op = "store";
			in.src1 = new src();
			in.src1.sort = 1;
			in.src1.r = inst.dest;
			in.size = 4;
			in.offset = 0;
			in.addr = f.s.r.addr;
			IR.instrlist.add(in);

		}
		inf.s = new src();
		inf.s.sort = 1;
		inf.s.r = inst.dest;

		inf.lvalue = 0;
        inf.exprtype = "int";
        inf.dim = 0;
		return inf;
	}
	@Override public info visitUnary4(aParser.Unary4Context ctx)
	{
		info inf = visit(ctx.unary());

		instr inst = new instr();
		inst.op = "not";
		inst.src1 = inf.s;
		inst.dest = new register();
		inst.dest.No = register.reglist.size();
		register.reglist.add(inst.dest);
		IR.instrlist.add(inst);

		inf.s = new src();
		inf.s.sort = 1;
		inf.s.r = inst.dest;

		inf.lvalue = 0;
		return inf;
	}
	//unaryOperator unary
	@Override public info visitUnary5(aParser.Unary5Context ctx)
	{
		info inf = visit(ctx.unary());
		inf.lvalue = 0;
		String o = ctx.unaryOperator().getText();
		if (o.equals("!"))
		{
			instr inst = new instr();
			inst.op = "not";
			inst.src1 = inf.s;
			inst.dest = new register();
			inst.dest.No = register.reglist.size();
			register.reglist.add(inst.dest);
			IR.instrlist.add(inst);

			inf.s = new src();
			inf.s.sort = 1;
			inf.s.r = inst.dest;
		}
		if (o.equals("-"))
		{
			instr inst = new instr();
			inst.op = "neg";
			inst.src1 = inf.s;
			inst.dest = new register();
			inst.dest.No = register.reglist.size();
			register.reglist.add(inst.dest);
			IR.instrlist.add(inst);

			inf.s = new src();
			inf.s.sort = 1;
			inf.s.r = inst.dest;
		}


		return inf;
	}

	@Override public info visitUnaryOperator(aParser.UnaryOperatorContext ctx)
	{
		return visitChildren(ctx);
	}

	@Override public info visitMe1(aParser.Me1Context ctx)
	{
		return visit(ctx.unary());
	}

	@Override public info visitMe3(aParser.Me3Context ctx)
	{
		info l = visit(ctx.multiplicativeExpression());
		info r = visit(ctx.unary());
		info inf = new info();

		instr inst = new instr();
		inst.op = "div";
		inst.src1 = l.s;
		inst.src2 = r.s;
		inst.dest = new register();
		inst.dest.No = register.reglist.size();
		register.reglist.add(inst.dest);
		IR.instrlist.add(inst);

		inf.s.sort = 1;
		inf.s.r = inst.dest;

		inf.lvalue = 0;
        inf.exprtype = "int";
        inf.dim = 0;
		return inf;
	}

	@Override public info visitMe2(aParser.Me2Context ctx)
	{
		info l = visit(ctx.multiplicativeExpression());
		info r = visit(ctx.unary());
		info inf = new info();

		instr inst = new instr();
		inst.op = "mul";
		inst.src1 = l.s;
		inst.src2 = r.s;
		inst.dest = new register();
		inst.dest.No = register.reglist.size();
		register.reglist.add(inst.dest);
		IR.instrlist.add(inst);

		inf.s.sort = 1;
		inf.s.r = inst.dest;

		inf.lvalue = 0;
        inf.exprtype = "int";
        inf.dim = 0;
		return inf;
	}

	//%
	@Override public info visitMe4(aParser.Me4Context ctx)
	{
		info l = visit(ctx.multiplicativeExpression());
		info r = visit(ctx.unary());
		info inf = new info();

		instr inst = new instr();
		inst.op = "rem";
		inst.src1 = l.s;
		inst.src2 = r.s;
		inst.dest = new register();
		inst.dest.No = register.reglist.size();
		register.reglist.add(inst.dest);
		IR.instrlist.add(inst);

		inf.s.sort = 1;
		inf.s.r = inst.dest;

		inf.lvalue = 0;
        inf.exprtype = "int";
        inf.dim = 0;
		return inf;
	}

	@Override public info visitAdd2(aParser.Add2Context ctx)
	{
		info inf = new info();
		info l = visit(ctx.additiveExpression());
		info r = visit(ctx.multiplicativeExpression());

		if (l.exprtype.equals("string"))
		{
			instr inst = new instr();
			inst.op = "dcall";
			inst.oplist.add(l.s);
			inst.oplist.add(r.s);
			inst.funcname = "func__stringConcatenate";
			inst.dest = new register();
			inst.dest.No = register.reglist.size();
			register.reglist.add(inst.dest);
			IR.instrlist.add(inst);
			if (infunc == 1)
			{
				if (2 > ins.maxarg) ins.maxarg = 2;
			}
			else
			{
				if (2 > IR.mainmaxarg) IR.mainmaxarg = 2;
			}

			inf.s.sort = 1;
			inf.s.r = inst.dest;
		}
		else
		{
			instr inst = new instr();
			inst.op = "add";
			inst.src1 = l.s;
			inst.src2 = r.s;
			inst.dest = new register();
			inst.dest.No = register.reglist.size();
			register.reglist.add(inst.dest);
			IR.instrlist.add(inst);

			inf.s.sort = 1;
			inf.s.r = inst.dest;
		}
		inf.lvalue = 0;
        inf.exprtype = l.exprtype;
        inf.dim = 0;
		return inf;
	}

	@Override public info visitAdd1(aParser.Add1Context ctx)
	{
		return visit(ctx.multiplicativeExpression());
	}

	@Override public info visitAdd3(aParser.Add3Context ctx)
	{
		info inf = new info();
		info l = visit(ctx.additiveExpression());
		info r = visit(ctx.multiplicativeExpression());

		instr inst = new instr();
		inst.op = "sub";
		inst.src1 = l.s;
		inst.src2 = r.s;
		inst.dest = new register();
		inst.dest.No = register.reglist.size();
		register.reglist.add(inst.dest);
		IR.instrlist.add(inst);

		inf.s.sort = 1;
		inf.s.r = inst.dest;

		inf.lvalue = 0;
        inf.exprtype = l.exprtype;
        inf.dim = 0;
		return inf;
	}

	@Override public info visitShift1(aParser.Shift1Context ctx)
	{
		return visit(ctx.additiveExpression());
	}

	@Override public info visitShift2(aParser.Shift2Context ctx)
	{
		info inf = new info();
		info l = visit(ctx.shiftExpression());
		info r = visit(ctx.additiveExpression());

		instr inst = new instr();
		inst.op = "shl";
		inst.src1 = l.s;
		inst.src2 = r.s;
		inst.dest = new register();
		inst.dest.No = register.reglist.size();
		register.reglist.add(inst.dest);
		IR.instrlist.add(inst);

		inf.s.sort = 1;
		inf.s.r = inst.dest;

		inf.lvalue = 0;
        inf.exprtype = l.exprtype;
        inf.dim = 0;
		return inf;
	}

	@Override public info visitShift3(aParser.Shift3Context ctx) {
		info inf = new info();
		info l = visit(ctx.shiftExpression());
		info r = visit(ctx.additiveExpression());

		instr inst = new instr();
		inst.op = "shr";
		inst.src1 = l.s;
		inst.src2 = r.s;
		inst.dest = new register();
		inst.dest.No = register.reglist.size();
		register.reglist.add(inst.dest);
		IR.instrlist.add(inst);

		inf.s.sort = 1;
		inf.s.r = inst.dest;

		inf.lvalue = 0;
        inf.exprtype = l.exprtype;
        inf.dim = 0;
		return inf;
	}
	//>=
	@Override public info visitRelation5(aParser.Relation5Context ctx)
	{
		info inf = new info();
		info l = visit(ctx.relationalExpression());
		info r = visit(ctx.shiftExpression());

		if ( l.exprtype.equals("string"))
		{
			instr inst = new instr();
			inst.op = "dcall";
			inst.oplist.add(l.s);
			inst.oplist.add(r.s);
			inst.funcname = "func__stringGeq";
			inst.dest = new register();
			inst.dest.No = register.reglist.size();
			register.reglist.add(inst.dest);
			IR.instrlist.add(inst);
			if (infunc == 1)
			{
				if (2 > ins.maxarg) ins.maxarg = 2;
			}
			else
			{
				if (2 > IR.mainmaxarg) IR.mainmaxarg = 2;
			}

			inf.s.sort = 1;
			inf.s.r = inst.dest;
		}
		else
		{

			instr inst = new instr();
			inst.op = "sge";
			inst.src1 = l.s;
			inst.src2 = r.s;
			inst.dest = new register();
			inst.dest.No = register.reglist.size();
			register.reglist.add(inst.dest);
			IR.instrlist.add(inst);

			inf.s.sort = 1;
			inf.s.r = inst.dest;
		}
		inf.exprtype = "bool";
		inf.dim = 0;
		inf.lvalue = 0;
		return inf;
	}
    //<=
	@Override public info visitRelation4(aParser.Relation4Context ctx)
	{
		info inf = new info();
		info l = visit(ctx.relationalExpression());
		info r = visit(ctx.shiftExpression());

		if ( l.exprtype.equals("string"))
		{
			instr inst = new instr();
			inst.op = "dcall";
			inst.oplist.add(l.s);
			inst.oplist.add(r.s);
			inst.funcname = "func__stringLeq";
			inst.dest = new register();
			inst.dest.No = register.reglist.size();
			register.reglist.add(inst.dest);
			IR.instrlist.add(inst);
			if (infunc == 1)
			{
				if (2 > ins.maxarg) ins.maxarg = 2;
			}
			else
			{
				if (2 > IR.mainmaxarg) IR.mainmaxarg = 2;
			}

			inf.s.sort = 1;
			inf.s.r = inst.dest;
		}
		else
		{
			instr inst = new instr();
			inst.op = "sle";
			inst.src1 = l.s;
			inst.src2 = r.s;
			inst.dest = new register();
			inst.dest.No = register.reglist.size();
			register.reglist.add(inst.dest);
			IR.instrlist.add(inst);

			inf.s.sort = 1;
			inf.s.r = inst.dest;
		}
		inf.exprtype = "bool";
		inf.dim = 0;
		inf.lvalue = 0;
		return inf;
	}
	//>
	@Override public info visitRelation3(aParser.Relation3Context ctx)
	{
		info inf = new info();
		info l = visit(ctx.relationalExpression());
		info r = visit(ctx.shiftExpression());

		if ( l.exprtype.equals("string"))
		{
			instr inst = new instr();
			inst.op = "dcall";
			inst.oplist.add(l.s);
			inst.oplist.add(r.s);
			inst.funcname = "func__stringLarge";
			inst.dest = new register();
			inst.dest.No = register.reglist.size();
			register.reglist.add(inst.dest);
			IR.instrlist.add(inst);
			if (infunc == 1)
			{
				if (2 > ins.maxarg) ins.maxarg = 2;
			}
			else
			{
				if (2 > IR.mainmaxarg) IR.mainmaxarg = 2;
			}

			inf.s.sort = 1;
			inf.s.r = inst.dest;
		}
		else
		{
			instr inst = new instr();
			inst.op = "sgt";
			inst.src1 = l.s;
			inst.src2 = r.s;
			inst.dest = new register();
			inst.dest.No = register.reglist.size();
			register.reglist.add(inst.dest);
			IR.instrlist.add(inst);

			inf.s.sort = 1;
			inf.s.r = inst.dest;
		}
		inf.exprtype = "bool";
		inf.dim = 0;
		inf.lvalue = 0;
		return inf;
	}
	//<
	@Override public info visitRelation2(aParser.Relation2Context ctx)
	{
		info inf = new info();
		info l = visit(ctx.relationalExpression());
		info r = visit(ctx.shiftExpression());


		if (l.exprtype.equals("string"))
		{
			instr inst = new instr();
			inst.op = "dcall";
			inst.oplist.add(l.s);
			inst.oplist.add(r.s);
			inst.funcname = "func__stringLess";
			inst.dest = new register();
			inst.dest.No = register.reglist.size();
			register.reglist.add(inst.dest);
			IR.instrlist.add(inst);
			if (infunc == 1)
			{
				if (2 > ins.maxarg) ins.maxarg = 2;
			}
			else
			{
				if (2 > IR.mainmaxarg) IR.mainmaxarg = 2;
			}

			inf.s.sort = 1;
			inf.s.r = inst.dest;
		}
		else
		{
			instr inst = new instr();
			inst.op = "slt";
			inst.src1 = l.s;
			inst.src2 = r.s;
            inst.dest = new register();
			inst.dest.No = register.reglist.size();
			register.reglist.add(inst.dest);
			IR.instrlist.add(inst);

			inf.s.sort = 1;
			inf.s.r = inst.dest;
		}
		inf.exprtype = "bool";
		inf.dim = 0;
		inf.lvalue = 0;
		return inf;
	}

	@Override public info visitRelation1(aParser.Relation1Context ctx)
	{
		return visit(ctx.shiftExpression());
	}

	@Override public info visitEqual1(aParser.Equal1Context ctx)
	{
		return visit(ctx.relationalExpression());
	}

	@Override public info visitEqual2(aParser.Equal2Context ctx)
	{
		info inf = new info();
		info l = visit(ctx.equalityExpression());
		info r = visit(ctx.relationalExpression());

		if ( l.exprtype.equals("string"))
		{
			instr inst = new instr();
			inst.op = "dcall";
			inst.oplist.add(l.s);
			inst.oplist.add(r.s);
			inst.funcname = "func__stringIsEqual";
			inst.dest = new register();
			inst.dest.No = register.reglist.size();
			register.reglist.add(inst.dest);
			IR.instrlist.add(inst);
			if (infunc == 1)
			{
				if (2 > ins.maxarg) ins.maxarg = 2;
			}
			else
			{
				if (2 > IR.mainmaxarg) IR.mainmaxarg = 2;
			}

			inf.s.sort = 1;
			inf.s.r = inst.dest;
		}
		else
		{
			instr inst = new instr();
			inst.op = "seq";
			inst.src1 = l.s;
			inst.src2 = r.s;
			inst.dest = new register();
			inst.dest.No = register.reglist.size();
			register.reglist.add(inst.dest);
			IR.instrlist.add(inst);

			inf.s.sort = 1;
			inf.s.r = inst.dest;
		}

		inf.exprtype = "bool";
		inf.dim = 0;
		inf.lvalue = 0;
		return inf;
	}

	//!=
	@Override public info visitEqual3(aParser.Equal3Context ctx)
	{
		info inf = new info();
		info l = visit(ctx.equalityExpression());
		info r = visit(ctx.relationalExpression());
		if (l.exprtype.equals("string"))
		{
			instr inst = new instr();
			inst.op = "dcall";
			inst.oplist.add(l.s);
			inst.oplist.add(r.s);
			inst.funcname = "func__stringNeq";
			inst.dest = new register();
			inst.dest.No = register.reglist.size();
			register.reglist.add(inst.dest);
			IR.instrlist.add(inst);
			if (infunc == 1)
			{
				if (2 > ins.maxarg) ins.maxarg = 2;
			}
			else
			{
				if (2 > IR.mainmaxarg) IR.mainmaxarg = 2;
			}

			inf.s.sort = 1;
			inf.s.r = inst.dest;
		}
		else
		{
			instr inst = new instr();
			inst.op = "sne";
			inst.src1 = l.s;
			inst.src2 = r.s;
			inst.dest = new register();
			inst.dest.No = register.reglist.size();
			register.reglist.add(inst.dest);
			IR.instrlist.add(inst);

			inf.s.sort = 1;
			inf.s.r = inst.dest;
		}

		inf.exprtype = "bool";
		inf.dim = 0;
		inf.lvalue = 0;
		return inf;
	}

	@Override public info visitAnd2(aParser.And2Context ctx)
	{
		info inf = new info();
		info l = visit(ctx.andExpression());

		info r = visit(ctx.equalityExpression());

		instr inst = new instr();
		inst.op = "and";
		inst.src1 = l.s;
		inst.src2 = r.s;
		inst.dest = new register();
		inst.dest.No = register.reglist.size();
		register.reglist.add(inst.dest);
		IR.instrlist.add(inst);

		inf.s.sort = 1;
		inf.s.r = inst.dest;
		inf.lvalue = 0;
        inf.exprtype = l.exprtype;
        inf.dim = 0;
		return inf;
	}

	@Override public info visitAnd1(aParser.And1Context ctx)
	{
		return visit(ctx.equalityExpression());
	}

	@Override public info visitEor2(aParser.Eor2Context ctx)
	{
		info inf = new info();
		info l = visit(ctx.exclusiveOrExpression());
		info r = visit(ctx.andExpression());

		instr inst = new instr();
		inst.op = "xor";
		inst.src1 = l.s;
		inst.src2 = r.s;
		inst.dest = new register();
		inst.dest.No = register.reglist.size();
		register.reglist.add(inst.dest);
		IR.instrlist.add(inst);

		inf.s.sort = 1;
		inf.s.r = inst.dest;
		inf.lvalue = 0;
        inf.exprtype = l.exprtype;
        inf.dim = 0;
		return inf;
	}

	@Override public info visitEor1(aParser.Eor1Context ctx)
	{
		return visit(ctx.andExpression());
	}

	@Override public info visitIor1(aParser.Ior1Context ctx)
	{
		return visit(ctx.exclusiveOrExpression());
	}

	@Override public info visitIor2(aParser.Ior2Context ctx)
	{
		info inf = new info();
		info l = visit(ctx.inclusiveOrExpression());
		info r = visit(ctx.exclusiveOrExpression());
		instr inst = new instr();
		inst.op = "or";
		inst.src1 = l.s;
		inst.src2 = r.s;
		inst.dest = new register();
		inst.dest.No = register.reglist.size();
		register.reglist.add(inst.dest);
		IR.instrlist.add(inst);

		inf.s.sort = 1;
		inf.s.r = inst.dest;

		inf.lvalue = 0;
        inf.exprtype = l.exprtype;
        inf.dim = 0;
		return inf;
	}

	@Override public info visitLand2(aParser.Land2Context ctx)
	{
		info inf = new info();
		info l = visit(ctx.logicalAndExpression());
		label l1 = new label();
		l1.No = label.lablist.size();
		l1.name = "true" + String.valueOf(l1.No);
		label.lablist.add(l1);

		label l2 = new label();
		l2.No = label.lablist.size();
		l2.name = "false" + String.valueOf(l2.No);
		label.lablist.add(l2);

		label l3 = new label();
		l3.No = label.lablist.size();
		l3.name = "out" + String.valueOf(l3.No);
		label.lablist.add(l3);

		register r1 = new register();
		r1.No = register.reglist.size();
		register.reglist.add(r1);

		register r2 = new register();
		r2.No = register.reglist.size();
		register.reglist.add(r2);

		instr inst = new instr();
		inst.op = "seq";
		inst.src1 = l.s;
		inst.src2 = new src();
		inst.src2.sort = 0;
		inst.src2.c = 0;
		inst.dest = r1;
		IR.instrlist.add(inst);

		inst = new instr();
		inst.op = "br";
		inst.src1 = new src();
		inst.src1.sort = 1;
		inst.src1.r = r1;
		inst.l1 = l1;
		inst.l2 = l2;
		IR.instrlist.add(inst);

		inst = new instr();
		inst.op = "label";
		inst.l1 = l1;
		IR.instrlist.add(inst);

		inst = new instr();
		inst.op = "move";
		inst.src1 = l.s;
		inst.dest = r2;
		IR.instrlist.add(inst);

		inst = new instr();
		inst.op = "jump";
		inst.l1 = l3;
		IR.instrlist.add(inst);

		inst = new instr();
		inst.op = "label";
		inst.l1 = l2;
		IR.instrlist.add(inst);

		info r = visit(ctx.inclusiveOrExpression());

		inst = new instr();
		inst.op = "move";
		inst.src1 = r.s;
		inst.dest = r2;
		IR.instrlist.add(inst);

		inst = new instr();
		inst.op = "jump";
		inst.l1 = l3;
		IR.instrlist.add(inst);

		inst = new instr();
		inst.op = "label";
		inst.l1 = l3;
		IR.instrlist.add(inst);

		inf.s.sort = 1;
		inf.s.r = r2;

		inf.lvalue = 0;
        inf.exprtype = l.exprtype;
        inf.dim = 0;
		return inf;
	}

	@Override public info visitLand1(aParser.Land1Context ctx)
	{
		return visit(ctx.inclusiveOrExpression());
	}

	@Override public info visitLor1(aParser.Lor1Context ctx)
	{
		return visit(ctx.logicalAndExpression());
	}

	@Override public info visitLor2(aParser.Lor2Context ctx)
	{
		info inf = new info();
		info l = visit(ctx.logicalOrExpression());
		label l1 = new label();
		l1.No = label.lablist.size();
		l1.name = "true" + String.valueOf(l1.No);
		label.lablist.add(l1);

		label l2 = new label();
		l2.No = label.lablist.size();
		l2.name = "false" + String.valueOf(l2.No);
		label.lablist.add(l2);

		label l3 = new label();
		l3.No = label.lablist.size();
		l3.name = "out" + String.valueOf(l3.No);
		label.lablist.add(l3);

		register r1 = new register();
		r1.No = register.reglist.size();
		register.reglist.add(r1);

		register r2 = new register();
		r2.No = register.reglist.size();
		register.reglist.add(r2);

		instr inst = new instr();
		inst.op = "seq";
		inst.src1 = l.s;
		inst.src2 = new src();
		inst.src2.sort = 0;
		inst.src2.c = 1;
		inst.dest = r1;
		IR.instrlist.add(inst);

		inst = new instr();
		inst.op = "br";
		inst.src1 = new src();
		inst.src1.sort = 1;
		inst.src1.r = r1;
		inst.l1 = l1;
		inst.l2 = l2;
		IR.instrlist.add(inst);

		inst = new instr();
		inst.op = "label";
		inst.l1 = l1;
		IR.instrlist.add(inst);

		inst = new instr();
		inst.op = "move";
		inst.src1 = l.s;
		inst.dest = r2;
		IR.instrlist.add(inst);

		inst = new instr();
		inst.op = "jump";
		inst.l1 = l3;
		IR.instrlist.add(inst);

		inst = new instr();
		inst.op = "label";
		inst.l1 = l2;
		IR.instrlist.add(inst);

		info r = visit(ctx.logicalAndExpression());

		inst = new instr();
		inst.op = "move";
		inst.src1 = r.s;
		inst.dest = r2;
		IR.instrlist.add(inst);

		inst = new instr();
		inst.op = "jump";
		inst.l1 = l3;
		IR.instrlist.add(inst);

		inst = new instr();
		inst.op = "label";
		inst.l1 = l3;
		IR.instrlist.add(inst);

		inf.s.sort = 1;
		inf.s.r = r2;

		inf.lvalue = 0;
        inf.exprtype = l.exprtype;
        inf.dim = 0;
		return inf;
	}


	@Override public info visitAssign1(aParser.Assign1Context ctx)
	{
		return visit(ctx.logicalOrExpression());
	}

	@Override public info visitAssign2(aParser.Assign2Context ctx)
	{
		info inf = new info();
		info r = visit(ctx.expr());
		info l = visit(ctx.unary());

		if (r.newexpr == 1)
		{	if (l.dim == 0)
			{
				Name t = Name.getSymbolName(l.exprtype);
				property p = sbtb.get(t);
				instr inst = new instr();
				inst.op = "alloc";
				inst.asize = new src();
				inst.asize.sort = 0;
				inst.asize.c = p.c.num;
				inst.dest = l.s.r;
				IR.instrlist.add(inst);
			}
			else
			{

				src s = r.s;
				instr inst = new instr();
				inst.op = "alloc";
				inst.asize = new src();
				inst.asize = s;

				inst.dest = l.s.r;
				IR.instrlist.add(inst);
			}
            if (l.s.r.inmemory == 1)
            {
                instr inst = new instr();
                inst.op = "store";
                inst.src1 = l.s;
                inst.size = 4;
                inst.offset = 0;
                inst.addr = l.s.r.addr;
                IR.instrlist.add(inst);

            }
		}
		else
		{
			if (l.s.r.inmemory == 1)
			{
				instr inst = new instr();
				inst.op = "store";
				inst.src1 = r.s;
				inst.size = 4;
				inst.offset = 0;
				inst.addr = l.s.r.addr;
				IR.instrlist.add(inst);

			}
			else
			{
				instr inst = new instr();
				inst.op = "move";
				inst.src1 = r.s;
				inst.dest = l.s.r;
				IR.instrlist.add(inst);
			}
		}
		inf.lvalue = 0;
		inf.s = r.s;
        inf.exprtype = l.exprtype;
        inf.dim = 0;
		return inf;
	}

	@Override public info visitAssignmentOperator(aParser.AssignmentOperatorContext ctx)
	{
		return visitChildren(ctx);
	}


	@Override public info visitDim1(aParser.Dim1Context ctx)
	{

		info inf  = visit(ctx.expr());

		return inf;
	}



	@Override public info visitExpr2(aParser.Expr2Context ctx)
	{
		String t = ctx.type().getText();

		info inf = new info();

		if (ctx.dim_expr() != null)
		{
			inf = visit(ctx.dim_expr());
		}

		inf.exprtype = t;
		inf.lvalue = 0;
		inf.newexpr = 1;
		return inf;
	}

	@Override public info visitExpr1(aParser.Expr1Context ctx)
	{

		return visit(ctx.assignmentExpression());
	}

	@Override public info visitConst1(aParser.Const1Context ctx)
	{
		info inf = new info();

		String t = ctx.IntegerConstant().getText();
		inf.s = new src();
		inf.s.sort = 0;
		inf.s.c = Integer.parseInt(t);

		inf.exprtype = "int";
		inf.dim = 0;
		inf.lvalue = 0;
		inf.func = 0;
		return inf;
	}

	@Override public info visitConst2(aParser.Const2Context ctx)
	{
		info inf = new info();
		String t = ctx.BoolConstant().getText();
		inf.s = new src();
		inf.s.sort = 0;
		if (t.equals("true")) inf.s.c = 1;
		else inf.s.c = 0;

		inf.exprtype = "bool";
		inf.dim = 0;
		inf.lvalue = 0;
		inf.func = 0;
		return inf;
	}

	@Override public info visitConst3(aParser.Const3Context ctx)
	{
		info inf = new info();
		String t = ctx.getText();
        int len = t.length();
        if (len == 2)
        {
            t = "";
        }
        else
        {
            t = t.substring(1, t.length() - 1);
        }

        rstring s = new rstring();
        s.No = rstring.strlist.size();
        s.ctx = t;
        len = t.length();
        s.len = len;
        int slash = 0;
        for (int i = 0; i < len - 1; ++i)
        {
            if (t.charAt(i) == '\\')
            {
                if (i > 0) {
                    if (t.charAt(i - 1) == '\\') {
                        if (slash == 1) {
                            slash = 0;
                        } else {
                            slash = 1;
                            --s.len;
                        }
                    }
                    else
                    {
                        --s.len;
                        slash = 1;
                    }
                }
                else
                {
                    --s.len;
                    slash = 1;
                }
            }
            else
            {
                slash = 0;
            }
        }

        rstring.strlist.add(s);
        s.r = new register();
		s.r.No = register.reglist.size();
		s.r.isstring = 1;
		register.reglist.add(s.r);

		inf.s = new src();
		inf.s.sort = 1;
		inf.s.r = s.r;

		inf.exprtype = "string";
		inf.dim = 0;
		inf.lvalue = 0;
		inf.func = 0;
		return inf;
	}

	@Override public info visitConst4(aParser.Const4Context ctx)
	{
		info inf = new info();

		inf.s = new src();
		inf.s.sort = 0;
		inf.s.c = 0;

		inf.exprtype = "null";
		inf.dim = 0;
		inf.lvalue = 0;
		inf.func = 0;
		return inf;
	}
}