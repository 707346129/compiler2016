grammar a;

main : program? EOF;

//TBD
program : class_decl                #program4
        | func_decl     #program5
        | var_decl_stmt             #program6
        | class_decl program					#program1
        | func_decl program				#program2
        | var_decl_stmt program				#program3
        ;

block_stmt : '{' stmt_list ? '}' // changed
           ;


stmt_list : (stmt)+ ;
var_decl_stmt : type ID ';'					#var1
              | type ID '=' expr ';'				#var2
              ;

class_decl : Class ID '{' member_decl_list '}'
           ;
member_decl_list : type ID ';'					#memberlist1
                 | type ID ';' member_decl_list		#memberlist2
                 ;
func_decl : type ID '(' param_decl_list ? ')' block_stmt	#func1
          | Void ID '(' param_decl_list ? ')' block_stmt	#func2
          ;
param_decl_list : type ID					#pdl1
                | type ID ',' param_decl_list			#pdl2
                ;
type : Int							#type1
     | String							#type2
     | ID							#type3
     | type '[' ']' 						#type4
     | Bool							#type5
     ;

stmt : block_stmt						#stmt1
     | var_decl_stmt						#stmt2
     | expr? ';'						#stmt3
     | If '(' expr ')' stmt					#stmt4
     | If '(' expr ')' stmt Else stmt				#stmt5
     | Return expr? ';'                  #stmt6
     | Break ';'                        #stmt7
     | Continue';'		                #stmt8
     | While '(' expr ')' stmt					#stmt9
     | For '(' aa = expr? ';' bb = expr? ';' cc = expr? ')' stmt		#stmt10
     ;




primary
    :   ID							#primary1
    |   constant						#primary2
    |   '(' expr ')'						#primary3
    ;



postfix
    :   primary							#post1
    |   postfix '[' expr ']'					#post2
    |   postfix '.' ID						#post3
    |   postfix '++'						#post4
    |   postfix '--'						#post5
    |   postfix '(' param_list ? ')'				#post6
    ;

param_list
    : expr							#pl1
    | expr ',' param_list					#pl2
    ;



unary
    :   postfix							#unary1
    |   '++' unary						#unary2
    |   '--' unary						#unary3
    |   '~' unary						#unary4
    |   unaryOperator unary					#unary5
    ;

unaryOperator
    :   '&' | '*' | '+' | '-' | '!'
    ;





multiplicativeExpression
    :   unary							#me1
    |   multiplicativeExpression '*' unary			#me2
    |   multiplicativeExpression '/' unary			#me3
    |   multiplicativeExpression '%' unary			#me4
    ;

additiveExpression
    :   multiplicativeExpression				#add1
    |   additiveExpression '+' multiplicativeExpression	#add2
    |   additiveExpression '-' multiplicativeExpression	#add3
    ;

shiftExpression
    :   additiveExpression					#shift1
    |   shiftExpression '<<' additiveExpression		#shift2
    |   shiftExpression '>>' additiveExpression		#shift3
    ;

relationalExpression
    :   shiftExpression					#relation1
    |   relationalExpression '<' shiftExpression		#relation2
    |   relationalExpression '>' shiftExpression		#relation3
    |   relationalExpression '<=' shiftExpression		#relation4
    |   relationalExpression '>=' shiftExpression		#relation5
    ;

equalityExpression
    :   relationalExpression					#equal1
    |   equalityExpression '==' relationalExpression		#equal2
    |   equalityExpression '!=' relationalExpression		#equal3
    ;

andExpression
    :   equalityExpression					#and1
    |   andExpression '&' equalityExpression			#and2
    ;

exclusiveOrExpression
    :   andExpression						#eor1
    |   exclusiveOrExpression '^' andExpression		#eor2
    ;

inclusiveOrExpression
    :   exclusiveOrExpression					#ior1
    |   inclusiveOrExpression '|' exclusiveOrExpression	#ior2
    ;

logicalAndExpression
    :   inclusiveOrExpression					#land1
    |   logicalAndExpression '&&' inclusiveOrExpression	#land2
    ;

logicalOrExpression
    :   logicalAndExpression					#lor1
    |   logicalOrExpression '||' logicalAndExpression	#lor2
    ;


assignmentExpression
    :   logicalOrExpression					#assign1
    |   unary assignmentOperator expr	#assign2
    ;

assignmentOperator
    :   '='
    ;

dim_expr
    :   '[' expr ']' dimexpr?            			#dim1
    |	'[' expr ']' dim_expr 					#dim2
    ;

dimexpr
    :   '[' ']'             					#dim3
    |	'[' ']' dimexpr 					#dim4
    ;


expr
    :   assignmentExpression					#expr1
    |   New type dim_expr? 					#expr2
    ;

constant
    :   IntegerConstant					#const1
    |   BoolConstant						#const2
    |   StringConstant						#const3
    |   Null							#const4
    ;


BoolConstant : True | False;
Bool : 'bool';
Break : 'break';
Class : 'class';
Continue : 'continue';

For : 'for';

If : 'if';
Else : 'else';
Int : 'int';
New : 'new';
Null : 'null';
Return : 'return';
String : 'string';
True : 'true';
False : 'false';
Void : 'void';
While : 'while';



LeftParen : '(';
RightParen : ')';
LeftBracket : '[';
RightBracket : ']';
LeftBrace : '{';
RightBrace : '}';

Plus : '+';
PlusPlus : '++';
Minus : '-';
MinusMinus : '--';
Star : '*';
Div : '/';
Mod : '%';

Equalequal : '==' ;
Less : '<';
LessEqual : '<=';
NotEqual : '!=';
Greater : '>';
GreaterEqual : '>=';





AndAnd : '&&';
OrOr : '||';
Not : '!';


LeftShift : '<<';
RightShift : '>>';
Or : '|';
Tilde : '~';
Caret : '^';
And : '&';


Assign : '=';

Dot : '.';
Question : '?';
Colon : ':';
Semi : ';';
Comma : ',';

WhiteSpace : [ \t\n\r]+ -> skip;

ID
    :   Nondigit
        (   Nondigit | Digit)*
    ;



Nondigit
    :   [a-zA-Z_]
    ;





IntegerConstant
    :  '0'
    |   NonzeroDigit Digit*
    ;

Digit
    :   [0-9]
    ;

NonzeroDigit
    :   [1-9]
    ;

Sign
    :   '+' | '-'
    ;

DigitSequence
    :   Digit+
    ;

StringConstant
    :   '\"' (~[\r\n"]|EscapeSequence)* '\"'
    ;

EscapeSequence
    :   '\\' ["n\\]
    ;




LineComment
    :   '//' ~[\n\r]*
        -> skip
    ;