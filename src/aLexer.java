// Generated from a.g4 by ANTLR 4.5
import org.antlr.v4.runtime.Lexer;
import org.antlr.v4.runtime.CharStream;
import org.antlr.v4.runtime.Token;
import org.antlr.v4.runtime.TokenStream;
import org.antlr.v4.runtime.*;
import org.antlr.v4.runtime.atn.*;
import org.antlr.v4.runtime.dfa.DFA;
import org.antlr.v4.runtime.misc.*;

@SuppressWarnings({"all", "warnings", "unchecked", "unused", "cast"})
public class aLexer extends Lexer {
	static { RuntimeMetaData.checkVersion("4.5", RuntimeMetaData.VERSION); }

	protected static final DFA[] _decisionToDFA;
	protected static final PredictionContextCache _sharedContextCache =
		new PredictionContextCache();
	public static final int
		BoolConstant=1, Bool=2, Break=3, Class=4, Continue=5, For=6, If=7, Else=8, 
		Int=9, New=10, Null=11, Return=12, String=13, True=14, False=15, Void=16, 
		While=17, LeftParen=18, RightParen=19, LeftBracket=20, RightBracket=21, 
		LeftBrace=22, RightBrace=23, Plus=24, PlusPlus=25, Minus=26, MinusMinus=27, 
		Star=28, Div=29, Mod=30, Equalequal=31, Less=32, LessEqual=33, NotEqual=34, 
		Greater=35, GreaterEqual=36, AndAnd=37, OrOr=38, Not=39, LeftShift=40, 
		RightShift=41, Or=42, Tilde=43, Caret=44, And=45, Assign=46, Dot=47, Question=48, 
		Colon=49, Semi=50, Comma=51, WhiteSpace=52, ID=53, Nondigit=54, IntegerConstant=55, 
		Digit=56, NonzeroDigit=57, Sign=58, DigitSequence=59, StringConstant=60, 
		EscapeSequence=61, LineComment=62;
	public static String[] modeNames = {
		"DEFAULT_MODE"
	};

	public static final String[] ruleNames = {
		"BoolConstant", "Bool", "Break", "Class", "Continue", "For", "If", "Else", 
		"Int", "New", "Null", "Return", "String", "True", "False", "Void", "While", 
		"LeftParen", "RightParen", "LeftBracket", "RightBracket", "LeftBrace", 
		"RightBrace", "Plus", "PlusPlus", "Minus", "MinusMinus", "Star", "Div", 
		"Mod", "Equalequal", "Less", "LessEqual", "NotEqual", "Greater", "GreaterEqual", 
		"AndAnd", "OrOr", "Not", "LeftShift", "RightShift", "Or", "Tilde", "Caret", 
		"And", "Assign", "Dot", "Question", "Colon", "Semi", "Comma", "WhiteSpace", 
		"ID", "Nondigit", "IntegerConstant", "Digit", "NonzeroDigit", "Sign", 
		"DigitSequence", "StringConstant", "EscapeSequence", "LineComment"
	};

	private static final String[] _LITERAL_NAMES = {
		null, null, "'bool'", "'break'", "'class'", "'continue'", "'for'", "'if'", 
		"'else'", "'int'", "'new'", "'null'", "'return'", "'string'", "'true'", 
		"'false'", "'void'", "'while'", "'('", "')'", "'['", "']'", "'{'", "'}'", 
		"'+'", "'++'", "'-'", "'--'", "'*'", "'/'", "'%'", "'=='", "'<'", "'<='", 
		"'!='", "'>'", "'>='", "'&&'", "'||'", "'!'", "'<<'", "'>>'", "'|'", "'~'", 
		"'^'", "'&'", "'='", "'.'", "'?'", "':'", "';'", "','"
	};
	private static final String[] _SYMBOLIC_NAMES = {
		null, "BoolConstant", "Bool", "Break", "Class", "Continue", "For", "If", 
		"Else", "Int", "New", "Null", "Return", "String", "True", "False", "Void", 
		"While", "LeftParen", "RightParen", "LeftBracket", "RightBracket", "LeftBrace", 
		"RightBrace", "Plus", "PlusPlus", "Minus", "MinusMinus", "Star", "Div", 
		"Mod", "Equalequal", "Less", "LessEqual", "NotEqual", "Greater", "GreaterEqual", 
		"AndAnd", "OrOr", "Not", "LeftShift", "RightShift", "Or", "Tilde", "Caret", 
		"And", "Assign", "Dot", "Question", "Colon", "Semi", "Comma", "WhiteSpace", 
		"ID", "Nondigit", "IntegerConstant", "Digit", "NonzeroDigit", "Sign", 
		"DigitSequence", "StringConstant", "EscapeSequence", "LineComment"
	};
	public static final Vocabulary VOCABULARY = new VocabularyImpl(_LITERAL_NAMES, _SYMBOLIC_NAMES);

	/**
	 * @deprecated Use {@link #VOCABULARY} instead.
	 */
	@Deprecated
	public static final String[] tokenNames;
	static {
		tokenNames = new String[_SYMBOLIC_NAMES.length];
		for (int i = 0; i < tokenNames.length; i++) {
			tokenNames[i] = VOCABULARY.getLiteralName(i);
			if (tokenNames[i] == null) {
				tokenNames[i] = VOCABULARY.getSymbolicName(i);
			}

			if (tokenNames[i] == null) {
				tokenNames[i] = "<INVALID>";
			}
		}
	}

	@Override
	@Deprecated
	public String[] getTokenNames() {
		return tokenNames;
	}

	@Override

	public Vocabulary getVocabulary() {
		return VOCABULARY;
	}


	public aLexer(CharStream input) {
		super(input);
		_interp = new LexerATNSimulator(this,_ATN,_decisionToDFA,_sharedContextCache);
	}

	@Override
	public String getGrammarFileName() { return "a.g4"; }

	@Override
	public String[] getRuleNames() { return ruleNames; }

	@Override
	public String getSerializedATN() { return _serializedATN; }

	@Override
	public String[] getModeNames() { return modeNames; }

	@Override
	public ATN getATN() { return _ATN; }

	public static final String _serializedATN =
		"\3\u0430\ud6d1\u8206\uad2d\u4417\uaef1\u8d80\uaadd\2@\u0166\b\1\4\2\t"+
		"\2\4\3\t\3\4\4\t\4\4\5\t\5\4\6\t\6\4\7\t\7\4\b\t\b\4\t\t\t\4\n\t\n\4\13"+
		"\t\13\4\f\t\f\4\r\t\r\4\16\t\16\4\17\t\17\4\20\t\20\4\21\t\21\4\22\t\22"+
		"\4\23\t\23\4\24\t\24\4\25\t\25\4\26\t\26\4\27\t\27\4\30\t\30\4\31\t\31"+
		"\4\32\t\32\4\33\t\33\4\34\t\34\4\35\t\35\4\36\t\36\4\37\t\37\4 \t \4!"+
		"\t!\4\"\t\"\4#\t#\4$\t$\4%\t%\4&\t&\4\'\t\'\4(\t(\4)\t)\4*\t*\4+\t+\4"+
		",\t,\4-\t-\4.\t.\4/\t/\4\60\t\60\4\61\t\61\4\62\t\62\4\63\t\63\4\64\t"+
		"\64\4\65\t\65\4\66\t\66\4\67\t\67\48\t8\49\t9\4:\t:\4;\t;\4<\t<\4=\t="+
		"\4>\t>\4?\t?\3\2\3\2\5\2\u0082\n\2\3\3\3\3\3\3\3\3\3\3\3\4\3\4\3\4\3\4"+
		"\3\4\3\4\3\5\3\5\3\5\3\5\3\5\3\5\3\6\3\6\3\6\3\6\3\6\3\6\3\6\3\6\3\6\3"+
		"\7\3\7\3\7\3\7\3\b\3\b\3\b\3\t\3\t\3\t\3\t\3\t\3\n\3\n\3\n\3\n\3\13\3"+
		"\13\3\13\3\13\3\f\3\f\3\f\3\f\3\f\3\r\3\r\3\r\3\r\3\r\3\r\3\r\3\16\3\16"+
		"\3\16\3\16\3\16\3\16\3\16\3\17\3\17\3\17\3\17\3\17\3\20\3\20\3\20\3\20"+
		"\3\20\3\20\3\21\3\21\3\21\3\21\3\21\3\22\3\22\3\22\3\22\3\22\3\22\3\23"+
		"\3\23\3\24\3\24\3\25\3\25\3\26\3\26\3\27\3\27\3\30\3\30\3\31\3\31\3\32"+
		"\3\32\3\32\3\33\3\33\3\34\3\34\3\34\3\35\3\35\3\36\3\36\3\37\3\37\3 \3"+
		" \3 \3!\3!\3\"\3\"\3\"\3#\3#\3#\3$\3$\3%\3%\3%\3&\3&\3&\3\'\3\'\3\'\3"+
		"(\3(\3)\3)\3)\3*\3*\3*\3+\3+\3,\3,\3-\3-\3.\3.\3/\3/\3\60\3\60\3\61\3"+
		"\61\3\62\3\62\3\63\3\63\3\64\3\64\3\65\6\65\u012a\n\65\r\65\16\65\u012b"+
		"\3\65\3\65\3\66\3\66\3\66\7\66\u0133\n\66\f\66\16\66\u0136\13\66\3\67"+
		"\3\67\38\38\38\78\u013d\n8\f8\168\u0140\138\58\u0142\n8\39\39\3:\3:\3"+
		";\3;\3<\6<\u014b\n<\r<\16<\u014c\3=\3=\3=\7=\u0152\n=\f=\16=\u0155\13"+
		"=\3=\3=\3>\3>\3>\3?\3?\3?\3?\7?\u0160\n?\f?\16?\u0163\13?\3?\3?\2\2@\3"+
		"\3\5\4\7\5\t\6\13\7\r\b\17\t\21\n\23\13\25\f\27\r\31\16\33\17\35\20\37"+
		"\21!\22#\23%\24\'\25)\26+\27-\30/\31\61\32\63\33\65\34\67\359\36;\37="+
		" ?!A\"C#E$G%I&K\'M(O)Q*S+U,W-Y.[/]\60_\61a\62c\63e\64g\65i\66k\67m8o9"+
		"q:s;u<w=y>{?}@\3\2\n\5\2\13\f\17\17\"\"\5\2C\\aac|\3\2\62;\3\2\63;\4\2"+
		"--//\5\2\f\f\17\17$$\5\2$$^^pp\4\2\f\f\17\17\u016f\2\3\3\2\2\2\2\5\3\2"+
		"\2\2\2\7\3\2\2\2\2\t\3\2\2\2\2\13\3\2\2\2\2\r\3\2\2\2\2\17\3\2\2\2\2\21"+
		"\3\2\2\2\2\23\3\2\2\2\2\25\3\2\2\2\2\27\3\2\2\2\2\31\3\2\2\2\2\33\3\2"+
		"\2\2\2\35\3\2\2\2\2\37\3\2\2\2\2!\3\2\2\2\2#\3\2\2\2\2%\3\2\2\2\2\'\3"+
		"\2\2\2\2)\3\2\2\2\2+\3\2\2\2\2-\3\2\2\2\2/\3\2\2\2\2\61\3\2\2\2\2\63\3"+
		"\2\2\2\2\65\3\2\2\2\2\67\3\2\2\2\29\3\2\2\2\2;\3\2\2\2\2=\3\2\2\2\2?\3"+
		"\2\2\2\2A\3\2\2\2\2C\3\2\2\2\2E\3\2\2\2\2G\3\2\2\2\2I\3\2\2\2\2K\3\2\2"+
		"\2\2M\3\2\2\2\2O\3\2\2\2\2Q\3\2\2\2\2S\3\2\2\2\2U\3\2\2\2\2W\3\2\2\2\2"+
		"Y\3\2\2\2\2[\3\2\2\2\2]\3\2\2\2\2_\3\2\2\2\2a\3\2\2\2\2c\3\2\2\2\2e\3"+
		"\2\2\2\2g\3\2\2\2\2i\3\2\2\2\2k\3\2\2\2\2m\3\2\2\2\2o\3\2\2\2\2q\3\2\2"+
		"\2\2s\3\2\2\2\2u\3\2\2\2\2w\3\2\2\2\2y\3\2\2\2\2{\3\2\2\2\2}\3\2\2\2\3"+
		"\u0081\3\2\2\2\5\u0083\3\2\2\2\7\u0088\3\2\2\2\t\u008e\3\2\2\2\13\u0094"+
		"\3\2\2\2\r\u009d\3\2\2\2\17\u00a1\3\2\2\2\21\u00a4\3\2\2\2\23\u00a9\3"+
		"\2\2\2\25\u00ad\3\2\2\2\27\u00b1\3\2\2\2\31\u00b6\3\2\2\2\33\u00bd\3\2"+
		"\2\2\35\u00c4\3\2\2\2\37\u00c9\3\2\2\2!\u00cf\3\2\2\2#\u00d4\3\2\2\2%"+
		"\u00da\3\2\2\2\'\u00dc\3\2\2\2)\u00de\3\2\2\2+\u00e0\3\2\2\2-\u00e2\3"+
		"\2\2\2/\u00e4\3\2\2\2\61\u00e6\3\2\2\2\63\u00e8\3\2\2\2\65\u00eb\3\2\2"+
		"\2\67\u00ed\3\2\2\29\u00f0\3\2\2\2;\u00f2\3\2\2\2=\u00f4\3\2\2\2?\u00f6"+
		"\3\2\2\2A\u00f9\3\2\2\2C\u00fb\3\2\2\2E\u00fe\3\2\2\2G\u0101\3\2\2\2I"+
		"\u0103\3\2\2\2K\u0106\3\2\2\2M\u0109\3\2\2\2O\u010c\3\2\2\2Q\u010e\3\2"+
		"\2\2S\u0111\3\2\2\2U\u0114\3\2\2\2W\u0116\3\2\2\2Y\u0118\3\2\2\2[\u011a"+
		"\3\2\2\2]\u011c\3\2\2\2_\u011e\3\2\2\2a\u0120\3\2\2\2c\u0122\3\2\2\2e"+
		"\u0124\3\2\2\2g\u0126\3\2\2\2i\u0129\3\2\2\2k\u012f\3\2\2\2m\u0137\3\2"+
		"\2\2o\u0141\3\2\2\2q\u0143\3\2\2\2s\u0145\3\2\2\2u\u0147\3\2\2\2w\u014a"+
		"\3\2\2\2y\u014e\3\2\2\2{\u0158\3\2\2\2}\u015b\3\2\2\2\177\u0082\5\35\17"+
		"\2\u0080\u0082\5\37\20\2\u0081\177\3\2\2\2\u0081\u0080\3\2\2\2\u0082\4"+
		"\3\2\2\2\u0083\u0084\7d\2\2\u0084\u0085\7q\2\2\u0085\u0086\7q\2\2\u0086"+
		"\u0087\7n\2\2\u0087\6\3\2\2\2\u0088\u0089\7d\2\2\u0089\u008a\7t\2\2\u008a"+
		"\u008b\7g\2\2\u008b\u008c\7c\2\2\u008c\u008d\7m\2\2\u008d\b\3\2\2\2\u008e"+
		"\u008f\7e\2\2\u008f\u0090\7n\2\2\u0090\u0091\7c\2\2\u0091\u0092\7u\2\2"+
		"\u0092\u0093\7u\2\2\u0093\n\3\2\2\2\u0094\u0095\7e\2\2\u0095\u0096\7q"+
		"\2\2\u0096\u0097\7p\2\2\u0097\u0098\7v\2\2\u0098\u0099\7k\2\2\u0099\u009a"+
		"\7p\2\2\u009a\u009b\7w\2\2\u009b\u009c\7g\2\2\u009c\f\3\2\2\2\u009d\u009e"+
		"\7h\2\2\u009e\u009f\7q\2\2\u009f\u00a0\7t\2\2\u00a0\16\3\2\2\2\u00a1\u00a2"+
		"\7k\2\2\u00a2\u00a3\7h\2\2\u00a3\20\3\2\2\2\u00a4\u00a5\7g\2\2\u00a5\u00a6"+
		"\7n\2\2\u00a6\u00a7\7u\2\2\u00a7\u00a8\7g\2\2\u00a8\22\3\2\2\2\u00a9\u00aa"+
		"\7k\2\2\u00aa\u00ab\7p\2\2\u00ab\u00ac\7v\2\2\u00ac\24\3\2\2\2\u00ad\u00ae"+
		"\7p\2\2\u00ae\u00af\7g\2\2\u00af\u00b0\7y\2\2\u00b0\26\3\2\2\2\u00b1\u00b2"+
		"\7p\2\2\u00b2\u00b3\7w\2\2\u00b3\u00b4\7n\2\2\u00b4\u00b5\7n\2\2\u00b5"+
		"\30\3\2\2\2\u00b6\u00b7\7t\2\2\u00b7\u00b8\7g\2\2\u00b8\u00b9\7v\2\2\u00b9"+
		"\u00ba\7w\2\2\u00ba\u00bb\7t\2\2\u00bb\u00bc\7p\2\2\u00bc\32\3\2\2\2\u00bd"+
		"\u00be\7u\2\2\u00be\u00bf\7v\2\2\u00bf\u00c0\7t\2\2\u00c0\u00c1\7k\2\2"+
		"\u00c1\u00c2\7p\2\2\u00c2\u00c3\7i\2\2\u00c3\34\3\2\2\2\u00c4\u00c5\7"+
		"v\2\2\u00c5\u00c6\7t\2\2\u00c6\u00c7\7w\2\2\u00c7\u00c8\7g\2\2\u00c8\36"+
		"\3\2\2\2\u00c9\u00ca\7h\2\2\u00ca\u00cb\7c\2\2\u00cb\u00cc\7n\2\2\u00cc"+
		"\u00cd\7u\2\2\u00cd\u00ce\7g\2\2\u00ce \3\2\2\2\u00cf\u00d0\7x\2\2\u00d0"+
		"\u00d1\7q\2\2\u00d1\u00d2\7k\2\2\u00d2\u00d3\7f\2\2\u00d3\"\3\2\2\2\u00d4"+
		"\u00d5\7y\2\2\u00d5\u00d6\7j\2\2\u00d6\u00d7\7k\2\2\u00d7\u00d8\7n\2\2"+
		"\u00d8\u00d9\7g\2\2\u00d9$\3\2\2\2\u00da\u00db\7*\2\2\u00db&\3\2\2\2\u00dc"+
		"\u00dd\7+\2\2\u00dd(\3\2\2\2\u00de\u00df\7]\2\2\u00df*\3\2\2\2\u00e0\u00e1"+
		"\7_\2\2\u00e1,\3\2\2\2\u00e2\u00e3\7}\2\2\u00e3.\3\2\2\2\u00e4\u00e5\7"+
		"\177\2\2\u00e5\60\3\2\2\2\u00e6\u00e7\7-\2\2\u00e7\62\3\2\2\2\u00e8\u00e9"+
		"\7-\2\2\u00e9\u00ea\7-\2\2\u00ea\64\3\2\2\2\u00eb\u00ec\7/\2\2\u00ec\66"+
		"\3\2\2\2\u00ed\u00ee\7/\2\2\u00ee\u00ef\7/\2\2\u00ef8\3\2\2\2\u00f0\u00f1"+
		"\7,\2\2\u00f1:\3\2\2\2\u00f2\u00f3\7\61\2\2\u00f3<\3\2\2\2\u00f4\u00f5"+
		"\7\'\2\2\u00f5>\3\2\2\2\u00f6\u00f7\7?\2\2\u00f7\u00f8\7?\2\2\u00f8@\3"+
		"\2\2\2\u00f9\u00fa\7>\2\2\u00faB\3\2\2\2\u00fb\u00fc\7>\2\2\u00fc\u00fd"+
		"\7?\2\2\u00fdD\3\2\2\2\u00fe\u00ff\7#\2\2\u00ff\u0100\7?\2\2\u0100F\3"+
		"\2\2\2\u0101\u0102\7@\2\2\u0102H\3\2\2\2\u0103\u0104\7@\2\2\u0104\u0105"+
		"\7?\2\2\u0105J\3\2\2\2\u0106\u0107\7(\2\2\u0107\u0108\7(\2\2\u0108L\3"+
		"\2\2\2\u0109\u010a\7~\2\2\u010a\u010b\7~\2\2\u010bN\3\2\2\2\u010c\u010d"+
		"\7#\2\2\u010dP\3\2\2\2\u010e\u010f\7>\2\2\u010f\u0110\7>\2\2\u0110R\3"+
		"\2\2\2\u0111\u0112\7@\2\2\u0112\u0113\7@\2\2\u0113T\3\2\2\2\u0114\u0115"+
		"\7~\2\2\u0115V\3\2\2\2\u0116\u0117\7\u0080\2\2\u0117X\3\2\2\2\u0118\u0119"+
		"\7`\2\2\u0119Z\3\2\2\2\u011a\u011b\7(\2\2\u011b\\\3\2\2\2\u011c\u011d"+
		"\7?\2\2\u011d^\3\2\2\2\u011e\u011f\7\60\2\2\u011f`\3\2\2\2\u0120\u0121"+
		"\7A\2\2\u0121b\3\2\2\2\u0122\u0123\7<\2\2\u0123d\3\2\2\2\u0124\u0125\7"+
		"=\2\2\u0125f\3\2\2\2\u0126\u0127\7.\2\2\u0127h\3\2\2\2\u0128\u012a\t\2"+
		"\2\2\u0129\u0128\3\2\2\2\u012a\u012b\3\2\2\2\u012b\u0129\3\2\2\2\u012b"+
		"\u012c\3\2\2\2\u012c\u012d\3\2\2\2\u012d\u012e\b\65\2\2\u012ej\3\2\2\2"+
		"\u012f\u0134\5m\67\2\u0130\u0133\5m\67\2\u0131\u0133\5q9\2\u0132\u0130"+
		"\3\2\2\2\u0132\u0131\3\2\2\2\u0133\u0136\3\2\2\2\u0134\u0132\3\2\2\2\u0134"+
		"\u0135\3\2\2\2\u0135l\3\2\2\2\u0136\u0134\3\2\2\2\u0137\u0138\t\3\2\2"+
		"\u0138n\3\2\2\2\u0139\u0142\7\62\2\2\u013a\u013e\5s:\2\u013b\u013d\5q"+
		"9\2\u013c\u013b\3\2\2\2\u013d\u0140\3\2\2\2\u013e\u013c\3\2\2\2\u013e"+
		"\u013f\3\2\2\2\u013f\u0142\3\2\2\2\u0140\u013e\3\2\2\2\u0141\u0139\3\2"+
		"\2\2\u0141\u013a\3\2\2\2\u0142p\3\2\2\2\u0143\u0144\t\4\2\2\u0144r\3\2"+
		"\2\2\u0145\u0146\t\5\2\2\u0146t\3\2\2\2\u0147\u0148\t\6\2\2\u0148v\3\2"+
		"\2\2\u0149\u014b\5q9\2\u014a\u0149\3\2\2\2\u014b\u014c\3\2\2\2\u014c\u014a"+
		"\3\2\2\2\u014c\u014d\3\2\2\2\u014dx\3\2\2\2\u014e\u0153\7$\2\2\u014f\u0152"+
		"\n\7\2\2\u0150\u0152\5{>\2\u0151\u014f\3\2\2\2\u0151\u0150\3\2\2\2\u0152"+
		"\u0155\3\2\2\2\u0153\u0151\3\2\2\2\u0153\u0154\3\2\2\2\u0154\u0156\3\2"+
		"\2\2\u0155\u0153\3\2\2\2\u0156\u0157\7$\2\2\u0157z\3\2\2\2\u0158\u0159"+
		"\7^\2\2\u0159\u015a\t\b\2\2\u015a|\3\2\2\2\u015b\u015c\7\61\2\2\u015c"+
		"\u015d\7\61\2\2\u015d\u0161\3\2\2\2\u015e\u0160\n\t\2\2\u015f\u015e\3"+
		"\2\2\2\u0160\u0163\3\2\2\2\u0161\u015f\3\2\2\2\u0161\u0162\3\2\2\2\u0162"+
		"\u0164\3\2\2\2\u0163\u0161\3\2\2\2\u0164\u0165\b?\2\2\u0165~\3\2\2\2\r"+
		"\2\u0081\u012b\u0132\u0134\u013e\u0141\u014c\u0151\u0153\u0161\3\b\2\2";
	public static final ATN _ATN =
		new ATNDeserializer().deserialize(_serializedATN.toCharArray());
	static {
		_decisionToDFA = new DFA[_ATN.getNumberOfDecisions()];
		for (int i = 0; i < _ATN.getNumberOfDecisions(); i++) {
			_decisionToDFA[i] = new DFA(_ATN.getDecisionState(i), i);
		}
	}
}