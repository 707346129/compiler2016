// Generated from a.g4 by ANTLR 4.5
import org.antlr.v4.runtime.tree.ParseTreeListener;

/**
 * This interface defines a complete listener for a parse tree produced by
 * {@link aParser}.
 */
public interface aListener extends ParseTreeListener {
	/**
	 * Enter a parse tree produced by {@link aParser#main}.
	 * @param ctx the parse tree
	 */
	void enterMain(aParser.MainContext ctx);
	/**
	 * Exit a parse tree produced by {@link aParser#main}.
	 * @param ctx the parse tree
	 */
	void exitMain(aParser.MainContext ctx);
	/**
	 * Enter a parse tree produced by the {@code program4}
	 * labeled alternative in {@link aParser#program}.
	 * @param ctx the parse tree
	 */
	void enterProgram4(aParser.Program4Context ctx);
	/**
	 * Exit a parse tree produced by the {@code program4}
	 * labeled alternative in {@link aParser#program}.
	 * @param ctx the parse tree
	 */
	void exitProgram4(aParser.Program4Context ctx);
	/**
	 * Enter a parse tree produced by the {@code program5}
	 * labeled alternative in {@link aParser#program}.
	 * @param ctx the parse tree
	 */
	void enterProgram5(aParser.Program5Context ctx);
	/**
	 * Exit a parse tree produced by the {@code program5}
	 * labeled alternative in {@link aParser#program}.
	 * @param ctx the parse tree
	 */
	void exitProgram5(aParser.Program5Context ctx);
	/**
	 * Enter a parse tree produced by the {@code program6}
	 * labeled alternative in {@link aParser#program}.
	 * @param ctx the parse tree
	 */
	void enterProgram6(aParser.Program6Context ctx);
	/**
	 * Exit a parse tree produced by the {@code program6}
	 * labeled alternative in {@link aParser#program}.
	 * @param ctx the parse tree
	 */
	void exitProgram6(aParser.Program6Context ctx);
	/**
	 * Enter a parse tree produced by the {@code program1}
	 * labeled alternative in {@link aParser#program}.
	 * @param ctx the parse tree
	 */
	void enterProgram1(aParser.Program1Context ctx);
	/**
	 * Exit a parse tree produced by the {@code program1}
	 * labeled alternative in {@link aParser#program}.
	 * @param ctx the parse tree
	 */
	void exitProgram1(aParser.Program1Context ctx);
	/**
	 * Enter a parse tree produced by the {@code program2}
	 * labeled alternative in {@link aParser#program}.
	 * @param ctx the parse tree
	 */
	void enterProgram2(aParser.Program2Context ctx);
	/**
	 * Exit a parse tree produced by the {@code program2}
	 * labeled alternative in {@link aParser#program}.
	 * @param ctx the parse tree
	 */
	void exitProgram2(aParser.Program2Context ctx);
	/**
	 * Enter a parse tree produced by the {@code program3}
	 * labeled alternative in {@link aParser#program}.
	 * @param ctx the parse tree
	 */
	void enterProgram3(aParser.Program3Context ctx);
	/**
	 * Exit a parse tree produced by the {@code program3}
	 * labeled alternative in {@link aParser#program}.
	 * @param ctx the parse tree
	 */
	void exitProgram3(aParser.Program3Context ctx);
	/**
	 * Enter a parse tree produced by {@link aParser#block_stmt}.
	 * @param ctx the parse tree
	 */
	void enterBlock_stmt(aParser.Block_stmtContext ctx);
	/**
	 * Exit a parse tree produced by {@link aParser#block_stmt}.
	 * @param ctx the parse tree
	 */
	void exitBlock_stmt(aParser.Block_stmtContext ctx);
	/**
	 * Enter a parse tree produced by {@link aParser#stmt_list}.
	 * @param ctx the parse tree
	 */
	void enterStmt_list(aParser.Stmt_listContext ctx);
	/**
	 * Exit a parse tree produced by {@link aParser#stmt_list}.
	 * @param ctx the parse tree
	 */
	void exitStmt_list(aParser.Stmt_listContext ctx);
	/**
	 * Enter a parse tree produced by the {@code var1}
	 * labeled alternative in {@link aParser#var_decl_stmt}.
	 * @param ctx the parse tree
	 */
	void enterVar1(aParser.Var1Context ctx);
	/**
	 * Exit a parse tree produced by the {@code var1}
	 * labeled alternative in {@link aParser#var_decl_stmt}.
	 * @param ctx the parse tree
	 */
	void exitVar1(aParser.Var1Context ctx);
	/**
	 * Enter a parse tree produced by the {@code var2}
	 * labeled alternative in {@link aParser#var_decl_stmt}.
	 * @param ctx the parse tree
	 */
	void enterVar2(aParser.Var2Context ctx);
	/**
	 * Exit a parse tree produced by the {@code var2}
	 * labeled alternative in {@link aParser#var_decl_stmt}.
	 * @param ctx the parse tree
	 */
	void exitVar2(aParser.Var2Context ctx);
	/**
	 * Enter a parse tree produced by {@link aParser#class_decl}.
	 * @param ctx the parse tree
	 */
	void enterClass_decl(aParser.Class_declContext ctx);
	/**
	 * Exit a parse tree produced by {@link aParser#class_decl}.
	 * @param ctx the parse tree
	 */
	void exitClass_decl(aParser.Class_declContext ctx);
	/**
	 * Enter a parse tree produced by the {@code memberlist1}
	 * labeled alternative in {@link aParser#member_decl_list}.
	 * @param ctx the parse tree
	 */
	void enterMemberlist1(aParser.Memberlist1Context ctx);
	/**
	 * Exit a parse tree produced by the {@code memberlist1}
	 * labeled alternative in {@link aParser#member_decl_list}.
	 * @param ctx the parse tree
	 */
	void exitMemberlist1(aParser.Memberlist1Context ctx);
	/**
	 * Enter a parse tree produced by the {@code memberlist2}
	 * labeled alternative in {@link aParser#member_decl_list}.
	 * @param ctx the parse tree
	 */
	void enterMemberlist2(aParser.Memberlist2Context ctx);
	/**
	 * Exit a parse tree produced by the {@code memberlist2}
	 * labeled alternative in {@link aParser#member_decl_list}.
	 * @param ctx the parse tree
	 */
	void exitMemberlist2(aParser.Memberlist2Context ctx);
	/**
	 * Enter a parse tree produced by the {@code func1}
	 * labeled alternative in {@link aParser#func_decl}.
	 * @param ctx the parse tree
	 */
	void enterFunc1(aParser.Func1Context ctx);
	/**
	 * Exit a parse tree produced by the {@code func1}
	 * labeled alternative in {@link aParser#func_decl}.
	 * @param ctx the parse tree
	 */
	void exitFunc1(aParser.Func1Context ctx);
	/**
	 * Enter a parse tree produced by the {@code func2}
	 * labeled alternative in {@link aParser#func_decl}.
	 * @param ctx the parse tree
	 */
	void enterFunc2(aParser.Func2Context ctx);
	/**
	 * Exit a parse tree produced by the {@code func2}
	 * labeled alternative in {@link aParser#func_decl}.
	 * @param ctx the parse tree
	 */
	void exitFunc2(aParser.Func2Context ctx);
	/**
	 * Enter a parse tree produced by the {@code pdl1}
	 * labeled alternative in {@link aParser#param_decl_list}.
	 * @param ctx the parse tree
	 */
	void enterPdl1(aParser.Pdl1Context ctx);
	/**
	 * Exit a parse tree produced by the {@code pdl1}
	 * labeled alternative in {@link aParser#param_decl_list}.
	 * @param ctx the parse tree
	 */
	void exitPdl1(aParser.Pdl1Context ctx);
	/**
	 * Enter a parse tree produced by the {@code pdl2}
	 * labeled alternative in {@link aParser#param_decl_list}.
	 * @param ctx the parse tree
	 */
	void enterPdl2(aParser.Pdl2Context ctx);
	/**
	 * Exit a parse tree produced by the {@code pdl2}
	 * labeled alternative in {@link aParser#param_decl_list}.
	 * @param ctx the parse tree
	 */
	void exitPdl2(aParser.Pdl2Context ctx);
	/**
	 * Enter a parse tree produced by the {@code type5}
	 * labeled alternative in {@link aParser#type}.
	 * @param ctx the parse tree
	 */
	void enterType5(aParser.Type5Context ctx);
	/**
	 * Exit a parse tree produced by the {@code type5}
	 * labeled alternative in {@link aParser#type}.
	 * @param ctx the parse tree
	 */
	void exitType5(aParser.Type5Context ctx);
	/**
	 * Enter a parse tree produced by the {@code type4}
	 * labeled alternative in {@link aParser#type}.
	 * @param ctx the parse tree
	 */
	void enterType4(aParser.Type4Context ctx);
	/**
	 * Exit a parse tree produced by the {@code type4}
	 * labeled alternative in {@link aParser#type}.
	 * @param ctx the parse tree
	 */
	void exitType4(aParser.Type4Context ctx);
	/**
	 * Enter a parse tree produced by the {@code type3}
	 * labeled alternative in {@link aParser#type}.
	 * @param ctx the parse tree
	 */
	void enterType3(aParser.Type3Context ctx);
	/**
	 * Exit a parse tree produced by the {@code type3}
	 * labeled alternative in {@link aParser#type}.
	 * @param ctx the parse tree
	 */
	void exitType3(aParser.Type3Context ctx);
	/**
	 * Enter a parse tree produced by the {@code type2}
	 * labeled alternative in {@link aParser#type}.
	 * @param ctx the parse tree
	 */
	void enterType2(aParser.Type2Context ctx);
	/**
	 * Exit a parse tree produced by the {@code type2}
	 * labeled alternative in {@link aParser#type}.
	 * @param ctx the parse tree
	 */
	void exitType2(aParser.Type2Context ctx);
	/**
	 * Enter a parse tree produced by the {@code type1}
	 * labeled alternative in {@link aParser#type}.
	 * @param ctx the parse tree
	 */
	void enterType1(aParser.Type1Context ctx);
	/**
	 * Exit a parse tree produced by the {@code type1}
	 * labeled alternative in {@link aParser#type}.
	 * @param ctx the parse tree
	 */
	void exitType1(aParser.Type1Context ctx);
	/**
	 * Enter a parse tree produced by the {@code stmt1}
	 * labeled alternative in {@link aParser#stmt}.
	 * @param ctx the parse tree
	 */
	void enterStmt1(aParser.Stmt1Context ctx);
	/**
	 * Exit a parse tree produced by the {@code stmt1}
	 * labeled alternative in {@link aParser#stmt}.
	 * @param ctx the parse tree
	 */
	void exitStmt1(aParser.Stmt1Context ctx);
	/**
	 * Enter a parse tree produced by the {@code stmt2}
	 * labeled alternative in {@link aParser#stmt}.
	 * @param ctx the parse tree
	 */
	void enterStmt2(aParser.Stmt2Context ctx);
	/**
	 * Exit a parse tree produced by the {@code stmt2}
	 * labeled alternative in {@link aParser#stmt}.
	 * @param ctx the parse tree
	 */
	void exitStmt2(aParser.Stmt2Context ctx);
	/**
	 * Enter a parse tree produced by the {@code stmt3}
	 * labeled alternative in {@link aParser#stmt}.
	 * @param ctx the parse tree
	 */
	void enterStmt3(aParser.Stmt3Context ctx);
	/**
	 * Exit a parse tree produced by the {@code stmt3}
	 * labeled alternative in {@link aParser#stmt}.
	 * @param ctx the parse tree
	 */
	void exitStmt3(aParser.Stmt3Context ctx);
	/**
	 * Enter a parse tree produced by the {@code stmt4}
	 * labeled alternative in {@link aParser#stmt}.
	 * @param ctx the parse tree
	 */
	void enterStmt4(aParser.Stmt4Context ctx);
	/**
	 * Exit a parse tree produced by the {@code stmt4}
	 * labeled alternative in {@link aParser#stmt}.
	 * @param ctx the parse tree
	 */
	void exitStmt4(aParser.Stmt4Context ctx);
	/**
	 * Enter a parse tree produced by the {@code stmt5}
	 * labeled alternative in {@link aParser#stmt}.
	 * @param ctx the parse tree
	 */
	void enterStmt5(aParser.Stmt5Context ctx);
	/**
	 * Exit a parse tree produced by the {@code stmt5}
	 * labeled alternative in {@link aParser#stmt}.
	 * @param ctx the parse tree
	 */
	void exitStmt5(aParser.Stmt5Context ctx);
	/**
	 * Enter a parse tree produced by the {@code stmt6}
	 * labeled alternative in {@link aParser#stmt}.
	 * @param ctx the parse tree
	 */
	void enterStmt6(aParser.Stmt6Context ctx);
	/**
	 * Exit a parse tree produced by the {@code stmt6}
	 * labeled alternative in {@link aParser#stmt}.
	 * @param ctx the parse tree
	 */
	void exitStmt6(aParser.Stmt6Context ctx);
	/**
	 * Enter a parse tree produced by the {@code stmt7}
	 * labeled alternative in {@link aParser#stmt}.
	 * @param ctx the parse tree
	 */
	void enterStmt7(aParser.Stmt7Context ctx);
	/**
	 * Exit a parse tree produced by the {@code stmt7}
	 * labeled alternative in {@link aParser#stmt}.
	 * @param ctx the parse tree
	 */
	void exitStmt7(aParser.Stmt7Context ctx);
	/**
	 * Enter a parse tree produced by the {@code stmt8}
	 * labeled alternative in {@link aParser#stmt}.
	 * @param ctx the parse tree
	 */
	void enterStmt8(aParser.Stmt8Context ctx);
	/**
	 * Exit a parse tree produced by the {@code stmt8}
	 * labeled alternative in {@link aParser#stmt}.
	 * @param ctx the parse tree
	 */
	void exitStmt8(aParser.Stmt8Context ctx);
	/**
	 * Enter a parse tree produced by the {@code stmt9}
	 * labeled alternative in {@link aParser#stmt}.
	 * @param ctx the parse tree
	 */
	void enterStmt9(aParser.Stmt9Context ctx);
	/**
	 * Exit a parse tree produced by the {@code stmt9}
	 * labeled alternative in {@link aParser#stmt}.
	 * @param ctx the parse tree
	 */
	void exitStmt9(aParser.Stmt9Context ctx);
	/**
	 * Enter a parse tree produced by the {@code stmt10}
	 * labeled alternative in {@link aParser#stmt}.
	 * @param ctx the parse tree
	 */
	void enterStmt10(aParser.Stmt10Context ctx);
	/**
	 * Exit a parse tree produced by the {@code stmt10}
	 * labeled alternative in {@link aParser#stmt}.
	 * @param ctx the parse tree
	 */
	void exitStmt10(aParser.Stmt10Context ctx);
	/**
	 * Enter a parse tree produced by the {@code primary1}
	 * labeled alternative in {@link aParser#primary}.
	 * @param ctx the parse tree
	 */
	void enterPrimary1(aParser.Primary1Context ctx);
	/**
	 * Exit a parse tree produced by the {@code primary1}
	 * labeled alternative in {@link aParser#primary}.
	 * @param ctx the parse tree
	 */
	void exitPrimary1(aParser.Primary1Context ctx);
	/**
	 * Enter a parse tree produced by the {@code primary2}
	 * labeled alternative in {@link aParser#primary}.
	 * @param ctx the parse tree
	 */
	void enterPrimary2(aParser.Primary2Context ctx);
	/**
	 * Exit a parse tree produced by the {@code primary2}
	 * labeled alternative in {@link aParser#primary}.
	 * @param ctx the parse tree
	 */
	void exitPrimary2(aParser.Primary2Context ctx);
	/**
	 * Enter a parse tree produced by the {@code primary3}
	 * labeled alternative in {@link aParser#primary}.
	 * @param ctx the parse tree
	 */
	void enterPrimary3(aParser.Primary3Context ctx);
	/**
	 * Exit a parse tree produced by the {@code primary3}
	 * labeled alternative in {@link aParser#primary}.
	 * @param ctx the parse tree
	 */
	void exitPrimary3(aParser.Primary3Context ctx);
	/**
	 * Enter a parse tree produced by the {@code post6}
	 * labeled alternative in {@link aParser#postfix}.
	 * @param ctx the parse tree
	 */
	void enterPost6(aParser.Post6Context ctx);
	/**
	 * Exit a parse tree produced by the {@code post6}
	 * labeled alternative in {@link aParser#postfix}.
	 * @param ctx the parse tree
	 */
	void exitPost6(aParser.Post6Context ctx);
	/**
	 * Enter a parse tree produced by the {@code post4}
	 * labeled alternative in {@link aParser#postfix}.
	 * @param ctx the parse tree
	 */
	void enterPost4(aParser.Post4Context ctx);
	/**
	 * Exit a parse tree produced by the {@code post4}
	 * labeled alternative in {@link aParser#postfix}.
	 * @param ctx the parse tree
	 */
	void exitPost4(aParser.Post4Context ctx);
	/**
	 * Enter a parse tree produced by the {@code post5}
	 * labeled alternative in {@link aParser#postfix}.
	 * @param ctx the parse tree
	 */
	void enterPost5(aParser.Post5Context ctx);
	/**
	 * Exit a parse tree produced by the {@code post5}
	 * labeled alternative in {@link aParser#postfix}.
	 * @param ctx the parse tree
	 */
	void exitPost5(aParser.Post5Context ctx);
	/**
	 * Enter a parse tree produced by the {@code post2}
	 * labeled alternative in {@link aParser#postfix}.
	 * @param ctx the parse tree
	 */
	void enterPost2(aParser.Post2Context ctx);
	/**
	 * Exit a parse tree produced by the {@code post2}
	 * labeled alternative in {@link aParser#postfix}.
	 * @param ctx the parse tree
	 */
	void exitPost2(aParser.Post2Context ctx);
	/**
	 * Enter a parse tree produced by the {@code post3}
	 * labeled alternative in {@link aParser#postfix}.
	 * @param ctx the parse tree
	 */
	void enterPost3(aParser.Post3Context ctx);
	/**
	 * Exit a parse tree produced by the {@code post3}
	 * labeled alternative in {@link aParser#postfix}.
	 * @param ctx the parse tree
	 */
	void exitPost3(aParser.Post3Context ctx);
	/**
	 * Enter a parse tree produced by the {@code post1}
	 * labeled alternative in {@link aParser#postfix}.
	 * @param ctx the parse tree
	 */
	void enterPost1(aParser.Post1Context ctx);
	/**
	 * Exit a parse tree produced by the {@code post1}
	 * labeled alternative in {@link aParser#postfix}.
	 * @param ctx the parse tree
	 */
	void exitPost1(aParser.Post1Context ctx);
	/**
	 * Enter a parse tree produced by the {@code pl1}
	 * labeled alternative in {@link aParser#param_list}.
	 * @param ctx the parse tree
	 */
	void enterPl1(aParser.Pl1Context ctx);
	/**
	 * Exit a parse tree produced by the {@code pl1}
	 * labeled alternative in {@link aParser#param_list}.
	 * @param ctx the parse tree
	 */
	void exitPl1(aParser.Pl1Context ctx);
	/**
	 * Enter a parse tree produced by the {@code pl2}
	 * labeled alternative in {@link aParser#param_list}.
	 * @param ctx the parse tree
	 */
	void enterPl2(aParser.Pl2Context ctx);
	/**
	 * Exit a parse tree produced by the {@code pl2}
	 * labeled alternative in {@link aParser#param_list}.
	 * @param ctx the parse tree
	 */
	void exitPl2(aParser.Pl2Context ctx);
	/**
	 * Enter a parse tree produced by the {@code unary1}
	 * labeled alternative in {@link aParser#unary}.
	 * @param ctx the parse tree
	 */
	void enterUnary1(aParser.Unary1Context ctx);
	/**
	 * Exit a parse tree produced by the {@code unary1}
	 * labeled alternative in {@link aParser#unary}.
	 * @param ctx the parse tree
	 */
	void exitUnary1(aParser.Unary1Context ctx);
	/**
	 * Enter a parse tree produced by the {@code unary2}
	 * labeled alternative in {@link aParser#unary}.
	 * @param ctx the parse tree
	 */
	void enterUnary2(aParser.Unary2Context ctx);
	/**
	 * Exit a parse tree produced by the {@code unary2}
	 * labeled alternative in {@link aParser#unary}.
	 * @param ctx the parse tree
	 */
	void exitUnary2(aParser.Unary2Context ctx);
	/**
	 * Enter a parse tree produced by the {@code unary3}
	 * labeled alternative in {@link aParser#unary}.
	 * @param ctx the parse tree
	 */
	void enterUnary3(aParser.Unary3Context ctx);
	/**
	 * Exit a parse tree produced by the {@code unary3}
	 * labeled alternative in {@link aParser#unary}.
	 * @param ctx the parse tree
	 */
	void exitUnary3(aParser.Unary3Context ctx);
	/**
	 * Enter a parse tree produced by the {@code unary4}
	 * labeled alternative in {@link aParser#unary}.
	 * @param ctx the parse tree
	 */
	void enterUnary4(aParser.Unary4Context ctx);
	/**
	 * Exit a parse tree produced by the {@code unary4}
	 * labeled alternative in {@link aParser#unary}.
	 * @param ctx the parse tree
	 */
	void exitUnary4(aParser.Unary4Context ctx);
	/**
	 * Enter a parse tree produced by the {@code unary5}
	 * labeled alternative in {@link aParser#unary}.
	 * @param ctx the parse tree
	 */
	void enterUnary5(aParser.Unary5Context ctx);
	/**
	 * Exit a parse tree produced by the {@code unary5}
	 * labeled alternative in {@link aParser#unary}.
	 * @param ctx the parse tree
	 */
	void exitUnary5(aParser.Unary5Context ctx);
	/**
	 * Enter a parse tree produced by {@link aParser#unaryOperator}.
	 * @param ctx the parse tree
	 */
	void enterUnaryOperator(aParser.UnaryOperatorContext ctx);
	/**
	 * Exit a parse tree produced by {@link aParser#unaryOperator}.
	 * @param ctx the parse tree
	 */
	void exitUnaryOperator(aParser.UnaryOperatorContext ctx);
	/**
	 * Enter a parse tree produced by the {@code me1}
	 * labeled alternative in {@link aParser#multiplicativeExpression}.
	 * @param ctx the parse tree
	 */
	void enterMe1(aParser.Me1Context ctx);
	/**
	 * Exit a parse tree produced by the {@code me1}
	 * labeled alternative in {@link aParser#multiplicativeExpression}.
	 * @param ctx the parse tree
	 */
	void exitMe1(aParser.Me1Context ctx);
	/**
	 * Enter a parse tree produced by the {@code me3}
	 * labeled alternative in {@link aParser#multiplicativeExpression}.
	 * @param ctx the parse tree
	 */
	void enterMe3(aParser.Me3Context ctx);
	/**
	 * Exit a parse tree produced by the {@code me3}
	 * labeled alternative in {@link aParser#multiplicativeExpression}.
	 * @param ctx the parse tree
	 */
	void exitMe3(aParser.Me3Context ctx);
	/**
	 * Enter a parse tree produced by the {@code me2}
	 * labeled alternative in {@link aParser#multiplicativeExpression}.
	 * @param ctx the parse tree
	 */
	void enterMe2(aParser.Me2Context ctx);
	/**
	 * Exit a parse tree produced by the {@code me2}
	 * labeled alternative in {@link aParser#multiplicativeExpression}.
	 * @param ctx the parse tree
	 */
	void exitMe2(aParser.Me2Context ctx);
	/**
	 * Enter a parse tree produced by the {@code me4}
	 * labeled alternative in {@link aParser#multiplicativeExpression}.
	 * @param ctx the parse tree
	 */
	void enterMe4(aParser.Me4Context ctx);
	/**
	 * Exit a parse tree produced by the {@code me4}
	 * labeled alternative in {@link aParser#multiplicativeExpression}.
	 * @param ctx the parse tree
	 */
	void exitMe4(aParser.Me4Context ctx);
	/**
	 * Enter a parse tree produced by the {@code add2}
	 * labeled alternative in {@link aParser#additiveExpression}.
	 * @param ctx the parse tree
	 */
	void enterAdd2(aParser.Add2Context ctx);
	/**
	 * Exit a parse tree produced by the {@code add2}
	 * labeled alternative in {@link aParser#additiveExpression}.
	 * @param ctx the parse tree
	 */
	void exitAdd2(aParser.Add2Context ctx);
	/**
	 * Enter a parse tree produced by the {@code add1}
	 * labeled alternative in {@link aParser#additiveExpression}.
	 * @param ctx the parse tree
	 */
	void enterAdd1(aParser.Add1Context ctx);
	/**
	 * Exit a parse tree produced by the {@code add1}
	 * labeled alternative in {@link aParser#additiveExpression}.
	 * @param ctx the parse tree
	 */
	void exitAdd1(aParser.Add1Context ctx);
	/**
	 * Enter a parse tree produced by the {@code add3}
	 * labeled alternative in {@link aParser#additiveExpression}.
	 * @param ctx the parse tree
	 */
	void enterAdd3(aParser.Add3Context ctx);
	/**
	 * Exit a parse tree produced by the {@code add3}
	 * labeled alternative in {@link aParser#additiveExpression}.
	 * @param ctx the parse tree
	 */
	void exitAdd3(aParser.Add3Context ctx);
	/**
	 * Enter a parse tree produced by the {@code shift1}
	 * labeled alternative in {@link aParser#shiftExpression}.
	 * @param ctx the parse tree
	 */
	void enterShift1(aParser.Shift1Context ctx);
	/**
	 * Exit a parse tree produced by the {@code shift1}
	 * labeled alternative in {@link aParser#shiftExpression}.
	 * @param ctx the parse tree
	 */
	void exitShift1(aParser.Shift1Context ctx);
	/**
	 * Enter a parse tree produced by the {@code shift2}
	 * labeled alternative in {@link aParser#shiftExpression}.
	 * @param ctx the parse tree
	 */
	void enterShift2(aParser.Shift2Context ctx);
	/**
	 * Exit a parse tree produced by the {@code shift2}
	 * labeled alternative in {@link aParser#shiftExpression}.
	 * @param ctx the parse tree
	 */
	void exitShift2(aParser.Shift2Context ctx);
	/**
	 * Enter a parse tree produced by the {@code shift3}
	 * labeled alternative in {@link aParser#shiftExpression}.
	 * @param ctx the parse tree
	 */
	void enterShift3(aParser.Shift3Context ctx);
	/**
	 * Exit a parse tree produced by the {@code shift3}
	 * labeled alternative in {@link aParser#shiftExpression}.
	 * @param ctx the parse tree
	 */
	void exitShift3(aParser.Shift3Context ctx);
	/**
	 * Enter a parse tree produced by the {@code relation5}
	 * labeled alternative in {@link aParser#relationalExpression}.
	 * @param ctx the parse tree
	 */
	void enterRelation5(aParser.Relation5Context ctx);
	/**
	 * Exit a parse tree produced by the {@code relation5}
	 * labeled alternative in {@link aParser#relationalExpression}.
	 * @param ctx the parse tree
	 */
	void exitRelation5(aParser.Relation5Context ctx);
	/**
	 * Enter a parse tree produced by the {@code relation4}
	 * labeled alternative in {@link aParser#relationalExpression}.
	 * @param ctx the parse tree
	 */
	void enterRelation4(aParser.Relation4Context ctx);
	/**
	 * Exit a parse tree produced by the {@code relation4}
	 * labeled alternative in {@link aParser#relationalExpression}.
	 * @param ctx the parse tree
	 */
	void exitRelation4(aParser.Relation4Context ctx);
	/**
	 * Enter a parse tree produced by the {@code relation3}
	 * labeled alternative in {@link aParser#relationalExpression}.
	 * @param ctx the parse tree
	 */
	void enterRelation3(aParser.Relation3Context ctx);
	/**
	 * Exit a parse tree produced by the {@code relation3}
	 * labeled alternative in {@link aParser#relationalExpression}.
	 * @param ctx the parse tree
	 */
	void exitRelation3(aParser.Relation3Context ctx);
	/**
	 * Enter a parse tree produced by the {@code relation2}
	 * labeled alternative in {@link aParser#relationalExpression}.
	 * @param ctx the parse tree
	 */
	void enterRelation2(aParser.Relation2Context ctx);
	/**
	 * Exit a parse tree produced by the {@code relation2}
	 * labeled alternative in {@link aParser#relationalExpression}.
	 * @param ctx the parse tree
	 */
	void exitRelation2(aParser.Relation2Context ctx);
	/**
	 * Enter a parse tree produced by the {@code relation1}
	 * labeled alternative in {@link aParser#relationalExpression}.
	 * @param ctx the parse tree
	 */
	void enterRelation1(aParser.Relation1Context ctx);
	/**
	 * Exit a parse tree produced by the {@code relation1}
	 * labeled alternative in {@link aParser#relationalExpression}.
	 * @param ctx the parse tree
	 */
	void exitRelation1(aParser.Relation1Context ctx);
	/**
	 * Enter a parse tree produced by the {@code equal1}
	 * labeled alternative in {@link aParser#equalityExpression}.
	 * @param ctx the parse tree
	 */
	void enterEqual1(aParser.Equal1Context ctx);
	/**
	 * Exit a parse tree produced by the {@code equal1}
	 * labeled alternative in {@link aParser#equalityExpression}.
	 * @param ctx the parse tree
	 */
	void exitEqual1(aParser.Equal1Context ctx);
	/**
	 * Enter a parse tree produced by the {@code equal2}
	 * labeled alternative in {@link aParser#equalityExpression}.
	 * @param ctx the parse tree
	 */
	void enterEqual2(aParser.Equal2Context ctx);
	/**
	 * Exit a parse tree produced by the {@code equal2}
	 * labeled alternative in {@link aParser#equalityExpression}.
	 * @param ctx the parse tree
	 */
	void exitEqual2(aParser.Equal2Context ctx);
	/**
	 * Enter a parse tree produced by the {@code equal3}
	 * labeled alternative in {@link aParser#equalityExpression}.
	 * @param ctx the parse tree
	 */
	void enterEqual3(aParser.Equal3Context ctx);
	/**
	 * Exit a parse tree produced by the {@code equal3}
	 * labeled alternative in {@link aParser#equalityExpression}.
	 * @param ctx the parse tree
	 */
	void exitEqual3(aParser.Equal3Context ctx);
	/**
	 * Enter a parse tree produced by the {@code and2}
	 * labeled alternative in {@link aParser#andExpression}.
	 * @param ctx the parse tree
	 */
	void enterAnd2(aParser.And2Context ctx);
	/**
	 * Exit a parse tree produced by the {@code and2}
	 * labeled alternative in {@link aParser#andExpression}.
	 * @param ctx the parse tree
	 */
	void exitAnd2(aParser.And2Context ctx);
	/**
	 * Enter a parse tree produced by the {@code and1}
	 * labeled alternative in {@link aParser#andExpression}.
	 * @param ctx the parse tree
	 */
	void enterAnd1(aParser.And1Context ctx);
	/**
	 * Exit a parse tree produced by the {@code and1}
	 * labeled alternative in {@link aParser#andExpression}.
	 * @param ctx the parse tree
	 */
	void exitAnd1(aParser.And1Context ctx);
	/**
	 * Enter a parse tree produced by the {@code eor2}
	 * labeled alternative in {@link aParser#exclusiveOrExpression}.
	 * @param ctx the parse tree
	 */
	void enterEor2(aParser.Eor2Context ctx);
	/**
	 * Exit a parse tree produced by the {@code eor2}
	 * labeled alternative in {@link aParser#exclusiveOrExpression}.
	 * @param ctx the parse tree
	 */
	void exitEor2(aParser.Eor2Context ctx);
	/**
	 * Enter a parse tree produced by the {@code eor1}
	 * labeled alternative in {@link aParser#exclusiveOrExpression}.
	 * @param ctx the parse tree
	 */
	void enterEor1(aParser.Eor1Context ctx);
	/**
	 * Exit a parse tree produced by the {@code eor1}
	 * labeled alternative in {@link aParser#exclusiveOrExpression}.
	 * @param ctx the parse tree
	 */
	void exitEor1(aParser.Eor1Context ctx);
	/**
	 * Enter a parse tree produced by the {@code ior1}
	 * labeled alternative in {@link aParser#inclusiveOrExpression}.
	 * @param ctx the parse tree
	 */
	void enterIor1(aParser.Ior1Context ctx);
	/**
	 * Exit a parse tree produced by the {@code ior1}
	 * labeled alternative in {@link aParser#inclusiveOrExpression}.
	 * @param ctx the parse tree
	 */
	void exitIor1(aParser.Ior1Context ctx);
	/**
	 * Enter a parse tree produced by the {@code ior2}
	 * labeled alternative in {@link aParser#inclusiveOrExpression}.
	 * @param ctx the parse tree
	 */
	void enterIor2(aParser.Ior2Context ctx);
	/**
	 * Exit a parse tree produced by the {@code ior2}
	 * labeled alternative in {@link aParser#inclusiveOrExpression}.
	 * @param ctx the parse tree
	 */
	void exitIor2(aParser.Ior2Context ctx);
	/**
	 * Enter a parse tree produced by the {@code land2}
	 * labeled alternative in {@link aParser#logicalAndExpression}.
	 * @param ctx the parse tree
	 */
	void enterLand2(aParser.Land2Context ctx);
	/**
	 * Exit a parse tree produced by the {@code land2}
	 * labeled alternative in {@link aParser#logicalAndExpression}.
	 * @param ctx the parse tree
	 */
	void exitLand2(aParser.Land2Context ctx);
	/**
	 * Enter a parse tree produced by the {@code land1}
	 * labeled alternative in {@link aParser#logicalAndExpression}.
	 * @param ctx the parse tree
	 */
	void enterLand1(aParser.Land1Context ctx);
	/**
	 * Exit a parse tree produced by the {@code land1}
	 * labeled alternative in {@link aParser#logicalAndExpression}.
	 * @param ctx the parse tree
	 */
	void exitLand1(aParser.Land1Context ctx);
	/**
	 * Enter a parse tree produced by the {@code lor1}
	 * labeled alternative in {@link aParser#logicalOrExpression}.
	 * @param ctx the parse tree
	 */
	void enterLor1(aParser.Lor1Context ctx);
	/**
	 * Exit a parse tree produced by the {@code lor1}
	 * labeled alternative in {@link aParser#logicalOrExpression}.
	 * @param ctx the parse tree
	 */
	void exitLor1(aParser.Lor1Context ctx);
	/**
	 * Enter a parse tree produced by the {@code lor2}
	 * labeled alternative in {@link aParser#logicalOrExpression}.
	 * @param ctx the parse tree
	 */
	void enterLor2(aParser.Lor2Context ctx);
	/**
	 * Exit a parse tree produced by the {@code lor2}
	 * labeled alternative in {@link aParser#logicalOrExpression}.
	 * @param ctx the parse tree
	 */
	void exitLor2(aParser.Lor2Context ctx);
	/**
	 * Enter a parse tree produced by the {@code assign1}
	 * labeled alternative in {@link aParser#assignmentExpression}.
	 * @param ctx the parse tree
	 */
	void enterAssign1(aParser.Assign1Context ctx);
	/**
	 * Exit a parse tree produced by the {@code assign1}
	 * labeled alternative in {@link aParser#assignmentExpression}.
	 * @param ctx the parse tree
	 */
	void exitAssign1(aParser.Assign1Context ctx);
	/**
	 * Enter a parse tree produced by the {@code assign2}
	 * labeled alternative in {@link aParser#assignmentExpression}.
	 * @param ctx the parse tree
	 */
	void enterAssign2(aParser.Assign2Context ctx);
	/**
	 * Exit a parse tree produced by the {@code assign2}
	 * labeled alternative in {@link aParser#assignmentExpression}.
	 * @param ctx the parse tree
	 */
	void exitAssign2(aParser.Assign2Context ctx);
	/**
	 * Enter a parse tree produced by {@link aParser#assignmentOperator}.
	 * @param ctx the parse tree
	 */
	void enterAssignmentOperator(aParser.AssignmentOperatorContext ctx);
	/**
	 * Exit a parse tree produced by {@link aParser#assignmentOperator}.
	 * @param ctx the parse tree
	 */
	void exitAssignmentOperator(aParser.AssignmentOperatorContext ctx);
	/**
	 * Enter a parse tree produced by the {@code dim1}
	 * labeled alternative in {@link aParser#dim_expr}.
	 * @param ctx the parse tree
	 */
	void enterDim1(aParser.Dim1Context ctx);
	/**
	 * Exit a parse tree produced by the {@code dim1}
	 * labeled alternative in {@link aParser#dim_expr}.
	 * @param ctx the parse tree
	 */
	void exitDim1(aParser.Dim1Context ctx);
	/**
	 * Enter a parse tree produced by the {@code dim2}
	 * labeled alternative in {@link aParser#dim_expr}.
	 * @param ctx the parse tree
	 */
	void enterDim2(aParser.Dim2Context ctx);
	/**
	 * Exit a parse tree produced by the {@code dim2}
	 * labeled alternative in {@link aParser#dim_expr}.
	 * @param ctx the parse tree
	 */
	void exitDim2(aParser.Dim2Context ctx);
	/**
	 * Enter a parse tree produced by the {@code dim3}
	 * labeled alternative in {@link aParser#dimexpr}.
	 * @param ctx the parse tree
	 */
	void enterDim3(aParser.Dim3Context ctx);
	/**
	 * Exit a parse tree produced by the {@code dim3}
	 * labeled alternative in {@link aParser#dimexpr}.
	 * @param ctx the parse tree
	 */
	void exitDim3(aParser.Dim3Context ctx);
	/**
	 * Enter a parse tree produced by the {@code dim4}
	 * labeled alternative in {@link aParser#dimexpr}.
	 * @param ctx the parse tree
	 */
	void enterDim4(aParser.Dim4Context ctx);
	/**
	 * Exit a parse tree produced by the {@code dim4}
	 * labeled alternative in {@link aParser#dimexpr}.
	 * @param ctx the parse tree
	 */
	void exitDim4(aParser.Dim4Context ctx);
	/**
	 * Enter a parse tree produced by the {@code expr1}
	 * labeled alternative in {@link aParser#expr}.
	 * @param ctx the parse tree
	 */
	void enterExpr1(aParser.Expr1Context ctx);
	/**
	 * Exit a parse tree produced by the {@code expr1}
	 * labeled alternative in {@link aParser#expr}.
	 * @param ctx the parse tree
	 */
	void exitExpr1(aParser.Expr1Context ctx);
	/**
	 * Enter a parse tree produced by the {@code expr2}
	 * labeled alternative in {@link aParser#expr}.
	 * @param ctx the parse tree
	 */
	void enterExpr2(aParser.Expr2Context ctx);
	/**
	 * Exit a parse tree produced by the {@code expr2}
	 * labeled alternative in {@link aParser#expr}.
	 * @param ctx the parse tree
	 */
	void exitExpr2(aParser.Expr2Context ctx);
	/**
	 * Enter a parse tree produced by the {@code const1}
	 * labeled alternative in {@link aParser#constant}.
	 * @param ctx the parse tree
	 */
	void enterConst1(aParser.Const1Context ctx);
	/**
	 * Exit a parse tree produced by the {@code const1}
	 * labeled alternative in {@link aParser#constant}.
	 * @param ctx the parse tree
	 */
	void exitConst1(aParser.Const1Context ctx);
	/**
	 * Enter a parse tree produced by the {@code const2}
	 * labeled alternative in {@link aParser#constant}.
	 * @param ctx the parse tree
	 */
	void enterConst2(aParser.Const2Context ctx);
	/**
	 * Exit a parse tree produced by the {@code const2}
	 * labeled alternative in {@link aParser#constant}.
	 * @param ctx the parse tree
	 */
	void exitConst2(aParser.Const2Context ctx);
	/**
	 * Enter a parse tree produced by the {@code const3}
	 * labeled alternative in {@link aParser#constant}.
	 * @param ctx the parse tree
	 */
	void enterConst3(aParser.Const3Context ctx);
	/**
	 * Exit a parse tree produced by the {@code const3}
	 * labeled alternative in {@link aParser#constant}.
	 * @param ctx the parse tree
	 */
	void exitConst3(aParser.Const3Context ctx);
	/**
	 * Enter a parse tree produced by the {@code const4}
	 * labeled alternative in {@link aParser#constant}.
	 * @param ctx the parse tree
	 */
	void enterConst4(aParser.Const4Context ctx);
	/**
	 * Exit a parse tree produced by the {@code const4}
	 * labeled alternative in {@link aParser#constant}.
	 * @param ctx the parse tree
	 */
	void exitConst4(aParser.Const4Context ctx);
}