// Generated from a.g4 by ANTLR 4.5
import org.antlr.v4.runtime.atn.*;
import org.antlr.v4.runtime.dfa.DFA;
import org.antlr.v4.runtime.*;
import org.antlr.v4.runtime.misc.*;
import org.antlr.v4.runtime.tree.*;
import java.util.List;
import java.util.Iterator;
import java.util.ArrayList;

@SuppressWarnings({"all", "warnings", "unchecked", "unused", "cast"})
public class aParser extends Parser {
	static { RuntimeMetaData.checkVersion("4.5", RuntimeMetaData.VERSION); }

	protected static final DFA[] _decisionToDFA;
	protected static final PredictionContextCache _sharedContextCache =
		new PredictionContextCache();
	public static final int
		BoolConstant=1, Bool=2, Break=3, Class=4, Continue=5, For=6, If=7, Else=8, 
		Int=9, New=10, Null=11, Return=12, String=13, True=14, False=15, Void=16, 
		While=17, LeftParen=18, RightParen=19, LeftBracket=20, RightBracket=21, 
		LeftBrace=22, RightBrace=23, Plus=24, PlusPlus=25, Minus=26, MinusMinus=27, 
		Star=28, Div=29, Mod=30, Equalequal=31, Less=32, LessEqual=33, NotEqual=34, 
		Greater=35, GreaterEqual=36, AndAnd=37, OrOr=38, Not=39, LeftShift=40, 
		RightShift=41, Or=42, Tilde=43, Caret=44, And=45, Assign=46, Dot=47, Question=48, 
		Colon=49, Semi=50, Comma=51, WhiteSpace=52, ID=53, Nondigit=54, IntegerConstant=55, 
		Digit=56, NonzeroDigit=57, Sign=58, DigitSequence=59, StringConstant=60, 
		EscapeSequence=61, LineComment=62;
	public static final int
		RULE_main = 0, RULE_program = 1, RULE_block_stmt = 2, RULE_stmt_list = 3, 
		RULE_var_decl_stmt = 4, RULE_class_decl = 5, RULE_member_decl_list = 6, 
		RULE_func_decl = 7, RULE_param_decl_list = 8, RULE_type = 9, RULE_stmt = 10, 
		RULE_primary = 11, RULE_postfix = 12, RULE_param_list = 13, RULE_unary = 14, 
		RULE_unaryOperator = 15, RULE_multiplicativeExpression = 16, RULE_additiveExpression = 17, 
		RULE_shiftExpression = 18, RULE_relationalExpression = 19, RULE_equalityExpression = 20, 
		RULE_andExpression = 21, RULE_exclusiveOrExpression = 22, RULE_inclusiveOrExpression = 23, 
		RULE_logicalAndExpression = 24, RULE_logicalOrExpression = 25, RULE_assignmentExpression = 26, 
		RULE_assignmentOperator = 27, RULE_dim_expr = 28, RULE_dimexpr = 29, RULE_expr = 30, 
		RULE_constant = 31;
	public static final String[] ruleNames = {
		"main", "program", "block_stmt", "stmt_list", "var_decl_stmt", "class_decl", 
		"member_decl_list", "func_decl", "param_decl_list", "type", "stmt", "primary", 
		"postfix", "param_list", "unary", "unaryOperator", "multiplicativeExpression", 
		"additiveExpression", "shiftExpression", "relationalExpression", "equalityExpression", 
		"andExpression", "exclusiveOrExpression", "inclusiveOrExpression", "logicalAndExpression", 
		"logicalOrExpression", "assignmentExpression", "assignmentOperator", "dim_expr", 
		"dimexpr", "expr", "constant"
	};

	private static final String[] _LITERAL_NAMES = {
		null, null, "'bool'", "'break'", "'class'", "'continue'", "'for'", "'if'", 
		"'else'", "'int'", "'new'", "'null'", "'return'", "'string'", "'true'", 
		"'false'", "'void'", "'while'", "'('", "')'", "'['", "']'", "'{'", "'}'", 
		"'+'", "'++'", "'-'", "'--'", "'*'", "'/'", "'%'", "'=='", "'<'", "'<='", 
		"'!='", "'>'", "'>='", "'&&'", "'||'", "'!'", "'<<'", "'>>'", "'|'", "'~'", 
		"'^'", "'&'", "'='", "'.'", "'?'", "':'", "';'", "','"
	};
	private static final String[] _SYMBOLIC_NAMES = {
		null, "BoolConstant", "Bool", "Break", "Class", "Continue", "For", "If", 
		"Else", "Int", "New", "Null", "Return", "String", "True", "False", "Void", 
		"While", "LeftParen", "RightParen", "LeftBracket", "RightBracket", "LeftBrace", 
		"RightBrace", "Plus", "PlusPlus", "Minus", "MinusMinus", "Star", "Div", 
		"Mod", "Equalequal", "Less", "LessEqual", "NotEqual", "Greater", "GreaterEqual", 
		"AndAnd", "OrOr", "Not", "LeftShift", "RightShift", "Or", "Tilde", "Caret", 
		"And", "Assign", "Dot", "Question", "Colon", "Semi", "Comma", "WhiteSpace", 
		"ID", "Nondigit", "IntegerConstant", "Digit", "NonzeroDigit", "Sign", 
		"DigitSequence", "StringConstant", "EscapeSequence", "LineComment"
	};
	public static final Vocabulary VOCABULARY = new VocabularyImpl(_LITERAL_NAMES, _SYMBOLIC_NAMES);

	/**
	 * @deprecated Use {@link #VOCABULARY} instead.
	 */
	@Deprecated
	public static final String[] tokenNames;
	static {
		tokenNames = new String[_SYMBOLIC_NAMES.length];
		for (int i = 0; i < tokenNames.length; i++) {
			tokenNames[i] = VOCABULARY.getLiteralName(i);
			if (tokenNames[i] == null) {
				tokenNames[i] = VOCABULARY.getSymbolicName(i);
			}

			if (tokenNames[i] == null) {
				tokenNames[i] = "<INVALID>";
			}
		}
	}

	@Override
	@Deprecated
	public String[] getTokenNames() {
		return tokenNames;
	}

	@Override

	public Vocabulary getVocabulary() {
		return VOCABULARY;
	}

	@Override
	public String getGrammarFileName() { return "a.g4"; }

	@Override
	public String[] getRuleNames() { return ruleNames; }

	@Override
	public String getSerializedATN() { return _serializedATN; }

	@Override
	public ATN getATN() { return _ATN; }

	public aParser(TokenStream input) {
		super(input);
		_interp = new ParserATNSimulator(this,_ATN,_decisionToDFA,_sharedContextCache);
	}
	public static class MainContext extends ParserRuleContext {
		public TerminalNode EOF() { return getToken(aParser.EOF, 0); }
		public ProgramContext program() {
			return getRuleContext(ProgramContext.class,0);
		}
		public MainContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_main; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof aListener ) ((aListener)listener).enterMain(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof aListener ) ((aListener)listener).exitMain(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof aVisitor ) return ((aVisitor<? extends T>)visitor).visitMain(this);
			else return visitor.visitChildren(this);
		}
	}

	public final MainContext main() throws RecognitionException {
		MainContext _localctx = new MainContext(_ctx, getState());
		enterRule(_localctx, 0, RULE_main);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(65);
			_la = _input.LA(1);
			if ((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << Bool) | (1L << Class) | (1L << Int) | (1L << String) | (1L << Void) | (1L << ID))) != 0)) {
				{
				setState(64);
				program();
				}
			}

			setState(67);
			match(EOF);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class ProgramContext extends ParserRuleContext {
		public ProgramContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_program; }
	 
		public ProgramContext() { }
		public void copyFrom(ProgramContext ctx) {
			super.copyFrom(ctx);
		}
	}
	public static class Program4Context extends ProgramContext {
		public Class_declContext class_decl() {
			return getRuleContext(Class_declContext.class,0);
		}
		public Program4Context(ProgramContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof aListener ) ((aListener)listener).enterProgram4(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof aListener ) ((aListener)listener).exitProgram4(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof aVisitor ) return ((aVisitor<? extends T>)visitor).visitProgram4(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class Program5Context extends ProgramContext {
		public Func_declContext func_decl() {
			return getRuleContext(Func_declContext.class,0);
		}
		public Program5Context(ProgramContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof aListener ) ((aListener)listener).enterProgram5(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof aListener ) ((aListener)listener).exitProgram5(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof aVisitor ) return ((aVisitor<? extends T>)visitor).visitProgram5(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class Program6Context extends ProgramContext {
		public Var_decl_stmtContext var_decl_stmt() {
			return getRuleContext(Var_decl_stmtContext.class,0);
		}
		public Program6Context(ProgramContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof aListener ) ((aListener)listener).enterProgram6(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof aListener ) ((aListener)listener).exitProgram6(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof aVisitor ) return ((aVisitor<? extends T>)visitor).visitProgram6(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class Program1Context extends ProgramContext {
		public Class_declContext class_decl() {
			return getRuleContext(Class_declContext.class,0);
		}
		public ProgramContext program() {
			return getRuleContext(ProgramContext.class,0);
		}
		public Program1Context(ProgramContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof aListener ) ((aListener)listener).enterProgram1(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof aListener ) ((aListener)listener).exitProgram1(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof aVisitor ) return ((aVisitor<? extends T>)visitor).visitProgram1(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class Program2Context extends ProgramContext {
		public Func_declContext func_decl() {
			return getRuleContext(Func_declContext.class,0);
		}
		public ProgramContext program() {
			return getRuleContext(ProgramContext.class,0);
		}
		public Program2Context(ProgramContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof aListener ) ((aListener)listener).enterProgram2(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof aListener ) ((aListener)listener).exitProgram2(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof aVisitor ) return ((aVisitor<? extends T>)visitor).visitProgram2(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class Program3Context extends ProgramContext {
		public Var_decl_stmtContext var_decl_stmt() {
			return getRuleContext(Var_decl_stmtContext.class,0);
		}
		public ProgramContext program() {
			return getRuleContext(ProgramContext.class,0);
		}
		public Program3Context(ProgramContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof aListener ) ((aListener)listener).enterProgram3(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof aListener ) ((aListener)listener).exitProgram3(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof aVisitor ) return ((aVisitor<? extends T>)visitor).visitProgram3(this);
			else return visitor.visitChildren(this);
		}
	}

	public final ProgramContext program() throws RecognitionException {
		ProgramContext _localctx = new ProgramContext(_ctx, getState());
		enterRule(_localctx, 2, RULE_program);
		try {
			setState(81);
			switch ( getInterpreter().adaptivePredict(_input,1,_ctx) ) {
			case 1:
				_localctx = new Program4Context(_localctx);
				enterOuterAlt(_localctx, 1);
				{
				setState(69);
				class_decl();
				}
				break;
			case 2:
				_localctx = new Program5Context(_localctx);
				enterOuterAlt(_localctx, 2);
				{
				setState(70);
				func_decl();
				}
				break;
			case 3:
				_localctx = new Program6Context(_localctx);
				enterOuterAlt(_localctx, 3);
				{
				setState(71);
				var_decl_stmt();
				}
				break;
			case 4:
				_localctx = new Program1Context(_localctx);
				enterOuterAlt(_localctx, 4);
				{
				setState(72);
				class_decl();
				setState(73);
				program();
				}
				break;
			case 5:
				_localctx = new Program2Context(_localctx);
				enterOuterAlt(_localctx, 5);
				{
				setState(75);
				func_decl();
				setState(76);
				program();
				}
				break;
			case 6:
				_localctx = new Program3Context(_localctx);
				enterOuterAlt(_localctx, 6);
				{
				setState(78);
				var_decl_stmt();
				setState(79);
				program();
				}
				break;
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Block_stmtContext extends ParserRuleContext {
		public Stmt_listContext stmt_list() {
			return getRuleContext(Stmt_listContext.class,0);
		}
		public Block_stmtContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_block_stmt; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof aListener ) ((aListener)listener).enterBlock_stmt(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof aListener ) ((aListener)listener).exitBlock_stmt(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof aVisitor ) return ((aVisitor<? extends T>)visitor).visitBlock_stmt(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Block_stmtContext block_stmt() throws RecognitionException {
		Block_stmtContext _localctx = new Block_stmtContext(_ctx, getState());
		enterRule(_localctx, 4, RULE_block_stmt);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(83);
			match(LeftBrace);
			setState(85);
			_la = _input.LA(1);
			if ((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << BoolConstant) | (1L << Bool) | (1L << Break) | (1L << Continue) | (1L << For) | (1L << If) | (1L << Int) | (1L << New) | (1L << Null) | (1L << Return) | (1L << String) | (1L << While) | (1L << LeftParen) | (1L << LeftBrace) | (1L << Plus) | (1L << PlusPlus) | (1L << Minus) | (1L << MinusMinus) | (1L << Star) | (1L << Not) | (1L << Tilde) | (1L << And) | (1L << Semi) | (1L << ID) | (1L << IntegerConstant) | (1L << StringConstant))) != 0)) {
				{
				setState(84);
				stmt_list();
				}
			}

			setState(87);
			match(RightBrace);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Stmt_listContext extends ParserRuleContext {
		public List<StmtContext> stmt() {
			return getRuleContexts(StmtContext.class);
		}
		public StmtContext stmt(int i) {
			return getRuleContext(StmtContext.class,i);
		}
		public Stmt_listContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_stmt_list; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof aListener ) ((aListener)listener).enterStmt_list(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof aListener ) ((aListener)listener).exitStmt_list(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof aVisitor ) return ((aVisitor<? extends T>)visitor).visitStmt_list(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Stmt_listContext stmt_list() throws RecognitionException {
		Stmt_listContext _localctx = new Stmt_listContext(_ctx, getState());
		enterRule(_localctx, 6, RULE_stmt_list);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(90); 
			_errHandler.sync(this);
			_la = _input.LA(1);
			do {
				{
				{
				setState(89);
				stmt();
				}
				}
				setState(92); 
				_errHandler.sync(this);
				_la = _input.LA(1);
			} while ( (((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << BoolConstant) | (1L << Bool) | (1L << Break) | (1L << Continue) | (1L << For) | (1L << If) | (1L << Int) | (1L << New) | (1L << Null) | (1L << Return) | (1L << String) | (1L << While) | (1L << LeftParen) | (1L << LeftBrace) | (1L << Plus) | (1L << PlusPlus) | (1L << Minus) | (1L << MinusMinus) | (1L << Star) | (1L << Not) | (1L << Tilde) | (1L << And) | (1L << Semi) | (1L << ID) | (1L << IntegerConstant) | (1L << StringConstant))) != 0) );
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Var_decl_stmtContext extends ParserRuleContext {
		public Var_decl_stmtContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_var_decl_stmt; }
	 
		public Var_decl_stmtContext() { }
		public void copyFrom(Var_decl_stmtContext ctx) {
			super.copyFrom(ctx);
		}
	}
	public static class Var2Context extends Var_decl_stmtContext {
		public TypeContext type() {
			return getRuleContext(TypeContext.class,0);
		}
		public TerminalNode ID() { return getToken(aParser.ID, 0); }
		public ExprContext expr() {
			return getRuleContext(ExprContext.class,0);
		}
		public Var2Context(Var_decl_stmtContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof aListener ) ((aListener)listener).enterVar2(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof aListener ) ((aListener)listener).exitVar2(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof aVisitor ) return ((aVisitor<? extends T>)visitor).visitVar2(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class Var1Context extends Var_decl_stmtContext {
		public TypeContext type() {
			return getRuleContext(TypeContext.class,0);
		}
		public TerminalNode ID() { return getToken(aParser.ID, 0); }
		public Var1Context(Var_decl_stmtContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof aListener ) ((aListener)listener).enterVar1(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof aListener ) ((aListener)listener).exitVar1(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof aVisitor ) return ((aVisitor<? extends T>)visitor).visitVar1(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Var_decl_stmtContext var_decl_stmt() throws RecognitionException {
		Var_decl_stmtContext _localctx = new Var_decl_stmtContext(_ctx, getState());
		enterRule(_localctx, 8, RULE_var_decl_stmt);
		try {
			setState(104);
			switch ( getInterpreter().adaptivePredict(_input,4,_ctx) ) {
			case 1:
				_localctx = new Var1Context(_localctx);
				enterOuterAlt(_localctx, 1);
				{
				setState(94);
				type(0);
				setState(95);
				match(ID);
				setState(96);
				match(Semi);
				}
				break;
			case 2:
				_localctx = new Var2Context(_localctx);
				enterOuterAlt(_localctx, 2);
				{
				setState(98);
				type(0);
				setState(99);
				match(ID);
				setState(100);
				match(Assign);
				setState(101);
				expr();
				setState(102);
				match(Semi);
				}
				break;
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Class_declContext extends ParserRuleContext {
		public TerminalNode Class() { return getToken(aParser.Class, 0); }
		public TerminalNode ID() { return getToken(aParser.ID, 0); }
		public Member_decl_listContext member_decl_list() {
			return getRuleContext(Member_decl_listContext.class,0);
		}
		public Class_declContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_class_decl; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof aListener ) ((aListener)listener).enterClass_decl(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof aListener ) ((aListener)listener).exitClass_decl(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof aVisitor ) return ((aVisitor<? extends T>)visitor).visitClass_decl(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Class_declContext class_decl() throws RecognitionException {
		Class_declContext _localctx = new Class_declContext(_ctx, getState());
		enterRule(_localctx, 10, RULE_class_decl);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(106);
			match(Class);
			setState(107);
			match(ID);
			setState(108);
			match(LeftBrace);
			setState(109);
			member_decl_list();
			setState(110);
			match(RightBrace);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Member_decl_listContext extends ParserRuleContext {
		public Member_decl_listContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_member_decl_list; }
	 
		public Member_decl_listContext() { }
		public void copyFrom(Member_decl_listContext ctx) {
			super.copyFrom(ctx);
		}
	}
	public static class Memberlist1Context extends Member_decl_listContext {
		public TypeContext type() {
			return getRuleContext(TypeContext.class,0);
		}
		public TerminalNode ID() { return getToken(aParser.ID, 0); }
		public Memberlist1Context(Member_decl_listContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof aListener ) ((aListener)listener).enterMemberlist1(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof aListener ) ((aListener)listener).exitMemberlist1(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof aVisitor ) return ((aVisitor<? extends T>)visitor).visitMemberlist1(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class Memberlist2Context extends Member_decl_listContext {
		public TypeContext type() {
			return getRuleContext(TypeContext.class,0);
		}
		public TerminalNode ID() { return getToken(aParser.ID, 0); }
		public Member_decl_listContext member_decl_list() {
			return getRuleContext(Member_decl_listContext.class,0);
		}
		public Memberlist2Context(Member_decl_listContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof aListener ) ((aListener)listener).enterMemberlist2(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof aListener ) ((aListener)listener).exitMemberlist2(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof aVisitor ) return ((aVisitor<? extends T>)visitor).visitMemberlist2(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Member_decl_listContext member_decl_list() throws RecognitionException {
		Member_decl_listContext _localctx = new Member_decl_listContext(_ctx, getState());
		enterRule(_localctx, 12, RULE_member_decl_list);
		try {
			setState(121);
			switch ( getInterpreter().adaptivePredict(_input,5,_ctx) ) {
			case 1:
				_localctx = new Memberlist1Context(_localctx);
				enterOuterAlt(_localctx, 1);
				{
				setState(112);
				type(0);
				setState(113);
				match(ID);
				setState(114);
				match(Semi);
				}
				break;
			case 2:
				_localctx = new Memberlist2Context(_localctx);
				enterOuterAlt(_localctx, 2);
				{
				setState(116);
				type(0);
				setState(117);
				match(ID);
				setState(118);
				match(Semi);
				setState(119);
				member_decl_list();
				}
				break;
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Func_declContext extends ParserRuleContext {
		public Func_declContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_func_decl; }
	 
		public Func_declContext() { }
		public void copyFrom(Func_declContext ctx) {
			super.copyFrom(ctx);
		}
	}
	public static class Func1Context extends Func_declContext {
		public TypeContext type() {
			return getRuleContext(TypeContext.class,0);
		}
		public TerminalNode ID() { return getToken(aParser.ID, 0); }
		public Block_stmtContext block_stmt() {
			return getRuleContext(Block_stmtContext.class,0);
		}
		public Param_decl_listContext param_decl_list() {
			return getRuleContext(Param_decl_listContext.class,0);
		}
		public Func1Context(Func_declContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof aListener ) ((aListener)listener).enterFunc1(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof aListener ) ((aListener)listener).exitFunc1(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof aVisitor ) return ((aVisitor<? extends T>)visitor).visitFunc1(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class Func2Context extends Func_declContext {
		public TerminalNode Void() { return getToken(aParser.Void, 0); }
		public TerminalNode ID() { return getToken(aParser.ID, 0); }
		public Block_stmtContext block_stmt() {
			return getRuleContext(Block_stmtContext.class,0);
		}
		public Param_decl_listContext param_decl_list() {
			return getRuleContext(Param_decl_listContext.class,0);
		}
		public Func2Context(Func_declContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof aListener ) ((aListener)listener).enterFunc2(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof aListener ) ((aListener)listener).exitFunc2(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof aVisitor ) return ((aVisitor<? extends T>)visitor).visitFunc2(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Func_declContext func_decl() throws RecognitionException {
		Func_declContext _localctx = new Func_declContext(_ctx, getState());
		enterRule(_localctx, 14, RULE_func_decl);
		int _la;
		try {
			setState(140);
			switch (_input.LA(1)) {
			case Bool:
			case Int:
			case String:
			case ID:
				_localctx = new Func1Context(_localctx);
				enterOuterAlt(_localctx, 1);
				{
				setState(123);
				type(0);
				setState(124);
				match(ID);
				setState(125);
				match(LeftParen);
				setState(127);
				_la = _input.LA(1);
				if ((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << Bool) | (1L << Int) | (1L << String) | (1L << ID))) != 0)) {
					{
					setState(126);
					param_decl_list();
					}
				}

				setState(129);
				match(RightParen);
				setState(130);
				block_stmt();
				}
				break;
			case Void:
				_localctx = new Func2Context(_localctx);
				enterOuterAlt(_localctx, 2);
				{
				setState(132);
				match(Void);
				setState(133);
				match(ID);
				setState(134);
				match(LeftParen);
				setState(136);
				_la = _input.LA(1);
				if ((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << Bool) | (1L << Int) | (1L << String) | (1L << ID))) != 0)) {
					{
					setState(135);
					param_decl_list();
					}
				}

				setState(138);
				match(RightParen);
				setState(139);
				block_stmt();
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Param_decl_listContext extends ParserRuleContext {
		public Param_decl_listContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_param_decl_list; }
	 
		public Param_decl_listContext() { }
		public void copyFrom(Param_decl_listContext ctx) {
			super.copyFrom(ctx);
		}
	}
	public static class Pdl1Context extends Param_decl_listContext {
		public TypeContext type() {
			return getRuleContext(TypeContext.class,0);
		}
		public TerminalNode ID() { return getToken(aParser.ID, 0); }
		public Pdl1Context(Param_decl_listContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof aListener ) ((aListener)listener).enterPdl1(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof aListener ) ((aListener)listener).exitPdl1(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof aVisitor ) return ((aVisitor<? extends T>)visitor).visitPdl1(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class Pdl2Context extends Param_decl_listContext {
		public TypeContext type() {
			return getRuleContext(TypeContext.class,0);
		}
		public TerminalNode ID() { return getToken(aParser.ID, 0); }
		public Param_decl_listContext param_decl_list() {
			return getRuleContext(Param_decl_listContext.class,0);
		}
		public Pdl2Context(Param_decl_listContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof aListener ) ((aListener)listener).enterPdl2(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof aListener ) ((aListener)listener).exitPdl2(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof aVisitor ) return ((aVisitor<? extends T>)visitor).visitPdl2(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Param_decl_listContext param_decl_list() throws RecognitionException {
		Param_decl_listContext _localctx = new Param_decl_listContext(_ctx, getState());
		enterRule(_localctx, 16, RULE_param_decl_list);
		try {
			setState(150);
			switch ( getInterpreter().adaptivePredict(_input,9,_ctx) ) {
			case 1:
				_localctx = new Pdl1Context(_localctx);
				enterOuterAlt(_localctx, 1);
				{
				setState(142);
				type(0);
				setState(143);
				match(ID);
				}
				break;
			case 2:
				_localctx = new Pdl2Context(_localctx);
				enterOuterAlt(_localctx, 2);
				{
				setState(145);
				type(0);
				setState(146);
				match(ID);
				setState(147);
				match(Comma);
				setState(148);
				param_decl_list();
				}
				break;
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class TypeContext extends ParserRuleContext {
		public TypeContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_type; }
	 
		public TypeContext() { }
		public void copyFrom(TypeContext ctx) {
			super.copyFrom(ctx);
		}
	}
	public static class Type5Context extends TypeContext {
		public TerminalNode Bool() { return getToken(aParser.Bool, 0); }
		public Type5Context(TypeContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof aListener ) ((aListener)listener).enterType5(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof aListener ) ((aListener)listener).exitType5(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof aVisitor ) return ((aVisitor<? extends T>)visitor).visitType5(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class Type4Context extends TypeContext {
		public TypeContext type() {
			return getRuleContext(TypeContext.class,0);
		}
		public Type4Context(TypeContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof aListener ) ((aListener)listener).enterType4(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof aListener ) ((aListener)listener).exitType4(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof aVisitor ) return ((aVisitor<? extends T>)visitor).visitType4(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class Type3Context extends TypeContext {
		public TerminalNode ID() { return getToken(aParser.ID, 0); }
		public Type3Context(TypeContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof aListener ) ((aListener)listener).enterType3(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof aListener ) ((aListener)listener).exitType3(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof aVisitor ) return ((aVisitor<? extends T>)visitor).visitType3(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class Type2Context extends TypeContext {
		public TerminalNode String() { return getToken(aParser.String, 0); }
		public Type2Context(TypeContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof aListener ) ((aListener)listener).enterType2(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof aListener ) ((aListener)listener).exitType2(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof aVisitor ) return ((aVisitor<? extends T>)visitor).visitType2(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class Type1Context extends TypeContext {
		public TerminalNode Int() { return getToken(aParser.Int, 0); }
		public Type1Context(TypeContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof aListener ) ((aListener)listener).enterType1(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof aListener ) ((aListener)listener).exitType1(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof aVisitor ) return ((aVisitor<? extends T>)visitor).visitType1(this);
			else return visitor.visitChildren(this);
		}
	}

	public final TypeContext type() throws RecognitionException {
		return type(0);
	}

	private TypeContext type(int _p) throws RecognitionException {
		ParserRuleContext _parentctx = _ctx;
		int _parentState = getState();
		TypeContext _localctx = new TypeContext(_ctx, _parentState);
		TypeContext _prevctx = _localctx;
		int _startState = 18;
		enterRecursionRule(_localctx, 18, RULE_type, _p);
		try {
			int _alt;
			enterOuterAlt(_localctx, 1);
			{
			setState(157);
			switch (_input.LA(1)) {
			case Int:
				{
				_localctx = new Type1Context(_localctx);
				_ctx = _localctx;
				_prevctx = _localctx;

				setState(153);
				match(Int);
				}
				break;
			case String:
				{
				_localctx = new Type2Context(_localctx);
				_ctx = _localctx;
				_prevctx = _localctx;
				setState(154);
				match(String);
				}
				break;
			case ID:
				{
				_localctx = new Type3Context(_localctx);
				_ctx = _localctx;
				_prevctx = _localctx;
				setState(155);
				match(ID);
				}
				break;
			case Bool:
				{
				_localctx = new Type5Context(_localctx);
				_ctx = _localctx;
				_prevctx = _localctx;
				setState(156);
				match(Bool);
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
			_ctx.stop = _input.LT(-1);
			setState(164);
			_errHandler.sync(this);
			_alt = getInterpreter().adaptivePredict(_input,11,_ctx);
			while ( _alt!=2 && _alt!=org.antlr.v4.runtime.atn.ATN.INVALID_ALT_NUMBER ) {
				if ( _alt==1 ) {
					if ( _parseListeners!=null ) triggerExitRuleEvent();
					_prevctx = _localctx;
					{
					{
					_localctx = new Type4Context(new TypeContext(_parentctx, _parentState));
					pushNewRecursionContext(_localctx, _startState, RULE_type);
					setState(159);
					if (!(precpred(_ctx, 2))) throw new FailedPredicateException(this, "precpred(_ctx, 2)");
					setState(160);
					match(LeftBracket);
					setState(161);
					match(RightBracket);
					}
					} 
				}
				setState(166);
				_errHandler.sync(this);
				_alt = getInterpreter().adaptivePredict(_input,11,_ctx);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			unrollRecursionContexts(_parentctx);
		}
		return _localctx;
	}

	public static class StmtContext extends ParserRuleContext {
		public StmtContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_stmt; }
	 
		public StmtContext() { }
		public void copyFrom(StmtContext ctx) {
			super.copyFrom(ctx);
		}
	}
	public static class Stmt2Context extends StmtContext {
		public Var_decl_stmtContext var_decl_stmt() {
			return getRuleContext(Var_decl_stmtContext.class,0);
		}
		public Stmt2Context(StmtContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof aListener ) ((aListener)listener).enterStmt2(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof aListener ) ((aListener)listener).exitStmt2(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof aVisitor ) return ((aVisitor<? extends T>)visitor).visitStmt2(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class Stmt3Context extends StmtContext {
		public ExprContext expr() {
			return getRuleContext(ExprContext.class,0);
		}
		public Stmt3Context(StmtContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof aListener ) ((aListener)listener).enterStmt3(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof aListener ) ((aListener)listener).exitStmt3(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof aVisitor ) return ((aVisitor<? extends T>)visitor).visitStmt3(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class Stmt1Context extends StmtContext {
		public Block_stmtContext block_stmt() {
			return getRuleContext(Block_stmtContext.class,0);
		}
		public Stmt1Context(StmtContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof aListener ) ((aListener)listener).enterStmt1(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof aListener ) ((aListener)listener).exitStmt1(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof aVisitor ) return ((aVisitor<? extends T>)visitor).visitStmt1(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class Stmt6Context extends StmtContext {
		public TerminalNode Return() { return getToken(aParser.Return, 0); }
		public ExprContext expr() {
			return getRuleContext(ExprContext.class,0);
		}
		public Stmt6Context(StmtContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof aListener ) ((aListener)listener).enterStmt6(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof aListener ) ((aListener)listener).exitStmt6(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof aVisitor ) return ((aVisitor<? extends T>)visitor).visitStmt6(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class Stmt10Context extends StmtContext {
		public ExprContext aa;
		public ExprContext bb;
		public ExprContext cc;
		public TerminalNode For() { return getToken(aParser.For, 0); }
		public StmtContext stmt() {
			return getRuleContext(StmtContext.class,0);
		}
		public List<ExprContext> expr() {
			return getRuleContexts(ExprContext.class);
		}
		public ExprContext expr(int i) {
			return getRuleContext(ExprContext.class,i);
		}
		public Stmt10Context(StmtContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof aListener ) ((aListener)listener).enterStmt10(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof aListener ) ((aListener)listener).exitStmt10(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof aVisitor ) return ((aVisitor<? extends T>)visitor).visitStmt10(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class Stmt7Context extends StmtContext {
		public TerminalNode Break() { return getToken(aParser.Break, 0); }
		public Stmt7Context(StmtContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof aListener ) ((aListener)listener).enterStmt7(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof aListener ) ((aListener)listener).exitStmt7(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof aVisitor ) return ((aVisitor<? extends T>)visitor).visitStmt7(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class Stmt4Context extends StmtContext {
		public TerminalNode If() { return getToken(aParser.If, 0); }
		public ExprContext expr() {
			return getRuleContext(ExprContext.class,0);
		}
		public StmtContext stmt() {
			return getRuleContext(StmtContext.class,0);
		}
		public Stmt4Context(StmtContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof aListener ) ((aListener)listener).enterStmt4(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof aListener ) ((aListener)listener).exitStmt4(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof aVisitor ) return ((aVisitor<? extends T>)visitor).visitStmt4(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class Stmt5Context extends StmtContext {
		public TerminalNode If() { return getToken(aParser.If, 0); }
		public ExprContext expr() {
			return getRuleContext(ExprContext.class,0);
		}
		public List<StmtContext> stmt() {
			return getRuleContexts(StmtContext.class);
		}
		public StmtContext stmt(int i) {
			return getRuleContext(StmtContext.class,i);
		}
		public TerminalNode Else() { return getToken(aParser.Else, 0); }
		public Stmt5Context(StmtContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof aListener ) ((aListener)listener).enterStmt5(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof aListener ) ((aListener)listener).exitStmt5(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof aVisitor ) return ((aVisitor<? extends T>)visitor).visitStmt5(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class Stmt8Context extends StmtContext {
		public TerminalNode Continue() { return getToken(aParser.Continue, 0); }
		public Stmt8Context(StmtContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof aListener ) ((aListener)listener).enterStmt8(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof aListener ) ((aListener)listener).exitStmt8(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof aVisitor ) return ((aVisitor<? extends T>)visitor).visitStmt8(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class Stmt9Context extends StmtContext {
		public TerminalNode While() { return getToken(aParser.While, 0); }
		public ExprContext expr() {
			return getRuleContext(ExprContext.class,0);
		}
		public StmtContext stmt() {
			return getRuleContext(StmtContext.class,0);
		}
		public Stmt9Context(StmtContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof aListener ) ((aListener)listener).enterStmt9(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof aListener ) ((aListener)listener).exitStmt9(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof aVisitor ) return ((aVisitor<? extends T>)visitor).visitStmt9(this);
			else return visitor.visitChildren(this);
		}
	}

	public final StmtContext stmt() throws RecognitionException {
		StmtContext _localctx = new StmtContext(_ctx, getState());
		enterRule(_localctx, 20, RULE_stmt);
		int _la;
		try {
			setState(217);
			switch ( getInterpreter().adaptivePredict(_input,17,_ctx) ) {
			case 1:
				_localctx = new Stmt1Context(_localctx);
				enterOuterAlt(_localctx, 1);
				{
				setState(167);
				block_stmt();
				}
				break;
			case 2:
				_localctx = new Stmt2Context(_localctx);
				enterOuterAlt(_localctx, 2);
				{
				setState(168);
				var_decl_stmt();
				}
				break;
			case 3:
				_localctx = new Stmt3Context(_localctx);
				enterOuterAlt(_localctx, 3);
				{
				setState(170);
				_la = _input.LA(1);
				if ((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << BoolConstant) | (1L << New) | (1L << Null) | (1L << LeftParen) | (1L << Plus) | (1L << PlusPlus) | (1L << Minus) | (1L << MinusMinus) | (1L << Star) | (1L << Not) | (1L << Tilde) | (1L << And) | (1L << ID) | (1L << IntegerConstant) | (1L << StringConstant))) != 0)) {
					{
					setState(169);
					expr();
					}
				}

				setState(172);
				match(Semi);
				}
				break;
			case 4:
				_localctx = new Stmt4Context(_localctx);
				enterOuterAlt(_localctx, 4);
				{
				setState(173);
				match(If);
				setState(174);
				match(LeftParen);
				setState(175);
				expr();
				setState(176);
				match(RightParen);
				setState(177);
				stmt();
				}
				break;
			case 5:
				_localctx = new Stmt5Context(_localctx);
				enterOuterAlt(_localctx, 5);
				{
				setState(179);
				match(If);
				setState(180);
				match(LeftParen);
				setState(181);
				expr();
				setState(182);
				match(RightParen);
				setState(183);
				stmt();
				setState(184);
				match(Else);
				setState(185);
				stmt();
				}
				break;
			case 6:
				_localctx = new Stmt6Context(_localctx);
				enterOuterAlt(_localctx, 6);
				{
				setState(187);
				match(Return);
				setState(189);
				_la = _input.LA(1);
				if ((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << BoolConstant) | (1L << New) | (1L << Null) | (1L << LeftParen) | (1L << Plus) | (1L << PlusPlus) | (1L << Minus) | (1L << MinusMinus) | (1L << Star) | (1L << Not) | (1L << Tilde) | (1L << And) | (1L << ID) | (1L << IntegerConstant) | (1L << StringConstant))) != 0)) {
					{
					setState(188);
					expr();
					}
				}

				setState(191);
				match(Semi);
				}
				break;
			case 7:
				_localctx = new Stmt7Context(_localctx);
				enterOuterAlt(_localctx, 7);
				{
				setState(192);
				match(Break);
				setState(193);
				match(Semi);
				}
				break;
			case 8:
				_localctx = new Stmt8Context(_localctx);
				enterOuterAlt(_localctx, 8);
				{
				setState(194);
				match(Continue);
				setState(195);
				match(Semi);
				}
				break;
			case 9:
				_localctx = new Stmt9Context(_localctx);
				enterOuterAlt(_localctx, 9);
				{
				setState(196);
				match(While);
				setState(197);
				match(LeftParen);
				setState(198);
				expr();
				setState(199);
				match(RightParen);
				setState(200);
				stmt();
				}
				break;
			case 10:
				_localctx = new Stmt10Context(_localctx);
				enterOuterAlt(_localctx, 10);
				{
				setState(202);
				match(For);
				setState(203);
				match(LeftParen);
				setState(205);
				_la = _input.LA(1);
				if ((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << BoolConstant) | (1L << New) | (1L << Null) | (1L << LeftParen) | (1L << Plus) | (1L << PlusPlus) | (1L << Minus) | (1L << MinusMinus) | (1L << Star) | (1L << Not) | (1L << Tilde) | (1L << And) | (1L << ID) | (1L << IntegerConstant) | (1L << StringConstant))) != 0)) {
					{
					setState(204);
					((Stmt10Context)_localctx).aa = expr();
					}
				}

				setState(207);
				match(Semi);
				setState(209);
				_la = _input.LA(1);
				if ((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << BoolConstant) | (1L << New) | (1L << Null) | (1L << LeftParen) | (1L << Plus) | (1L << PlusPlus) | (1L << Minus) | (1L << MinusMinus) | (1L << Star) | (1L << Not) | (1L << Tilde) | (1L << And) | (1L << ID) | (1L << IntegerConstant) | (1L << StringConstant))) != 0)) {
					{
					setState(208);
					((Stmt10Context)_localctx).bb = expr();
					}
				}

				setState(211);
				match(Semi);
				setState(213);
				_la = _input.LA(1);
				if ((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << BoolConstant) | (1L << New) | (1L << Null) | (1L << LeftParen) | (1L << Plus) | (1L << PlusPlus) | (1L << Minus) | (1L << MinusMinus) | (1L << Star) | (1L << Not) | (1L << Tilde) | (1L << And) | (1L << ID) | (1L << IntegerConstant) | (1L << StringConstant))) != 0)) {
					{
					setState(212);
					((Stmt10Context)_localctx).cc = expr();
					}
				}

				setState(215);
				match(RightParen);
				setState(216);
				stmt();
				}
				break;
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class PrimaryContext extends ParserRuleContext {
		public PrimaryContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_primary; }
	 
		public PrimaryContext() { }
		public void copyFrom(PrimaryContext ctx) {
			super.copyFrom(ctx);
		}
	}
	public static class Primary2Context extends PrimaryContext {
		public ConstantContext constant() {
			return getRuleContext(ConstantContext.class,0);
		}
		public Primary2Context(PrimaryContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof aListener ) ((aListener)listener).enterPrimary2(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof aListener ) ((aListener)listener).exitPrimary2(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof aVisitor ) return ((aVisitor<? extends T>)visitor).visitPrimary2(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class Primary3Context extends PrimaryContext {
		public ExprContext expr() {
			return getRuleContext(ExprContext.class,0);
		}
		public Primary3Context(PrimaryContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof aListener ) ((aListener)listener).enterPrimary3(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof aListener ) ((aListener)listener).exitPrimary3(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof aVisitor ) return ((aVisitor<? extends T>)visitor).visitPrimary3(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class Primary1Context extends PrimaryContext {
		public TerminalNode ID() { return getToken(aParser.ID, 0); }
		public Primary1Context(PrimaryContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof aListener ) ((aListener)listener).enterPrimary1(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof aListener ) ((aListener)listener).exitPrimary1(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof aVisitor ) return ((aVisitor<? extends T>)visitor).visitPrimary1(this);
			else return visitor.visitChildren(this);
		}
	}

	public final PrimaryContext primary() throws RecognitionException {
		PrimaryContext _localctx = new PrimaryContext(_ctx, getState());
		enterRule(_localctx, 22, RULE_primary);
		try {
			setState(225);
			switch (_input.LA(1)) {
			case ID:
				_localctx = new Primary1Context(_localctx);
				enterOuterAlt(_localctx, 1);
				{
				setState(219);
				match(ID);
				}
				break;
			case BoolConstant:
			case Null:
			case IntegerConstant:
			case StringConstant:
				_localctx = new Primary2Context(_localctx);
				enterOuterAlt(_localctx, 2);
				{
				setState(220);
				constant();
				}
				break;
			case LeftParen:
				_localctx = new Primary3Context(_localctx);
				enterOuterAlt(_localctx, 3);
				{
				setState(221);
				match(LeftParen);
				setState(222);
				expr();
				setState(223);
				match(RightParen);
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class PostfixContext extends ParserRuleContext {
		public PostfixContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_postfix; }
	 
		public PostfixContext() { }
		public void copyFrom(PostfixContext ctx) {
			super.copyFrom(ctx);
		}
	}
	public static class Post6Context extends PostfixContext {
		public PostfixContext postfix() {
			return getRuleContext(PostfixContext.class,0);
		}
		public Param_listContext param_list() {
			return getRuleContext(Param_listContext.class,0);
		}
		public Post6Context(PostfixContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof aListener ) ((aListener)listener).enterPost6(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof aListener ) ((aListener)listener).exitPost6(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof aVisitor ) return ((aVisitor<? extends T>)visitor).visitPost6(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class Post4Context extends PostfixContext {
		public PostfixContext postfix() {
			return getRuleContext(PostfixContext.class,0);
		}
		public Post4Context(PostfixContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof aListener ) ((aListener)listener).enterPost4(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof aListener ) ((aListener)listener).exitPost4(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof aVisitor ) return ((aVisitor<? extends T>)visitor).visitPost4(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class Post5Context extends PostfixContext {
		public PostfixContext postfix() {
			return getRuleContext(PostfixContext.class,0);
		}
		public Post5Context(PostfixContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof aListener ) ((aListener)listener).enterPost5(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof aListener ) ((aListener)listener).exitPost5(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof aVisitor ) return ((aVisitor<? extends T>)visitor).visitPost5(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class Post2Context extends PostfixContext {
		public PostfixContext postfix() {
			return getRuleContext(PostfixContext.class,0);
		}
		public ExprContext expr() {
			return getRuleContext(ExprContext.class,0);
		}
		public Post2Context(PostfixContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof aListener ) ((aListener)listener).enterPost2(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof aListener ) ((aListener)listener).exitPost2(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof aVisitor ) return ((aVisitor<? extends T>)visitor).visitPost2(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class Post3Context extends PostfixContext {
		public PostfixContext postfix() {
			return getRuleContext(PostfixContext.class,0);
		}
		public TerminalNode ID() { return getToken(aParser.ID, 0); }
		public Post3Context(PostfixContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof aListener ) ((aListener)listener).enterPost3(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof aListener ) ((aListener)listener).exitPost3(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof aVisitor ) return ((aVisitor<? extends T>)visitor).visitPost3(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class Post1Context extends PostfixContext {
		public PrimaryContext primary() {
			return getRuleContext(PrimaryContext.class,0);
		}
		public Post1Context(PostfixContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof aListener ) ((aListener)listener).enterPost1(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof aListener ) ((aListener)listener).exitPost1(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof aVisitor ) return ((aVisitor<? extends T>)visitor).visitPost1(this);
			else return visitor.visitChildren(this);
		}
	}

	public final PostfixContext postfix() throws RecognitionException {
		return postfix(0);
	}

	private PostfixContext postfix(int _p) throws RecognitionException {
		ParserRuleContext _parentctx = _ctx;
		int _parentState = getState();
		PostfixContext _localctx = new PostfixContext(_ctx, _parentState);
		PostfixContext _prevctx = _localctx;
		int _startState = 24;
		enterRecursionRule(_localctx, 24, RULE_postfix, _p);
		int _la;
		try {
			int _alt;
			enterOuterAlt(_localctx, 1);
			{
			{
			_localctx = new Post1Context(_localctx);
			_ctx = _localctx;
			_prevctx = _localctx;

			setState(228);
			primary();
			}
			_ctx.stop = _input.LT(-1);
			setState(250);
			_errHandler.sync(this);
			_alt = getInterpreter().adaptivePredict(_input,21,_ctx);
			while ( _alt!=2 && _alt!=org.antlr.v4.runtime.atn.ATN.INVALID_ALT_NUMBER ) {
				if ( _alt==1 ) {
					if ( _parseListeners!=null ) triggerExitRuleEvent();
					_prevctx = _localctx;
					{
					setState(248);
					switch ( getInterpreter().adaptivePredict(_input,20,_ctx) ) {
					case 1:
						{
						_localctx = new Post2Context(new PostfixContext(_parentctx, _parentState));
						pushNewRecursionContext(_localctx, _startState, RULE_postfix);
						setState(230);
						if (!(precpred(_ctx, 5))) throw new FailedPredicateException(this, "precpred(_ctx, 5)");
						setState(231);
						match(LeftBracket);
						setState(232);
						expr();
						setState(233);
						match(RightBracket);
						}
						break;
					case 2:
						{
						_localctx = new Post3Context(new PostfixContext(_parentctx, _parentState));
						pushNewRecursionContext(_localctx, _startState, RULE_postfix);
						setState(235);
						if (!(precpred(_ctx, 4))) throw new FailedPredicateException(this, "precpred(_ctx, 4)");
						setState(236);
						match(Dot);
						setState(237);
						match(ID);
						}
						break;
					case 3:
						{
						_localctx = new Post4Context(new PostfixContext(_parentctx, _parentState));
						pushNewRecursionContext(_localctx, _startState, RULE_postfix);
						setState(238);
						if (!(precpred(_ctx, 3))) throw new FailedPredicateException(this, "precpred(_ctx, 3)");
						setState(239);
						match(PlusPlus);
						}
						break;
					case 4:
						{
						_localctx = new Post5Context(new PostfixContext(_parentctx, _parentState));
						pushNewRecursionContext(_localctx, _startState, RULE_postfix);
						setState(240);
						if (!(precpred(_ctx, 2))) throw new FailedPredicateException(this, "precpred(_ctx, 2)");
						setState(241);
						match(MinusMinus);
						}
						break;
					case 5:
						{
						_localctx = new Post6Context(new PostfixContext(_parentctx, _parentState));
						pushNewRecursionContext(_localctx, _startState, RULE_postfix);
						setState(242);
						if (!(precpred(_ctx, 1))) throw new FailedPredicateException(this, "precpred(_ctx, 1)");
						setState(243);
						match(LeftParen);
						setState(245);
						_la = _input.LA(1);
						if ((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << BoolConstant) | (1L << New) | (1L << Null) | (1L << LeftParen) | (1L << Plus) | (1L << PlusPlus) | (1L << Minus) | (1L << MinusMinus) | (1L << Star) | (1L << Not) | (1L << Tilde) | (1L << And) | (1L << ID) | (1L << IntegerConstant) | (1L << StringConstant))) != 0)) {
							{
							setState(244);
							param_list();
							}
						}

						setState(247);
						match(RightParen);
						}
						break;
					}
					} 
				}
				setState(252);
				_errHandler.sync(this);
				_alt = getInterpreter().adaptivePredict(_input,21,_ctx);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			unrollRecursionContexts(_parentctx);
		}
		return _localctx;
	}

	public static class Param_listContext extends ParserRuleContext {
		public Param_listContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_param_list; }
	 
		public Param_listContext() { }
		public void copyFrom(Param_listContext ctx) {
			super.copyFrom(ctx);
		}
	}
	public static class Pl1Context extends Param_listContext {
		public ExprContext expr() {
			return getRuleContext(ExprContext.class,0);
		}
		public Pl1Context(Param_listContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof aListener ) ((aListener)listener).enterPl1(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof aListener ) ((aListener)listener).exitPl1(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof aVisitor ) return ((aVisitor<? extends T>)visitor).visitPl1(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class Pl2Context extends Param_listContext {
		public ExprContext expr() {
			return getRuleContext(ExprContext.class,0);
		}
		public Param_listContext param_list() {
			return getRuleContext(Param_listContext.class,0);
		}
		public Pl2Context(Param_listContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof aListener ) ((aListener)listener).enterPl2(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof aListener ) ((aListener)listener).exitPl2(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof aVisitor ) return ((aVisitor<? extends T>)visitor).visitPl2(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Param_listContext param_list() throws RecognitionException {
		Param_listContext _localctx = new Param_listContext(_ctx, getState());
		enterRule(_localctx, 26, RULE_param_list);
		try {
			setState(258);
			switch ( getInterpreter().adaptivePredict(_input,22,_ctx) ) {
			case 1:
				_localctx = new Pl1Context(_localctx);
				enterOuterAlt(_localctx, 1);
				{
				setState(253);
				expr();
				}
				break;
			case 2:
				_localctx = new Pl2Context(_localctx);
				enterOuterAlt(_localctx, 2);
				{
				setState(254);
				expr();
				setState(255);
				match(Comma);
				setState(256);
				param_list();
				}
				break;
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class UnaryContext extends ParserRuleContext {
		public UnaryContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_unary; }
	 
		public UnaryContext() { }
		public void copyFrom(UnaryContext ctx) {
			super.copyFrom(ctx);
		}
	}
	public static class Unary1Context extends UnaryContext {
		public PostfixContext postfix() {
			return getRuleContext(PostfixContext.class,0);
		}
		public Unary1Context(UnaryContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof aListener ) ((aListener)listener).enterUnary1(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof aListener ) ((aListener)listener).exitUnary1(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof aVisitor ) return ((aVisitor<? extends T>)visitor).visitUnary1(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class Unary2Context extends UnaryContext {
		public UnaryContext unary() {
			return getRuleContext(UnaryContext.class,0);
		}
		public Unary2Context(UnaryContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof aListener ) ((aListener)listener).enterUnary2(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof aListener ) ((aListener)listener).exitUnary2(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof aVisitor ) return ((aVisitor<? extends T>)visitor).visitUnary2(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class Unary5Context extends UnaryContext {
		public UnaryOperatorContext unaryOperator() {
			return getRuleContext(UnaryOperatorContext.class,0);
		}
		public UnaryContext unary() {
			return getRuleContext(UnaryContext.class,0);
		}
		public Unary5Context(UnaryContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof aListener ) ((aListener)listener).enterUnary5(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof aListener ) ((aListener)listener).exitUnary5(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof aVisitor ) return ((aVisitor<? extends T>)visitor).visitUnary5(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class Unary3Context extends UnaryContext {
		public UnaryContext unary() {
			return getRuleContext(UnaryContext.class,0);
		}
		public Unary3Context(UnaryContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof aListener ) ((aListener)listener).enterUnary3(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof aListener ) ((aListener)listener).exitUnary3(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof aVisitor ) return ((aVisitor<? extends T>)visitor).visitUnary3(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class Unary4Context extends UnaryContext {
		public UnaryContext unary() {
			return getRuleContext(UnaryContext.class,0);
		}
		public Unary4Context(UnaryContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof aListener ) ((aListener)listener).enterUnary4(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof aListener ) ((aListener)listener).exitUnary4(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof aVisitor ) return ((aVisitor<? extends T>)visitor).visitUnary4(this);
			else return visitor.visitChildren(this);
		}
	}

	public final UnaryContext unary() throws RecognitionException {
		UnaryContext _localctx = new UnaryContext(_ctx, getState());
		enterRule(_localctx, 28, RULE_unary);
		try {
			setState(270);
			switch (_input.LA(1)) {
			case BoolConstant:
			case Null:
			case LeftParen:
			case ID:
			case IntegerConstant:
			case StringConstant:
				_localctx = new Unary1Context(_localctx);
				enterOuterAlt(_localctx, 1);
				{
				setState(260);
				postfix(0);
				}
				break;
			case PlusPlus:
				_localctx = new Unary2Context(_localctx);
				enterOuterAlt(_localctx, 2);
				{
				setState(261);
				match(PlusPlus);
				setState(262);
				unary();
				}
				break;
			case MinusMinus:
				_localctx = new Unary3Context(_localctx);
				enterOuterAlt(_localctx, 3);
				{
				setState(263);
				match(MinusMinus);
				setState(264);
				unary();
				}
				break;
			case Tilde:
				_localctx = new Unary4Context(_localctx);
				enterOuterAlt(_localctx, 4);
				{
				setState(265);
				match(Tilde);
				setState(266);
				unary();
				}
				break;
			case Plus:
			case Minus:
			case Star:
			case Not:
			case And:
				_localctx = new Unary5Context(_localctx);
				enterOuterAlt(_localctx, 5);
				{
				setState(267);
				unaryOperator();
				setState(268);
				unary();
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class UnaryOperatorContext extends ParserRuleContext {
		public UnaryOperatorContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_unaryOperator; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof aListener ) ((aListener)listener).enterUnaryOperator(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof aListener ) ((aListener)listener).exitUnaryOperator(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof aVisitor ) return ((aVisitor<? extends T>)visitor).visitUnaryOperator(this);
			else return visitor.visitChildren(this);
		}
	}

	public final UnaryOperatorContext unaryOperator() throws RecognitionException {
		UnaryOperatorContext _localctx = new UnaryOperatorContext(_ctx, getState());
		enterRule(_localctx, 30, RULE_unaryOperator);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(272);
			_la = _input.LA(1);
			if ( !((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << Plus) | (1L << Minus) | (1L << Star) | (1L << Not) | (1L << And))) != 0)) ) {
			_errHandler.recoverInline(this);
			} else {
				consume();
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class MultiplicativeExpressionContext extends ParserRuleContext {
		public MultiplicativeExpressionContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_multiplicativeExpression; }
	 
		public MultiplicativeExpressionContext() { }
		public void copyFrom(MultiplicativeExpressionContext ctx) {
			super.copyFrom(ctx);
		}
	}
	public static class Me1Context extends MultiplicativeExpressionContext {
		public UnaryContext unary() {
			return getRuleContext(UnaryContext.class,0);
		}
		public Me1Context(MultiplicativeExpressionContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof aListener ) ((aListener)listener).enterMe1(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof aListener ) ((aListener)listener).exitMe1(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof aVisitor ) return ((aVisitor<? extends T>)visitor).visitMe1(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class Me3Context extends MultiplicativeExpressionContext {
		public MultiplicativeExpressionContext multiplicativeExpression() {
			return getRuleContext(MultiplicativeExpressionContext.class,0);
		}
		public UnaryContext unary() {
			return getRuleContext(UnaryContext.class,0);
		}
		public Me3Context(MultiplicativeExpressionContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof aListener ) ((aListener)listener).enterMe3(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof aListener ) ((aListener)listener).exitMe3(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof aVisitor ) return ((aVisitor<? extends T>)visitor).visitMe3(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class Me2Context extends MultiplicativeExpressionContext {
		public MultiplicativeExpressionContext multiplicativeExpression() {
			return getRuleContext(MultiplicativeExpressionContext.class,0);
		}
		public UnaryContext unary() {
			return getRuleContext(UnaryContext.class,0);
		}
		public Me2Context(MultiplicativeExpressionContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof aListener ) ((aListener)listener).enterMe2(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof aListener ) ((aListener)listener).exitMe2(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof aVisitor ) return ((aVisitor<? extends T>)visitor).visitMe2(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class Me4Context extends MultiplicativeExpressionContext {
		public MultiplicativeExpressionContext multiplicativeExpression() {
			return getRuleContext(MultiplicativeExpressionContext.class,0);
		}
		public UnaryContext unary() {
			return getRuleContext(UnaryContext.class,0);
		}
		public Me4Context(MultiplicativeExpressionContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof aListener ) ((aListener)listener).enterMe4(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof aListener ) ((aListener)listener).exitMe4(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof aVisitor ) return ((aVisitor<? extends T>)visitor).visitMe4(this);
			else return visitor.visitChildren(this);
		}
	}

	public final MultiplicativeExpressionContext multiplicativeExpression() throws RecognitionException {
		return multiplicativeExpression(0);
	}

	private MultiplicativeExpressionContext multiplicativeExpression(int _p) throws RecognitionException {
		ParserRuleContext _parentctx = _ctx;
		int _parentState = getState();
		MultiplicativeExpressionContext _localctx = new MultiplicativeExpressionContext(_ctx, _parentState);
		MultiplicativeExpressionContext _prevctx = _localctx;
		int _startState = 32;
		enterRecursionRule(_localctx, 32, RULE_multiplicativeExpression, _p);
		try {
			int _alt;
			enterOuterAlt(_localctx, 1);
			{
			{
			_localctx = new Me1Context(_localctx);
			_ctx = _localctx;
			_prevctx = _localctx;

			setState(275);
			unary();
			}
			_ctx.stop = _input.LT(-1);
			setState(288);
			_errHandler.sync(this);
			_alt = getInterpreter().adaptivePredict(_input,25,_ctx);
			while ( _alt!=2 && _alt!=org.antlr.v4.runtime.atn.ATN.INVALID_ALT_NUMBER ) {
				if ( _alt==1 ) {
					if ( _parseListeners!=null ) triggerExitRuleEvent();
					_prevctx = _localctx;
					{
					setState(286);
					switch ( getInterpreter().adaptivePredict(_input,24,_ctx) ) {
					case 1:
						{
						_localctx = new Me2Context(new MultiplicativeExpressionContext(_parentctx, _parentState));
						pushNewRecursionContext(_localctx, _startState, RULE_multiplicativeExpression);
						setState(277);
						if (!(precpred(_ctx, 3))) throw new FailedPredicateException(this, "precpred(_ctx, 3)");
						setState(278);
						match(Star);
						setState(279);
						unary();
						}
						break;
					case 2:
						{
						_localctx = new Me3Context(new MultiplicativeExpressionContext(_parentctx, _parentState));
						pushNewRecursionContext(_localctx, _startState, RULE_multiplicativeExpression);
						setState(280);
						if (!(precpred(_ctx, 2))) throw new FailedPredicateException(this, "precpred(_ctx, 2)");
						setState(281);
						match(Div);
						setState(282);
						unary();
						}
						break;
					case 3:
						{
						_localctx = new Me4Context(new MultiplicativeExpressionContext(_parentctx, _parentState));
						pushNewRecursionContext(_localctx, _startState, RULE_multiplicativeExpression);
						setState(283);
						if (!(precpred(_ctx, 1))) throw new FailedPredicateException(this, "precpred(_ctx, 1)");
						setState(284);
						match(Mod);
						setState(285);
						unary();
						}
						break;
					}
					} 
				}
				setState(290);
				_errHandler.sync(this);
				_alt = getInterpreter().adaptivePredict(_input,25,_ctx);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			unrollRecursionContexts(_parentctx);
		}
		return _localctx;
	}

	public static class AdditiveExpressionContext extends ParserRuleContext {
		public AdditiveExpressionContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_additiveExpression; }
	 
		public AdditiveExpressionContext() { }
		public void copyFrom(AdditiveExpressionContext ctx) {
			super.copyFrom(ctx);
		}
	}
	public static class Add2Context extends AdditiveExpressionContext {
		public AdditiveExpressionContext additiveExpression() {
			return getRuleContext(AdditiveExpressionContext.class,0);
		}
		public MultiplicativeExpressionContext multiplicativeExpression() {
			return getRuleContext(MultiplicativeExpressionContext.class,0);
		}
		public Add2Context(AdditiveExpressionContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof aListener ) ((aListener)listener).enterAdd2(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof aListener ) ((aListener)listener).exitAdd2(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof aVisitor ) return ((aVisitor<? extends T>)visitor).visitAdd2(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class Add1Context extends AdditiveExpressionContext {
		public MultiplicativeExpressionContext multiplicativeExpression() {
			return getRuleContext(MultiplicativeExpressionContext.class,0);
		}
		public Add1Context(AdditiveExpressionContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof aListener ) ((aListener)listener).enterAdd1(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof aListener ) ((aListener)listener).exitAdd1(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof aVisitor ) return ((aVisitor<? extends T>)visitor).visitAdd1(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class Add3Context extends AdditiveExpressionContext {
		public AdditiveExpressionContext additiveExpression() {
			return getRuleContext(AdditiveExpressionContext.class,0);
		}
		public MultiplicativeExpressionContext multiplicativeExpression() {
			return getRuleContext(MultiplicativeExpressionContext.class,0);
		}
		public Add3Context(AdditiveExpressionContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof aListener ) ((aListener)listener).enterAdd3(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof aListener ) ((aListener)listener).exitAdd3(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof aVisitor ) return ((aVisitor<? extends T>)visitor).visitAdd3(this);
			else return visitor.visitChildren(this);
		}
	}

	public final AdditiveExpressionContext additiveExpression() throws RecognitionException {
		return additiveExpression(0);
	}

	private AdditiveExpressionContext additiveExpression(int _p) throws RecognitionException {
		ParserRuleContext _parentctx = _ctx;
		int _parentState = getState();
		AdditiveExpressionContext _localctx = new AdditiveExpressionContext(_ctx, _parentState);
		AdditiveExpressionContext _prevctx = _localctx;
		int _startState = 34;
		enterRecursionRule(_localctx, 34, RULE_additiveExpression, _p);
		try {
			int _alt;
			enterOuterAlt(_localctx, 1);
			{
			{
			_localctx = new Add1Context(_localctx);
			_ctx = _localctx;
			_prevctx = _localctx;

			setState(292);
			multiplicativeExpression(0);
			}
			_ctx.stop = _input.LT(-1);
			setState(302);
			_errHandler.sync(this);
			_alt = getInterpreter().adaptivePredict(_input,27,_ctx);
			while ( _alt!=2 && _alt!=org.antlr.v4.runtime.atn.ATN.INVALID_ALT_NUMBER ) {
				if ( _alt==1 ) {
					if ( _parseListeners!=null ) triggerExitRuleEvent();
					_prevctx = _localctx;
					{
					setState(300);
					switch ( getInterpreter().adaptivePredict(_input,26,_ctx) ) {
					case 1:
						{
						_localctx = new Add2Context(new AdditiveExpressionContext(_parentctx, _parentState));
						pushNewRecursionContext(_localctx, _startState, RULE_additiveExpression);
						setState(294);
						if (!(precpred(_ctx, 2))) throw new FailedPredicateException(this, "precpred(_ctx, 2)");
						setState(295);
						match(Plus);
						setState(296);
						multiplicativeExpression(0);
						}
						break;
					case 2:
						{
						_localctx = new Add3Context(new AdditiveExpressionContext(_parentctx, _parentState));
						pushNewRecursionContext(_localctx, _startState, RULE_additiveExpression);
						setState(297);
						if (!(precpred(_ctx, 1))) throw new FailedPredicateException(this, "precpred(_ctx, 1)");
						setState(298);
						match(Minus);
						setState(299);
						multiplicativeExpression(0);
						}
						break;
					}
					} 
				}
				setState(304);
				_errHandler.sync(this);
				_alt = getInterpreter().adaptivePredict(_input,27,_ctx);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			unrollRecursionContexts(_parentctx);
		}
		return _localctx;
	}

	public static class ShiftExpressionContext extends ParserRuleContext {
		public ShiftExpressionContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_shiftExpression; }
	 
		public ShiftExpressionContext() { }
		public void copyFrom(ShiftExpressionContext ctx) {
			super.copyFrom(ctx);
		}
	}
	public static class Shift1Context extends ShiftExpressionContext {
		public AdditiveExpressionContext additiveExpression() {
			return getRuleContext(AdditiveExpressionContext.class,0);
		}
		public Shift1Context(ShiftExpressionContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof aListener ) ((aListener)listener).enterShift1(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof aListener ) ((aListener)listener).exitShift1(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof aVisitor ) return ((aVisitor<? extends T>)visitor).visitShift1(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class Shift2Context extends ShiftExpressionContext {
		public ShiftExpressionContext shiftExpression() {
			return getRuleContext(ShiftExpressionContext.class,0);
		}
		public AdditiveExpressionContext additiveExpression() {
			return getRuleContext(AdditiveExpressionContext.class,0);
		}
		public Shift2Context(ShiftExpressionContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof aListener ) ((aListener)listener).enterShift2(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof aListener ) ((aListener)listener).exitShift2(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof aVisitor ) return ((aVisitor<? extends T>)visitor).visitShift2(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class Shift3Context extends ShiftExpressionContext {
		public ShiftExpressionContext shiftExpression() {
			return getRuleContext(ShiftExpressionContext.class,0);
		}
		public AdditiveExpressionContext additiveExpression() {
			return getRuleContext(AdditiveExpressionContext.class,0);
		}
		public Shift3Context(ShiftExpressionContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof aListener ) ((aListener)listener).enterShift3(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof aListener ) ((aListener)listener).exitShift3(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof aVisitor ) return ((aVisitor<? extends T>)visitor).visitShift3(this);
			else return visitor.visitChildren(this);
		}
	}

	public final ShiftExpressionContext shiftExpression() throws RecognitionException {
		return shiftExpression(0);
	}

	private ShiftExpressionContext shiftExpression(int _p) throws RecognitionException {
		ParserRuleContext _parentctx = _ctx;
		int _parentState = getState();
		ShiftExpressionContext _localctx = new ShiftExpressionContext(_ctx, _parentState);
		ShiftExpressionContext _prevctx = _localctx;
		int _startState = 36;
		enterRecursionRule(_localctx, 36, RULE_shiftExpression, _p);
		try {
			int _alt;
			enterOuterAlt(_localctx, 1);
			{
			{
			_localctx = new Shift1Context(_localctx);
			_ctx = _localctx;
			_prevctx = _localctx;

			setState(306);
			additiveExpression(0);
			}
			_ctx.stop = _input.LT(-1);
			setState(316);
			_errHandler.sync(this);
			_alt = getInterpreter().adaptivePredict(_input,29,_ctx);
			while ( _alt!=2 && _alt!=org.antlr.v4.runtime.atn.ATN.INVALID_ALT_NUMBER ) {
				if ( _alt==1 ) {
					if ( _parseListeners!=null ) triggerExitRuleEvent();
					_prevctx = _localctx;
					{
					setState(314);
					switch ( getInterpreter().adaptivePredict(_input,28,_ctx) ) {
					case 1:
						{
						_localctx = new Shift2Context(new ShiftExpressionContext(_parentctx, _parentState));
						pushNewRecursionContext(_localctx, _startState, RULE_shiftExpression);
						setState(308);
						if (!(precpred(_ctx, 2))) throw new FailedPredicateException(this, "precpred(_ctx, 2)");
						setState(309);
						match(LeftShift);
						setState(310);
						additiveExpression(0);
						}
						break;
					case 2:
						{
						_localctx = new Shift3Context(new ShiftExpressionContext(_parentctx, _parentState));
						pushNewRecursionContext(_localctx, _startState, RULE_shiftExpression);
						setState(311);
						if (!(precpred(_ctx, 1))) throw new FailedPredicateException(this, "precpred(_ctx, 1)");
						setState(312);
						match(RightShift);
						setState(313);
						additiveExpression(0);
						}
						break;
					}
					} 
				}
				setState(318);
				_errHandler.sync(this);
				_alt = getInterpreter().adaptivePredict(_input,29,_ctx);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			unrollRecursionContexts(_parentctx);
		}
		return _localctx;
	}

	public static class RelationalExpressionContext extends ParserRuleContext {
		public RelationalExpressionContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_relationalExpression; }
	 
		public RelationalExpressionContext() { }
		public void copyFrom(RelationalExpressionContext ctx) {
			super.copyFrom(ctx);
		}
	}
	public static class Relation5Context extends RelationalExpressionContext {
		public RelationalExpressionContext relationalExpression() {
			return getRuleContext(RelationalExpressionContext.class,0);
		}
		public ShiftExpressionContext shiftExpression() {
			return getRuleContext(ShiftExpressionContext.class,0);
		}
		public Relation5Context(RelationalExpressionContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof aListener ) ((aListener)listener).enterRelation5(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof aListener ) ((aListener)listener).exitRelation5(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof aVisitor ) return ((aVisitor<? extends T>)visitor).visitRelation5(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class Relation4Context extends RelationalExpressionContext {
		public RelationalExpressionContext relationalExpression() {
			return getRuleContext(RelationalExpressionContext.class,0);
		}
		public ShiftExpressionContext shiftExpression() {
			return getRuleContext(ShiftExpressionContext.class,0);
		}
		public Relation4Context(RelationalExpressionContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof aListener ) ((aListener)listener).enterRelation4(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof aListener ) ((aListener)listener).exitRelation4(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof aVisitor ) return ((aVisitor<? extends T>)visitor).visitRelation4(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class Relation3Context extends RelationalExpressionContext {
		public RelationalExpressionContext relationalExpression() {
			return getRuleContext(RelationalExpressionContext.class,0);
		}
		public ShiftExpressionContext shiftExpression() {
			return getRuleContext(ShiftExpressionContext.class,0);
		}
		public Relation3Context(RelationalExpressionContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof aListener ) ((aListener)listener).enterRelation3(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof aListener ) ((aListener)listener).exitRelation3(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof aVisitor ) return ((aVisitor<? extends T>)visitor).visitRelation3(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class Relation2Context extends RelationalExpressionContext {
		public RelationalExpressionContext relationalExpression() {
			return getRuleContext(RelationalExpressionContext.class,0);
		}
		public ShiftExpressionContext shiftExpression() {
			return getRuleContext(ShiftExpressionContext.class,0);
		}
		public Relation2Context(RelationalExpressionContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof aListener ) ((aListener)listener).enterRelation2(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof aListener ) ((aListener)listener).exitRelation2(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof aVisitor ) return ((aVisitor<? extends T>)visitor).visitRelation2(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class Relation1Context extends RelationalExpressionContext {
		public ShiftExpressionContext shiftExpression() {
			return getRuleContext(ShiftExpressionContext.class,0);
		}
		public Relation1Context(RelationalExpressionContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof aListener ) ((aListener)listener).enterRelation1(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof aListener ) ((aListener)listener).exitRelation1(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof aVisitor ) return ((aVisitor<? extends T>)visitor).visitRelation1(this);
			else return visitor.visitChildren(this);
		}
	}

	public final RelationalExpressionContext relationalExpression() throws RecognitionException {
		return relationalExpression(0);
	}

	private RelationalExpressionContext relationalExpression(int _p) throws RecognitionException {
		ParserRuleContext _parentctx = _ctx;
		int _parentState = getState();
		RelationalExpressionContext _localctx = new RelationalExpressionContext(_ctx, _parentState);
		RelationalExpressionContext _prevctx = _localctx;
		int _startState = 38;
		enterRecursionRule(_localctx, 38, RULE_relationalExpression, _p);
		try {
			int _alt;
			enterOuterAlt(_localctx, 1);
			{
			{
			_localctx = new Relation1Context(_localctx);
			_ctx = _localctx;
			_prevctx = _localctx;

			setState(320);
			shiftExpression(0);
			}
			_ctx.stop = _input.LT(-1);
			setState(336);
			_errHandler.sync(this);
			_alt = getInterpreter().adaptivePredict(_input,31,_ctx);
			while ( _alt!=2 && _alt!=org.antlr.v4.runtime.atn.ATN.INVALID_ALT_NUMBER ) {
				if ( _alt==1 ) {
					if ( _parseListeners!=null ) triggerExitRuleEvent();
					_prevctx = _localctx;
					{
					setState(334);
					switch ( getInterpreter().adaptivePredict(_input,30,_ctx) ) {
					case 1:
						{
						_localctx = new Relation2Context(new RelationalExpressionContext(_parentctx, _parentState));
						pushNewRecursionContext(_localctx, _startState, RULE_relationalExpression);
						setState(322);
						if (!(precpred(_ctx, 4))) throw new FailedPredicateException(this, "precpred(_ctx, 4)");
						setState(323);
						match(Less);
						setState(324);
						shiftExpression(0);
						}
						break;
					case 2:
						{
						_localctx = new Relation3Context(new RelationalExpressionContext(_parentctx, _parentState));
						pushNewRecursionContext(_localctx, _startState, RULE_relationalExpression);
						setState(325);
						if (!(precpred(_ctx, 3))) throw new FailedPredicateException(this, "precpred(_ctx, 3)");
						setState(326);
						match(Greater);
						setState(327);
						shiftExpression(0);
						}
						break;
					case 3:
						{
						_localctx = new Relation4Context(new RelationalExpressionContext(_parentctx, _parentState));
						pushNewRecursionContext(_localctx, _startState, RULE_relationalExpression);
						setState(328);
						if (!(precpred(_ctx, 2))) throw new FailedPredicateException(this, "precpred(_ctx, 2)");
						setState(329);
						match(LessEqual);
						setState(330);
						shiftExpression(0);
						}
						break;
					case 4:
						{
						_localctx = new Relation5Context(new RelationalExpressionContext(_parentctx, _parentState));
						pushNewRecursionContext(_localctx, _startState, RULE_relationalExpression);
						setState(331);
						if (!(precpred(_ctx, 1))) throw new FailedPredicateException(this, "precpred(_ctx, 1)");
						setState(332);
						match(GreaterEqual);
						setState(333);
						shiftExpression(0);
						}
						break;
					}
					} 
				}
				setState(338);
				_errHandler.sync(this);
				_alt = getInterpreter().adaptivePredict(_input,31,_ctx);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			unrollRecursionContexts(_parentctx);
		}
		return _localctx;
	}

	public static class EqualityExpressionContext extends ParserRuleContext {
		public EqualityExpressionContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_equalityExpression; }
	 
		public EqualityExpressionContext() { }
		public void copyFrom(EqualityExpressionContext ctx) {
			super.copyFrom(ctx);
		}
	}
	public static class Equal1Context extends EqualityExpressionContext {
		public RelationalExpressionContext relationalExpression() {
			return getRuleContext(RelationalExpressionContext.class,0);
		}
		public Equal1Context(EqualityExpressionContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof aListener ) ((aListener)listener).enterEqual1(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof aListener ) ((aListener)listener).exitEqual1(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof aVisitor ) return ((aVisitor<? extends T>)visitor).visitEqual1(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class Equal2Context extends EqualityExpressionContext {
		public EqualityExpressionContext equalityExpression() {
			return getRuleContext(EqualityExpressionContext.class,0);
		}
		public RelationalExpressionContext relationalExpression() {
			return getRuleContext(RelationalExpressionContext.class,0);
		}
		public Equal2Context(EqualityExpressionContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof aListener ) ((aListener)listener).enterEqual2(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof aListener ) ((aListener)listener).exitEqual2(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof aVisitor ) return ((aVisitor<? extends T>)visitor).visitEqual2(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class Equal3Context extends EqualityExpressionContext {
		public EqualityExpressionContext equalityExpression() {
			return getRuleContext(EqualityExpressionContext.class,0);
		}
		public RelationalExpressionContext relationalExpression() {
			return getRuleContext(RelationalExpressionContext.class,0);
		}
		public Equal3Context(EqualityExpressionContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof aListener ) ((aListener)listener).enterEqual3(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof aListener ) ((aListener)listener).exitEqual3(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof aVisitor ) return ((aVisitor<? extends T>)visitor).visitEqual3(this);
			else return visitor.visitChildren(this);
		}
	}

	public final EqualityExpressionContext equalityExpression() throws RecognitionException {
		return equalityExpression(0);
	}

	private EqualityExpressionContext equalityExpression(int _p) throws RecognitionException {
		ParserRuleContext _parentctx = _ctx;
		int _parentState = getState();
		EqualityExpressionContext _localctx = new EqualityExpressionContext(_ctx, _parentState);
		EqualityExpressionContext _prevctx = _localctx;
		int _startState = 40;
		enterRecursionRule(_localctx, 40, RULE_equalityExpression, _p);
		try {
			int _alt;
			enterOuterAlt(_localctx, 1);
			{
			{
			_localctx = new Equal1Context(_localctx);
			_ctx = _localctx;
			_prevctx = _localctx;

			setState(340);
			relationalExpression(0);
			}
			_ctx.stop = _input.LT(-1);
			setState(350);
			_errHandler.sync(this);
			_alt = getInterpreter().adaptivePredict(_input,33,_ctx);
			while ( _alt!=2 && _alt!=org.antlr.v4.runtime.atn.ATN.INVALID_ALT_NUMBER ) {
				if ( _alt==1 ) {
					if ( _parseListeners!=null ) triggerExitRuleEvent();
					_prevctx = _localctx;
					{
					setState(348);
					switch ( getInterpreter().adaptivePredict(_input,32,_ctx) ) {
					case 1:
						{
						_localctx = new Equal2Context(new EqualityExpressionContext(_parentctx, _parentState));
						pushNewRecursionContext(_localctx, _startState, RULE_equalityExpression);
						setState(342);
						if (!(precpred(_ctx, 2))) throw new FailedPredicateException(this, "precpred(_ctx, 2)");
						setState(343);
						match(Equalequal);
						setState(344);
						relationalExpression(0);
						}
						break;
					case 2:
						{
						_localctx = new Equal3Context(new EqualityExpressionContext(_parentctx, _parentState));
						pushNewRecursionContext(_localctx, _startState, RULE_equalityExpression);
						setState(345);
						if (!(precpred(_ctx, 1))) throw new FailedPredicateException(this, "precpred(_ctx, 1)");
						setState(346);
						match(NotEqual);
						setState(347);
						relationalExpression(0);
						}
						break;
					}
					} 
				}
				setState(352);
				_errHandler.sync(this);
				_alt = getInterpreter().adaptivePredict(_input,33,_ctx);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			unrollRecursionContexts(_parentctx);
		}
		return _localctx;
	}

	public static class AndExpressionContext extends ParserRuleContext {
		public AndExpressionContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_andExpression; }
	 
		public AndExpressionContext() { }
		public void copyFrom(AndExpressionContext ctx) {
			super.copyFrom(ctx);
		}
	}
	public static class And2Context extends AndExpressionContext {
		public AndExpressionContext andExpression() {
			return getRuleContext(AndExpressionContext.class,0);
		}
		public EqualityExpressionContext equalityExpression() {
			return getRuleContext(EqualityExpressionContext.class,0);
		}
		public And2Context(AndExpressionContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof aListener ) ((aListener)listener).enterAnd2(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof aListener ) ((aListener)listener).exitAnd2(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof aVisitor ) return ((aVisitor<? extends T>)visitor).visitAnd2(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class And1Context extends AndExpressionContext {
		public EqualityExpressionContext equalityExpression() {
			return getRuleContext(EqualityExpressionContext.class,0);
		}
		public And1Context(AndExpressionContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof aListener ) ((aListener)listener).enterAnd1(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof aListener ) ((aListener)listener).exitAnd1(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof aVisitor ) return ((aVisitor<? extends T>)visitor).visitAnd1(this);
			else return visitor.visitChildren(this);
		}
	}

	public final AndExpressionContext andExpression() throws RecognitionException {
		return andExpression(0);
	}

	private AndExpressionContext andExpression(int _p) throws RecognitionException {
		ParserRuleContext _parentctx = _ctx;
		int _parentState = getState();
		AndExpressionContext _localctx = new AndExpressionContext(_ctx, _parentState);
		AndExpressionContext _prevctx = _localctx;
		int _startState = 42;
		enterRecursionRule(_localctx, 42, RULE_andExpression, _p);
		try {
			int _alt;
			enterOuterAlt(_localctx, 1);
			{
			{
			_localctx = new And1Context(_localctx);
			_ctx = _localctx;
			_prevctx = _localctx;

			setState(354);
			equalityExpression(0);
			}
			_ctx.stop = _input.LT(-1);
			setState(361);
			_errHandler.sync(this);
			_alt = getInterpreter().adaptivePredict(_input,34,_ctx);
			while ( _alt!=2 && _alt!=org.antlr.v4.runtime.atn.ATN.INVALID_ALT_NUMBER ) {
				if ( _alt==1 ) {
					if ( _parseListeners!=null ) triggerExitRuleEvent();
					_prevctx = _localctx;
					{
					{
					_localctx = new And2Context(new AndExpressionContext(_parentctx, _parentState));
					pushNewRecursionContext(_localctx, _startState, RULE_andExpression);
					setState(356);
					if (!(precpred(_ctx, 1))) throw new FailedPredicateException(this, "precpred(_ctx, 1)");
					setState(357);
					match(And);
					setState(358);
					equalityExpression(0);
					}
					} 
				}
				setState(363);
				_errHandler.sync(this);
				_alt = getInterpreter().adaptivePredict(_input,34,_ctx);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			unrollRecursionContexts(_parentctx);
		}
		return _localctx;
	}

	public static class ExclusiveOrExpressionContext extends ParserRuleContext {
		public ExclusiveOrExpressionContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_exclusiveOrExpression; }
	 
		public ExclusiveOrExpressionContext() { }
		public void copyFrom(ExclusiveOrExpressionContext ctx) {
			super.copyFrom(ctx);
		}
	}
	public static class Eor2Context extends ExclusiveOrExpressionContext {
		public ExclusiveOrExpressionContext exclusiveOrExpression() {
			return getRuleContext(ExclusiveOrExpressionContext.class,0);
		}
		public AndExpressionContext andExpression() {
			return getRuleContext(AndExpressionContext.class,0);
		}
		public Eor2Context(ExclusiveOrExpressionContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof aListener ) ((aListener)listener).enterEor2(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof aListener ) ((aListener)listener).exitEor2(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof aVisitor ) return ((aVisitor<? extends T>)visitor).visitEor2(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class Eor1Context extends ExclusiveOrExpressionContext {
		public AndExpressionContext andExpression() {
			return getRuleContext(AndExpressionContext.class,0);
		}
		public Eor1Context(ExclusiveOrExpressionContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof aListener ) ((aListener)listener).enterEor1(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof aListener ) ((aListener)listener).exitEor1(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof aVisitor ) return ((aVisitor<? extends T>)visitor).visitEor1(this);
			else return visitor.visitChildren(this);
		}
	}

	public final ExclusiveOrExpressionContext exclusiveOrExpression() throws RecognitionException {
		return exclusiveOrExpression(0);
	}

	private ExclusiveOrExpressionContext exclusiveOrExpression(int _p) throws RecognitionException {
		ParserRuleContext _parentctx = _ctx;
		int _parentState = getState();
		ExclusiveOrExpressionContext _localctx = new ExclusiveOrExpressionContext(_ctx, _parentState);
		ExclusiveOrExpressionContext _prevctx = _localctx;
		int _startState = 44;
		enterRecursionRule(_localctx, 44, RULE_exclusiveOrExpression, _p);
		try {
			int _alt;
			enterOuterAlt(_localctx, 1);
			{
			{
			_localctx = new Eor1Context(_localctx);
			_ctx = _localctx;
			_prevctx = _localctx;

			setState(365);
			andExpression(0);
			}
			_ctx.stop = _input.LT(-1);
			setState(372);
			_errHandler.sync(this);
			_alt = getInterpreter().adaptivePredict(_input,35,_ctx);
			while ( _alt!=2 && _alt!=org.antlr.v4.runtime.atn.ATN.INVALID_ALT_NUMBER ) {
				if ( _alt==1 ) {
					if ( _parseListeners!=null ) triggerExitRuleEvent();
					_prevctx = _localctx;
					{
					{
					_localctx = new Eor2Context(new ExclusiveOrExpressionContext(_parentctx, _parentState));
					pushNewRecursionContext(_localctx, _startState, RULE_exclusiveOrExpression);
					setState(367);
					if (!(precpred(_ctx, 1))) throw new FailedPredicateException(this, "precpred(_ctx, 1)");
					setState(368);
					match(Caret);
					setState(369);
					andExpression(0);
					}
					} 
				}
				setState(374);
				_errHandler.sync(this);
				_alt = getInterpreter().adaptivePredict(_input,35,_ctx);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			unrollRecursionContexts(_parentctx);
		}
		return _localctx;
	}

	public static class InclusiveOrExpressionContext extends ParserRuleContext {
		public InclusiveOrExpressionContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_inclusiveOrExpression; }
	 
		public InclusiveOrExpressionContext() { }
		public void copyFrom(InclusiveOrExpressionContext ctx) {
			super.copyFrom(ctx);
		}
	}
	public static class Ior1Context extends InclusiveOrExpressionContext {
		public ExclusiveOrExpressionContext exclusiveOrExpression() {
			return getRuleContext(ExclusiveOrExpressionContext.class,0);
		}
		public Ior1Context(InclusiveOrExpressionContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof aListener ) ((aListener)listener).enterIor1(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof aListener ) ((aListener)listener).exitIor1(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof aVisitor ) return ((aVisitor<? extends T>)visitor).visitIor1(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class Ior2Context extends InclusiveOrExpressionContext {
		public InclusiveOrExpressionContext inclusiveOrExpression() {
			return getRuleContext(InclusiveOrExpressionContext.class,0);
		}
		public ExclusiveOrExpressionContext exclusiveOrExpression() {
			return getRuleContext(ExclusiveOrExpressionContext.class,0);
		}
		public Ior2Context(InclusiveOrExpressionContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof aListener ) ((aListener)listener).enterIor2(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof aListener ) ((aListener)listener).exitIor2(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof aVisitor ) return ((aVisitor<? extends T>)visitor).visitIor2(this);
			else return visitor.visitChildren(this);
		}
	}

	public final InclusiveOrExpressionContext inclusiveOrExpression() throws RecognitionException {
		return inclusiveOrExpression(0);
	}

	private InclusiveOrExpressionContext inclusiveOrExpression(int _p) throws RecognitionException {
		ParserRuleContext _parentctx = _ctx;
		int _parentState = getState();
		InclusiveOrExpressionContext _localctx = new InclusiveOrExpressionContext(_ctx, _parentState);
		InclusiveOrExpressionContext _prevctx = _localctx;
		int _startState = 46;
		enterRecursionRule(_localctx, 46, RULE_inclusiveOrExpression, _p);
		try {
			int _alt;
			enterOuterAlt(_localctx, 1);
			{
			{
			_localctx = new Ior1Context(_localctx);
			_ctx = _localctx;
			_prevctx = _localctx;

			setState(376);
			exclusiveOrExpression(0);
			}
			_ctx.stop = _input.LT(-1);
			setState(383);
			_errHandler.sync(this);
			_alt = getInterpreter().adaptivePredict(_input,36,_ctx);
			while ( _alt!=2 && _alt!=org.antlr.v4.runtime.atn.ATN.INVALID_ALT_NUMBER ) {
				if ( _alt==1 ) {
					if ( _parseListeners!=null ) triggerExitRuleEvent();
					_prevctx = _localctx;
					{
					{
					_localctx = new Ior2Context(new InclusiveOrExpressionContext(_parentctx, _parentState));
					pushNewRecursionContext(_localctx, _startState, RULE_inclusiveOrExpression);
					setState(378);
					if (!(precpred(_ctx, 1))) throw new FailedPredicateException(this, "precpred(_ctx, 1)");
					setState(379);
					match(Or);
					setState(380);
					exclusiveOrExpression(0);
					}
					} 
				}
				setState(385);
				_errHandler.sync(this);
				_alt = getInterpreter().adaptivePredict(_input,36,_ctx);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			unrollRecursionContexts(_parentctx);
		}
		return _localctx;
	}

	public static class LogicalAndExpressionContext extends ParserRuleContext {
		public LogicalAndExpressionContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_logicalAndExpression; }
	 
		public LogicalAndExpressionContext() { }
		public void copyFrom(LogicalAndExpressionContext ctx) {
			super.copyFrom(ctx);
		}
	}
	public static class Land2Context extends LogicalAndExpressionContext {
		public LogicalAndExpressionContext logicalAndExpression() {
			return getRuleContext(LogicalAndExpressionContext.class,0);
		}
		public InclusiveOrExpressionContext inclusiveOrExpression() {
			return getRuleContext(InclusiveOrExpressionContext.class,0);
		}
		public Land2Context(LogicalAndExpressionContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof aListener ) ((aListener)listener).enterLand2(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof aListener ) ((aListener)listener).exitLand2(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof aVisitor ) return ((aVisitor<? extends T>)visitor).visitLand2(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class Land1Context extends LogicalAndExpressionContext {
		public InclusiveOrExpressionContext inclusiveOrExpression() {
			return getRuleContext(InclusiveOrExpressionContext.class,0);
		}
		public Land1Context(LogicalAndExpressionContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof aListener ) ((aListener)listener).enterLand1(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof aListener ) ((aListener)listener).exitLand1(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof aVisitor ) return ((aVisitor<? extends T>)visitor).visitLand1(this);
			else return visitor.visitChildren(this);
		}
	}

	public final LogicalAndExpressionContext logicalAndExpression() throws RecognitionException {
		return logicalAndExpression(0);
	}

	private LogicalAndExpressionContext logicalAndExpression(int _p) throws RecognitionException {
		ParserRuleContext _parentctx = _ctx;
		int _parentState = getState();
		LogicalAndExpressionContext _localctx = new LogicalAndExpressionContext(_ctx, _parentState);
		LogicalAndExpressionContext _prevctx = _localctx;
		int _startState = 48;
		enterRecursionRule(_localctx, 48, RULE_logicalAndExpression, _p);
		try {
			int _alt;
			enterOuterAlt(_localctx, 1);
			{
			{
			_localctx = new Land1Context(_localctx);
			_ctx = _localctx;
			_prevctx = _localctx;

			setState(387);
			inclusiveOrExpression(0);
			}
			_ctx.stop = _input.LT(-1);
			setState(394);
			_errHandler.sync(this);
			_alt = getInterpreter().adaptivePredict(_input,37,_ctx);
			while ( _alt!=2 && _alt!=org.antlr.v4.runtime.atn.ATN.INVALID_ALT_NUMBER ) {
				if ( _alt==1 ) {
					if ( _parseListeners!=null ) triggerExitRuleEvent();
					_prevctx = _localctx;
					{
					{
					_localctx = new Land2Context(new LogicalAndExpressionContext(_parentctx, _parentState));
					pushNewRecursionContext(_localctx, _startState, RULE_logicalAndExpression);
					setState(389);
					if (!(precpred(_ctx, 1))) throw new FailedPredicateException(this, "precpred(_ctx, 1)");
					setState(390);
					match(AndAnd);
					setState(391);
					inclusiveOrExpression(0);
					}
					} 
				}
				setState(396);
				_errHandler.sync(this);
				_alt = getInterpreter().adaptivePredict(_input,37,_ctx);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			unrollRecursionContexts(_parentctx);
		}
		return _localctx;
	}

	public static class LogicalOrExpressionContext extends ParserRuleContext {
		public LogicalOrExpressionContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_logicalOrExpression; }
	 
		public LogicalOrExpressionContext() { }
		public void copyFrom(LogicalOrExpressionContext ctx) {
			super.copyFrom(ctx);
		}
	}
	public static class Lor1Context extends LogicalOrExpressionContext {
		public LogicalAndExpressionContext logicalAndExpression() {
			return getRuleContext(LogicalAndExpressionContext.class,0);
		}
		public Lor1Context(LogicalOrExpressionContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof aListener ) ((aListener)listener).enterLor1(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof aListener ) ((aListener)listener).exitLor1(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof aVisitor ) return ((aVisitor<? extends T>)visitor).visitLor1(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class Lor2Context extends LogicalOrExpressionContext {
		public LogicalOrExpressionContext logicalOrExpression() {
			return getRuleContext(LogicalOrExpressionContext.class,0);
		}
		public LogicalAndExpressionContext logicalAndExpression() {
			return getRuleContext(LogicalAndExpressionContext.class,0);
		}
		public Lor2Context(LogicalOrExpressionContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof aListener ) ((aListener)listener).enterLor2(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof aListener ) ((aListener)listener).exitLor2(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof aVisitor ) return ((aVisitor<? extends T>)visitor).visitLor2(this);
			else return visitor.visitChildren(this);
		}
	}

	public final LogicalOrExpressionContext logicalOrExpression() throws RecognitionException {
		return logicalOrExpression(0);
	}

	private LogicalOrExpressionContext logicalOrExpression(int _p) throws RecognitionException {
		ParserRuleContext _parentctx = _ctx;
		int _parentState = getState();
		LogicalOrExpressionContext _localctx = new LogicalOrExpressionContext(_ctx, _parentState);
		LogicalOrExpressionContext _prevctx = _localctx;
		int _startState = 50;
		enterRecursionRule(_localctx, 50, RULE_logicalOrExpression, _p);
		try {
			int _alt;
			enterOuterAlt(_localctx, 1);
			{
			{
			_localctx = new Lor1Context(_localctx);
			_ctx = _localctx;
			_prevctx = _localctx;

			setState(398);
			logicalAndExpression(0);
			}
			_ctx.stop = _input.LT(-1);
			setState(405);
			_errHandler.sync(this);
			_alt = getInterpreter().adaptivePredict(_input,38,_ctx);
			while ( _alt!=2 && _alt!=org.antlr.v4.runtime.atn.ATN.INVALID_ALT_NUMBER ) {
				if ( _alt==1 ) {
					if ( _parseListeners!=null ) triggerExitRuleEvent();
					_prevctx = _localctx;
					{
					{
					_localctx = new Lor2Context(new LogicalOrExpressionContext(_parentctx, _parentState));
					pushNewRecursionContext(_localctx, _startState, RULE_logicalOrExpression);
					setState(400);
					if (!(precpred(_ctx, 1))) throw new FailedPredicateException(this, "precpred(_ctx, 1)");
					setState(401);
					match(OrOr);
					setState(402);
					logicalAndExpression(0);
					}
					} 
				}
				setState(407);
				_errHandler.sync(this);
				_alt = getInterpreter().adaptivePredict(_input,38,_ctx);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			unrollRecursionContexts(_parentctx);
		}
		return _localctx;
	}

	public static class AssignmentExpressionContext extends ParserRuleContext {
		public AssignmentExpressionContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_assignmentExpression; }
	 
		public AssignmentExpressionContext() { }
		public void copyFrom(AssignmentExpressionContext ctx) {
			super.copyFrom(ctx);
		}
	}
	public static class Assign2Context extends AssignmentExpressionContext {
		public UnaryContext unary() {
			return getRuleContext(UnaryContext.class,0);
		}
		public AssignmentOperatorContext assignmentOperator() {
			return getRuleContext(AssignmentOperatorContext.class,0);
		}
		public ExprContext expr() {
			return getRuleContext(ExprContext.class,0);
		}
		public Assign2Context(AssignmentExpressionContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof aListener ) ((aListener)listener).enterAssign2(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof aListener ) ((aListener)listener).exitAssign2(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof aVisitor ) return ((aVisitor<? extends T>)visitor).visitAssign2(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class Assign1Context extends AssignmentExpressionContext {
		public LogicalOrExpressionContext logicalOrExpression() {
			return getRuleContext(LogicalOrExpressionContext.class,0);
		}
		public Assign1Context(AssignmentExpressionContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof aListener ) ((aListener)listener).enterAssign1(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof aListener ) ((aListener)listener).exitAssign1(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof aVisitor ) return ((aVisitor<? extends T>)visitor).visitAssign1(this);
			else return visitor.visitChildren(this);
		}
	}

	public final AssignmentExpressionContext assignmentExpression() throws RecognitionException {
		AssignmentExpressionContext _localctx = new AssignmentExpressionContext(_ctx, getState());
		enterRule(_localctx, 52, RULE_assignmentExpression);
		try {
			setState(413);
			switch ( getInterpreter().adaptivePredict(_input,39,_ctx) ) {
			case 1:
				_localctx = new Assign1Context(_localctx);
				enterOuterAlt(_localctx, 1);
				{
				setState(408);
				logicalOrExpression(0);
				}
				break;
			case 2:
				_localctx = new Assign2Context(_localctx);
				enterOuterAlt(_localctx, 2);
				{
				setState(409);
				unary();
				setState(410);
				assignmentOperator();
				setState(411);
				expr();
				}
				break;
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class AssignmentOperatorContext extends ParserRuleContext {
		public AssignmentOperatorContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_assignmentOperator; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof aListener ) ((aListener)listener).enterAssignmentOperator(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof aListener ) ((aListener)listener).exitAssignmentOperator(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof aVisitor ) return ((aVisitor<? extends T>)visitor).visitAssignmentOperator(this);
			else return visitor.visitChildren(this);
		}
	}

	public final AssignmentOperatorContext assignmentOperator() throws RecognitionException {
		AssignmentOperatorContext _localctx = new AssignmentOperatorContext(_ctx, getState());
		enterRule(_localctx, 54, RULE_assignmentOperator);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(415);
			match(Assign);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Dim_exprContext extends ParserRuleContext {
		public Dim_exprContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_dim_expr; }
	 
		public Dim_exprContext() { }
		public void copyFrom(Dim_exprContext ctx) {
			super.copyFrom(ctx);
		}
	}
	public static class Dim2Context extends Dim_exprContext {
		public ExprContext expr() {
			return getRuleContext(ExprContext.class,0);
		}
		public Dim_exprContext dim_expr() {
			return getRuleContext(Dim_exprContext.class,0);
		}
		public Dim2Context(Dim_exprContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof aListener ) ((aListener)listener).enterDim2(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof aListener ) ((aListener)listener).exitDim2(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof aVisitor ) return ((aVisitor<? extends T>)visitor).visitDim2(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class Dim1Context extends Dim_exprContext {
		public ExprContext expr() {
			return getRuleContext(ExprContext.class,0);
		}
		public DimexprContext dimexpr() {
			return getRuleContext(DimexprContext.class,0);
		}
		public Dim1Context(Dim_exprContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof aListener ) ((aListener)listener).enterDim1(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof aListener ) ((aListener)listener).exitDim1(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof aVisitor ) return ((aVisitor<? extends T>)visitor).visitDim1(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Dim_exprContext dim_expr() throws RecognitionException {
		Dim_exprContext _localctx = new Dim_exprContext(_ctx, getState());
		enterRule(_localctx, 56, RULE_dim_expr);
		int _la;
		try {
			setState(428);
			switch ( getInterpreter().adaptivePredict(_input,41,_ctx) ) {
			case 1:
				_localctx = new Dim1Context(_localctx);
				enterOuterAlt(_localctx, 1);
				{
				setState(417);
				match(LeftBracket);
				setState(418);
				expr();
				setState(419);
				match(RightBracket);
				setState(421);
				_la = _input.LA(1);
				if (_la==LeftBracket) {
					{
					setState(420);
					dimexpr();
					}
				}

				}
				break;
			case 2:
				_localctx = new Dim2Context(_localctx);
				enterOuterAlt(_localctx, 2);
				{
				setState(423);
				match(LeftBracket);
				setState(424);
				expr();
				setState(425);
				match(RightBracket);
				setState(426);
				dim_expr();
				}
				break;
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class DimexprContext extends ParserRuleContext {
		public DimexprContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_dimexpr; }
	 
		public DimexprContext() { }
		public void copyFrom(DimexprContext ctx) {
			super.copyFrom(ctx);
		}
	}
	public static class Dim4Context extends DimexprContext {
		public DimexprContext dimexpr() {
			return getRuleContext(DimexprContext.class,0);
		}
		public Dim4Context(DimexprContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof aListener ) ((aListener)listener).enterDim4(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof aListener ) ((aListener)listener).exitDim4(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof aVisitor ) return ((aVisitor<? extends T>)visitor).visitDim4(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class Dim3Context extends DimexprContext {
		public Dim3Context(DimexprContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof aListener ) ((aListener)listener).enterDim3(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof aListener ) ((aListener)listener).exitDim3(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof aVisitor ) return ((aVisitor<? extends T>)visitor).visitDim3(this);
			else return visitor.visitChildren(this);
		}
	}

	public final DimexprContext dimexpr() throws RecognitionException {
		DimexprContext _localctx = new DimexprContext(_ctx, getState());
		enterRule(_localctx, 58, RULE_dimexpr);
		try {
			setState(435);
			switch ( getInterpreter().adaptivePredict(_input,42,_ctx) ) {
			case 1:
				_localctx = new Dim3Context(_localctx);
				enterOuterAlt(_localctx, 1);
				{
				setState(430);
				match(LeftBracket);
				setState(431);
				match(RightBracket);
				}
				break;
			case 2:
				_localctx = new Dim4Context(_localctx);
				enterOuterAlt(_localctx, 2);
				{
				setState(432);
				match(LeftBracket);
				setState(433);
				match(RightBracket);
				setState(434);
				dimexpr();
				}
				break;
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class ExprContext extends ParserRuleContext {
		public ExprContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_expr; }
	 
		public ExprContext() { }
		public void copyFrom(ExprContext ctx) {
			super.copyFrom(ctx);
		}
	}
	public static class Expr2Context extends ExprContext {
		public TerminalNode New() { return getToken(aParser.New, 0); }
		public TypeContext type() {
			return getRuleContext(TypeContext.class,0);
		}
		public Dim_exprContext dim_expr() {
			return getRuleContext(Dim_exprContext.class,0);
		}
		public Expr2Context(ExprContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof aListener ) ((aListener)listener).enterExpr2(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof aListener ) ((aListener)listener).exitExpr2(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof aVisitor ) return ((aVisitor<? extends T>)visitor).visitExpr2(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class Expr1Context extends ExprContext {
		public AssignmentExpressionContext assignmentExpression() {
			return getRuleContext(AssignmentExpressionContext.class,0);
		}
		public Expr1Context(ExprContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof aListener ) ((aListener)listener).enterExpr1(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof aListener ) ((aListener)listener).exitExpr1(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof aVisitor ) return ((aVisitor<? extends T>)visitor).visitExpr1(this);
			else return visitor.visitChildren(this);
		}
	}

	public final ExprContext expr() throws RecognitionException {
		ExprContext _localctx = new ExprContext(_ctx, getState());
		enterRule(_localctx, 60, RULE_expr);
		int _la;
		try {
			setState(443);
			switch (_input.LA(1)) {
			case BoolConstant:
			case Null:
			case LeftParen:
			case Plus:
			case PlusPlus:
			case Minus:
			case MinusMinus:
			case Star:
			case Not:
			case Tilde:
			case And:
			case ID:
			case IntegerConstant:
			case StringConstant:
				_localctx = new Expr1Context(_localctx);
				enterOuterAlt(_localctx, 1);
				{
				setState(437);
				assignmentExpression();
				}
				break;
			case New:
				_localctx = new Expr2Context(_localctx);
				enterOuterAlt(_localctx, 2);
				{
				setState(438);
				match(New);
				setState(439);
				type(0);
				setState(441);
				_la = _input.LA(1);
				if (_la==LeftBracket) {
					{
					setState(440);
					dim_expr();
					}
				}

				}
				break;
			default:
				throw new NoViableAltException(this);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class ConstantContext extends ParserRuleContext {
		public ConstantContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_constant; }
	 
		public ConstantContext() { }
		public void copyFrom(ConstantContext ctx) {
			super.copyFrom(ctx);
		}
	}
	public static class Const2Context extends ConstantContext {
		public TerminalNode BoolConstant() { return getToken(aParser.BoolConstant, 0); }
		public Const2Context(ConstantContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof aListener ) ((aListener)listener).enterConst2(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof aListener ) ((aListener)listener).exitConst2(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof aVisitor ) return ((aVisitor<? extends T>)visitor).visitConst2(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class Const1Context extends ConstantContext {
		public TerminalNode IntegerConstant() { return getToken(aParser.IntegerConstant, 0); }
		public Const1Context(ConstantContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof aListener ) ((aListener)listener).enterConst1(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof aListener ) ((aListener)listener).exitConst1(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof aVisitor ) return ((aVisitor<? extends T>)visitor).visitConst1(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class Const4Context extends ConstantContext {
		public TerminalNode Null() { return getToken(aParser.Null, 0); }
		public Const4Context(ConstantContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof aListener ) ((aListener)listener).enterConst4(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof aListener ) ((aListener)listener).exitConst4(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof aVisitor ) return ((aVisitor<? extends T>)visitor).visitConst4(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class Const3Context extends ConstantContext {
		public TerminalNode StringConstant() { return getToken(aParser.StringConstant, 0); }
		public Const3Context(ConstantContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof aListener ) ((aListener)listener).enterConst3(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof aListener ) ((aListener)listener).exitConst3(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof aVisitor ) return ((aVisitor<? extends T>)visitor).visitConst3(this);
			else return visitor.visitChildren(this);
		}
	}

	public final ConstantContext constant() throws RecognitionException {
		ConstantContext _localctx = new ConstantContext(_ctx, getState());
		enterRule(_localctx, 62, RULE_constant);
		try {
			setState(449);
			switch (_input.LA(1)) {
			case IntegerConstant:
				_localctx = new Const1Context(_localctx);
				enterOuterAlt(_localctx, 1);
				{
				setState(445);
				match(IntegerConstant);
				}
				break;
			case BoolConstant:
				_localctx = new Const2Context(_localctx);
				enterOuterAlt(_localctx, 2);
				{
				setState(446);
				match(BoolConstant);
				}
				break;
			case StringConstant:
				_localctx = new Const3Context(_localctx);
				enterOuterAlt(_localctx, 3);
				{
				setState(447);
				match(StringConstant);
				}
				break;
			case Null:
				_localctx = new Const4Context(_localctx);
				enterOuterAlt(_localctx, 4);
				{
				setState(448);
				match(Null);
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public boolean sempred(RuleContext _localctx, int ruleIndex, int predIndex) {
		switch (ruleIndex) {
		case 9:
			return type_sempred((TypeContext)_localctx, predIndex);
		case 12:
			return postfix_sempred((PostfixContext)_localctx, predIndex);
		case 16:
			return multiplicativeExpression_sempred((MultiplicativeExpressionContext)_localctx, predIndex);
		case 17:
			return additiveExpression_sempred((AdditiveExpressionContext)_localctx, predIndex);
		case 18:
			return shiftExpression_sempred((ShiftExpressionContext)_localctx, predIndex);
		case 19:
			return relationalExpression_sempred((RelationalExpressionContext)_localctx, predIndex);
		case 20:
			return equalityExpression_sempred((EqualityExpressionContext)_localctx, predIndex);
		case 21:
			return andExpression_sempred((AndExpressionContext)_localctx, predIndex);
		case 22:
			return exclusiveOrExpression_sempred((ExclusiveOrExpressionContext)_localctx, predIndex);
		case 23:
			return inclusiveOrExpression_sempred((InclusiveOrExpressionContext)_localctx, predIndex);
		case 24:
			return logicalAndExpression_sempred((LogicalAndExpressionContext)_localctx, predIndex);
		case 25:
			return logicalOrExpression_sempred((LogicalOrExpressionContext)_localctx, predIndex);
		}
		return true;
	}
	private boolean type_sempred(TypeContext _localctx, int predIndex) {
		switch (predIndex) {
		case 0:
			return precpred(_ctx, 2);
		}
		return true;
	}
	private boolean postfix_sempred(PostfixContext _localctx, int predIndex) {
		switch (predIndex) {
		case 1:
			return precpred(_ctx, 5);
		case 2:
			return precpred(_ctx, 4);
		case 3:
			return precpred(_ctx, 3);
		case 4:
			return precpred(_ctx, 2);
		case 5:
			return precpred(_ctx, 1);
		}
		return true;
	}
	private boolean multiplicativeExpression_sempred(MultiplicativeExpressionContext _localctx, int predIndex) {
		switch (predIndex) {
		case 6:
			return precpred(_ctx, 3);
		case 7:
			return precpred(_ctx, 2);
		case 8:
			return precpred(_ctx, 1);
		}
		return true;
	}
	private boolean additiveExpression_sempred(AdditiveExpressionContext _localctx, int predIndex) {
		switch (predIndex) {
		case 9:
			return precpred(_ctx, 2);
		case 10:
			return precpred(_ctx, 1);
		}
		return true;
	}
	private boolean shiftExpression_sempred(ShiftExpressionContext _localctx, int predIndex) {
		switch (predIndex) {
		case 11:
			return precpred(_ctx, 2);
		case 12:
			return precpred(_ctx, 1);
		}
		return true;
	}
	private boolean relationalExpression_sempred(RelationalExpressionContext _localctx, int predIndex) {
		switch (predIndex) {
		case 13:
			return precpred(_ctx, 4);
		case 14:
			return precpred(_ctx, 3);
		case 15:
			return precpred(_ctx, 2);
		case 16:
			return precpred(_ctx, 1);
		}
		return true;
	}
	private boolean equalityExpression_sempred(EqualityExpressionContext _localctx, int predIndex) {
		switch (predIndex) {
		case 17:
			return precpred(_ctx, 2);
		case 18:
			return precpred(_ctx, 1);
		}
		return true;
	}
	private boolean andExpression_sempred(AndExpressionContext _localctx, int predIndex) {
		switch (predIndex) {
		case 19:
			return precpred(_ctx, 1);
		}
		return true;
	}
	private boolean exclusiveOrExpression_sempred(ExclusiveOrExpressionContext _localctx, int predIndex) {
		switch (predIndex) {
		case 20:
			return precpred(_ctx, 1);
		}
		return true;
	}
	private boolean inclusiveOrExpression_sempred(InclusiveOrExpressionContext _localctx, int predIndex) {
		switch (predIndex) {
		case 21:
			return precpred(_ctx, 1);
		}
		return true;
	}
	private boolean logicalAndExpression_sempred(LogicalAndExpressionContext _localctx, int predIndex) {
		switch (predIndex) {
		case 22:
			return precpred(_ctx, 1);
		}
		return true;
	}
	private boolean logicalOrExpression_sempred(LogicalOrExpressionContext _localctx, int predIndex) {
		switch (predIndex) {
		case 23:
			return precpred(_ctx, 1);
		}
		return true;
	}

	public static final String _serializedATN =
		"\3\u0430\ud6d1\u8206\uad2d\u4417\uaef1\u8d80\uaadd\3@\u01c6\4\2\t\2\4"+
		"\3\t\3\4\4\t\4\4\5\t\5\4\6\t\6\4\7\t\7\4\b\t\b\4\t\t\t\4\n\t\n\4\13\t"+
		"\13\4\f\t\f\4\r\t\r\4\16\t\16\4\17\t\17\4\20\t\20\4\21\t\21\4\22\t\22"+
		"\4\23\t\23\4\24\t\24\4\25\t\25\4\26\t\26\4\27\t\27\4\30\t\30\4\31\t\31"+
		"\4\32\t\32\4\33\t\33\4\34\t\34\4\35\t\35\4\36\t\36\4\37\t\37\4 \t \4!"+
		"\t!\3\2\5\2D\n\2\3\2\3\2\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3"+
		"\3\5\3T\n\3\3\4\3\4\5\4X\n\4\3\4\3\4\3\5\6\5]\n\5\r\5\16\5^\3\6\3\6\3"+
		"\6\3\6\3\6\3\6\3\6\3\6\3\6\3\6\5\6k\n\6\3\7\3\7\3\7\3\7\3\7\3\7\3\b\3"+
		"\b\3\b\3\b\3\b\3\b\3\b\3\b\3\b\5\b|\n\b\3\t\3\t\3\t\3\t\5\t\u0082\n\t"+
		"\3\t\3\t\3\t\3\t\3\t\3\t\3\t\5\t\u008b\n\t\3\t\3\t\5\t\u008f\n\t\3\n\3"+
		"\n\3\n\3\n\3\n\3\n\3\n\3\n\5\n\u0099\n\n\3\13\3\13\3\13\3\13\3\13\5\13"+
		"\u00a0\n\13\3\13\3\13\3\13\7\13\u00a5\n\13\f\13\16\13\u00a8\13\13\3\f"+
		"\3\f\3\f\5\f\u00ad\n\f\3\f\3\f\3\f\3\f\3\f\3\f\3\f\3\f\3\f\3\f\3\f\3\f"+
		"\3\f\3\f\3\f\3\f\3\f\5\f\u00c0\n\f\3\f\3\f\3\f\3\f\3\f\3\f\3\f\3\f\3\f"+
		"\3\f\3\f\3\f\3\f\3\f\5\f\u00d0\n\f\3\f\3\f\5\f\u00d4\n\f\3\f\3\f\5\f\u00d8"+
		"\n\f\3\f\3\f\5\f\u00dc\n\f\3\r\3\r\3\r\3\r\3\r\3\r\5\r\u00e4\n\r\3\16"+
		"\3\16\3\16\3\16\3\16\3\16\3\16\3\16\3\16\3\16\3\16\3\16\3\16\3\16\3\16"+
		"\3\16\3\16\3\16\5\16\u00f8\n\16\3\16\7\16\u00fb\n\16\f\16\16\16\u00fe"+
		"\13\16\3\17\3\17\3\17\3\17\3\17\5\17\u0105\n\17\3\20\3\20\3\20\3\20\3"+
		"\20\3\20\3\20\3\20\3\20\3\20\5\20\u0111\n\20\3\21\3\21\3\22\3\22\3\22"+
		"\3\22\3\22\3\22\3\22\3\22\3\22\3\22\3\22\3\22\7\22\u0121\n\22\f\22\16"+
		"\22\u0124\13\22\3\23\3\23\3\23\3\23\3\23\3\23\3\23\3\23\3\23\7\23\u012f"+
		"\n\23\f\23\16\23\u0132\13\23\3\24\3\24\3\24\3\24\3\24\3\24\3\24\3\24\3"+
		"\24\7\24\u013d\n\24\f\24\16\24\u0140\13\24\3\25\3\25\3\25\3\25\3\25\3"+
		"\25\3\25\3\25\3\25\3\25\3\25\3\25\3\25\3\25\3\25\7\25\u0151\n\25\f\25"+
		"\16\25\u0154\13\25\3\26\3\26\3\26\3\26\3\26\3\26\3\26\3\26\3\26\7\26\u015f"+
		"\n\26\f\26\16\26\u0162\13\26\3\27\3\27\3\27\3\27\3\27\3\27\7\27\u016a"+
		"\n\27\f\27\16\27\u016d\13\27\3\30\3\30\3\30\3\30\3\30\3\30\7\30\u0175"+
		"\n\30\f\30\16\30\u0178\13\30\3\31\3\31\3\31\3\31\3\31\3\31\7\31\u0180"+
		"\n\31\f\31\16\31\u0183\13\31\3\32\3\32\3\32\3\32\3\32\3\32\7\32\u018b"+
		"\n\32\f\32\16\32\u018e\13\32\3\33\3\33\3\33\3\33\3\33\3\33\7\33\u0196"+
		"\n\33\f\33\16\33\u0199\13\33\3\34\3\34\3\34\3\34\3\34\5\34\u01a0\n\34"+
		"\3\35\3\35\3\36\3\36\3\36\3\36\5\36\u01a8\n\36\3\36\3\36\3\36\3\36\3\36"+
		"\5\36\u01af\n\36\3\37\3\37\3\37\3\37\3\37\5\37\u01b6\n\37\3 \3 \3 \3 "+
		"\5 \u01bc\n \5 \u01be\n \3!\3!\3!\3!\5!\u01c4\n!\3!\2\16\24\32\"$&(*,"+
		".\60\62\64\"\2\4\6\b\n\f\16\20\22\24\26\30\32\34\36 \"$&(*,.\60\62\64"+
		"\668:<>@\2\3\7\2\32\32\34\34\36\36))//\u01ed\2C\3\2\2\2\4S\3\2\2\2\6U"+
		"\3\2\2\2\b\\\3\2\2\2\nj\3\2\2\2\fl\3\2\2\2\16{\3\2\2\2\20\u008e\3\2\2"+
		"\2\22\u0098\3\2\2\2\24\u009f\3\2\2\2\26\u00db\3\2\2\2\30\u00e3\3\2\2\2"+
		"\32\u00e5\3\2\2\2\34\u0104\3\2\2\2\36\u0110\3\2\2\2 \u0112\3\2\2\2\"\u0114"+
		"\3\2\2\2$\u0125\3\2\2\2&\u0133\3\2\2\2(\u0141\3\2\2\2*\u0155\3\2\2\2,"+
		"\u0163\3\2\2\2.\u016e\3\2\2\2\60\u0179\3\2\2\2\62\u0184\3\2\2\2\64\u018f"+
		"\3\2\2\2\66\u019f\3\2\2\28\u01a1\3\2\2\2:\u01ae\3\2\2\2<\u01b5\3\2\2\2"+
		">\u01bd\3\2\2\2@\u01c3\3\2\2\2BD\5\4\3\2CB\3\2\2\2CD\3\2\2\2DE\3\2\2\2"+
		"EF\7\2\2\3F\3\3\2\2\2GT\5\f\7\2HT\5\20\t\2IT\5\n\6\2JK\5\f\7\2KL\5\4\3"+
		"\2LT\3\2\2\2MN\5\20\t\2NO\5\4\3\2OT\3\2\2\2PQ\5\n\6\2QR\5\4\3\2RT\3\2"+
		"\2\2SG\3\2\2\2SH\3\2\2\2SI\3\2\2\2SJ\3\2\2\2SM\3\2\2\2SP\3\2\2\2T\5\3"+
		"\2\2\2UW\7\30\2\2VX\5\b\5\2WV\3\2\2\2WX\3\2\2\2XY\3\2\2\2YZ\7\31\2\2Z"+
		"\7\3\2\2\2[]\5\26\f\2\\[\3\2\2\2]^\3\2\2\2^\\\3\2\2\2^_\3\2\2\2_\t\3\2"+
		"\2\2`a\5\24\13\2ab\7\67\2\2bc\7\64\2\2ck\3\2\2\2de\5\24\13\2ef\7\67\2"+
		"\2fg\7\60\2\2gh\5> \2hi\7\64\2\2ik\3\2\2\2j`\3\2\2\2jd\3\2\2\2k\13\3\2"+
		"\2\2lm\7\6\2\2mn\7\67\2\2no\7\30\2\2op\5\16\b\2pq\7\31\2\2q\r\3\2\2\2"+
		"rs\5\24\13\2st\7\67\2\2tu\7\64\2\2u|\3\2\2\2vw\5\24\13\2wx\7\67\2\2xy"+
		"\7\64\2\2yz\5\16\b\2z|\3\2\2\2{r\3\2\2\2{v\3\2\2\2|\17\3\2\2\2}~\5\24"+
		"\13\2~\177\7\67\2\2\177\u0081\7\24\2\2\u0080\u0082\5\22\n\2\u0081\u0080"+
		"\3\2\2\2\u0081\u0082\3\2\2\2\u0082\u0083\3\2\2\2\u0083\u0084\7\25\2\2"+
		"\u0084\u0085\5\6\4\2\u0085\u008f\3\2\2\2\u0086\u0087\7\22\2\2\u0087\u0088"+
		"\7\67\2\2\u0088\u008a\7\24\2\2\u0089\u008b\5\22\n\2\u008a\u0089\3\2\2"+
		"\2\u008a\u008b\3\2\2\2\u008b\u008c\3\2\2\2\u008c\u008d\7\25\2\2\u008d"+
		"\u008f\5\6\4\2\u008e}\3\2\2\2\u008e\u0086\3\2\2\2\u008f\21\3\2\2\2\u0090"+
		"\u0091\5\24\13\2\u0091\u0092\7\67\2\2\u0092\u0099\3\2\2\2\u0093\u0094"+
		"\5\24\13\2\u0094\u0095\7\67\2\2\u0095\u0096\7\65\2\2\u0096\u0097\5\22"+
		"\n\2\u0097\u0099\3\2\2\2\u0098\u0090\3\2\2\2\u0098\u0093\3\2\2\2\u0099"+
		"\23\3\2\2\2\u009a\u009b\b\13\1\2\u009b\u00a0\7\13\2\2\u009c\u00a0\7\17"+
		"\2\2\u009d\u00a0\7\67\2\2\u009e\u00a0\7\4\2\2\u009f\u009a\3\2\2\2\u009f"+
		"\u009c\3\2\2\2\u009f\u009d\3\2\2\2\u009f\u009e\3\2\2\2\u00a0\u00a6\3\2"+
		"\2\2\u00a1\u00a2\f\4\2\2\u00a2\u00a3\7\26\2\2\u00a3\u00a5\7\27\2\2\u00a4"+
		"\u00a1\3\2\2\2\u00a5\u00a8\3\2\2\2\u00a6\u00a4\3\2\2\2\u00a6\u00a7\3\2"+
		"\2\2\u00a7\25\3\2\2\2\u00a8\u00a6\3\2\2\2\u00a9\u00dc\5\6\4\2\u00aa\u00dc"+
		"\5\n\6\2\u00ab\u00ad\5> \2\u00ac\u00ab\3\2\2\2\u00ac\u00ad\3\2\2\2\u00ad"+
		"\u00ae\3\2\2\2\u00ae\u00dc\7\64\2\2\u00af\u00b0\7\t\2\2\u00b0\u00b1\7"+
		"\24\2\2\u00b1\u00b2\5> \2\u00b2\u00b3\7\25\2\2\u00b3\u00b4\5\26\f\2\u00b4"+
		"\u00dc\3\2\2\2\u00b5\u00b6\7\t\2\2\u00b6\u00b7\7\24\2\2\u00b7\u00b8\5"+
		"> \2\u00b8\u00b9\7\25\2\2\u00b9\u00ba\5\26\f\2\u00ba\u00bb\7\n\2\2\u00bb"+
		"\u00bc\5\26\f\2\u00bc\u00dc\3\2\2\2\u00bd\u00bf\7\16\2\2\u00be\u00c0\5"+
		"> \2\u00bf\u00be\3\2\2\2\u00bf\u00c0\3\2\2\2\u00c0\u00c1\3\2\2\2\u00c1"+
		"\u00dc\7\64\2\2\u00c2\u00c3\7\5\2\2\u00c3\u00dc\7\64\2\2\u00c4\u00c5\7"+
		"\7\2\2\u00c5\u00dc\7\64\2\2\u00c6\u00c7\7\23\2\2\u00c7\u00c8\7\24\2\2"+
		"\u00c8\u00c9\5> \2\u00c9\u00ca\7\25\2\2\u00ca\u00cb\5\26\f\2\u00cb\u00dc"+
		"\3\2\2\2\u00cc\u00cd\7\b\2\2\u00cd\u00cf\7\24\2\2\u00ce\u00d0\5> \2\u00cf"+
		"\u00ce\3\2\2\2\u00cf\u00d0\3\2\2\2\u00d0\u00d1\3\2\2\2\u00d1\u00d3\7\64"+
		"\2\2\u00d2\u00d4\5> \2\u00d3\u00d2\3\2\2\2\u00d3\u00d4\3\2\2\2\u00d4\u00d5"+
		"\3\2\2\2\u00d5\u00d7\7\64\2\2\u00d6\u00d8\5> \2\u00d7\u00d6\3\2\2\2\u00d7"+
		"\u00d8\3\2\2\2\u00d8\u00d9\3\2\2\2\u00d9\u00da\7\25\2\2\u00da\u00dc\5"+
		"\26\f\2\u00db\u00a9\3\2\2\2\u00db\u00aa\3\2\2\2\u00db\u00ac\3\2\2\2\u00db"+
		"\u00af\3\2\2\2\u00db\u00b5\3\2\2\2\u00db\u00bd\3\2\2\2\u00db\u00c2\3\2"+
		"\2\2\u00db\u00c4\3\2\2\2\u00db\u00c6\3\2\2\2\u00db\u00cc\3\2\2\2\u00dc"+
		"\27\3\2\2\2\u00dd\u00e4\7\67\2\2\u00de\u00e4\5@!\2\u00df\u00e0\7\24\2"+
		"\2\u00e0\u00e1\5> \2\u00e1\u00e2\7\25\2\2\u00e2\u00e4\3\2\2\2\u00e3\u00dd"+
		"\3\2\2\2\u00e3\u00de\3\2\2\2\u00e3\u00df\3\2\2\2\u00e4\31\3\2\2\2\u00e5"+
		"\u00e6\b\16\1\2\u00e6\u00e7\5\30\r\2\u00e7\u00fc\3\2\2\2\u00e8\u00e9\f"+
		"\7\2\2\u00e9\u00ea\7\26\2\2\u00ea\u00eb\5> \2\u00eb\u00ec\7\27\2\2\u00ec"+
		"\u00fb\3\2\2\2\u00ed\u00ee\f\6\2\2\u00ee\u00ef\7\61\2\2\u00ef\u00fb\7"+
		"\67\2\2\u00f0\u00f1\f\5\2\2\u00f1\u00fb\7\33\2\2\u00f2\u00f3\f\4\2\2\u00f3"+
		"\u00fb\7\35\2\2\u00f4\u00f5\f\3\2\2\u00f5\u00f7\7\24\2\2\u00f6\u00f8\5"+
		"\34\17\2\u00f7\u00f6\3\2\2\2\u00f7\u00f8\3\2\2\2\u00f8\u00f9\3\2\2\2\u00f9"+
		"\u00fb\7\25\2\2\u00fa\u00e8\3\2\2\2\u00fa\u00ed\3\2\2\2\u00fa\u00f0\3"+
		"\2\2\2\u00fa\u00f2\3\2\2\2\u00fa\u00f4\3\2\2\2\u00fb\u00fe\3\2\2\2\u00fc"+
		"\u00fa\3\2\2\2\u00fc\u00fd\3\2\2\2\u00fd\33\3\2\2\2\u00fe\u00fc\3\2\2"+
		"\2\u00ff\u0105\5> \2\u0100\u0101\5> \2\u0101\u0102\7\65\2\2\u0102\u0103"+
		"\5\34\17\2\u0103\u0105\3\2\2\2\u0104\u00ff\3\2\2\2\u0104\u0100\3\2\2\2"+
		"\u0105\35\3\2\2\2\u0106\u0111\5\32\16\2\u0107\u0108\7\33\2\2\u0108\u0111"+
		"\5\36\20\2\u0109\u010a\7\35\2\2\u010a\u0111\5\36\20\2\u010b\u010c\7-\2"+
		"\2\u010c\u0111\5\36\20\2\u010d\u010e\5 \21\2\u010e\u010f\5\36\20\2\u010f"+
		"\u0111\3\2\2\2\u0110\u0106\3\2\2\2\u0110\u0107\3\2\2\2\u0110\u0109\3\2"+
		"\2\2\u0110\u010b\3\2\2\2\u0110\u010d\3\2\2\2\u0111\37\3\2\2\2\u0112\u0113"+
		"\t\2\2\2\u0113!\3\2\2\2\u0114\u0115\b\22\1\2\u0115\u0116\5\36\20\2\u0116"+
		"\u0122\3\2\2\2\u0117\u0118\f\5\2\2\u0118\u0119\7\36\2\2\u0119\u0121\5"+
		"\36\20\2\u011a\u011b\f\4\2\2\u011b\u011c\7\37\2\2\u011c\u0121\5\36\20"+
		"\2\u011d\u011e\f\3\2\2\u011e\u011f\7 \2\2\u011f\u0121\5\36\20\2\u0120"+
		"\u0117\3\2\2\2\u0120\u011a\3\2\2\2\u0120\u011d\3\2\2\2\u0121\u0124\3\2"+
		"\2\2\u0122\u0120\3\2\2\2\u0122\u0123\3\2\2\2\u0123#\3\2\2\2\u0124\u0122"+
		"\3\2\2\2\u0125\u0126\b\23\1\2\u0126\u0127\5\"\22\2\u0127\u0130\3\2\2\2"+
		"\u0128\u0129\f\4\2\2\u0129\u012a\7\32\2\2\u012a\u012f\5\"\22\2\u012b\u012c"+
		"\f\3\2\2\u012c\u012d\7\34\2\2\u012d\u012f\5\"\22\2\u012e\u0128\3\2\2\2"+
		"\u012e\u012b\3\2\2\2\u012f\u0132\3\2\2\2\u0130\u012e\3\2\2\2\u0130\u0131"+
		"\3\2\2\2\u0131%\3\2\2\2\u0132\u0130\3\2\2\2\u0133\u0134\b\24\1\2\u0134"+
		"\u0135\5$\23\2\u0135\u013e\3\2\2\2\u0136\u0137\f\4\2\2\u0137\u0138\7*"+
		"\2\2\u0138\u013d\5$\23\2\u0139\u013a\f\3\2\2\u013a\u013b\7+\2\2\u013b"+
		"\u013d\5$\23\2\u013c\u0136\3\2\2\2\u013c\u0139\3\2\2\2\u013d\u0140\3\2"+
		"\2\2\u013e\u013c\3\2\2\2\u013e\u013f\3\2\2\2\u013f\'\3\2\2\2\u0140\u013e"+
		"\3\2\2\2\u0141\u0142\b\25\1\2\u0142\u0143\5&\24\2\u0143\u0152\3\2\2\2"+
		"\u0144\u0145\f\6\2\2\u0145\u0146\7\"\2\2\u0146\u0151\5&\24\2\u0147\u0148"+
		"\f\5\2\2\u0148\u0149\7%\2\2\u0149\u0151\5&\24\2\u014a\u014b\f\4\2\2\u014b"+
		"\u014c\7#\2\2\u014c\u0151\5&\24\2\u014d\u014e\f\3\2\2\u014e\u014f\7&\2"+
		"\2\u014f\u0151\5&\24\2\u0150\u0144\3\2\2\2\u0150\u0147\3\2\2\2\u0150\u014a"+
		"\3\2\2\2\u0150\u014d\3\2\2\2\u0151\u0154\3\2\2\2\u0152\u0150\3\2\2\2\u0152"+
		"\u0153\3\2\2\2\u0153)\3\2\2\2\u0154\u0152\3\2\2\2\u0155\u0156\b\26\1\2"+
		"\u0156\u0157\5(\25\2\u0157\u0160\3\2\2\2\u0158\u0159\f\4\2\2\u0159\u015a"+
		"\7!\2\2\u015a\u015f\5(\25\2\u015b\u015c\f\3\2\2\u015c\u015d\7$\2\2\u015d"+
		"\u015f\5(\25\2\u015e\u0158\3\2\2\2\u015e\u015b\3\2\2\2\u015f\u0162\3\2"+
		"\2\2\u0160\u015e\3\2\2\2\u0160\u0161\3\2\2\2\u0161+\3\2\2\2\u0162\u0160"+
		"\3\2\2\2\u0163\u0164\b\27\1\2\u0164\u0165\5*\26\2\u0165\u016b\3\2\2\2"+
		"\u0166\u0167\f\3\2\2\u0167\u0168\7/\2\2\u0168\u016a\5*\26\2\u0169\u0166"+
		"\3\2\2\2\u016a\u016d\3\2\2\2\u016b\u0169\3\2\2\2\u016b\u016c\3\2\2\2\u016c"+
		"-\3\2\2\2\u016d\u016b\3\2\2\2\u016e\u016f\b\30\1\2\u016f\u0170\5,\27\2"+
		"\u0170\u0176\3\2\2\2\u0171\u0172\f\3\2\2\u0172\u0173\7.\2\2\u0173\u0175"+
		"\5,\27\2\u0174\u0171\3\2\2\2\u0175\u0178\3\2\2\2\u0176\u0174\3\2\2\2\u0176"+
		"\u0177\3\2\2\2\u0177/\3\2\2\2\u0178\u0176\3\2\2\2\u0179\u017a\b\31\1\2"+
		"\u017a\u017b\5.\30\2\u017b\u0181\3\2\2\2\u017c\u017d\f\3\2\2\u017d\u017e"+
		"\7,\2\2\u017e\u0180\5.\30\2\u017f\u017c\3\2\2\2\u0180\u0183\3\2\2\2\u0181"+
		"\u017f\3\2\2\2\u0181\u0182\3\2\2\2\u0182\61\3\2\2\2\u0183\u0181\3\2\2"+
		"\2\u0184\u0185\b\32\1\2\u0185\u0186\5\60\31\2\u0186\u018c\3\2\2\2\u0187"+
		"\u0188\f\3\2\2\u0188\u0189\7\'\2\2\u0189\u018b\5\60\31\2\u018a\u0187\3"+
		"\2\2\2\u018b\u018e\3\2\2\2\u018c\u018a\3\2\2\2\u018c\u018d\3\2\2\2\u018d"+
		"\63\3\2\2\2\u018e\u018c\3\2\2\2\u018f\u0190\b\33\1\2\u0190\u0191\5\62"+
		"\32\2\u0191\u0197\3\2\2\2\u0192\u0193\f\3\2\2\u0193\u0194\7(\2\2\u0194"+
		"\u0196\5\62\32\2\u0195\u0192\3\2\2\2\u0196\u0199\3\2\2\2\u0197\u0195\3"+
		"\2\2\2\u0197\u0198\3\2\2\2\u0198\65\3\2\2\2\u0199\u0197\3\2\2\2\u019a"+
		"\u01a0\5\64\33\2\u019b\u019c\5\36\20\2\u019c\u019d\58\35\2\u019d\u019e"+
		"\5> \2\u019e\u01a0\3\2\2\2\u019f\u019a\3\2\2\2\u019f\u019b\3\2\2\2\u01a0"+
		"\67\3\2\2\2\u01a1\u01a2\7\60\2\2\u01a29\3\2\2\2\u01a3\u01a4\7\26\2\2\u01a4"+
		"\u01a5\5> \2\u01a5\u01a7\7\27\2\2\u01a6\u01a8\5<\37\2\u01a7\u01a6\3\2"+
		"\2\2\u01a7\u01a8\3\2\2\2\u01a8\u01af\3\2\2\2\u01a9\u01aa\7\26\2\2\u01aa"+
		"\u01ab\5> \2\u01ab\u01ac\7\27\2\2\u01ac\u01ad\5:\36\2\u01ad\u01af\3\2"+
		"\2\2\u01ae\u01a3\3\2\2\2\u01ae\u01a9\3\2\2\2\u01af;\3\2\2\2\u01b0\u01b1"+
		"\7\26\2\2\u01b1\u01b6\7\27\2\2\u01b2\u01b3\7\26\2\2\u01b3\u01b4\7\27\2"+
		"\2\u01b4\u01b6\5<\37\2\u01b5\u01b0\3\2\2\2\u01b5\u01b2\3\2\2\2\u01b6="+
		"\3\2\2\2\u01b7\u01be\5\66\34\2\u01b8\u01b9\7\f\2\2\u01b9\u01bb\5\24\13"+
		"\2\u01ba\u01bc\5:\36\2\u01bb\u01ba\3\2\2\2\u01bb\u01bc\3\2\2\2\u01bc\u01be"+
		"\3\2\2\2\u01bd\u01b7\3\2\2\2\u01bd\u01b8\3\2\2\2\u01be?\3\2\2\2\u01bf"+
		"\u01c4\79\2\2\u01c0\u01c4\7\3\2\2\u01c1\u01c4\7>\2\2\u01c2\u01c4\7\r\2"+
		"\2\u01c3\u01bf\3\2\2\2\u01c3\u01c0\3\2\2\2\u01c3\u01c1\3\2\2\2\u01c3\u01c2"+
		"\3\2\2\2\u01c4A\3\2\2\2\60CSW^j{\u0081\u008a\u008e\u0098\u009f\u00a6\u00ac"+
		"\u00bf\u00cf\u00d3\u00d7\u00db\u00e3\u00f7\u00fa\u00fc\u0104\u0110\u0120"+
		"\u0122\u012e\u0130\u013c\u013e\u0150\u0152\u015e\u0160\u016b\u0176\u0181"+
		"\u018c\u0197\u019f\u01a7\u01ae\u01b5\u01bb\u01bd\u01c3";
	public static final ATN _ATN =
		new ATNDeserializer().deserialize(_serializedATN.toCharArray());
	static {
		_decisionToDFA = new DFA[_ATN.getNumberOfDecisions()];
		for (int i = 0; i < _ATN.getNumberOfDecisions(); i++) {
			_decisionToDFA[i] = new DFA(_ATN.getDecisionState(i), i);
		}
	}
}