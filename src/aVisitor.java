// Generated from a.g4 by ANTLR 4.5
import org.antlr.v4.runtime.tree.ParseTreeVisitor;

/**
 * This interface defines a complete generic visitor for a parse tree produced
 * by {@link aParser}.
 *
 * @param <T> The return type of the visit operation. Use {@link Void} for
 * operations with no return type.
 */
public interface aVisitor<T> extends ParseTreeVisitor<T> {
	/**
	 * Visit a parse tree produced by {@link aParser#main}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitMain(aParser.MainContext ctx);
	/**
	 * Visit a parse tree produced by the {@code program4}
	 * labeled alternative in {@link aParser#program}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitProgram4(aParser.Program4Context ctx);
	/**
	 * Visit a parse tree produced by the {@code program5}
	 * labeled alternative in {@link aParser#program}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitProgram5(aParser.Program5Context ctx);
	/**
	 * Visit a parse tree produced by the {@code program6}
	 * labeled alternative in {@link aParser#program}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitProgram6(aParser.Program6Context ctx);
	/**
	 * Visit a parse tree produced by the {@code program1}
	 * labeled alternative in {@link aParser#program}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitProgram1(aParser.Program1Context ctx);
	/**
	 * Visit a parse tree produced by the {@code program2}
	 * labeled alternative in {@link aParser#program}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitProgram2(aParser.Program2Context ctx);
	/**
	 * Visit a parse tree produced by the {@code program3}
	 * labeled alternative in {@link aParser#program}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitProgram3(aParser.Program3Context ctx);
	/**
	 * Visit a parse tree produced by {@link aParser#block_stmt}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitBlock_stmt(aParser.Block_stmtContext ctx);
	/**
	 * Visit a parse tree produced by {@link aParser#stmt_list}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitStmt_list(aParser.Stmt_listContext ctx);
	/**
	 * Visit a parse tree produced by the {@code var1}
	 * labeled alternative in {@link aParser#var_decl_stmt}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitVar1(aParser.Var1Context ctx);
	/**
	 * Visit a parse tree produced by the {@code var2}
	 * labeled alternative in {@link aParser#var_decl_stmt}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitVar2(aParser.Var2Context ctx);
	/**
	 * Visit a parse tree produced by {@link aParser#class_decl}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitClass_decl(aParser.Class_declContext ctx);
	/**
	 * Visit a parse tree produced by the {@code memberlist1}
	 * labeled alternative in {@link aParser#member_decl_list}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitMemberlist1(aParser.Memberlist1Context ctx);
	/**
	 * Visit a parse tree produced by the {@code memberlist2}
	 * labeled alternative in {@link aParser#member_decl_list}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitMemberlist2(aParser.Memberlist2Context ctx);
	/**
	 * Visit a parse tree produced by the {@code func1}
	 * labeled alternative in {@link aParser#func_decl}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitFunc1(aParser.Func1Context ctx);
	/**
	 * Visit a parse tree produced by the {@code func2}
	 * labeled alternative in {@link aParser#func_decl}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitFunc2(aParser.Func2Context ctx);
	/**
	 * Visit a parse tree produced by the {@code pdl1}
	 * labeled alternative in {@link aParser#param_decl_list}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitPdl1(aParser.Pdl1Context ctx);
	/**
	 * Visit a parse tree produced by the {@code pdl2}
	 * labeled alternative in {@link aParser#param_decl_list}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitPdl2(aParser.Pdl2Context ctx);
	/**
	 * Visit a parse tree produced by the {@code type5}
	 * labeled alternative in {@link aParser#type}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitType5(aParser.Type5Context ctx);
	/**
	 * Visit a parse tree produced by the {@code type4}
	 * labeled alternative in {@link aParser#type}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitType4(aParser.Type4Context ctx);
	/**
	 * Visit a parse tree produced by the {@code type3}
	 * labeled alternative in {@link aParser#type}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitType3(aParser.Type3Context ctx);
	/**
	 * Visit a parse tree produced by the {@code type2}
	 * labeled alternative in {@link aParser#type}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitType2(aParser.Type2Context ctx);
	/**
	 * Visit a parse tree produced by the {@code type1}
	 * labeled alternative in {@link aParser#type}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitType1(aParser.Type1Context ctx);
	/**
	 * Visit a parse tree produced by the {@code stmt1}
	 * labeled alternative in {@link aParser#stmt}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitStmt1(aParser.Stmt1Context ctx);
	/**
	 * Visit a parse tree produced by the {@code stmt2}
	 * labeled alternative in {@link aParser#stmt}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitStmt2(aParser.Stmt2Context ctx);
	/**
	 * Visit a parse tree produced by the {@code stmt3}
	 * labeled alternative in {@link aParser#stmt}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitStmt3(aParser.Stmt3Context ctx);
	/**
	 * Visit a parse tree produced by the {@code stmt4}
	 * labeled alternative in {@link aParser#stmt}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitStmt4(aParser.Stmt4Context ctx);
	/**
	 * Visit a parse tree produced by the {@code stmt5}
	 * labeled alternative in {@link aParser#stmt}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitStmt5(aParser.Stmt5Context ctx);
	/**
	 * Visit a parse tree produced by the {@code stmt6}
	 * labeled alternative in {@link aParser#stmt}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitStmt6(aParser.Stmt6Context ctx);
	/**
	 * Visit a parse tree produced by the {@code stmt7}
	 * labeled alternative in {@link aParser#stmt}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitStmt7(aParser.Stmt7Context ctx);
	/**
	 * Visit a parse tree produced by the {@code stmt8}
	 * labeled alternative in {@link aParser#stmt}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitStmt8(aParser.Stmt8Context ctx);
	/**
	 * Visit a parse tree produced by the {@code stmt9}
	 * labeled alternative in {@link aParser#stmt}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitStmt9(aParser.Stmt9Context ctx);
	/**
	 * Visit a parse tree produced by the {@code stmt10}
	 * labeled alternative in {@link aParser#stmt}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitStmt10(aParser.Stmt10Context ctx);
	/**
	 * Visit a parse tree produced by the {@code primary1}
	 * labeled alternative in {@link aParser#primary}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitPrimary1(aParser.Primary1Context ctx);
	/**
	 * Visit a parse tree produced by the {@code primary2}
	 * labeled alternative in {@link aParser#primary}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitPrimary2(aParser.Primary2Context ctx);
	/**
	 * Visit a parse tree produced by the {@code primary3}
	 * labeled alternative in {@link aParser#primary}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitPrimary3(aParser.Primary3Context ctx);
	/**
	 * Visit a parse tree produced by the {@code post6}
	 * labeled alternative in {@link aParser#postfix}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitPost6(aParser.Post6Context ctx);
	/**
	 * Visit a parse tree produced by the {@code post4}
	 * labeled alternative in {@link aParser#postfix}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitPost4(aParser.Post4Context ctx);
	/**
	 * Visit a parse tree produced by the {@code post5}
	 * labeled alternative in {@link aParser#postfix}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitPost5(aParser.Post5Context ctx);
	/**
	 * Visit a parse tree produced by the {@code post2}
	 * labeled alternative in {@link aParser#postfix}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitPost2(aParser.Post2Context ctx);
	/**
	 * Visit a parse tree produced by the {@code post3}
	 * labeled alternative in {@link aParser#postfix}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitPost3(aParser.Post3Context ctx);
	/**
	 * Visit a parse tree produced by the {@code post1}
	 * labeled alternative in {@link aParser#postfix}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitPost1(aParser.Post1Context ctx);
	/**
	 * Visit a parse tree produced by the {@code pl1}
	 * labeled alternative in {@link aParser#param_list}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitPl1(aParser.Pl1Context ctx);
	/**
	 * Visit a parse tree produced by the {@code pl2}
	 * labeled alternative in {@link aParser#param_list}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitPl2(aParser.Pl2Context ctx);
	/**
	 * Visit a parse tree produced by the {@code unary1}
	 * labeled alternative in {@link aParser#unary}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitUnary1(aParser.Unary1Context ctx);
	/**
	 * Visit a parse tree produced by the {@code unary2}
	 * labeled alternative in {@link aParser#unary}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitUnary2(aParser.Unary2Context ctx);
	/**
	 * Visit a parse tree produced by the {@code unary3}
	 * labeled alternative in {@link aParser#unary}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitUnary3(aParser.Unary3Context ctx);
	/**
	 * Visit a parse tree produced by the {@code unary4}
	 * labeled alternative in {@link aParser#unary}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitUnary4(aParser.Unary4Context ctx);
	/**
	 * Visit a parse tree produced by the {@code unary5}
	 * labeled alternative in {@link aParser#unary}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitUnary5(aParser.Unary5Context ctx);
	/**
	 * Visit a parse tree produced by {@link aParser#unaryOperator}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitUnaryOperator(aParser.UnaryOperatorContext ctx);
	/**
	 * Visit a parse tree produced by the {@code me1}
	 * labeled alternative in {@link aParser#multiplicativeExpression}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitMe1(aParser.Me1Context ctx);
	/**
	 * Visit a parse tree produced by the {@code me3}
	 * labeled alternative in {@link aParser#multiplicativeExpression}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitMe3(aParser.Me3Context ctx);
	/**
	 * Visit a parse tree produced by the {@code me2}
	 * labeled alternative in {@link aParser#multiplicativeExpression}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitMe2(aParser.Me2Context ctx);
	/**
	 * Visit a parse tree produced by the {@code me4}
	 * labeled alternative in {@link aParser#multiplicativeExpression}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitMe4(aParser.Me4Context ctx);
	/**
	 * Visit a parse tree produced by the {@code add2}
	 * labeled alternative in {@link aParser#additiveExpression}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitAdd2(aParser.Add2Context ctx);
	/**
	 * Visit a parse tree produced by the {@code add1}
	 * labeled alternative in {@link aParser#additiveExpression}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitAdd1(aParser.Add1Context ctx);
	/**
	 * Visit a parse tree produced by the {@code add3}
	 * labeled alternative in {@link aParser#additiveExpression}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitAdd3(aParser.Add3Context ctx);
	/**
	 * Visit a parse tree produced by the {@code shift1}
	 * labeled alternative in {@link aParser#shiftExpression}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitShift1(aParser.Shift1Context ctx);
	/**
	 * Visit a parse tree produced by the {@code shift2}
	 * labeled alternative in {@link aParser#shiftExpression}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitShift2(aParser.Shift2Context ctx);
	/**
	 * Visit a parse tree produced by the {@code shift3}
	 * labeled alternative in {@link aParser#shiftExpression}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitShift3(aParser.Shift3Context ctx);
	/**
	 * Visit a parse tree produced by the {@code relation5}
	 * labeled alternative in {@link aParser#relationalExpression}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitRelation5(aParser.Relation5Context ctx);
	/**
	 * Visit a parse tree produced by the {@code relation4}
	 * labeled alternative in {@link aParser#relationalExpression}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitRelation4(aParser.Relation4Context ctx);
	/**
	 * Visit a parse tree produced by the {@code relation3}
	 * labeled alternative in {@link aParser#relationalExpression}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitRelation3(aParser.Relation3Context ctx);
	/**
	 * Visit a parse tree produced by the {@code relation2}
	 * labeled alternative in {@link aParser#relationalExpression}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitRelation2(aParser.Relation2Context ctx);
	/**
	 * Visit a parse tree produced by the {@code relation1}
	 * labeled alternative in {@link aParser#relationalExpression}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitRelation1(aParser.Relation1Context ctx);
	/**
	 * Visit a parse tree produced by the {@code equal1}
	 * labeled alternative in {@link aParser#equalityExpression}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitEqual1(aParser.Equal1Context ctx);
	/**
	 * Visit a parse tree produced by the {@code equal2}
	 * labeled alternative in {@link aParser#equalityExpression}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitEqual2(aParser.Equal2Context ctx);
	/**
	 * Visit a parse tree produced by the {@code equal3}
	 * labeled alternative in {@link aParser#equalityExpression}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitEqual3(aParser.Equal3Context ctx);
	/**
	 * Visit a parse tree produced by the {@code and2}
	 * labeled alternative in {@link aParser#andExpression}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitAnd2(aParser.And2Context ctx);
	/**
	 * Visit a parse tree produced by the {@code and1}
	 * labeled alternative in {@link aParser#andExpression}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitAnd1(aParser.And1Context ctx);
	/**
	 * Visit a parse tree produced by the {@code eor2}
	 * labeled alternative in {@link aParser#exclusiveOrExpression}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitEor2(aParser.Eor2Context ctx);
	/**
	 * Visit a parse tree produced by the {@code eor1}
	 * labeled alternative in {@link aParser#exclusiveOrExpression}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitEor1(aParser.Eor1Context ctx);
	/**
	 * Visit a parse tree produced by the {@code ior1}
	 * labeled alternative in {@link aParser#inclusiveOrExpression}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitIor1(aParser.Ior1Context ctx);
	/**
	 * Visit a parse tree produced by the {@code ior2}
	 * labeled alternative in {@link aParser#inclusiveOrExpression}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitIor2(aParser.Ior2Context ctx);
	/**
	 * Visit a parse tree produced by the {@code land2}
	 * labeled alternative in {@link aParser#logicalAndExpression}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitLand2(aParser.Land2Context ctx);
	/**
	 * Visit a parse tree produced by the {@code land1}
	 * labeled alternative in {@link aParser#logicalAndExpression}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitLand1(aParser.Land1Context ctx);
	/**
	 * Visit a parse tree produced by the {@code lor1}
	 * labeled alternative in {@link aParser#logicalOrExpression}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitLor1(aParser.Lor1Context ctx);
	/**
	 * Visit a parse tree produced by the {@code lor2}
	 * labeled alternative in {@link aParser#logicalOrExpression}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitLor2(aParser.Lor2Context ctx);
	/**
	 * Visit a parse tree produced by the {@code assign1}
	 * labeled alternative in {@link aParser#assignmentExpression}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitAssign1(aParser.Assign1Context ctx);
	/**
	 * Visit a parse tree produced by the {@code assign2}
	 * labeled alternative in {@link aParser#assignmentExpression}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitAssign2(aParser.Assign2Context ctx);
	/**
	 * Visit a parse tree produced by {@link aParser#assignmentOperator}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitAssignmentOperator(aParser.AssignmentOperatorContext ctx);
	/**
	 * Visit a parse tree produced by the {@code dim1}
	 * labeled alternative in {@link aParser#dim_expr}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitDim1(aParser.Dim1Context ctx);
	/**
	 * Visit a parse tree produced by the {@code dim2}
	 * labeled alternative in {@link aParser#dim_expr}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitDim2(aParser.Dim2Context ctx);
	/**
	 * Visit a parse tree produced by the {@code dim3}
	 * labeled alternative in {@link aParser#dimexpr}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitDim3(aParser.Dim3Context ctx);
	/**
	 * Visit a parse tree produced by the {@code dim4}
	 * labeled alternative in {@link aParser#dimexpr}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitDim4(aParser.Dim4Context ctx);
	/**
	 * Visit a parse tree produced by the {@code expr1}
	 * labeled alternative in {@link aParser#expr}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitExpr1(aParser.Expr1Context ctx);
	/**
	 * Visit a parse tree produced by the {@code expr2}
	 * labeled alternative in {@link aParser#expr}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitExpr2(aParser.Expr2Context ctx);
	/**
	 * Visit a parse tree produced by the {@code const1}
	 * labeled alternative in {@link aParser#constant}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitConst1(aParser.Const1Context ctx);
	/**
	 * Visit a parse tree produced by the {@code const2}
	 * labeled alternative in {@link aParser#constant}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitConst2(aParser.Const2Context ctx);
	/**
	 * Visit a parse tree produced by the {@code const3}
	 * labeled alternative in {@link aParser#constant}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitConst3(aParser.Const3Context ctx);
	/**
	 * Visit a parse tree produced by the {@code const4}
	 * labeled alternative in {@link aParser#constant}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitConst4(aParser.Const4Context ctx);
}