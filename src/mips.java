/*
ret		ret $src
retnull retnull;
jump	jump %target
br		br $cond %ifTrue %ifFalse


store	store size $addr $src offset    // M[$addr+offset : $addr+offset+size-1] <- $src
load	$dest = load size $addr offset  // $dest <- M[$addr+offset : $addr+offset+size-1]
alloc	$dest = alloc $size

call	call funcname $op1 $op2 $op3 ...
dcall	$dest = call funcname $op1 $op2 $op3 ...

move	$dest = move $src

phi		$dest = phi %block1 $reg1 %block2 $reg2 ...

neg		$dest = neg $src
add		$dest = add $src1 $src2
sub		$dest = sub $src1 $src2
mul		$dest = mul $src1 $src2
div		$dest = div $src1 $src2
rem		$dest = rem $src1 $src2

shl		$dest = shl $src1 $src2
shr		$dest = shr $src1 $src2
and		$dest = and $src1 $src2
xor		$dest = xor $src1 $src2
or		$dest = or $src1 $src2
not		$dest = not $src

slt		$dest = slt $src1 $src2
sgt		$dest = sgt $src1 $src2
sle		$dest = sle $src1 $src2
sge		$dest = sge $src1 $src2
seq		$dest = seq $src1 $src2
sne		$dest = sne $src1 $src2
func 	func funcname $op1 $op2 ...
lable   %lable
*/
import java.util.ArrayList;
import java.util.jar.Pack200;

/**
 * Created by xuezhendong on 16/4/27.
 */



public class mips
{
    static int regnum = 0;
    static int maxarg = 0;
    void getmips()
    {
        System.out.println(".data");
        System.out.println("#" + rstring.strlist.size());
        for (int i = 0; i < rstring.strlist.size(); ++i)
        {
            rstring rs = rstring.strlist.get(i);
            System.out.println(".word "+ rs.len);
            System.out.println("r" + rs.r.No + ": .asciiz \"" + rs.ctx + "\"");
            System.out.println(".align 2");
        }
        for (int i = 0; i < register.globallist.size(); ++i)
        {
            register gr = register.globallist.get(i);
            System.out.println("r" + gr.No + ": .word "+ 0);

        }
        System.out.println(".text");
        for (int i = 0; i < IR.instrlist.size(); ++i)
        {
            instr inst = IR.instrlist.get(i);
            if (inst.op.equals("func"))
            {
                if (inst.funcname.equals("main"))
                {
                    inst.regnum = IR.mainregnum;
                    if (IR.mainmaxarg > inst.maxarg)
                    {
                        inst.maxarg = IR.mainmaxarg;
                    }
                }
                regnum = inst.regnum;
                maxarg = inst.maxarg;
                int fs = regnum * 4 + maxarg * 4 + 16;
                System.out.println(inst.funcname + ":");
                System.out.println("subu $sp,$sp," + fs);
                System.out.println("sw $fp," + (fs - 8) + "($sp)");
                System.out.println("sw $ra," + (fs - 4) + "($sp)"); //todo check -4 or 0 12
                System.out.println("sw $t0," + (fs - 16) + "($sp)");
                System.out.println("sw $t1," + (fs - 12) + "($sp)");
                System.out.println("addiu $fp,$sp," + (fs - 4));    //todo check

                if (inst.funcname.equals("main"))
                {
                    for (int j = 0; j < register.globallist.size(); ++j)
                    {
                        register gr = register.globallist.get(j);
                        if (gr.isstring == 0)
                        {
                            for (int k = 0; k < gr.glbinstrlist.size(); ++k)
                            {
                                print(gr.glbinstrlist.get(k));
                            }
                        }
                    }
                }
                else
                {
                }
            }
            else
            {
                System.out.println();
                System.out.println("#" + inst.op);
                print(inst);
            }
        }
    }
    void print(instr inst)
    {
        int fs = mips.regnum * 4 + mips.maxarg * 4 + 16;
        if (inst.op .equals( "ret"))
        {
            if (inst.src1.sort == 0)
            {
                System.out.println("li $v0," + inst.src1.c);
            }
            else
            {
                if (inst.src1.r.isstring == 1)
                {
                    System.out.println("la $v0,r" + inst.src1.r.No);
                }
                else
                {
                    if (inst.src1.r.global < 0) {
                        if (inst.src1.r.arg < 0) {
                            System.out.println("lw $v0," + (inst.src1.r.offset + mips.maxarg * 4) + "($sp)");
                        } else {
                            System.out.println("lw $v0," + (fs + inst.src1.r.arg * 4) + "($sp)");
                        }
                    }
                    else {
                        System.out.println("lw $v0," + "r" + inst.src1.r.No);
                    }
                }
            }
            System.out.println("lw $fp," + (fs - 8) + "($sp)");
            System.out.println("lw $ra," + (fs - 4) + "($sp)"); //todo check -4 or 0 12
            System.out.println("lw $t0," + (fs - 16) + "($sp)");
            System.out.println("lw $t1," + (fs - 12) + "($sp)");
            System.out.println("addiu $sp,$sp," + fs);
            System.out.println("jr $ra");
        }
        if (inst.op .equals( "retnull"))
        {
            System.out.println("lw $fp," + (fs - 8) + "($sp)");
            System.out.println("lw $ra," + (fs - 4) + "($sp)"); //todo check -4 or 0 12
            System.out.println("lw $t0," + (fs - 16) + "($sp)");
            System.out.println("lw $t1," + (fs - 12) + "($sp)");
            System.out.println("addiu $sp,$sp," + fs);
            System.out.println("jr $ra");
        }
        if (inst.op .equals( "jump"))
        {
            System.out.println("jal " + inst.l1.name);
        }
        if (inst.op .equals( "br"))
        {
            if (inst.src1.r.global < 0)
            {
                if (inst.src1.r.arg < 0)
                {
                    System.out.println("lw $t0," + (inst.src1.r.offset + mips.maxarg * 4) + "($sp)");
                }
                else
                {
                    System.out.println("lw $t0," + (fs + inst.src1.r.arg * 4)+ "($sp)");
                }
            }
            else
            {
                System.out.println("lw $t0," + "r" + inst.src1.r.No);
            }
            System.out.println("beq $t0,1," + inst.l1.name);
            System.out.println("jal " + inst.l2.name);
        }
        if (inst.op .equals( "store"))
        {
            if (inst.src1.sort == 0)
            {
                System.out.println("li $t0," + inst.src1.c);
            }
            else
            {
                if (inst.src1.r.isstring == 1)
                {
                    System.out.println("la $t0,r" + inst.src1.r.No);
                }
                else
                {
                    if (inst.src1.r.global < 0) {

                        if (inst.src1.r.arg < 0) {
                            System.out.println("lw $t0," + (inst.src1.r.offset + mips.maxarg * 4) + "($sp)");
                        } else {
                            System.out.println("lw $t0," + (fs + inst.src1.r.arg * 4) + "($sp)");
                        }
                    } else {
                        System.out.println("lw $t0," + "r" + inst.src1.r.No);
                    }
                }
            }
            if (inst.addr.sort == 0)
            {
                    System.out.println("li $t1," + inst.addr.c);
            }
            else
            {
                if (inst.addr.r.global < 0)
                {
                    if (inst.addr.r.arg < 0)
                    {
                        System.out.println("lw $t1," + (inst.addr.r.offset + mips.maxarg * 4) + "($sp)");

                    }
                    else
                    {
                        System.out.println("lw $t1," + (fs + inst.addr.r.arg * 4) + "($sp)");
                    }
                }
                else
                {
                    System.out.println("lw $t1," + "r" + inst.addr.r.No);
                }
            }

            System.out.println("sw $t0," + inst.offset +"($t1)");
        }
        if (inst.op .equals("load"))
        {

            if (inst.addr.sort == 0)
            {
                System.out.println("li $t1," + inst.addr.c);
            }
            else
            {
                if (inst.addr.r.global < 0)
                {
                    if (inst.addr.r.arg < 0)
                    {
                        System.out.println("lw $t1," + (inst.addr.r.offset + mips.maxarg * 4) + "($sp)");
                    }
                    else
                    {
                        System.out.println("lw $t1," + (fs + inst.addr.r.arg * 4) + "($sp)");
                    }
                }
                else
                {
                    System.out.println("lw $t1," + "r" + inst.addr.r.No);
                }
            }
            System.out.println("lw $t0," + inst.offset +"($t1)");
            if (inst.dest.global < 0)
            {
                if (inst.dest.arg < 0)
                {
                    System.out.println("sw $t0," + (inst.dest.offset + mips.maxarg * 4) + "($sp)");
                }
                else
                {
                    System.out.println("sw $t0," + (fs + inst.dest.arg * 4)+ "($sp)");
                }
            }
            else
            {
                System.out.println("sw $t0," + "r" + inst.dest.No);
            }
        }

        if (inst.op .equals( "alloc"))
        {
            System.out.println("li $v0,9");
            if (inst.asize.sort == 0)
            {
                System.out.println("li $t0," + 4 * inst.asize.c);
            }
            else
            {

                if (inst.asize.r.global < 0)
                {
                    if (inst.asize.r.arg < 0)
                    {
                        System.out.println("lw $t0," + (inst.asize.r.offset + mips.maxarg * 4) + "($sp)");
                    }
                    else
                    {
                        System.out.println("lw $t0," + (fs + inst.asize.r.arg * 4) + "($sp)");
                    }
                }
                else
                {
                    System.out.println("lw $t0," + "r" + inst.asize.r.No);
                }
                System.out.println("li $t1,4");
                System.out.println("mul $t0,$t0,$t1");
            }

            System.out.println("addiu $a0,$t0,4");
            System.out.println("syscall");
            System.out.println("divu  $t0,$t0,4");
            System.out.println("sw $t0,0($v0)");     //todo check
            System.out.println("addiu $v0,$v0,4");

            if (inst.dest.global < 0)
            {

                if (inst.dest.arg < 0)
                {
                    System.out.println("sw $v0," + (inst.dest.offset + mips.maxarg * 4) + "($sp)");
                }
                else
                {
                    System.out.println("sw $v0," + (fs + inst.dest.arg * 4)+ "($sp)");
                }
            }
            else
            {
                System.out.println("sw $v0," + "r" + inst.dest.No);
            }

        }
        if (inst.op .equals("dcall"))
        {

            for (int j = 0; j < inst.oplist.size(); ++j)
            {
                src op = inst.oplist.get(j);
                if (op.sort == 0)
                {
                    System.out.println("li $t0," + op.c);
                }
                else
                {
                    if (op.r.isstring == 1)
                    {
                        System.out.println("la $t0,r" + op.r.No);
                    }
                    else
                    {
                        if (op.r.global < 0) {
                            if (op.r.arg < 0) {
                                System.out.println("lw $t0," + (op.r.offset + mips.maxarg * 4) + "($sp)");
                            } else {
                                System.out.println("lw $t0," + (fs + op.r.arg * 4) + "($sp)");
                            }
                        } else {
                            System.out.println("lw $t0," + "r" + op.r.No);
                        }
                    }
                }
                System.out.println("sw $t0," + j * 4 + "($sp)");
                if (inst.funcname.equals("func__print") ||
                        inst.funcname.equals("func__println") ||
                        inst.funcname.equals("func__getString") ||
                        inst.funcname.equals("func__getInt") ||
                        inst.funcname.equals("func__toString") ||
                        inst.funcname.equals("func__string.length") ||
                        inst.funcname.equals("func__string.substring") ||
                        inst.funcname.equals("func__string.parseInt") ||
                        inst.funcname.equals("func__string.ord") ||
                        inst.funcname.equals("func__array.size") ||
                        inst.funcname.equals("func__stringConcatenate") ||
                        inst.funcname.equals("func__stringIsEqual") ||
                        inst.funcname.equals("func__stringLess") ||
                        inst.funcname.equals("func__stringLeq") ||
                        inst.funcname.equals("func__stringGeq") ||
                        inst.funcname.equals("func__stringNeq") ||
                        inst.funcname.equals("func__stringLarge"))
                {
                    System.out.println("move $a" + j + ",$t0");
                }
            }
            System.out.println("jal " + inst.funcname);
            System.out.println("move $t0,$v0");
            if (inst.dest.global < 0)
            {
                if (inst.dest.arg < 0)
                {
                    System.out.println("sw $t0," + (inst.dest.offset + mips.maxarg * 4) + "($sp)");
                }
                else
                {
                    System.out.println("sw $t0," + (fs + inst.dest.arg * 4)+ "($sp)");
                }
            }
            else
            {
                System.out.println("sw $t0," + "r" + inst.dest.No);
            }
        }
        if (inst.op .equals( "move"))
        {
            if (inst.src1.sort == 0)
            {
                System.out.println("li $t1," + inst.src1.c);
            }
            else
            {
                if (inst.src1.r.isstring == 1)
                {
                    System.out.println("la $t1,r" + inst.src1.r.No);
                }
                else
                {
                    if (inst.src1.r.global < 0) {
                        if (inst.src1.r.arg < 0) {
                            System.out.println("lw $t1," + (inst.src1.r.offset + mips.maxarg * 4) + "($sp)");
                        } else {
                            System.out.println("lw $t1," + (fs + inst.src1.r.arg * 4) + "($sp)");
                        }
                    } else {
                        System.out.println("lw $t1," + "r" + inst.src1.r.No);
                    }
                }
            }
            System.out.println("move $t0,$t1");
            if (inst.dest.global < 0)
            {
                if (inst.dest.arg < 0)
                {
                    System.out.println("sw $t0," + (inst.dest.offset + mips.maxarg * 4) + "($sp)");
                }
                else
                {
                    System.out.println("sw $t0," + (fs + inst.dest.arg * 4)+ "($sp)");
                }
            }
            else
            {
                System.out.println("sw $t0," + "r" + inst.dest.No);
            }

        }
        if (inst.op .equals( "neg"))
        {
            if (inst.src1.sort == 0)
            {
                System.out.println("li $t1," + inst.src1.c);
            }
            else
            {
                if (inst.src1.r.global < 0)
                {
                    if (inst.src1.r.arg < 0)
                    {
                        System.out.println("lw $t1," + (inst.src1.r.offset + mips.maxarg * 4) + "($sp)");
                    }
                    else
                    {
                        System.out.println("lw $t1," + (fs + inst.src1.r.arg * 4) + "($sp)");
                    }
                }
                else
                {
                    System.out.println("lw $t1," + "r" + inst.src1.r.No);
                }
            }
            System.out.println("neg $t0,$t1");
            if (inst.dest.global < 0)
            {
                if (inst.dest.arg < 0)
                {
                    System.out.println("sw $t0," + (inst.dest.offset + mips.maxarg * 4) + "($sp)");
                }
                else
                {
                    System.out.println("sw $t0," + (fs + inst.dest.arg * 4)+ "($sp)");
                }
            }
            else
            {
                System.out.println("sw $t0," + "r" + inst.dest.No);
            }
        }
        if (inst.op .equals( "add"))
        {
            if (inst.src1.sort == 0)
            {
                if (inst.src2.sort == 0)
                {
                    System.out.println("li $t0," + inst.src2.c);
                }
                else
                {
                    if (inst.src2.r.global < 0)
                    {
                        if (inst.src2.r.arg < 0)
                        {
                            System.out.println("lw $t0," + (inst.src2.r.offset + mips.maxarg * 4) + "($sp)");
                        }
                        else
                        {
                            System.out.println("lw $t0," + (fs + inst.src2.r.arg * 4) + "($sp)");
                        }
                    }
                    else
                    {
                        System.out.println("lw $t0," + "r" + inst.src2.r.No);
                    }
                }
                System.out.println("addi $t0,$t0," + inst.src1.c);
            }
            else
            {
                if (inst.src2.sort == 0)
                {
                    if (inst.src1.r.global < 0)
                    {
                        if (inst.src1.r.arg < 0)
                        {
                            System.out.println("lw $t0," + (inst.src1.r.offset + mips.maxarg * 4) + "($sp)");
                        }
                        else
                        {
                            System.out.println("lw $t0," + (fs + inst.src1.r.arg * 4) + "($sp)");
                        }
                    }
                    else
                    {
                        System.out.println("lw $t0," + "r" + inst.src1.r.No);
                    }
                    System.out.println("addi $t0,$t0," + inst.src2.c);
                }
                else
                {
                    if (inst.src1.r.global < 0)
                    {
                        if (inst.src1.r.arg < 0)
                        {
                            System.out.println("lw $t0," + (inst.src1.r.offset + mips.maxarg * 4) + "($sp)");
                        }
                        else
                        {
                            System.out.println("lw $t0," + (fs + inst.src1.r.arg * 4) + "($sp)");
                        }
                    }
                    else
                    {
                        System.out.println("lw $t0," + "r" + inst.src1.r.No);
                    }
                    if (inst.src2.r.global < 0)
                    {
                        if (inst.src2.r.arg < 0)
                        {
                            System.out.println("lw $t1," + (inst.src2.r.offset + mips.maxarg * 4) + "($sp)");
                        }
                        else
                        {
                            System.out.println("lw $t1," + (fs + inst.src2.r.arg * 4) + "($sp)");
                        }
                    }
                    else
                    {
                        System.out.println("lw $t1," + "r" + inst.src2.r.No);
                    }
                    System.out.println("add $t0,$t0,$t1");
                }
            }

            if (inst.dest.global < 0)
            {
                if (inst.dest.arg < 0)
                {
                    System.out.println("sw $t0," + (inst.dest.offset + mips.maxarg * 4) + "($sp)");
                }
                else
                {
                    System.out.println("sw $t0," + (fs + inst.dest.arg * 4)+ "($sp)");
                }
            }
            else
            {
                System.out.println("sw $t0," + "r" + inst.dest.No);
            }
        }
        if (inst.op.equals("sub"))
        {
            if (inst.src1.sort == 0)
            {
                System.out.println("li $t0," + inst.src1.c);
            }
            else
            {
                if (inst.src1.r.global < 0)
                {
                    if (inst.src1.r.arg < 0)
                    {
                        System.out.println("lw $t0," + (inst.src1.r.offset + mips.maxarg * 4) + "($sp)");
                    }
                    else
                    {
                        System.out.println("lw $t0," + (fs + inst.src1.r.arg * 4) + "($sp)");
                    }
                }
                else
                {
                    System.out.println("lw $t0," + "r" + inst.src1.r.No);
                }
            }
            if (inst.src2.sort == 0)
            {
                System.out.println("li $t1," + inst.src2.c);
            }
            else
            {
                if (inst.src2.r.global < 0)
                {
                    if (inst.src2.r.arg < 0)
                    {
                        System.out.println("lw $t1," + (inst.src2.r.offset + mips.maxarg * 4) + "($sp)");
                    }
                    else
                    {
                        System.out.println("lw $t1," + (fs + inst.src2.r.arg * 4) + "($sp)");
                    }
                }
                else
                {
                    System.out.println("lw $t1," + "r" + inst.src2.r.No);
                }
            }
            System.out.println("subu $t0,$t0,$t1");
            if (inst.dest.global < 0)
            {
                if (inst.dest.arg < 0)
                {
                    System.out.println("sw $t0," + (inst.dest.offset + mips.maxarg * 4) + "($sp)");
                }
                else
                {
                    System.out.println("sw $t0," + (fs + inst.dest.arg * 4)+ "($sp)");
                }
            }
            else
            {
                System.out.println("sw $t0," + "r" + inst.dest.No);
            }
        }
        if (inst.op .equals( "mul"))
        {
            if (inst.src1.sort == 0)
            {
                System.out.println("li $t0," + inst.src1.c);
            }
            else
            {
                if (inst.src1.r.global < 0)
                {
                    if (inst.src1.r.arg < 0)
                    {
                        System.out.println("lw $t0," + (inst.src1.r.offset + mips.maxarg * 4) + "($sp)");
                    }
                    else
                    {
                        System.out.println("lw $t0," + (fs + inst.src1.r.arg * 4) + "($sp)");
                    }
                }
                else
                {
                    System.out.println("lw $t0," + "r" + inst.src1.r.No);
                }
            }
            if (inst.src2.sort == 0)
            {
                System.out.println("li $t1," + inst.src2.c);
            }
            else
            {
                if (inst.src2.r.global < 0)
                {
                    if (inst.src2.r.arg < 0)
                    {
                        System.out.println("lw $t1," + (inst.src2.r.offset + mips.maxarg * 4) + "($sp)");
                    }
                    else
                    {
                        System.out.println("lw $t1," + (fs + inst.src2.r.arg * 4) + "($sp)");
                    }
                }
                else
                {
                    System.out.println("lw $t1," + "r" + inst.src2.r.No);
                }
            }
            System.out.println("mul $t0,$t0,$t1");
            if (inst.dest.global < 0)
            {
                if (inst.dest.arg < 0)
                {
                    System.out.println("sw $t0," + (inst.dest.offset + mips.maxarg * 4) + "($sp)");
                }
                else
                {
                    System.out.println("sw $t0," + (fs + inst.dest.arg * 4)+ "($sp)");
                }
            }
            else
            {
                System.out.println("sw $t0," + "r" + inst.dest.No);
            }
        }
        if (inst.op .equals( "div"))
        {
            if (inst.src1.sort == 0)
            {
                System.out.println("li $t0," + inst.src1.c);
            }
            else
            {
                if (inst.src1.r.global < 0)
                {
                    if (inst.src1.r.arg < 0)
                    {
                        System.out.println("lw $t0," + (inst.src1.r.offset + mips.maxarg * 4) + "($sp)");
                    }
                    else
                    {
                        System.out.println("lw $t0," + (fs + inst.src1.r.arg * 4) + "($sp)");
                    }
                }
                else
                {
                    System.out.println("lw $t0," + "r" + inst.src1.r.No);
                }
            }
            if (inst.src2.sort == 0)
            {
                System.out.println("li $t1," + inst.src2.c);
            }
            else
            {
                if (inst.src2.r.global < 0)
                {
                    if (inst.src2.r.arg < 0)
                    {
                        System.out.println("lw $t1," + (inst.src2.r.offset + mips.maxarg * 4) + "($sp)");
                    }
                    else
                    {
                        System.out.println("lw $t1," + (fs + inst.src2.r.arg * 4) + "($sp)");
                    }
                }
                else
                {
                    System.out.println("lw $t1," + "r" + inst.src2.r.No);
                }
            }
            System.out.println("divu $t0,$t0,$t1");
            if (inst.dest.global < 0)
            {
                if (inst.dest.arg < 0)
                {
                    System.out.println("sw $t0," + (inst.dest.offset + mips.maxarg * 4) + "($sp)");
                }
                else
                {
                    System.out.println("sw $t0," + (fs + inst.dest.arg * 4)+ "($sp)");
                }
            }
            else
            {
                System.out.println("sw $t0," + "r" + inst.dest.No);
            }
        }
        if (inst.op .equals( "rem"))
        {
            if (inst.src1.sort == 0)
            {
                System.out.println("li $t0," + inst.src1.c);
            }
            else
            {
                if (inst.src1.r.global < 0)
                {
                    if (inst.src1.r.arg < 0)
                    {
                        System.out.println("lw $t0," + (inst.src1.r.offset + mips.maxarg * 4) + "($sp)");
                    }
                    else
                    {
                        System.out.println("lw $t0," + (fs + inst.src1.r.arg * 4) + "($sp)");
                    }
                }
                else
                {
                    System.out.println("lw $t0," + "r" + inst.src1.r.No);
                }
            }
            if (inst.src2.sort == 0)
            {
                System.out.println("li $t1," + inst.src2.c);
            }
            else
            {
                if (inst.src2.r.global < 0)
                {
                    if (inst.src2.r.arg < 0)
                    {
                        System.out.println("lw $t1," + (inst.src2.r.offset + mips.maxarg * 4) + "($sp)");
                    }
                    else
                    {
                        System.out.println("lw $t1," + (fs + inst.src2.r.arg * 4) + "($sp)");
                    }
                }
                else
                {
                    System.out.println("lw $t1," + "r" + inst.src2.r.No);
                }
            }
            System.out.println("rem $t0,$t0,$t1");
            if (inst.dest.global < 0)
            {
                if (inst.dest.arg < 0)
                {
                    System.out.println("sw $t0," + (inst.dest.offset + mips.maxarg * 4) + "($sp)");
                }
                else
                {
                    System.out.println("sw $t0," + (fs + inst.dest.arg * 4)+ "($sp)");
                }
            }
            else
            {
                System.out.println("sw $t0," + "r" + inst.dest.No);
            }
        }
        if (inst.op .equals( "shl"))
        {
            if (inst.src1.sort == 0)
            {
                System.out.println("li $t0," + inst.src1.c);
            }
            else
            {
                if (inst.src1.r.global < 0)
                {
                    if (inst.src1.r.arg < 0)
                    {
                        System.out.println("lw $t0," + (inst.src1.r.offset + mips.maxarg * 4) + "($sp)");
                    }
                    else
                    {
                        System.out.println("lw $t0," + (fs + inst.src1.r.arg * 4) + "($sp)");
                    }
                }
                else
                {
                    System.out.println("lw $t0," + "r" + inst.src1.r.No);
                }
            }
            if (inst.src2.sort == 0)
            {
                System.out.println("li $t1," + inst.src2.c);
            }
            else
            {
                if (inst.src2.r.global < 0)
                {
                    if (inst.src2.r.arg < 0)
                    {
                        System.out.println("lw $t1," + (inst.src2.r.offset + mips.maxarg * 4) + "($sp)");
                    }
                    else
                    {
                        System.out.println("lw $t1," + (fs + inst.src2.r.arg * 4) + "($sp)");
                    }
                }
                else
                {
                    System.out.println("lw $t1," + "r" + inst.src2.r.No);
                }
            }
            System.out.println("sll $t0,$t0,$t1");
            if (inst.dest.global < 0)
            {
                if (inst.dest.arg < 0)
                {
                    System.out.println("sw $t0," + (inst.dest.offset + mips.maxarg * 4) + "($sp)");
                }
                else
                {
                    System.out.println("sw $t0," + (fs + inst.dest.arg * 4)+ "($sp)");
                }
            }
            else
            {
                System.out.println("sw $t0," + "r" + inst.dest.No);
            }
        }
        if (inst.op .equals( "shr"))
        {
            if (inst.src1.sort == 0)
            {
                System.out.println("li $t0," + inst.src1.c);
            }
            else
            {
                if (inst.src1.r.global < 0)
                {
                    if (inst.src1.r.arg < 0)
                    {
                        System.out.println("lw $t0," + (inst.src1.r.offset + mips.maxarg * 4) + "($sp)");
                    }
                    else
                    {
                        System.out.println("lw $t0," + (fs + inst.src1.r.arg * 4) + "($sp)");
                    }
                }
                else
                {
                    System.out.println("lw $t0," + "r" + inst.src1.r.No);
                }
            }
            if (inst.src2.sort == 0)
            {
                System.out.println("li $t1," + inst.src2.c);
            }
            else
            {
                if (inst.src2.r.global < 0)
                {
                    if (inst.src2.r.arg < 0)
                    {
                        System.out.println("lw $t1," + (inst.src2.r.offset + mips.maxarg * 4) + "($sp)");
                    }
                    else
                    {
                        System.out.println("lw $t1," + (fs + inst.src2.r.arg * 4) + "($sp)");
                    }
                }
                else
                {
                    System.out.println("lw $t1," + "r" + inst.src2.r.No);
                }
            }
            System.out.println("sra $t0,$t0,$t1");
            if (inst.dest.global < 0)
            {
                if (inst.dest.arg < 0)
                {
                    System.out.println("sw $t0," + (inst.dest.offset + mips.maxarg * 4) + "($sp)");
                }
                else
                {
                    System.out.println("sw $t0," + (fs + inst.dest.arg * 4)+ "($sp)");
                }
            }
            else
            {
                System.out.println("sw $t0," + "r" + inst.dest.No);
            }
        }
        if (inst.op .equals( "and"))
        {
            if (inst.src1.sort == 0)
            {
                System.out.println("li $t0," + inst.src1.c);
            }
            else
            {
                if (inst.src1.r.global < 0)
                {
                    if (inst.src1.r.arg < 0)
                    {
                        System.out.println("lw $t0," + (inst.src1.r.offset + mips.maxarg * 4) + "($sp)");
                    }
                    else
                    {
                        System.out.println("lw $t0," + (fs + inst.src1.r.arg * 4) + "($sp)");
                    }
                }
                else
                {
                    System.out.println("lw $t0," + "r" + inst.src1.r.No);
                }
            }
            if (inst.src2.sort == 0)
            {
                System.out.println("li $t1," + inst.src2.c);
            }
            else
            {
                if (inst.src2.r.global < 0)
                {
                    if (inst.src2.r.arg < 0)
                    {
                        System.out.println("lw $t1," + (inst.src2.r.offset + mips.maxarg * 4) + "($sp)");
                    }
                    else
                    {
                        System.out.println("lw $t1," + (fs + inst.src2.r.arg * 4) + "($sp)");
                    }
                }
                else
                {
                    System.out.println("lw $t1," + "r" + inst.src2.r.No);
                }
            }
            System.out.println("and $t0,$t0,$t1");
            if (inst.dest.global < 0)
            {
                if (inst.dest.arg < 0)
                {
                    System.out.println("sw $t0," + (inst.dest.offset + mips.maxarg * 4) + "($sp)");
                }
                else
                {
                    System.out.println("sw $t0," + (fs + inst.dest.arg * 4)+ "($sp)");
                }
            }
            else
            {
                System.out.println("sw $t0," + "r" + inst.dest.No);
            }
        }
        if (inst.op .equals( "or"))
        {
            if (inst.src1.sort == 0)
            {
                System.out.println("li $t0," + inst.src1.c);
            }
            else
            {
                if (inst.src1.r.global < 0)
                {
                    if (inst.src1.r.arg < 0)
                    {
                        System.out.println("lw $t0," + (inst.src1.r.offset + mips.maxarg * 4) + "($sp)");
                    }
                    else
                    {
                        System.out.println("lw $t0," + (fs + inst.src1.r.arg * 4) + "($sp)");
                    }
                }
                else
                {
                    System.out.println("lw $t0," + "r" + inst.src1.r.No);
                }
            }
            if (inst.src2.sort == 0)
            {
                System.out.println("li $t1," + inst.src2.c);
            }
            else
            {
                if (inst.src2.r.global < 0)
                {
                    if (inst.src2.r.arg < 0)
                    {
                        System.out.println("lw $t1," + (inst.src2.r.offset + mips.maxarg * 4) + "($sp)");
                    }
                    else
                    {
                        System.out.println("lw $t1," + (fs + inst.src2.r.arg * 4) + "($sp)");
                    }
                }
                else
                {
                    System.out.println("lw $t1," + "r" + inst.src2.r.No);
                }
            }
            System.out.println("or $t0,$t0,$t1");
            if (inst.dest.global < 0)
            {
                if (inst.dest.arg < 0)
                {
                    System.out.println("sw $t0," + (inst.dest.offset + mips.maxarg * 4) + "($sp)");
                }
                else
                {
                    System.out.println("sw $t0," + (fs + inst.dest.arg * 4)+ "($sp)");
                }
            }
            else
            {
                System.out.println("sw $t0," + "r" + inst.dest.No);
            }
        }
        if (inst.op .equals( "xor"))
        {
            if (inst.src1.sort == 0)
            {
                System.out.println("li $t0," + inst.src1.c);
            }
            else
            {
                if (inst.src1.r.global < 0)
                {
                    if (inst.src1.r.arg < 0)
                    {
                        System.out.println("lw $t0," + (inst.src1.r.offset + mips.maxarg * 4) + "($sp)");
                    }
                    else
                    {
                        System.out.println("lw $t0," + (fs + inst.src1.r.arg * 4) + "($sp)");
                    }
                }
                else
                {
                    System.out.println("lw $t0," + "r" + inst.src1.r.No);
                }
            }
            if (inst.src2.sort == 0)
            {
                System.out.println("li $t1," + inst.src2.c);
            }
            else
            {
                if (inst.src2.r.global < 0)
                {
                    if (inst.src2.r.arg < 0)
                    {
                        System.out.println("lw $t1," + (inst.src2.r.offset + mips.maxarg * 4) + "($sp)");
                    }
                    else
                    {
                        System.out.println("lw $t1," + (fs + inst.src2.r.arg * 4) + "($sp)");
                    }
                }
                else
                {
                    System.out.println("lw $t1," + "r" + inst.src2.r.No);
                }
            }
            System.out.println("xor $t0,$t0,$t1");
            if (inst.dest.global < 0)
            {
                if (inst.dest.arg < 0)
                {
                    System.out.println("sw $t0," + (inst.dest.offset + mips.maxarg * 4) + "($sp)");
                }
                else
                {
                    System.out.println("sw $t0," + (fs + inst.dest.arg * 4)+ "($sp)");
                }
            }
            else
            {
                System.out.println("sw $t0," + "r" + inst.dest.No);
            }
        }
        if (inst.op .equals( "not"))
        {
            System.out.println("li $t0,1");
            if (inst.src1.sort == 0)
            {
                System.out.println("li $t1," + inst.src1.c);
            }
            else
            {
                if (inst.src1.r.global < 0)
                {
                    if (inst.src1.r.arg < 0)
                    {
                        System.out.println("lw $t1," + (inst.src1.r.offset + mips.maxarg * 4) + "($sp)");
                    }
                    else
                    {
                        System.out.println("lw $t1," + (fs + inst.src1.r.arg * 4) + "($sp)");
                    }
                }
                else
                {
                    System.out.println("lw $t1," + "r" + inst.src1.r.No);
                }
            }
            System.out.println("sub $t0,$t0,$t1");
            if (inst.dest.global < 0)
            {
                if (inst.dest.arg < 0)
                {
                    System.out.println("sw $t0," + (inst.dest.offset + mips.maxarg * 4) + "($sp)");
                }
                else
                {
                    System.out.println("sw $t0," + (fs + inst.dest.arg * 4)+ "($sp)");
                }
            }
            else
            {
                System.out.println("sw $t0," + "r" + inst.dest.No);
            }
        }
        if (inst.op .equals( "slt"))
        {
            if (inst.src1.sort == 0)
            {
                System.out.println("li $t0," + inst.src1.c);
            }
            else
            {
                if (inst.src1.r.global < 0)
                {
                    if (inst.src1.r.arg < 0)
                    {
                        System.out.println("lw $t0," + (inst.src1.r.offset + mips.maxarg * 4) + "($sp)");
                    }
                    else
                    {
                        System.out.println("lw $t0," + (fs + inst.src1.r.arg * 4) + "($sp)");
                    }
                }
                else
                {
                    System.out.println("lw $t0," + "r" + inst.src1.r.No);
                }
            }
            if (inst.src2.sort == 0)
            {
                System.out.println("li $t1," + inst.src2.c);
            }
            else
            {
                if (inst.src2.r.global < 0)
                {
                    if (inst.src2.r.arg < 0)
                    {
                        System.out.println("lw $t1," + (inst.src2.r.offset + mips.maxarg * 4) + "($sp)");
                    }
                    else
                    {
                        System.out.println("lw $t1," + (fs + inst.src2.r.arg * 4) + "($sp)");
                    }
                }
                else
                {
                    System.out.println("lw $t1," + "r" + inst.src2.r.No);
                }
            }
            System.out.println("slt $t0,$t0,$t1");
            if (inst.dest.global < 0)
            {
                if (inst.dest.arg < 0)
                {
                    System.out.println("sw $t0," + (inst.dest.offset + mips.maxarg * 4) + "($sp)");
                }
                else
                {
                    System.out.println("sw $t0," + (fs + inst.dest.arg * 4)+ "($sp)");
                }
            }
            else
            {
                System.out.println("sw $t0," + "r" + inst.dest.No);
            }
        }
        if (inst.op .equals( "sgt"))
        {
            if (inst.src1.sort == 0)
            {
                System.out.println("li $t0," + inst.src1.c);
            }
            else
            {
                if (inst.src1.r.global < 0)
                {
                    if (inst.src1.r.arg < 0)
                    {
                        System.out.println("lw $t0," + (inst.src1.r.offset + mips.maxarg * 4) + "($sp)");
                    }
                    else
                    {
                        System.out.println("lw $t0," + (fs + inst.src1.r.arg * 4) + "($sp)");
                    }
                }
                else
                {
                    System.out.println("lw $t0," + "r" + inst.src1.r.No);
                }
            }
            if (inst.src2.sort == 0)
            {
                System.out.println("li $t1," + inst.src2.c);
            }
            else
            {
                if (inst.src2.r.global < 0)
                {
                    if (inst.src2.r.arg < 0)
                    {
                        System.out.println("lw $t1," + (inst.src2.r.offset + mips.maxarg * 4) + "($sp)");
                    }
                    else
                    {
                        System.out.println("lw $t1," + (fs + inst.src2.r.arg * 4) + "($sp)");
                    }
                }
                else
                {
                    System.out.println("lw $t1," + "r" + inst.src2.r.No);
                }
            }
            System.out.println("sgt $t0,$t0,$t1");
            if (inst.dest.global < 0)
            {
                if (inst.dest.arg < 0)
                {
                    System.out.println("sw $t0," + (inst.dest.offset + mips.maxarg * 4) + "($sp)");
                }
                else
                {
                    System.out.println("sw $t0," + (fs + inst.dest.arg * 4)+ "($sp)");
                }
            }
            else
            {
                System.out.println("sw $t0," + "r" + inst.dest.No);
            }
        }
        if (inst.op .equals( "sle"))
        {
            if (inst.src1.sort == 0)
            {
                System.out.println("li $t0," + inst.src1.c);
            }
            else
            {
                if (inst.src1.r.global < 0)
                {
                    if (inst.src1.r.arg < 0)
                    {
                        System.out.println("lw $t0," + (inst.src1.r.offset + mips.maxarg * 4) + "($sp)");
                    }
                    else
                    {
                        System.out.println("lw $t0," + (fs + inst.src1.r.arg * 4) + "($sp)");
                    }
                }
                else
                {
                    System.out.println("lw $t0," + "r" + inst.src1.r.No);
                }
            }
            if (inst.src2.sort == 0)
            {
                System.out.println("li $t1," + inst.src2.c);
            }
            else
            {
                if (inst.src2.r.global < 0)
                {
                    if (inst.src2.r.arg < 0)
                    {
                        System.out.println("lw $t1," + (inst.src2.r.offset + mips.maxarg * 4) + "($sp)");
                    }
                    else
                    {
                        System.out.println("lw $t1," + (fs + inst.src2.r.arg * 4) + "($sp)");
                    }
                }
                else
                {
                    System.out.println("lw $t1," + "r" + inst.src2.r.No);
                }
            }
            System.out.println("sle $t0,$t0,$t1");
            if (inst.dest.global < 0)
            {
                if (inst.dest.arg < 0)
                {
                    System.out.println("sw $t0," + (inst.dest.offset + mips.maxarg * 4) + "($sp)");
                }
                else
                {
                    System.out.println("sw $t0," + (fs + inst.dest.arg * 4)+ "($sp)");
                }
            }
            else
            {
                System.out.println("sw $t0," + "r" + inst.dest.No);
            }
        }
        if (inst.op .equals( "sge"))
        {
            if (inst.src1.sort == 0)
            {
                System.out.println("li $t0," + inst.src1.c);
            }
            else
            {
                if (inst.src1.r.global < 0)
                {
                    if (inst.src1.r.arg < 0)
                    {
                        System.out.println("lw $t0," + (inst.src1.r.offset + mips.maxarg * 4) + "($sp)");
                    }
                    else
                    {
                        System.out.println("lw $t0," + (fs + inst.src1.r.arg * 4) + "($sp)");
                    }
                }
                else
                {
                    System.out.println("lw $t0," + "r" + inst.src1.r.No);
                }
            }
            if (inst.src2.sort == 0)
            {
                System.out.println("li $t1," + inst.src2.c);
            }
            else
            {
                if (inst.src2.r.global < 0)
                {
                    if (inst.src2.r.arg < 0)
                    {
                        System.out.println("lw $t1," + (inst.src2.r.offset + mips.maxarg * 4) + "($sp)");
                    }
                    else
                    {
                        System.out.println("lw $t1," + (fs + inst.src2.r.arg * 4) + "($sp)");
                    }
                }
                else
                {
                    System.out.println("lw $t1," + "r" + inst.src2.r.No);
                }
            }
            System.out.println("sge $t0,$t0,$t1");
            if (inst.dest.global < 0)
            {
                if (inst.dest.arg < 0)
                {
                    System.out.println("sw $t0," + (inst.dest.offset + mips.maxarg * 4) + "($sp)");
                }
                else
                {
                    System.out.println("sw $t0," + (fs + inst.dest.arg * 4)+ "($sp)");
                }
            }
            else
            {
                System.out.println("sw $t0," + "r" + inst.dest.No);
            }
        }
        if (inst.op .equals( "seq"))
        {
            if (inst.src1.sort == 0)
            {
                System.out.println("li $t0," + inst.src1.c);
            }
            else
            {
                if (inst.src1.r.global < 0)
                {
                    if (inst.src1.r.arg < 0)
                    {
                        System.out.println("lw $t0," + (inst.src1.r.offset + mips.maxarg * 4) + "($sp)");
                    }
                    else
                    {
                        System.out.println("lw $t0," + (fs + inst.src1.r.arg * 4) + "($sp)");
                    }
                }
                else
                {
                    System.out.println("lw $t0," + "r" + inst.src1.r.No);
                }
            }
            if (inst.src2.sort == 0)
            {
                System.out.println("li $t1," + inst.src2.c);
            }
            else
            {
                if (inst.src2.r.global < 0)
                {
                    if (inst.src2.r.arg < 0)
                    {
                        System.out.println("lw $t1," + (inst.src2.r.offset + mips.maxarg * 4) + "($sp)");
                    }
                    else
                    {
                        System.out.println("lw $t1," + (fs + inst.src2.r.arg * 4) + "($sp)");
                    }
                }
                else
                {
                    System.out.println("lw $t1," + "r" + inst.src2.r.No);
                }
            }
            System.out.println("seq $t0,$t0,$t1");
            if (inst.dest.global < 0)
            {
                if (inst.dest.arg < 0)
                {
                    System.out.println("sw $t0," + (inst.dest.offset + mips.maxarg * 4) + "($sp)");
                }
                else
                {
                    System.out.println("sw $t0," + (fs + inst.dest.arg * 4)+ "($sp)");
                }
            }
            else
            {
                System.out.println("sw $t0," + "r" + inst.dest.No);
            }
        }
        if (inst.op .equals( "sne"))
        {
            if (inst.src1.sort == 0)
            {
                System.out.println("li $t0," + inst.src1.c);
            }
            else
            {
                if (inst.src1.r.global < 0)
                {
                    if (inst.src1.r.arg < 0)
                    {
                        System.out.println("lw $t0," + (inst.src1.r.offset + mips.maxarg * 4) + "($sp)");
                    }
                    else
                    {
                        System.out.println("lw $t0," + (fs + inst.src1.r.arg * 4) + "($sp)");
                    }
                }
                else
                {
                    System.out.println("lw $t0," + "r" + inst.src1.r.No);
                }
            }
            if (inst.src2.sort == 0)
            {
                System.out.println("li $t1," + inst.src2.c);
            }
            else
            {
                if (inst.src2.r.global < 0)
                {
                    if (inst.src2.r.arg < 0)
                    {
                        System.out.println("lw $t1," + (inst.src2.r.offset + mips.maxarg * 4) + "($sp)");
                    }
                    else
                    {
                        System.out.println("lw $t1," + (fs + inst.src2.r.arg * 4) + "($sp)");
                    }
                }
                else
                {
                    System.out.println("lw $t1," + "r" + inst.src2.r.No);
                }
            }
            System.out.println("sne $t0,$t0,$t1");
            if (inst.dest.global < 0)
            {
                if (inst.dest.arg < 0)
                {
                    System.out.println("sw $t0," + (inst.dest.offset + mips.maxarg * 4) + "($sp)");
                }
                else
                {
                    System.out.println("sw $t0," + (fs + inst.dest.arg * 4)+ "($sp)");
                }
            }
            else
            {
                System.out.println("sw $t0," + "r" + inst.dest.No);
            }
        }

        if (inst.op .equals( "label"))
        {
            System.out.println(inst.l1.name + ":");
        }
    }
}
