import java.util.ArrayList;
import java.util.Hashtable;
class binder
{
    property value;
    Name next;
    binder parent;
    binder(property v, Name p, binder t)
    {
        value = v; next = p; parent = t;
    }
}
public class symboltable {

    private Hashtable<Name, binder> table = new Hashtable <Name, binder>();
    private Name top;
    private binder marks;

    void put(Name key, property value)
    {
        table.put(key, new binder(value, top, table.get(key)));
        top = key;
    }
    property get(Name key)
    {
        binder e = table.get(key);
        if (e == null) return null;
        else return e.value;
    }

    property getscope(Name key)
    {
        if (top == null) return null;
        binder e = table.get(top);
        if (e == null) return null;
        if (top == key) return e.value;
        while (e.next != null)
        {
            binder n = table.get(e.next);
            if (e.next == key)
            {
                return n.value;
            }
            e = n;
        }
        return null;

    }

    void beginscope()
    {
        marks = new binder(null,top,marks);
        top = null;
    }
    void endscope()
    {
        while (top != null)
        {
            binder e = table.get(top);
            if (e.parent != null) table.put(top, e.parent);
            else table.remove(top);
            top = e.next;
        }
        top = marks.next;
        marks = marks.parent;
    }

}

class Name {
    private String name;
    private static Hashtable<String, Name> dict
            = new Hashtable <String, Name>();
    private Name(String text)
    {
        name = text;
    }
    public String toString()
    {
        return name;
    }
    public static Name getSymbolName(String text) {
        String unique = text.intern();
        Name s = dict.get(unique);
        if (s == null) {
            s = new Name(unique);
            dict.put(unique, s);
        }
        return s;
    }

}

class param
{
    property type;
    Name name;
}
class property {
    int sort = 0;


    class Class {
        Name name;
        Hashtable<Name, property> list = new Hashtable<Name, property>();

        int num = 0;
    }

    Class c = new Class();

    class Var {
        Name name;
        Name classname;
        int dim = 0;

        int memberNo = 0;
        register reg = new register();
    }

    Var v = new Var();

    class Func {
        Name name;
        Name typereturn;
        int dim = 0;
        ArrayList <param> list = new ArrayList <param>();
    }

    Func f = new Func();
}




