
#func exchange $r2 $r3
#$r4 = mul 4 $r2
#$r5 = add $r1 $r4
#$r6 = load 4 $r5 0
#$r7 = move $r6
#$r8 = mul 4 $r3
#$r9 = add $r1 $r8
#$r10 = load 4 $r9 0
#$r11 = mul 4 $r2
#$r12 = add $r1 $r11
#$r13 = load 4 $r12 0
#store 4 $r12 $r10 0
#$r14 = mul 4 $r3
#$r15 = add $r1 $r14
#$r16 = load 4 $r15 0
#store 4 $r15 $r7 0
#retnull
#func makeHeap
#$r20 = sub $r0 1
#$r21 = div $r20 2
#$r17 = move $r21
#$r19 = move 0
#$r18 = move 0
#jump %cond0
#%cond0
#$r22 = sge $r17 0
#br $r22 %loop1 %out2
#%loop1
#$r23 = mul $r17 2
#$r19 = move $r23
#$r24 = mul $r17 2
#$r25 = add $r24 1
#$r26 = slt $r25 $r0
#$r27 = seq $r26 0
#br $r27 %true3 %false4
#%true3
#$r28 = move $r26
#jump %out5
#%false4
#$r29 = mul $r17 2
#$r30 = add $r29 1
#$r31 = mul 4 $r30
#$r32 = add $r1 $r31
#$r33 = load 4 $r32 0
#$r34 = mul $r17 2
#$r35 = mul 4 $r34
#$r36 = add $r1 $r35
#$r37 = load 4 $r36 0
#$r38 = slt $r33 $r37
#$r28 = move $r38
#jump %out5
#%out5
#br $r28 %true6 %out7
#%true6
#$r39 = mul $r17 2
#$r40 = add $r39 1
#$r19 = move $r40
#jump %out7
#%out7
#$r41 = mul 4 $r17
#$r42 = add $r1 $r41
#$r43 = load 4 $r42 0
#$r44 = mul 4 $r19
#$r45 = add $r1 $r44
#$r46 = load 4 $r45 0
#$r47 = sgt $r43 $r46
#br $r47 %true8 %out9
#%true8
#$r48 = call exchange $r17 $r19
#jump %out9
#%out9
#$r49 = sub $r17 1
#$r17 = move $r49
#jump %cond0
#%out2
#ret 0
#retnull
#func adjustHeap $r50
#$r53 = move 0
#$r52 = move 0
#$r51 = move 0
#jump %cond10
#%cond10
#$r54 = mul $r51 2
#$r55 = slt $r54 $r50
#br $r55 %loop11 %out12
#%loop11
#$r56 = mul $r51 2
#$r52 = move $r56
#$r57 = mul $r51 2
#$r58 = add $r57 1
#$r59 = slt $r58 $r50
#$r60 = seq $r59 0
#br $r60 %true13 %false14
#%true13
#$r61 = move $r59
#jump %out15
#%false14
#$r62 = mul $r51 2
#$r63 = add $r62 1
#$r64 = mul 4 $r63
#$r65 = add $r1 $r64
#$r66 = load 4 $r65 0
#$r67 = mul $r51 2
#$r68 = mul 4 $r67
#$r69 = add $r1 $r68
#$r70 = load 4 $r69 0
#$r71 = slt $r66 $r70
#$r61 = move $r71
#jump %out15
#%out15
#br $r61 %true16 %out17
#%true16
#$r72 = mul $r51 2
#$r73 = add $r72 1
#$r52 = move $r73
#jump %out17
#%out17
#$r74 = mul 4 $r51
#$r75 = add $r1 $r74
#$r76 = load 4 $r75 0
#$r77 = mul 4 $r52
#$r78 = add $r1 $r77
#$r79 = load 4 $r78 0
#$r80 = sgt $r76 $r79
#br $r80 %true18 %false19
#%true18
#$r81 = mul 4 $r51
#$r82 = add $r1 $r81
#$r83 = load 4 $r82 0
#$r84 = move $r83
#$r85 = mul 4 $r52
#$r86 = add $r1 $r85
#$r87 = load 4 $r86 0
#$r88 = mul 4 $r51
#$r89 = add $r1 $r88
#$r90 = load 4 $r89 0
#store 4 $r89 $r87 0
#$r91 = mul 4 $r52
#$r92 = add $r1 $r91
#$r93 = load 4 $r92 0
#store 4 $r92 $r84 0
#$r51 = move $r52
#jump %out20
#%false19
#jump %out12
#jump %out20
#%out20
#jump %cond10
#%out12
#ret 0
#retnull
#func heapSort
#$r94 = move 0
#$r95 = move 0
#jump %cond21
#%cond21
#$r96 = slt $r95 $r0
#br $r96 %loop22 %out24
#%loop22
#$r97 = mul 4 0
#$r98 = add $r1 $r97
#$r99 = load 4 $r98 0
#$r94 = move $r99
#$r100 = sub $r0 $r95
#$r101 = sub $r100 1
#$r102 = mul 4 $r101
#$r103 = add $r1 $r102
#$r104 = load 4 $r103 0
#$r105 = mul 4 0
#$r106 = add $r1 $r105
#$r107 = load 4 $r106 0
#store 4 $r106 $r104 0
#$r108 = sub $r0 $r95
#$r109 = sub $r108 1
#$r110 = mul 4 $r109
#$r111 = add $r1 $r110
#$r112 = load 4 $r111 0
#store 4 $r111 $r94 0
#$r113 = sub $r0 $r95
#$r114 = sub $r113 1
#$r115 = call adjustHeap $r114
#jump %step23
#%step23
#$r116 = add $r95 1
#$r95 = move $r116
#jump %cond21
#%out24
#ret 0
#retnull
#func main
#$r118 = call func__getString
#$r119 = call func__string.parseInt $r118
#$r0 = move $r119
#$r1 = alloc $r0
#$r117 = move 0
#jump %cond25
#%cond25
#$r120 = call func__array.size $r1
#$r121 = slt $r117 $r120
#br $r121 %loop26 %out28
#%loop26
#$r122 = mul 4 $r117
#$r123 = add $r1 $r122
#$r124 = load 4 $r123 0
#store 4 $r123 $r117 0
#jump %step27
#%step27
#$r125 = add $r117 1
#$r117 = move $r125
#jump %cond25
#%out28
#$r126 = call makeHeap
#$r127 = call heapSort
#$r117 = move 0
#jump %cond29
#%cond29
#$r128 = call func__array.size $r1
#$r129 = slt $r117 $r128
#br $r129 %loop30 %out32
#%loop30
#$r130 = mul 4 $r117
#$r131 = add $r1 $r130
#$r132 = load 4 $r131 0
#$r133 = call func__toString $r132
#$r135 = call func__stringConcatenate $r133 $r134
#$r136 = call func__print $r135
#jump %step31
#%step31
#$r137 = add $r117 1
#$r117 = move $r137
#jump %cond29
#%out32
#$r139 = call func__print $r138
#ret 0
#retnull
# Built by Ficos 16/5/2
# All rights reserved.
#
#
# All test passed.
#
# Attention:
# 1. to use the built-in functions, you need to call "_buffer_init" function without any args before entering the source main function
# 	(jal _buffer_init)
# 2. just paste all of this in front of your MIPS code
#
# All supported functions:
# 		FunctionName			args
# 1.	func__print 			$a0: the string
# 2.	func__println			$a0: the string
# 3.	func__getString			---
# 4.	func__getInt			---
# 5.	func__toString			$a0: the integer
# 6.	func__string.length 	$a0: the string
# 7.	func__string.substring  $a0: the string,  $a1: left pos(int), $a2: right pos(int)
# 8.	func__string.parseInt 	$a0: the string
# 9.	func__string.ord 		$a0: the string,  $a1: pos(int)
# 10.	func__array.size 		$a0: the array
# 11.	func__stringConcatenate $a0: left string, $a1: right string
# 12.	func__stringIsEqual 	$a0: left string, $a1: right string
# 13.	func__stringLess 		$a0: left string, $a1: right string
# 14.	func__stringLeq	 		$a0: left string, $a1: right string
# 15.	func__stringGeq	 		$a0: left string, $a1: right string
# 16.	func__stringNeq	 		$a0: left string, $a1: right string
# 17.	func__stringLarge 		$a0: left string, $a1: right string
#
# Calling Conventions:
# 1. args placed in $a0, $a1, $a2
# 2. return in $v0
# 3. follow the MIPS calling convention, be careful on regs when calling these functions
# 4. all used regs are presented in the front of the function
#
# Conventions in using string:
# 1. string object is simply a register contains the initial address of the string
# 2. front of every initial address of a string are a word containing the length of the string
#    e.g.
#    .data
#  		  .word 6
# 	 str: .asciiz "hello\n"
# 		  .align 2
# 3. every string ends with '\0', which is not counted in the length
#
# Conventions in using array:
# 1. front of every initial address of a array are a word containing the size of the array

.data
_end: .asciiz "\n"
	.align 2
_buffer: .space 256
	.align 2
# 	.word 19
# str: .asciiz "-123456abcdefgh\n"
# 	.align 2

# 	.word 6
# str2: .asciiz "hello\n"
# 	.align 2

.text
# main:
# 	subu $sp, $sp, 4
# 	sw $ra, 0($sp)
	# jal _buffer_init

	# Test print/println
	# la $a0, str
	# jal func_println
	# la $a0, str2
	# jal func_print

	# Test getString, string_copy
	# jal func__getString
	# move $s0, $v0
	# move $a0, $s0
	# jal func__print
	# move $a0, $s0
	# jal func__string.length


	# Test string.length
	# la $a0, str2
	# jal func_string.length
	# move $a0, $v0
	# li $v0, 1
	# syscall

	# Test getInt
	# jal func_getInt
	# move $a0, $v0
	# li $v0, 1
	# syscall

	# Test toString
	# li $a0, -232312312
	# jal func__toString
	# move $a0, $v0
	# jal func__println

	# Test subString
	# la $a0 str
	# li $a1 1
	# li $a2 9
	# jal func__string.substring
	# move $a0, $v0
	# li $v0, 4
	# syscall
	# la $a0 str
	# syscall

	# Test parseInt
	# la $a0 str
	# jal func__string.parseInt
	# move $a0, $v0
	# li $v0, 1
	# syscall

	# Test string.ord
	# la $a0 str
	# li $a1, 5
	# jal func_string.ord
	# move $a0, $v0
	# li $v0, 1
	# syscall

	# Test stringconcatinate
	# la $a0 str
	# la $a1 str2
	# jal func__stringConcatenate
	# move $a0, $v0
	# jal func__print


	# Test StringIsEqual
	# la $a0 str
	# la $a1 str2
	# jal func__stringIsEqual
	# move $a0, $v0
	# li $v0, 1
	# syscall

	# Test StringLess
	# la $a0 str
	# la $a1 str2
	# jal func_stringLess
	# move $a0, $v0
	# li $v0, 1
	# syscall

	# lw $ra, 0($sp)
	# addu $sp, $sp, 4
	# jr $ra

# _buffer_init:
# 	li $a0, 256
# 	li $v0, 9
# 	syscall
# 	sw $v0, _buffer
# 	jr $ra

# copy the string in $a0 to buffer in $a1, with putting '\0' in the end of the buffer
###### Checked ######
# used $v0, $a0, $a1
_string_copy:
	_begin_string_copy:
	lb $v0, 0($a0)
	beqz $v0, _exit_string_copy
	sb $v0, 0($a1)
	add $a0, $a0, 1
	add $a1, $a1, 1
	j _begin_string_copy
	_exit_string_copy:
	sb $zero, 0($a1)
	jr $ra

# string arg in $a0
###### Checked ######
# Change(5/4): you don't need to preserve reg before calling it
func__print:
	li $v0, 4
	syscall
	jr $ra

# string arg in $a0
###### Checked ######
# Change(5/4): you don't need to preserve reg before calling it
func__println:
	li $v0, 4
	syscall
	la $a0, _end
	syscall
	jr $ra

# count the length of given string in $a0
###### Checked ######
# used $v0, $v1, $a0
_count_string_length:
	move $v0, $a0

	_begin_count_string_length:
	lb $v1, 0($a0)
	beqz $v1, _exit_count_string_length
	add $a0, $a0, 1
	j _begin_count_string_length

	_exit_count_string_length:
	sub $v0, $a0, $v0
	jr $ra

# non arg, string in $v0
###### Checked ######
# used $a0, $a1, $t0, $v0, (used in _count_string_length) $v1
func__getString:
	subu $sp, $sp, 4
	sw $ra, 0($sp)

	la $a0, _buffer
	li $a1, 255
	li $v0, 8
	syscall

	jal _count_string_length

	move $a1, $v0			# now $a1 contains the length of the string
	add $a0, $v0, 5			# total required space = length + 1('\0') + 1 word(record the length of the string)
	li $v0, 9
	syscall
	sw $a1, 0($v0)
	add $v0, $v0, 4
	la $a0, _buffer
	move $a1, $v0
	move $t0, $v0
	jal _string_copy
	move $v0, $t0

	lw $ra, 0($sp)
	addu $sp, $sp, 4
	jr $ra

# non arg, int in $v0
###### Checked ######
# Change(5/4): you don't need to preserve reg before calling it
func__getInt:
	li $v0, 5
	syscall
	jr $ra

# int arg in $a0
###### Checked ######
# Bug fixed(5/2): when the arg is a neg number
# Change(5/4): use less regs, you don't need to preserve reg before calling it
# used $v0, $v1
func__toString:
	subu $sp, $sp, 24
	sw $a0, 0($sp)
	sw $t0, 4($sp)
	sw $t1, 8($sp)
	sw $t2, 12($sp)
	sw $t3, 16($sp)
	sw $t5, 20($sp)

	# first count the #digits
	li $t0, 0			# $t0 = 0 if the number is a negnum
	bgez $a0, _skip_set_less_than_zero
	li $t0, 1			# now $t0 must be 1
	neg $a0, $a0
	_skip_set_less_than_zero:
	beqz $a0, _set_zero

	li $t1, 0			# the #digits is in $t1
	move $t2, $a0
	move $t3, $a0
	li $t5, 10

	_begin_count_digit:
	div $t2, $t5
	mflo $v0			# get the quotient
	mfhi $v1			# get the remainder
	bgtz $v0 _not_yet
	bgtz $v1 _not_yet
	j _yet
	_not_yet:
	add $t1, $t1, 1
	move $t2, $v0
	j _begin_count_digit

	_yet:
	beqz $t0, _skip_reserve_neg
	add $t1, $t1, 1
	_skip_reserve_neg:
	add $a0, $t1, 5
	li $v0, 9
	syscall
	sw $t1, 0($v0)
	add $v0, $v0, 4
	add $t1, $t1, $v0
	sb $zero, 0($t1)
	sub $t1, $t1, 1

	_continue_toString:
	div $t3, $t5
	mfhi $v1
	add $v1, $v1, 48	# in ascii 48 = '0'
	sb $v1, 0($t1)
	sub $t1, $t1, 1
	mflo $t3
	# bge $t1, $v0, _continue_toString
	bnez $t3, _continue_toString

	beqz $t0, _skip_place_neg
	li $v1, 45
	sb $v1, 0($t1)
	_skip_place_neg:
	# lw $ra, 0($sp)
	# addu $sp, $sp, 4

	lw $a0, 0($sp)
	lw $t0, 4($sp)
	lw $t1, 8($sp)
	lw $t2, 12($sp)
	lw $t3, 16($sp)
	lw $t5, 20($sp)

	addu $sp, $sp, 24
	jr $ra

	_set_zero:
	li $a0, 6
	li $v0, 9
	syscall
	li $a0, 1
	sw $a0, 0($v0)
	add $v0, $v0, 4
	li $a0, 48
	sb $a0, 0($v0)

	lw $a0, 0($sp)
	lw $t0, 4($sp)
	lw $t1, 8($sp)
	lw $t2, 12($sp)
	lw $t3, 16($sp)
	lw $t5, 20($sp)

	addu $sp, $sp, 24
	jr $ra


# string arg in $a0
# the zero in the end of the string will not be counted
###### Checked ######
# you don't need to preserve reg before calling it
func__string.length:
	lw $v0, -4($a0)
	jr $ra

# string arg in $a0, left in $a1, right in $a2
###### Checked ######
# used $a0, $a1, $t0, $t1, $t2, $v1, $v0
func__string.substring:
	subu $sp, $sp, 4
	sw $ra, 0($sp)

	move $t0, $a0

	sub $t1, $a2, $a1
	add $t1, $t1, 1		# $t1 is the length of the substring
	add $a0, $t1, 5
	li $v0, 9
	syscall
	sw $t1, 0($v0)
	add $v1, $v0, 4

	add $a0, $t0, $a1
	add $t2, $t0, $a2
	lb $t1, 1($t2)		# store the ori_begin + right + 1 char in $t1
	sb $zero, 1($t2)	# change it to 0 for the convenience of copying
	move $a1, $v1
	jal _string_copy
	move $v0, $v1
	sb $t1, 1($t2)

	lw $ra, 0($sp)
	addu $sp, $sp, 4
	jr $ra

# string arg in
###### Checked ######
# 16/5/4 Fixed a serious bug: can not parse negtive number
# used $v0, $v1
func__string.parseInt:
	subu $sp, $sp, 16
	sw $a0, 0($sp)
	sw $t0, 4($sp)
	sw $t1, 8($sp)
	sw $t2, 12($sp)

	li $v0, 0

	lb $t1, 0($a0)
	li $t2, 45
	bne $t1, $t2, _skip_parse_neg
	li $t1, 1			#if there is a '-' sign, $t1 = 1
	add $a0, $a0, 1
	j _skip_set_t1_zero

	_skip_parse_neg:
	li $t1, 0
	_skip_set_t1_zero:
	move $t0, $a0
	li $t2, 1

	_count_number_pos:
	lb $v1, 0($t0)
	bgt $v1, 57, _begin_parse_int
	blt $v1, 48, _begin_parse_int
	add $t0, $t0, 1
	j _count_number_pos

	_begin_parse_int:
	sub $t0, $t0, 1

	_parsing_int:
	blt $t0, $a0, _finish_parse_int
	lb $v1, 0($t0)
	sub $v1, $v1, 48
	mul $v1, $v1, $t2
	add $v0, $v0, $v1
	mul $t2, $t2, 10
	sub $t0, $t0, 1
	j _parsing_int

	_finish_parse_int:
	beqz $t1, _skip_neg
	neg $v0, $v0
	_skip_neg:

	lw $a0, 0($sp)
	lw $t0, 4($sp)
	lw $t1, 8($sp)
	lw $t2, 12($sp)
	addu $sp, $sp, 16
	jr $ra

# string arg in $a0, pos in $a1
###### Checked ######
# used $v0, $v1
func__string.ord:
	add $v1, $a0, $a1
	lb $v0, 0($v1)
	jr $ra

# array arg in $a0
# used $v0
func__array.size:
	lw $v0, -4($a0)
	jr $ra

# string1 in $a0, string2 in $a1
###### Checked ######
# change(16/5/4): use less regs, you don't need to preserve reg before calling it
# used $v0, $v1
func__stringConcatenate:

	subu $sp, $sp, 24
	sw $ra, 0($sp)
	sw $a0, 4($sp)
	sw $a1, 8($sp)
	sw $t0, 12($sp)
	sw $t1, 16($sp)
	sw $t2, 20($sp)

	lw $t0, -4($a0)		# $t0 is the length of lhs
	lw $t1, -4($a1)		# $t1 is the length of rhs
	add $t2, $t0, $t1

	move $t1, $a0

	add $a0, $t2, 5
	li $v0, 9
	syscall

	sw $t2, 0($v0)
	move $t2, $a1

	add $v0, $v0, 4
	move $v1, $v0

	move $a0, $t1
	move $a1, $v1
	jal _string_copy

	move $a0, $t2
	add $a1, $v1, $t0
	# add $a1, $a1, 1
	jal _string_copy

	move $v0, $v1
	lw $ra, 0($sp)
	lw $a0, 4($sp)
	lw $a1, 8($sp)
	lw $t0, 12($sp)
	lw $t1, 16($sp)
	lw $t2, 20($sp)
	addu $sp, $sp, 24
	jr $ra

# string1 in $a0, string2 in $a1
###### Checked ######
# change(16/5/4): use less regs, you don't need to preserve reg before calling it
# used $a0, $a1, $v0, $v1
func__stringIsEqual:
	# subu $sp, $sp, 8
	# sw $a0, 0($sp)
	# sw $a1, 4($sp)

	lw $v0, -4($a0)
	lw $v1, -4($a1)
	bne $v0, $v1, _not_equal

	_continue_compare_equal:
	lb $v0, 0($a0)
	lb $v1, 0($a1)
	beqz $v0, _equal
	bne $v0, $v1, _not_equal
	add $a0, $a0, 1
	add $a1, $a1, 1
	j _continue_compare_equal

	_not_equal:
	li $v0, 0
	j _compare_final

	_equal:
	li $v0, 1

	_compare_final:
	# lw $a0, 0($sp)
	# lw $a1, 4($sp)
	# addu $sp, $sp, 8
	jr $ra


# string1 in $a0, string2 in $a1
###### Checked ######
# change(16/5/4): use less regs, you don't need to preserve reg before calling it
# used $a0, $a1, $v0, $v1
func__stringLess:
	# subu $sp, $sp, 8
	# sw $a0, 0($sp)
	# sw $a1, 4($sp)

	_begin_compare_less:
	lb $v0, 0($a0)
	lb $v1, 0($a1)
	blt $v0, $v1, _less_correct
	bgt $v0, $v1, _less_false
	beqz $v0, _less_false
	add $a0, $a0, 1
	add $a1, $a1, 1
	j _begin_compare_less

	_less_correct:
	li $v0, 1
	j _less_compare_final

	_less_false:
	li $v0, 0

	_less_compare_final:

	# lw $a0, 0($sp)
	# lw $a1, 4($sp)
	# addu $sp, $sp, 8
	jr $ra

# string1 in $a0, string2 in $a1
# used $a0, $a1, $v0, $v1
func__stringLarge:
	subu $sp, $sp, 4
	sw $ra, 0($sp)

	jal func__stringLess

	xor $v0, $v0, 1

	lw $ra, 0($sp)
	addu $sp, $sp, 4
	jr $ra

# string1 in $a0, string2 in $a1
# used $a0, $a1, $v0, $v1
func__stringLeq:
	subu $sp, $sp, 12
	sw $ra, 0($sp)
	sw $a0, 4($sp)
	sw $a1, 8($sp)

	jal func__stringLess

	bnez $v0, _skip_compare_equal_in_Leq

	lw $a0, 4($sp)
	lw $a1, 8($sp)
	jal func__stringIsEqual

	_skip_compare_equal_in_Leq:
	lw $ra, 0($sp)
	addu $sp, $sp, 12
	jr $ra

# string1 in $a0, string2 in $a1
# used $a0, $a1, $v0, $v1
func__stringGeq:
	subu $sp, $sp, 12
	sw $ra, 0($sp)
	sw $a0, 4($sp)
	sw $a1, 8($sp)

	jal func__stringLess

	beqz $v0, _skip_compare_equal_in_Geq

	lw $a0, 4($sp)
	lw $a1, 8($sp)
	jal func__stringIsEqual
	xor $v0, $v0, 1

	_skip_compare_equal_in_Geq:
	xor $v0, $v0, 1
	lw $ra, 0($sp)
	addu $sp, $sp, 12
	jr $ra

# string1 in $a0, string2 in $a1
# used $a0, $a1, $v0, $v1
func__stringNeq:
	subu $sp, $sp, 4
	sw $ra, 0($sp)

	jal func__stringIsEqual

	xor $v0, $v0, 1

	lw $ra, 0($sp)
	addu $sp, $sp, 4
	jr $ra
.data
#2
.word 1
r134: .asciiz " "
.align 2
.word 1
r138: .asciiz "\n"
.align 2
r0: .word 0
r1: .word 0
.text
exchange:
subu $sp,$sp,68
sw $fp,60($sp)
sw $ra,64($sp)
sw $t0,52($sp)
sw $t1,56($sp)
addiu $fp,$sp,64

#mul
li $t0,4
lw $t1,68($sp)
mul $t0,$t0,$t1
sw $t0,0($sp)

#add
lw $t0,r1
lw $t1,0($sp)
add $t0,$t0,$t1
sw $t0,4($sp)

#load
lw $t1,4($sp)
lw $t0,0($t1)
sw $t0,8($sp)

#move
lw $t1,8($sp)
move $t0,$t1
sw $t0,12($sp)

#mul
li $t0,4
lw $t1,72($sp)
mul $t0,$t0,$t1
sw $t0,16($sp)

#add
lw $t0,r1
lw $t1,16($sp)
add $t0,$t0,$t1
sw $t0,20($sp)

#load
lw $t1,20($sp)
lw $t0,0($t1)
sw $t0,24($sp)

#mul
li $t0,4
lw $t1,68($sp)
mul $t0,$t0,$t1
sw $t0,28($sp)

#add
lw $t0,r1
lw $t1,28($sp)
add $t0,$t0,$t1
sw $t0,32($sp)

#load
lw $t1,32($sp)
lw $t0,0($t1)
sw $t0,36($sp)

#store
lw $t0,24($sp)
lw $t1,32($sp)
sw $t0,0($t1)

#mul
li $t0,4
lw $t1,72($sp)
mul $t0,$t0,$t1
sw $t0,40($sp)

#add
lw $t0,r1
lw $t1,40($sp)
add $t0,$t0,$t1
sw $t0,44($sp)

#load
lw $t1,44($sp)
lw $t0,0($t1)
sw $t0,48($sp)

#store
lw $t0,12($sp)
lw $t1,44($sp)
sw $t0,0($t1)

#retnull
lw $fp,60($sp)
lw $ra,64($sp)
lw $t0,52($sp)
lw $t1,56($sp)
addiu $sp,$sp,68
jr $ra
makeHeap:
subu $sp,$sp,156
sw $fp,148($sp)
sw $ra,152($sp)
sw $t0,140($sp)
sw $t1,144($sp)
addiu $fp,$sp,152

#sub
lw $t0,r0
li $t1,1
subu $t0,$t0,$t1
sw $t0,20($sp)

#div
lw $t0,20($sp)
li $t1,2
divu $t0,$t0,$t1
sw $t0,24($sp)

#move
lw $t1,24($sp)
move $t0,$t1
sw $t0,8($sp)

#move
li $t1,0
move $t0,$t1
sw $t0,16($sp)

#move
li $t1,0
move $t0,$t1
sw $t0,12($sp)

#jump
jal cond0

#label
cond0:

#sge
lw $t0,8($sp)
li $t1,0
sge $t0,$t0,$t1
sw $t0,28($sp)

#br
lw $t0,28($sp)
beq $t0,1,loop1
jal out2

#label
loop1:

#mul
lw $t0,8($sp)
li $t1,2
mul $t0,$t0,$t1
sw $t0,32($sp)

#move
lw $t1,32($sp)
move $t0,$t1
sw $t0,16($sp)

#mul
lw $t0,8($sp)
li $t1,2
mul $t0,$t0,$t1
sw $t0,36($sp)

#add
lw $t0,36($sp)
addi $t0,$t0,1
sw $t0,40($sp)

#slt
lw $t0,40($sp)
lw $t1,r0
slt $t0,$t0,$t1
sw $t0,44($sp)

#seq
lw $t0,44($sp)
li $t1,0
seq $t0,$t0,$t1
sw $t0,48($sp)

#br
lw $t0,48($sp)
beq $t0,1,true3
jal false4

#label
true3:

#move
lw $t1,44($sp)
move $t0,$t1
sw $t0,52($sp)

#jump
jal out5

#label
false4:

#mul
lw $t0,8($sp)
li $t1,2
mul $t0,$t0,$t1
sw $t0,56($sp)

#add
lw $t0,56($sp)
addi $t0,$t0,1
sw $t0,60($sp)

#mul
li $t0,4
lw $t1,60($sp)
mul $t0,$t0,$t1
sw $t0,64($sp)

#add
lw $t0,r1
lw $t1,64($sp)
add $t0,$t0,$t1
sw $t0,68($sp)

#load
lw $t1,68($sp)
lw $t0,0($t1)
sw $t0,72($sp)

#mul
lw $t0,8($sp)
li $t1,2
mul $t0,$t0,$t1
sw $t0,76($sp)

#mul
li $t0,4
lw $t1,76($sp)
mul $t0,$t0,$t1
sw $t0,80($sp)

#add
lw $t0,r1
lw $t1,80($sp)
add $t0,$t0,$t1
sw $t0,84($sp)

#load
lw $t1,84($sp)
lw $t0,0($t1)
sw $t0,88($sp)

#slt
lw $t0,72($sp)
lw $t1,88($sp)
slt $t0,$t0,$t1
sw $t0,92($sp)

#move
lw $t1,92($sp)
move $t0,$t1
sw $t0,52($sp)

#jump
jal out5

#label
out5:

#br
lw $t0,52($sp)
beq $t0,1,true6
jal out7

#label
true6:

#mul
lw $t0,8($sp)
li $t1,2
mul $t0,$t0,$t1
sw $t0,96($sp)

#add
lw $t0,96($sp)
addi $t0,$t0,1
sw $t0,100($sp)

#move
lw $t1,100($sp)
move $t0,$t1
sw $t0,16($sp)

#jump
jal out7

#label
out7:

#mul
li $t0,4
lw $t1,8($sp)
mul $t0,$t0,$t1
sw $t0,104($sp)

#add
lw $t0,r1
lw $t1,104($sp)
add $t0,$t0,$t1
sw $t0,108($sp)

#load
lw $t1,108($sp)
lw $t0,0($t1)
sw $t0,112($sp)

#mul
li $t0,4
lw $t1,16($sp)
mul $t0,$t0,$t1
sw $t0,116($sp)

#add
lw $t0,r1
lw $t1,116($sp)
add $t0,$t0,$t1
sw $t0,120($sp)

#load
lw $t1,120($sp)
lw $t0,0($t1)
sw $t0,124($sp)

#sgt
lw $t0,112($sp)
lw $t1,124($sp)
sgt $t0,$t0,$t1
sw $t0,128($sp)

#br
lw $t0,128($sp)
beq $t0,1,true8
jal out9

#label
true8:

#dcall
lw $t0,8($sp)
sw $t0,0($sp)
lw $t0,16($sp)
sw $t0,4($sp)
jal exchange
move $t0,$v0
sw $t0,132($sp)

#jump
jal out9

#label
out9:

#sub
lw $t0,8($sp)
li $t1,1
subu $t0,$t0,$t1
sw $t0,136($sp)

#move
lw $t1,136($sp)
move $t0,$t1
sw $t0,8($sp)

#jump
jal cond0

#label
out2:

#ret
li $v0,0
lw $fp,148($sp)
lw $ra,152($sp)
lw $t0,140($sp)
lw $t1,144($sp)
addiu $sp,$sp,156
jr $ra

#retnull
lw $fp,148($sp)
lw $ra,152($sp)
lw $t0,140($sp)
lw $t1,144($sp)
addiu $sp,$sp,156
jr $ra
adjustHeap:
subu $sp,$sp,188
sw $fp,180($sp)
sw $ra,184($sp)
sw $t0,172($sp)
sw $t1,176($sp)
addiu $fp,$sp,184

#move
li $t1,0
move $t0,$t1
sw $t0,8($sp)

#move
li $t1,0
move $t0,$t1
sw $t0,4($sp)

#move
li $t1,0
move $t0,$t1
sw $t0,0($sp)

#jump
jal cond10

#label
cond10:

#mul
lw $t0,0($sp)
li $t1,2
mul $t0,$t0,$t1
sw $t0,12($sp)

#slt
lw $t0,12($sp)
lw $t1,188($sp)
slt $t0,$t0,$t1
sw $t0,16($sp)

#br
lw $t0,16($sp)
beq $t0,1,loop11
jal out12

#label
loop11:

#mul
lw $t0,0($sp)
li $t1,2
mul $t0,$t0,$t1
sw $t0,20($sp)

#move
lw $t1,20($sp)
move $t0,$t1
sw $t0,4($sp)

#mul
lw $t0,0($sp)
li $t1,2
mul $t0,$t0,$t1
sw $t0,24($sp)

#add
lw $t0,24($sp)
addi $t0,$t0,1
sw $t0,28($sp)

#slt
lw $t0,28($sp)
lw $t1,188($sp)
slt $t0,$t0,$t1
sw $t0,32($sp)

#seq
lw $t0,32($sp)
li $t1,0
seq $t0,$t0,$t1
sw $t0,36($sp)

#br
lw $t0,36($sp)
beq $t0,1,true13
jal false14

#label
true13:

#move
lw $t1,32($sp)
move $t0,$t1
sw $t0,40($sp)

#jump
jal out15

#label
false14:

#mul
lw $t0,0($sp)
li $t1,2
mul $t0,$t0,$t1
sw $t0,44($sp)

#add
lw $t0,44($sp)
addi $t0,$t0,1
sw $t0,48($sp)

#mul
li $t0,4
lw $t1,48($sp)
mul $t0,$t0,$t1
sw $t0,52($sp)

#add
lw $t0,r1
lw $t1,52($sp)
add $t0,$t0,$t1
sw $t0,56($sp)

#load
lw $t1,56($sp)
lw $t0,0($t1)
sw $t0,60($sp)

#mul
lw $t0,0($sp)
li $t1,2
mul $t0,$t0,$t1
sw $t0,64($sp)

#mul
li $t0,4
lw $t1,64($sp)
mul $t0,$t0,$t1
sw $t0,68($sp)

#add
lw $t0,r1
lw $t1,68($sp)
add $t0,$t0,$t1
sw $t0,72($sp)

#load
lw $t1,72($sp)
lw $t0,0($t1)
sw $t0,76($sp)

#slt
lw $t0,60($sp)
lw $t1,76($sp)
slt $t0,$t0,$t1
sw $t0,80($sp)

#move
lw $t1,80($sp)
move $t0,$t1
sw $t0,40($sp)

#jump
jal out15

#label
out15:

#br
lw $t0,40($sp)
beq $t0,1,true16
jal out17

#label
true16:

#mul
lw $t0,0($sp)
li $t1,2
mul $t0,$t0,$t1
sw $t0,84($sp)

#add
lw $t0,84($sp)
addi $t0,$t0,1
sw $t0,88($sp)

#move
lw $t1,88($sp)
move $t0,$t1
sw $t0,4($sp)

#jump
jal out17

#label
out17:

#mul
li $t0,4
lw $t1,0($sp)
mul $t0,$t0,$t1
sw $t0,92($sp)

#add
lw $t0,r1
lw $t1,92($sp)
add $t0,$t0,$t1
sw $t0,96($sp)

#load
lw $t1,96($sp)
lw $t0,0($t1)
sw $t0,100($sp)

#mul
li $t0,4
lw $t1,4($sp)
mul $t0,$t0,$t1
sw $t0,104($sp)

#add
lw $t0,r1
lw $t1,104($sp)
add $t0,$t0,$t1
sw $t0,108($sp)

#load
lw $t1,108($sp)
lw $t0,0($t1)
sw $t0,112($sp)

#sgt
lw $t0,100($sp)
lw $t1,112($sp)
sgt $t0,$t0,$t1
sw $t0,116($sp)

#br
lw $t0,116($sp)
beq $t0,1,true18
jal false19

#label
true18:

#mul
li $t0,4
lw $t1,0($sp)
mul $t0,$t0,$t1
sw $t0,120($sp)

#add
lw $t0,r1
lw $t1,120($sp)
add $t0,$t0,$t1
sw $t0,124($sp)

#load
lw $t1,124($sp)
lw $t0,0($t1)
sw $t0,128($sp)

#move
lw $t1,128($sp)
move $t0,$t1
sw $t0,132($sp)

#mul
li $t0,4
lw $t1,4($sp)
mul $t0,$t0,$t1
sw $t0,136($sp)

#add
lw $t0,r1
lw $t1,136($sp)
add $t0,$t0,$t1
sw $t0,140($sp)

#load
lw $t1,140($sp)
lw $t0,0($t1)
sw $t0,144($sp)

#mul
li $t0,4
lw $t1,0($sp)
mul $t0,$t0,$t1
sw $t0,148($sp)

#add
lw $t0,r1
lw $t1,148($sp)
add $t0,$t0,$t1
sw $t0,152($sp)

#load
lw $t1,152($sp)
lw $t0,0($t1)
sw $t0,156($sp)

#store
lw $t0,144($sp)
lw $t1,152($sp)
sw $t0,0($t1)

#mul
li $t0,4
lw $t1,4($sp)
mul $t0,$t0,$t1
sw $t0,160($sp)

#add
lw $t0,r1
lw $t1,160($sp)
add $t0,$t0,$t1
sw $t0,164($sp)

#load
lw $t1,164($sp)
lw $t0,0($t1)
sw $t0,168($sp)

#store
lw $t0,132($sp)
lw $t1,164($sp)
sw $t0,0($t1)

#move
lw $t1,4($sp)
move $t0,$t1
sw $t0,0($sp)

#jump
jal out20

#label
false19:

#jump
jal out12

#jump
jal out20

#label
out20:

#jump
jal cond10

#label
out12:

#ret
li $v0,0
lw $fp,180($sp)
lw $ra,184($sp)
lw $t0,172($sp)
lw $t1,176($sp)
addiu $sp,$sp,188
jr $ra

#retnull
lw $fp,180($sp)
lw $ra,184($sp)
lw $t0,172($sp)
lw $t1,176($sp)
addiu $sp,$sp,188
jr $ra
heapSort:
subu $sp,$sp,112
sw $fp,104($sp)
sw $ra,108($sp)
sw $t0,96($sp)
sw $t1,100($sp)
addiu $fp,$sp,108

#move
li $t1,0
move $t0,$t1
sw $t0,4($sp)

#move
li $t1,0
move $t0,$t1
sw $t0,8($sp)

#jump
jal cond21

#label
cond21:

#slt
lw $t0,8($sp)
lw $t1,r0
slt $t0,$t0,$t1
sw $t0,12($sp)

#br
lw $t0,12($sp)
beq $t0,1,loop22
jal out24

#label
loop22:

#mul
li $t0,4
li $t1,0
mul $t0,$t0,$t1
sw $t0,16($sp)

#add
lw $t0,r1
lw $t1,16($sp)
add $t0,$t0,$t1
sw $t0,20($sp)

#load
lw $t1,20($sp)
lw $t0,0($t1)
sw $t0,24($sp)

#move
lw $t1,24($sp)
move $t0,$t1
sw $t0,4($sp)

#sub
lw $t0,r0
lw $t1,8($sp)
subu $t0,$t0,$t1
sw $t0,28($sp)

#sub
lw $t0,28($sp)
li $t1,1
subu $t0,$t0,$t1
sw $t0,32($sp)

#mul
li $t0,4
lw $t1,32($sp)
mul $t0,$t0,$t1
sw $t0,36($sp)

#add
lw $t0,r1
lw $t1,36($sp)
add $t0,$t0,$t1
sw $t0,40($sp)

#load
lw $t1,40($sp)
lw $t0,0($t1)
sw $t0,44($sp)

#mul
li $t0,4
li $t1,0
mul $t0,$t0,$t1
sw $t0,48($sp)

#add
lw $t0,r1
lw $t1,48($sp)
add $t0,$t0,$t1
sw $t0,52($sp)

#load
lw $t1,52($sp)
lw $t0,0($t1)
sw $t0,56($sp)

#store
lw $t0,44($sp)
lw $t1,52($sp)
sw $t0,0($t1)

#sub
lw $t0,r0
lw $t1,8($sp)
subu $t0,$t0,$t1
sw $t0,60($sp)

#sub
lw $t0,60($sp)
li $t1,1
subu $t0,$t0,$t1
sw $t0,64($sp)

#mul
li $t0,4
lw $t1,64($sp)
mul $t0,$t0,$t1
sw $t0,68($sp)

#add
lw $t0,r1
lw $t1,68($sp)
add $t0,$t0,$t1
sw $t0,72($sp)

#load
lw $t1,72($sp)
lw $t0,0($t1)
sw $t0,76($sp)

#store
lw $t0,4($sp)
lw $t1,72($sp)
sw $t0,0($t1)

#sub
lw $t0,r0
lw $t1,8($sp)
subu $t0,$t0,$t1
sw $t0,80($sp)

#sub
lw $t0,80($sp)
li $t1,1
subu $t0,$t0,$t1
sw $t0,84($sp)

#dcall
lw $t0,84($sp)
sw $t0,0($sp)
jal adjustHeap
move $t0,$v0
sw $t0,88($sp)

#jump
jal step23

#label
step23:

#add
lw $t0,8($sp)
addi $t0,$t0,1
sw $t0,92($sp)

#move
lw $t1,92($sp)
move $t0,$t1
sw $t0,8($sp)

#jump
jal cond21

#label
out24:

#ret
li $v0,0
lw $fp,104($sp)
lw $ra,108($sp)
lw $t0,96($sp)
lw $t1,100($sp)
addiu $sp,$sp,112
jr $ra

#retnull
lw $fp,104($sp)
lw $ra,108($sp)
lw $t0,96($sp)
lw $t1,100($sp)
addiu $sp,$sp,112
jr $ra
main:
subu $sp,$sp,116
sw $fp,108($sp)
sw $ra,112($sp)
sw $t0,100($sp)
sw $t1,104($sp)
addiu $fp,$sp,112

#dcall
jal func__getString
move $t0,$v0
sw $t0,12($sp)

#dcall
lw $t0,12($sp)
sw $t0,0($sp)
move $a0,$t0
jal func__string.parseInt
move $t0,$v0
sw $t0,16($sp)

#move
lw $t1,16($sp)
move $t0,$t1
sw $t0,r0

#alloc
li $v0,9
lw $t0,r0
li $t1,4
mul $t0,$t0,$t1
addiu $a0,$t0,4
syscall
divu  $t0,$t0,4
sw $t0,0($v0)
addiu $v0,$v0,4
sw $v0,r1

#move
li $t1,0
move $t0,$t1
sw $t0,8($sp)

#jump
jal cond25

#label
cond25:

#dcall
lw $t0,r1
sw $t0,0($sp)
move $a0,$t0
jal func__array.size
move $t0,$v0
sw $t0,20($sp)

#slt
lw $t0,8($sp)
lw $t1,20($sp)
slt $t0,$t0,$t1
sw $t0,24($sp)

#br
lw $t0,24($sp)
beq $t0,1,loop26
jal out28

#label
loop26:

#mul
li $t0,4
lw $t1,8($sp)
mul $t0,$t0,$t1
sw $t0,28($sp)

#add
lw $t0,r1
lw $t1,28($sp)
add $t0,$t0,$t1
sw $t0,32($sp)

#load
lw $t1,32($sp)
lw $t0,0($t1)
sw $t0,36($sp)

#store
lw $t0,8($sp)
lw $t1,32($sp)
sw $t0,0($t1)

#jump
jal step27

#label
step27:

#add
lw $t0,8($sp)
addi $t0,$t0,1
sw $t0,40($sp)

#move
lw $t1,40($sp)
move $t0,$t1
sw $t0,8($sp)

#jump
jal cond25

#label
out28:

#dcall
jal makeHeap
move $t0,$v0
sw $t0,44($sp)

#dcall
jal heapSort
move $t0,$v0
sw $t0,48($sp)

#move
li $t1,0
move $t0,$t1
sw $t0,8($sp)

#jump
jal cond29

#label
cond29:

#dcall
lw $t0,r1
sw $t0,0($sp)
move $a0,$t0
jal func__array.size
move $t0,$v0
sw $t0,52($sp)

#slt
lw $t0,8($sp)
lw $t1,52($sp)
slt $t0,$t0,$t1
sw $t0,56($sp)

#br
lw $t0,56($sp)
beq $t0,1,loop30
jal out32

#label
loop30:

#mul
li $t0,4
lw $t1,8($sp)
mul $t0,$t0,$t1
sw $t0,60($sp)

#add
lw $t0,r1
lw $t1,60($sp)
add $t0,$t0,$t1
sw $t0,64($sp)

#load
lw $t1,64($sp)
lw $t0,0($t1)
sw $t0,68($sp)

#dcall
lw $t0,68($sp)
sw $t0,0($sp)
move $a0,$t0
jal func__toString
move $t0,$v0
sw $t0,72($sp)

#dcall
lw $t0,72($sp)
sw $t0,0($sp)
move $a0,$t0
la $t0,r134
sw $t0,4($sp)
move $a1,$t0
jal func__stringConcatenate
move $t0,$v0
sw $t0,80($sp)

#dcall
lw $t0,80($sp)
sw $t0,0($sp)
move $a0,$t0
jal func__print
move $t0,$v0
sw $t0,84($sp)

#jump
jal step31

#label
step31:

#add
lw $t0,8($sp)
addi $t0,$t0,1
sw $t0,88($sp)

#move
lw $t1,88($sp)
move $t0,$t1
sw $t0,8($sp)

#jump
jal cond29

#label
out32:

#dcall
la $t0,r138
sw $t0,0($sp)
move $a0,$t0
jal func__print
move $t0,$v0
sw $t0,96($sp)

#ret
li $v0,0
lw $fp,108($sp)
lw $ra,112($sp)
lw $t0,100($sp)
lw $t1,104($sp)
addiu $sp,$sp,116
jr $ra

#retnull
lw $fp,108($sp)
lw $ra,112($sp)
lw $t0,100($sp)
lw $t1,104($sp)
addiu $sp,$sp,116
jr $ra

